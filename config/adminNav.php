<?php

return [
    'template' => [
        'title' => 'Gas Application',
        'author' => 'ergauravsethi376',
        'description' => 'Web-Application is used demonstrate Gas Application Products',
        'keywords' => 'Cloths',
        'active_page' => basename($_SERVER['PHP_SELF'])
    ],
    'primary_nav' => [
        [
            'name' => 'Dashboard',
            'opt' => '',
            'url' => 'dashboard',
            'icon' => 'icon-speedometer',
            'sub' => 0
        ],
        [
            'name' => 'Admin Users',
            'icon' => 'icon-user',
            'url' => 'adminuser',
            'sub' => 0
        ],
        [
            'name' => 'Admin User Roles',
            'url' => 'admin-roles',
            'icon' => 'fa fa-tasks',
            'opt' => '',
            'sub' => 0
        ],
        // [
        //     'name' => 'User Roles',
        //     'url' => 'roles',
        //     'icon' => 'fa fa-tasks',
        //     'opt' => '',
        //     'sub' => 0
        // ],
        [
            'name' => 'User Supplier',
            'url' => 'suppliers',
            'icon' => 'fa fa-users',
            'opt' => '',
            'sub' => 0
        ],
        [
            'name' => 'User Customers',
            'url' => 'customers',
            'icon' => 'fa fa-users',
            'opt' => '',
            'sub' => 0
        ],
        [
            'name' => 'Services',
            'url' => 'services',
            'icon' => 'fa fa-cog',
            'opt' => '',
            'sub' => 0
        ],
        [
            'name' => 'Measurement Units',
            'url' => 'measurements',
            'icon' => 'fas fa-balance-scale',
            'opt' => '',
            'sub' => 0
        ],
        [
            'name' => 'Location Governorate',
            'url' => 'governorate',
            'icon' => 'fa fa-map-marker',
            'opt' => '',
            'sub' => 0

        ],
        [
            'name' => 'Location City',
            'url' => 'city',
            'icon' => 'fa fa-map-marker',
            'opt' => '',
            'sub' => 0
        ],
        [
            'name' => 'Location Section',
            'url' => 'section',
            'icon' => 'fa fa-map-marker',
            'opt' => '',
            'sub' => 0
        ],
        [
            'name'  => 'Terms & Condtions ',
            'url'   =>  'configuration',
            'icon'  => 'fa fa-cog',
            'opt'   => '',
            'sub'   => 0
        ],
        [
            'name' => 'How To',
            'url' => 'how_to',
            'icon' => 'fa fa-question-circle',
            'opt' => '',
            'sub' => 0
        ],
        [
            'name' => 'Ads',
            'url' => 'ads',
            'icon' => 'fa fa-camera-retro',
            'opt' => '',
            'sub' => 0
        ],
         [
            'name' => 'User Supplier Search',
            'url' => 'suppliers-search',
            'icon' => 'fa fa-users',
            'opt' => '',
            'sub' => 0
        ],
        [
            'name' => 'User Customers Search',
            'url' => 'customers-search',
            'icon' => 'fa fa-users',
            'opt' => '',
            'sub' => 0
        ],
        [
            'name' => 'Order',
            'url' => 'orders',
            'icon' => 'fa fa-calendar',
            'opt' => '',
            'sub' => 0
        ],
        [
            'name' => 'Ticket',
            'url' => 'tickets',
            'icon' => 'fa fa-calendar',
            'opt' => '',
            'sub' => 0
        ],
        [
            'name' => 'Notification History',
            'url' => 'push_notification',
            'icon' => 'fa fa-bell',
            'opt' => '',
            'sub' => 0
        ],
        [
            'name'  => 'GasApp Service Fee',
            'url'   => 'service_fees',
            'icon'  => 'fa fa-calendar',
            'opt'   => '',
            'sub'   => 0
        ],


    ],
 ];



