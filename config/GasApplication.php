<?php

return [
    'user_roles' => [
        '1' => 'super admin',
        '2' => 'home customer',
        '3' => 'small business customer',
        '4' => 'gas supplier',
        '5' => 'lpg supplier',
        '6' => 'oil supplier',
        '7' => 'bottled water supplier',
        '8' => 'tanker water supplier',
        '9' => 'barrel fuel supplier',
        '10' => 'per liter fuel supplier',
        '11' => 'diesel supplier',
        '12' => 'driver',
    ],
    'dummy' => ''
];
