<?php

//

use Illuminate\Http\Request;

/*
  |--------------------------------------------------------------------------
  | API Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register API routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | is assigned the "api" middleware group. Enjoy building your API!
  |
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'middleware' => 'auth:api'
        ], function () {
//      Route::post('get-details', 'API\PassportController@getDetails');
});

//Route::post('login', 'API\AuthController@login');
Route::get('configuration/{type}', 'API\ConfigurationController@getItem');
//Authentication
//Customer
Route::post('auth/change-password', 'API\AuthController@changePasswordWithAuth');
Route::post('without-auth/change-password', 'API\AuthController@changePasswordWithOutAuth');
Route::post('customer/login', 'API\AuthController@login.WithOTP');
Route::post('customer/verify-login', 'API\AuthController@verifyLogin');
Route::post('customer/resend-otp', 'API\AuthController@resendOTP');
Route::post('customer/registeration', 'API\AuthController@registerCustomer');
Route::post('customer/update/{id}', 'API\AuthController@updateCustomer');
Route::post('customer/update-number/{id}', 'API\AuthController@updateMobileNumber');
Route::post('customer/verify-number/{id}', 'API\AuthController@verifyMobileNumber');
Route::post('customer/location/store', 'API\UserAddressController@store');
Route::post('customer/location/update/{id}', 'API\UserAddressController@update');
Route::delete('customer/location/{id}', 'API\UserAddressController@destroy');
Route::get('customer/location/{id}', 'API\UserAddressController@getItems');
Route::get('customer/services/', 'API\ServiceController@getItems');
Route::get('customer/service/{governateid}', 'API\ServiceController@getItems');
Route::post('customer/order/store', 'API\OrdersController@store');
Route::get('customer/order/{id}', 'API\OrdersController@getItems');
Route::delete('customer/order/{customerid}/{orderid}', 'API\OrdersController@destroy');
Route::get('customer/order/{id}/{serviceid}', 'API\OrdersController@getItems');
Route::post('customer/order/update/{supplierId}/{orderid}', 'API\OrdersController@updateStatusByCustomer');
//setrvicename = serviceroleid so not get confused
Route::get('customer/suppliers/{servicename}/{governateid}/{mobilenumber}', 'API\SupplierController@getItems');
Route::get('customer/suppliers/{servicename}/{governateid}', 'API\SupplierController@getItems');


//
//Supplier
Route::post('supplier/login/{service_id}', 'API\AuthController@sendOTPSupplier');
Route::post('supplier/registeration', 'API\AuthController@registerSupplier');
Route::get('supplier/user/{id}', 'API\AuthController@changeStatus');

//Route::post('supplier/location/store', 'API\UserSupplierController@store');
//Route::post('supplier/location/update/{id}', 'API\UserSupplierController@updateByID');
Route::post('supplier/location/update', 'API\UserSupplierController@update');
Route::post('supplier/location/update', 'API\UserSupplierController@update');
Route::get('supplier/location/{id}', 'API\UserSupplierController@getItems');

Route::post('supplier/verify-login', 'API\AuthController@verifyLogin');
Route::get('supplier/order/{supplierId}', 'API\OrdersController@getOrderBySupplier');
Route::get('supplier/order/{supplierId}/{serviceid}', 'API\OrdersController@getOrderBySupplier');

//Driver
Route::get('supplier/driver/{supplierid}', 'API\UserDriverController@getDriverList');
Route::post('supplier/driver/{supplierid}', 'API\UserDriverController@getDriverCreate');
Route::post('supplier/driver/update/{driverid}', 'API\UserDriverController@update');
Route::delete('supplier/driver/{driverid}', 'API\UserDriverController@destroy');
Route::post('supplier/order/update/{supplierId}/{orderid}', 'API\OrdersController@updateStatusBySupplier');
Route::get('supplier/services/', 'API\ServiceController@getItems');
//Location
Route::get('location/{type}', 'API\LocationController@getItem');
Route::get('location/{type}/{parent_id}', 'API\LocationController@getItem');
Route::get('supplier/favourite/{supplier_id}/{customer_id}', 'API\UserSupplierFavouriteController@getfavourite');
Route::get('supplier/favourite/{supplier_id}', 'API\UserSupplierFavouriteController@getCustomersList');
Route::get('notifications/{user_type}/{user_id}', 'API\NotificationController@getItems');

//Supplier service rate cards
Route::post('supplier/rate-card', 'API\UserSupplierServiceRateController@update');
Route::post('supplier/get-rate-card', 'API\UserSupplierServiceRateController@getItemList');

//Servicecontroller
Route::get('gasServiceFee/{id}', 'API\ServiceController@gasServiceFee');

//NEW APIS UPDATE (sachtech)

Route::post('customer/signUp', 'API\RegisterController@register');
Route::post('customer/login', 'API\RegisterController@login');
Route::post('customer/terms_condtion', 'API\RegisterController@termsAndCondtion');
Route::post('location/getLocation', 'API\LocationController@getLocation');
Route::post('location/getCity', 'API\LocationController@getCity');
Route::post('location/getSection', 'API\LocationController@getSection');

Route::post('supplier/getSupplierServices', 'API\SupplierController@getSupplierServiceList');
Route::post('supplier/supplierSignUp', 'API\SupplierController@supplierSignUp');
Route::post('supplier/searchSupplierByNumber', 'API\SupplierController@searchSupplierByNumber');
Route::post('customer/mobileAlreadyExist', 'API\RegisterController@mobileAlreadyExist');
Route::post('register/generateOtp', 'API\RegisterController@generateOtp');
Route::post('supplier/servicesByLocation', 'API\OrderController@servicesByLocation');
Route::post('register/forgotPassword', 'API\RegisterController@forgotPassword');
Route::post('supplier/getSubServices', 'API\OrderController@getSubServices');

Route::post('supplier/createSupplier', 'API\SupplierController@createSupplier');

Route::group(['middleware' => 'auth:api'], function(){
    Route::post('customer/userDetails', 'API\RegisterController@userDetails');
    Route::post('customer/changePassword','API\RegisterController@changePassword');
    Route::post('customer/editProfile', 'API\RegisterController@editProfile');
    Route::post('customer/addAddress', 'API\RegisterController@addAddress');
    Route::post('customer/getAddress', 'API\RegisterController@getAddress');
    Route::post('customer/deleteAddress', 'API\RegisterController@deleteAddress');
    Route::post('customer/updateAddress', 'API\RegisterController@updateAddress');
    Route::post('order/getServiceList', 'API\OrderController@getServiceList');
    Route::post('supplier/updateSupplier', 'API\SupplierController@updateSupplier');
    Route::post('supplier/addLocation', 'API\SupplierController@addLocation');
    Route::post('supplier/addDriver', 'API\SupplierController@addDriver');
    Route::post('supplier/getSupplierDriver', 'API\SupplierController@getSupplierDriver');
    Route::post('supplier/deleteSupplierDriver', 'API\SupplierController@deleteSupplierDriver');
    Route::post('supplier/getFavCustomer', 'API\SupplierController@getSupplierFavCustomer');
    Route::post('supplier/addFavCustomer', 'API\SupplierController@addFavCustomer');
    Route::post('supplier/deleteFavCustomer', 'API\SupplierController@deleteFavCustomer');
    Route::post('supplier/supplierStatus', 'API\SupplierController@supplierStatus');
    Route::post('customer/placeOrder', 'API\OrderController@placeOrder');
    Route::post('customer/findSupplier', 'API\OrderController@findSupplierByPhoneNumber');
    Route::post('customer/rateSupplier', 'API\CustomerController@rateSupplier');
    Route::post('customer/customerOrderList', 'API\CustomerController@customerOrderList');
    Route::post('customer/myCalculation', 'API\CustomerController@myCalculation');
    Route::post('order/customer_cancelOrder', 'API\OrderController@cancelOrder_Cusotmer');
    Route::post('order/nearBySupplier', 'API\OrderController@findNearBySupplier');
    Route::post('supplier/getSupplierOrders', 'API\SupplierController@getSupplierOrders');
    Route::post('supplier/updateSupplierSection', 'API\SupplierController@updateSupplierSection');
    Route::post('supplier/updateSupplierProfile', 'API\SupplierController@updateSupplierProfile');
    Route::post('customer/getHowTo', 'API\CustomerController@getHowTo');
    Route::post('customer/getAds', 'API\CustomerController@getAds');
    Route::post('customer/orderHistoryList', 'API\CustomerController@orderHistoryList');
    Route::post('supplier/changeOrderStatus', 'API\SupplierController@changeOrderStatus');
    Route::post('supplier/supplierAcceptedOrders', 'API\SupplierController@supplierAcceptedOrders');
    Route::post('customer/logout', 'API\RegisterController@logout');
    Route::post('customer/orderList','API\OrderController@orderLIst');

    //Customer
    Route::post('customer/createOrder', 'API\OrderController@createOrder');
    Route::post('customer/customerOrdersList', 'API\CustomerController@customerOrdersList');
    Route::post('order/updateFeedback', 'API\OrderController@updateFeedback');
    // Supplier
    Route::post('supplier/getProfile', 'API\SupplierController@getProfile');
});

// Route::post('customer/signIn', 'API\RegisterController@signIn');
