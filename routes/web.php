<?php
/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */


$adminPrefix = config('app.admin_prefix');
if (version_compare(PHP_VERSION, '7.2.0', '>=')) {
    // Ignores notices and reports all other kinds... and warnings
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
    // error_reporting(E_ALL ^ E_WARNING); // Maybe this is enough
}

Route::any('/', function()use($adminPrefix) {
    return redirect('/login');
});
Route::any($adminPrefix, function()use($adminPrefix) {
    return redirect($adminPrefix . '/login');
});
Route::group(['prefix' => 'laravel-filemanager', 'middleware' => ['web']], function () {
    '\vendor\uniSharp\LaravelFilemanager\Lfm::routes()';
});
/* * ** Admin Routes **** */
Route::group(['middleware' => ['web']], function ()use($adminPrefix) {
//    Route::get('login', 'UserLoginController@getUserLogin');
//    Route::post('login', ['as' => 'user.auth', 'uses' => 'UserLoginController@userAuth']);
    Route::get($adminPrefix . '/delete-all/{model}', 'AdminController@deleteAll');
    Route::get($adminPrefix . '/login', 'AdminLoginController@getAdminLogin')->name('admin.login');
    Route::post($adminPrefix . '/login', ['as' => 'admin.auth', 'uses' => 'AdminLoginController@adminAuth']);
    Route::get($adminPrefix . '/password-reset', ['as' => 'admin.reset', 'uses' => 'Auth\PasswordController@resetPassoword']);

    // Password reset routes...
    Route::get($adminPrefix . '/password/reset/{token}', 'Auth\PasswordController@getReset');
    Route::post($adminPrefix . '/password/reset', 'Auth\PasswordController@postReset')->name('password.reset');

//    Route::group(['middleware' => ['admin']], function ()use($adminPrefix) {
    Route::get($adminPrefix . '/dashboard', ['as' => 'admin.dashboard', 'uses' => 'AdminController@dashboard']);
    Route::resource($adminPrefix . '/adminuser', 'Admin\AdminUserController');
    Route::resource($adminPrefix . '/users', 'Admin\UserController');
    Route::resource($adminPrefix . '/roles', 'Admin\RoleController');
    Route::any($adminPrefix . '/role-fetch', 'Admin\RoleController@FetchIndex')->name('role-fetch.index');
    Route::resource($adminPrefix . '/customers', 'Admin\UserCustomerController');
    Route::get($adminPrefix . '/customers-search', 'Admin\UserCustomerController@indexSearch');

    Route::resource($adminPrefix . '/suppliers', 'Admin\UserSupplierController');
    Route::any($adminPrefix . '/search-manage/{id}', 'Admin\UserSupplierController@manageSupplier');
    Route::get($adminPrefix . '/suppliers-search', 'Admin\UserSupplierController@indexSearch');
    Route::resource($adminPrefix . '/drivers', 'Admin\UserDriverController');
    Route::resource($adminPrefix . '/services', 'Admin\ServiceController');
    Route::resource($adminPrefix . '/governorate', 'Admin\LocationGovernorateController');
    Route::resource($adminPrefix . '/city', 'Admin\LocationCityController');
    Route::resource($adminPrefix . '/section', 'Admin\LocationSectionController');
    Route::post($adminPrefix . '/filterSection', 'Admin\LocationSectionController@filterSection');
    Route::resource($adminPrefix . '/admin-roles', 'Admin\AdminRoleController');
    Route::any($adminPrefix . '/admin-role-fetch', 'Admin\AdminRoleController@FetchIndex')->name('admin-role-fetch.index');


    Route::post($adminPrefix . '/import-csv-excel', array('as' => 'import-csv-excel', 'uses' => 'Admin\LocationGovernorateController@importFileIntoDB'));
    Route::post($adminPrefix . '/import-customer-csv-excel', array('as' => 'import-customer-csv-excel', 'uses' => 'Admin\UserController@customerimportFileIntoDB'));
    Route::post($adminPrefix . '/import-supplier-csv-excel', array('as' => 'import-supplier-csv-excel', 'uses' => 'Admin\UserController@supplierimportFileIntoDB'));
    // Route::post($adminPrefix . '/import-csv-excel', array('as' => 'import-csv-excel', 'uses' => 'Admin\Import_ExportController@importFileIntoDBGovernorate'));
    // Route::post($adminPrefix . '/import-customer-csv-excel', array('as' => 'import-customer-csv-excel', 'uses' => 'Admin\UserController@customerimportFileIntoDB'));
    // Route::post($adminPrefix . '/import-supplier-csv-excel', array('as' => 'import-supplier-csv-excel', 'uses' => 'Admin\Import_ExportController@supplierimportFileIntoDB'));

    Route::get($adminPrefix . '/configuration', 'Admin\ConfigurationController@index')->name('configuration.edit');
    Route::put($adminPrefix . '/configuration', 'Admin\ConfigurationController@update');
    Route::resource($adminPrefix . '/how_to', 'Admin\HowToController');
    Route::put($adminPrefix . '/how_to', 'Admin\HowToController@update');
    Route::get($adminPrefix . '/howto_supplier', 'Admin\HowToController@supplier_view');
    Route::put($adminPrefix . '/howto_supplier', 'Admin\HowToController@update_supplier');
    Route::resource($adminPrefix . '/ads', 'Admin\AdsController');
    Route::put($adminPrefix . '/ads', 'Admin\HowToController@update_homecustomer_ads');
    Route::any($adminPrefix .'/ads_business', 'Admin\HowToController@ads_businesscustomer');
    Route::put($adminPrefix .'/ads_business', 'Admin\HowToController@update_businesscustomer_ads');
    Route::any($adminPrefix .'/ads_supplier', 'Admin\HowToController@ads_supplier');
    Route::put($adminPrefix .'/ads_supplier', 'Admin\HowToController@update_supplier_ads');

    Route::any($adminPrefix . '/location/governorate/changestate/{id}', 'Admin\LocationGovernorateController@changeState')->name('location.changestate');
    Route::any($adminPrefix . '/delete-state', 'Admin\LocationGovernorateController@multipledestroy')->name('multiple.delete');
    Route::any($adminPrefix . '/services/changestate/{id}', 'Admin\ServiceController@changeState')->name('services.changestate');
    Route::any($adminPrefix . '/multipledestroy_services', 'Admin\OrderController@multipledestroy_services');
    Route::any($adminPrefix . '/multipledestroy_units', 'Admin\MeasurementUnitController@multipledestroy_units');
    Route::any($adminPrefix . '/multipledestroy_customers', 'Admin\MeasurementUnitController@multipledestroy_customers');
    Route::any($adminPrefix . '/setGasAppFav_Supplier', 'Admin\OrderController@setGasAppFav_Supplier');
    Route::any($adminPrefix . '/multipleSupplier_notification', 'Admin\OrderController@multipleSupplier_notification');
    Route::any($adminPrefix . '/user/changestate/{id}', 'Admin\UserController@changeState')->name('user.changestate');
    Route::post($adminPrefix . '/notification/{type}/{target}', 'Admin\NotificationController@notify')->name('notify.send');
    Route::resource($adminPrefix . '/measurements', 'Admin\MeasurementUnitController');


    Route::resource($adminPrefix . '/orders', 'Admin\OrderController');
    Route::any($adminPrefix . '/forward_order/{order_id}/{supplier_id}', 'Admin\OrderController@forward_view');
    Route::post($adminPrefix . '/update_order_supplier/{id}', 'Admin\OrderController@update_order_supplier');
    Route::any($adminPrefix . '/order_detail/{order_id}', ['uses' =>'Admin\OrderController@orderDetail']);
    Route::resource($adminPrefix . '/supplier_location', 'Admin\SupplierLocationController');
    Route::resource($adminPrefix . '/supplier_product', 'Admin\SupplierProductController');
    Route::post($adminPrefix . '/supplier_product/{id}', 'Admin\SupplierProductController@update');
    Route::any($adminPrefix . '/delete-orders', 'Admin\OrderController@multipledestroy');
    Route::any($adminPrefix . '/filterSupplier', 'Admin\OrderController@filterSupplier');
    Route::any($adminPrefix . '/filterCustomer', 'Admin\OrderController@filterCustomer');
    Route::any($adminPrefix . '/filterOrder', 'Admin\OrderController@filterOrder');
    Route::any($adminPrefix . '/deleteVehicle/{id}', 'Admin\SupplierLocationController@deleteVehicle');
    Route::any($adminPrefix . '/deleteLicense/{id}', 'Admin\SupplierLocationController@deleteLicense');

    Route::resource($adminPrefix . '/tickets', 'Admin\TicketController');

    // push notification
    Route::resource($adminPrefix . '/push_notification', 'Admin\PushNotificationController');
    Route::any($adminPrefix . '/filterNotification', 'Admin\PushNotificationController@filterNotification');


    //Fees
    Route::get($adminPrefix . '/service_fees/{id}', 'Admin\GasAppFeeController@index');
    Route::resource($adminPrefix . '/service_fees', 'Admin\GasAppFeeController');
    Route::any($adminPrefix . '/service_fees/getGasApp_fees', 'Admin\GasAppFeeController@getGasApp_fees');

    //Downloads
    Route::get('downloadExcel_orders/{type}', 'Admin\Import_ExportController@downloadExcel_orders');
    Route::get('downloadExcel_customers/{type}', 'Admin\Import_ExportController@downloadExcel_customer');
    Route::get('downloadExcel_suppliers/{type}', 'Admin\Import_ExportController@downloadExcel_suppier');
    Route::get('downloadExcel_service/{type}', 'Admin\Import_ExportController@downloadExcel_service');
    Route::get('downloadExcel_units/{type}', 'Admin\Import_ExportController@downloadExcel_units');
    Route::get('downloadExcel_governorate/{type}', 'Admin\Import_ExportController@downloadExcel_governorate');
    Route::get('downloadExcel_city/{type}', 'Admin\Import_ExportController@downloadExcel_city');
    Route::get('downloadExcel_section/{type}', 'Admin\Import_ExportController@downloadExcel_section');
    Route::post('/logout', 'AdminLoginController@logout')->name('logout');

});



/* userlogin routes */
    Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('login.user');
    Route::get('lang/{locale}', 'LocalizationController@index');
    Route::get('/login', 'UserLoginController@Login')->name('login.user');
    Route::get('/logout', 'UserLoginController@logout')->name('user.logout');

    Route::post('/login', 'UserLoginController@Auth')->name('login.user.auth');
    Route::get('/dashboard', 'HomeController@dashboard')->name('home.dashboard');
    Route::get('/services', 'HomeController@getservice')->name('home.service');
    Route::get('/profile', 'HomeController@profile')->name('home.profile');
    Route::get('/order-gas', 'HomeController@ordergas')->name('order.gas');
    Route::get('/service_child/{id}', 'HomeController@orderquantity')->name('home.order');
    Route::post('/save', 'HomeController@profilesave')->name('profile.save');
    Route::get('/number', 'HomeController@getphonenumber')->name('phone.no');
    Route::get('/order/{id}', 'HomeController@ordershow')->name('order.show');
    Route::get('/order_history/{id}', 'HomeController@order_historyshow')->name('order_history.show');
    Route::get('/terms_and_condtions', 'HomeController@terms_and_conditions')->name('home.terms_and_condtions');

    Route::post('/reset_password', 'UserLoginController@reset_password')->name('login.reset');
    Route::get('/forgot', 'UserLoginController@forgot')->name('login.forgot');
    Route::post('/validation', 'UserLoginController@validation')->name('login.validation');
    Route::post('/save_changes', 'UserLoginController@save_changes')->name('login.save_changes');
    Route::post('/problem', 'HomeController@problem')->name('home.problem');
    Route::post('/orderproblem', 'HomeController@submitProblem')->name('home.problem_submit');
    Route::post('/order_delete', 'HomeController@orderDelete_Customer')->name('order.delete_order');
    Route::post('/placeorder_1', 'HomeController@placeOrder_Step1')->name('place_order.step1');
    Route::post('/placeorder_2', 'HomeController@placeOrder_Step1Multiple')->name('place_order.step1Multiple');
    Route::post('/search_supplier', 'HomeController@search_supplier')->name('order.search_supplier');
    Route::post('/search_supplier_multiple', 'HomeController@search_supplier_multiple')->name('order.search_supplier_multiple');

    Route::post('/storeOrder', 'HomeController@placeOrderFinalStep')->name('order.submit_confirm');
    Route::post('/storeOrderMultiple', 'HomeController@placeOrderFinalStepMultiple')->name('order.submit_confirm_multiple');
    Route::post('/multipleOrder', 'OrderController@multipleOrderPlace')->name('order.multiple_order');

    



});










