-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 21, 2019 at 05:09 PM
-- Server version: 5.6.35-1+deb.sury.org~xenial+0.1
-- PHP Version: 7.3.10-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gas-application`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE `admin_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin', 'admin@gasapplication.com', '$2y$10$lYCMGRFQsUSR6kyKkHA2ku85wngYxmuQDvxeIL55geOuccIqRF4k2', NULL, '2019-10-18 05:33:37', '2019-10-18 05:33:37', NULL),
(3, 'new admin', 'admin_new@gasapplication.com', '$2y$10$YWE96zXjIG138FA/9KdoxOV/1.i0.HqVaHeAgp.6xKTtvH6hzLiKK', 'G3QKVMgSJne37WzU3Sqt9l803S78H7O2cLlSqCha3EVOlKviVrUqgfMYeQsF', '2019-10-18 05:44:15', '2019-10-18 05:44:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `configurations`
--

CREATE TABLE `configurations` (
  `id` int(10) UNSIGNED NOT NULL,
  `tnc` text COLLATE utf8mb4_unicode_ci,
  `tnc_kr` text COLLATE utf8mb4_unicode_ci,
  `tnc_ar` text COLLATE utf8mb4_unicode_ci,
  `howto` text COLLATE utf8mb4_unicode_ci,
  `howto_kr` text COLLATE utf8mb4_unicode_ci,
  `howto_ar` text COLLATE utf8mb4_unicode_ci,
  `ads_ar` text COLLATE utf8mb4_unicode_ci,
  `ads_kr` text COLLATE utf8mb4_unicode_ci,
  `ads` text COLLATE utf8mb4_unicode_ci,
  `params` text COLLATE utf8mb4_unicode_ci,
  `state` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0->Unactive, 1->Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `configurations`
--

INSERT INTO `configurations` (`id`, `tnc`, `tnc_kr`, `tnc_ar`, `howto`, `howto_kr`, `howto_ar`, `ads_ar`, `ads_kr`, `ads`, `params`, `state`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Gas Application\nOur aim to build a smart communication between suppliers and consumers.\n\nAs a supplier Gas App will provide me information about consumer\'s request (name, phone, location,quantity) in opposite small amount of money and I agreed.\n\nAs a supplier I promise that I deliver the requests at a time.\n\nI\'m us user of Gas App I promise that I will not use Gas App for scam and fraud otherwise Gas App has the right to file a complaim against me.\n\nTo avoid fake purchases Gas App has lock option please active it during children play by your device.\n\nGas App aim to build a smart communication between suppliers and consumers. We do not have any role in quality and price of Gas.\n\nThe above information and terms may be modified or replaced at any time. Pleace if you have any question contact us.\n\nFor more Information and keeping update follow us in our website and social media.\n', 'tnckurdish', 'tncarbic', 'how to english', NULL, NULL, NULL, NULL, NULL, NULL, '0', '2019-10-19 05:58:36', '2019-10-19 05:58:36', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gas_app_fees`
--

CREATE TABLE `gas_app_fees` (
  `id` int(10) UNSIGNED NOT NULL,
  `city_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `service_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price_for_one` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price_for_multiple` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gas_notifications`
--

CREATE TABLE `gas_notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8mb4_unicode_ci,
  `user_type` enum('supplier','customer') COLLATE utf8mb4_unicode_ci NOT NULL,
  `notification_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` text COLLATE utf8mb4_unicode_ci,
  `state` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0->Unactive, 1->Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `how_to_models`
--

CREATE TABLE `how_to_models` (
  `id` int(10) UNSIGNED NOT NULL,
  `howto_customer_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `howto_customer_ar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `howto_customer_kr` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `howto_supplier_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `howto_supplier_ar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `howto_supplier_kr` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ads_customer_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ads_customer_ar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ads_customer_kr` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ads_supplier_en` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ads_supplier_ar` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `ads_supplier_kr` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `how_to_models` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE `locations` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `type` enum('governorate','city','section') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_kr` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `params` text COLLATE utf8mb4_unicode_ci,
  `state` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0->Unactive, 1->Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `measurement_units`
--

CREATE TABLE `measurement_units` (
  `id` int(10) UNSIGNED NOT NULL,
  `units` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `row_status` enum('active','delete') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2018_06_12_000000_create_admin_table', 1),
(9, '2018_12_21_134623_create_permission_tables', 1),
(10, '2019_05_16_142213_create_user_drivers_table', 1),
(11, '2019_05_22_184902_create_services_table', 2),
(12, '2019_05_22_222530_create_configurations_table', 3),
(13, '2019_05_22_222533_create_locations_table', 4),
(14, '2019_05_24_121642_alter_configurations', 5),
(15, '2019_05_30_080827_create_user_addresses_table', 6),
(16, '2019_06_03_065210_create_orders_table', 7),
(17, '2019_06_12_110809_add_default_qty_to_services', 8),
(18, '2019_06_19_062027_create_order_children_table', 9),
(19, '2019_06_25_133421_create_user_supplier_favourites_table', 10),
(20, '2019_07_04_050821_create_gas_notifications_table', 11),
(21, '2019_07_10_083243_create_user_supplier_service_rates_table', 12),
(22, '2019_08_02_050808_create_registermodels_table', 13),
(23, '2019_08_07_130002_create_support_models_table', 14),
(24, '2019_08_22_045934_create_how_to_models_table', 15),
(25, '2019_08_26_101532_create_measurement_units_table', 16),
(26, '2019_09_07_072201_create_push_notifications_table', 17),
(27, '2019_09_10_093152_create_gas_app_fees_table', 18),
(28, '2019_05_16_101305_create_user_customers_table', 19);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\AdminUser', 1),
(1, 'App\\AdminUser', 3);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_customer_id` int(10) UNSIGNED NOT NULL,
  `user_customer_address_id` int(10) UNSIGNED NOT NULL,
  `service_id` int(10) UNSIGNED NOT NULL,
  `is_child` int(11) NOT NULL DEFAULT '0',
  `service_quantity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_price` double(8,2) DEFAULT '0.00',
  `user_supplier_id` int(10) UNSIGNED NOT NULL,
  `request_status` enum('not accepted','canceled','processing','completed','rejected') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'not accepted',
  `review_customer` enum('0','1','2','3','4','5') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `review_supplier` enum('0','1','2','3','4','5') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `params` text COLLATE utf8mb4_unicode_ci,
  `state` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0->Unactive, 1->Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_children`
--

CREATE TABLE `order_children` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `service_id` int(10) UNSIGNED NOT NULL,
  `service_quantity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_price` double(8,2) DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'api/user', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(2, 'api/configuration/{type}', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(3, 'api/auth/change-password', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(4, 'api/without-auth/change-password', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(5, 'api/customer/login', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(6, 'api/customer/verify-login', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(7, 'api/customer/resend-otp', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(8, 'api/customer/registeration', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(9, 'api/customer/update/{id}', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(10, 'api/customer/update-number/{id}', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(11, 'api/customer/verify-number/{id}', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(12, 'api/customer/location/store', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(13, 'api/customer/location/update/{id}', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(14, 'api/customer/location/{id}', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(15, 'api/customer/services', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(16, 'api/customer/service/{governateid}', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(17, 'api/customer/order/store', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(18, 'api/customer/order/{id}', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(19, 'api/customer/order/{customerid}/{orderid}', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(20, 'api/customer/order/{id}/{serviceid}', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(21, 'api/customer/order/update/{supplierId}/{orderid}', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(22, 'api/customer/suppliers/{servicename}/{governateid}/{mobilenumber}', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(23, 'api/customer/suppliers/{servicename}/{governateid}', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(24, 'api/supplier/login/{service_id}', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(25, 'api/supplier/registeration', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(26, 'api/supplier/user/{id}', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(27, 'api/supplier/location/update', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(28, 'api/supplier/location/{id}', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(29, 'api/supplier/verify-login', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(30, 'api/supplier/order/{supplierId}', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(31, 'api/supplier/order/{supplierId}/{serviceid}', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(32, 'api/supplier/driver/{supplierid}', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(33, 'api/supplier/driver/update/{driverid}', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(34, 'api/supplier/driver/{driverid}', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(35, 'api/supplier/order/update/{supplierId}/{orderid}', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(36, 'api/supplier/services', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(37, 'api/location/{type}', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(38, 'api/location/{type}/{parent_id}', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(39, 'api/supplier/favourite/{supplier_id}/{customer_id}', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(40, 'api/supplier/favourite/{supplier_id}', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(41, 'api/notifications/{user_type}/{user_id}', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(42, 'api/supplier/rate-card', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(43, 'api/supplier/get-rate-card', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(44, 'api/customer/signUp', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(45, 'api/customer/terms_condtion', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(46, 'api/location/getLocation', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(47, 'api/location/getCity', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(48, 'api/location/getSection', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(49, 'api/supplier/getSupplierServices', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(50, 'api/supplier/supplierSignUp', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(51, 'api/supplier/searchSupplierByNumber', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(52, 'api/customer/userDetails', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(53, 'api/customer/changePassword', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(54, 'api/customer/editProfile', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(55, 'api/customer/addAddress', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(56, 'api/customer/getAddress', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(57, 'api/customer/deleteAddress', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(58, 'api/customer/updateAddress', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(59, 'api/order/getServiceList', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(60, 'api/supplier/updateSupplier', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(61, 'api/supplier/addLocation', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(62, 'api/supplier/addDriver', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(63, 'api/supplier/getSupplierDriver', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(64, 'api/supplier/deleteSupplierDriver', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(65, 'api/supplier/getFavCustomer', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(66, 'api/supplier/supplierStatus', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(67, 'api/customer/placeOrder', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(68, 'api/customer/findSupplier', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(69, 'api/customer/rateSupplier', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(70, 'api/customer/customerOrderList', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(71, 'api/customer/myCalculation', 'api', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(72, 'api/order/customer_cancelOrder', 'api', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(73, 'api/order/nearBySupplier', 'api', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(74, 'api/supplier/getSupplierOrders', 'api', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(75, 'api/supplier/getHowTo', 'api', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(76, 'api/customer/logout', 'api', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(77, '/', 'api', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(78, 'admin', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(79, 'admin/delete-all/{model}', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(80, 'admin/login', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(81, 'admin/password-reset', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(82, 'admin/password/reset/{token}', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(83, 'admin/password/reset', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(84, 'admin/dashboard', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(85, 'admin/adminuser', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(86, 'admin/adminuser/create', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(87, 'admin/adminuser/{adminuser}', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(88, 'admin/adminuser/{adminuser}/edit', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(89, 'admin/users', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(90, 'admin/users/create', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(91, 'admin/users/{user}', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(92, 'admin/users/{user}/edit', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(93, 'admin/roles', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(94, 'admin/roles/create', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(95, 'admin/roles/{role}', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(96, 'admin/roles/{role}/edit', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(97, 'admin/role-fetch', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(98, 'admin/customers', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(99, 'admin/customers/create', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(100, 'admin/customers/{customer}', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(101, 'admin/customers/{customer}/edit', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(102, 'admin/customers-search', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(103, 'admin/suppliers', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(104, 'admin/suppliers/create', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(105, 'admin/suppliers/{supplier}', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(106, 'admin/suppliers/{supplier}/edit', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(107, 'admin/suppliers-search', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(108, 'admin/drivers', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(109, 'admin/drivers/create', 'web-admin', '2019-10-18 05:33:13', '2019-10-18 05:33:13'),
(110, 'admin/drivers/{driver}', 'web-admin', '2019-10-18 05:33:14', '2019-10-18 05:33:14'),
(111, 'admin/drivers/{driver}/edit', 'web-admin', '2019-10-18 05:33:14', '2019-10-18 05:33:14'),
(112, 'admin/services', 'web-admin', '2019-10-18 05:33:14', '2019-10-18 05:33:14'),
(113, 'admin/services/create', 'web-admin', '2019-10-18 05:33:14', '2019-10-18 05:33:14'),
(114, 'admin/services/{service}', 'web-admin', '2019-10-18 05:33:14', '2019-10-18 05:33:14'),
(115, 'admin/services/{service}/edit', 'web-admin', '2019-10-18 05:33:14', '2019-10-18 05:33:14'),
(116, 'admin/governorate', 'web-admin', '2019-10-18 05:33:14', '2019-10-18 05:33:14'),
(117, 'admin/governorate/create', 'web-admin', '2019-10-18 05:33:14', '2019-10-18 05:33:14'),
(118, 'admin/governorate/{governorate}', 'web-admin', '2019-10-18 05:33:14', '2019-10-18 05:33:14'),
(119, 'admin/governorate/{governorate}/edit', 'web-admin', '2019-10-18 05:33:14', '2019-10-18 05:33:14'),
(120, 'admin/city', 'web-admin', '2019-10-18 05:33:14', '2019-10-18 05:33:14'),
(121, 'admin/city/create', 'web-admin', '2019-10-18 05:33:14', '2019-10-18 05:33:14'),
(122, 'admin/city/{city}', 'web-admin', '2019-10-18 05:33:14', '2019-10-18 05:33:14'),
(123, 'admin/city/{city}/edit', 'web-admin', '2019-10-18 05:33:14', '2019-10-18 05:33:14'),
(124, 'admin/section', 'web-admin', '2019-10-18 05:33:14', '2019-10-18 05:33:14'),
(125, 'admin/section/create', 'web-admin', '2019-10-18 05:33:14', '2019-10-18 05:33:14'),
(126, 'admin/section/{section}', 'web-admin', '2019-10-18 05:33:14', '2019-10-18 05:33:14'),
(127, 'admin/section/{section}/edit', 'web-admin', '2019-10-18 05:33:14', '2019-10-18 05:33:14'),
(128, 'admin/admin-roles', 'web-admin', '2019-10-18 05:33:14', '2019-10-18 05:33:14'),
(129, 'admin/admin-roles/create', 'web-admin', '2019-10-18 05:33:14', '2019-10-18 05:33:14'),
(130, 'admin/admin-roles/{admin_role}', 'web-admin', '2019-10-18 05:33:14', '2019-10-18 05:33:14'),
(131, 'admin/admin-roles/{admin_role}/edit', 'web-admin', '2019-10-18 05:33:14', '2019-10-18 05:33:14'),
(132, 'admin/admin-role-fetch', 'web-admin', '2019-10-18 05:33:14', '2019-10-18 05:33:14'),
(133, 'admin/import-csv-excel', 'web-admin', '2019-10-18 05:33:14', '2019-10-18 05:33:14'),
(134, 'admin/import-customer-csv-excel', 'web-admin', '2019-10-18 05:33:14', '2019-10-18 05:33:14'),
(135, 'admin/import-supplier-csv-excel', 'web-admin', '2019-10-18 05:33:14', '2019-10-18 05:33:14'),
(136, 'admin/configuration', 'web-admin', '2019-10-18 05:33:14', '2019-10-18 05:33:14'),
(137, 'admin/how_to', 'web-admin', '2019-10-18 05:33:14', '2019-10-18 05:33:14'),
(138, 'admin/how_to/create', 'web-admin', '2019-10-18 05:33:14', '2019-10-18 05:33:14'),
(139, 'admin/how_to/{how_to}', 'web-admin', '2019-10-18 05:33:15', '2019-10-18 05:33:15'),
(140, 'admin/how_to/{how_to}/edit', 'web-admin', '2019-10-18 05:33:15', '2019-10-18 05:33:15'),
(141, 'admin/howto_supplier', 'web-admin', '2019-10-18 05:33:15', '2019-10-18 05:33:15'),
(142, 'admin/ads', 'web-admin', '2019-10-18 05:33:15', '2019-10-18 05:33:15'),
(143, 'admin/ads/create', 'web-admin', '2019-10-18 05:33:15', '2019-10-18 05:33:15'),
(144, 'admin/ads/{ad}', 'web-admin', '2019-10-18 05:33:15', '2019-10-18 05:33:15'),
(145, 'admin/ads/{ad}/edit', 'web-admin', '2019-10-18 05:33:15', '2019-10-18 05:33:15'),
(146, 'admin/ads_business', 'web-admin', '2019-10-18 05:33:15', '2019-10-18 05:33:15'),
(147, 'admin/ads_supplier', 'web-admin', '2019-10-18 05:33:15', '2019-10-18 05:33:15'),
(148, 'admin/location/governorate/changestate/{id}', 'web-admin', '2019-10-18 05:33:15', '2019-10-18 05:33:15'),
(149, 'admin/delete-state', 'web-admin', '2019-10-18 05:33:15', '2019-10-18 05:33:15'),
(150, 'admin/services/changestate/{id}', 'web-admin', '2019-10-18 05:33:15', '2019-10-18 05:33:15'),
(151, 'admin/multipledestroy_services', 'web-admin', '2019-10-18 05:33:15', '2019-10-18 05:33:15'),
(152, 'admin/multipledestroy_units', 'web-admin', '2019-10-18 05:33:15', '2019-10-18 05:33:15'),
(153, 'admin/multipledestroy_customers', 'web-admin', '2019-10-18 05:33:15', '2019-10-18 05:33:15'),
(154, 'admin/setGasAppFav_Supplier', 'web-admin', '2019-10-18 05:33:15', '2019-10-18 05:33:15'),
(155, 'admin/multipleSupplier_notification', 'web-admin', '2019-10-18 05:33:15', '2019-10-18 05:33:15'),
(156, 'admin/user/changestate/{id}', 'web-admin', '2019-10-18 05:33:15', '2019-10-18 05:33:15'),
(157, 'admin/notification/{type}/{target}', 'web-admin', '2019-10-18 05:33:15', '2019-10-18 05:33:15'),
(158, 'admin/measurements', 'web-admin', '2019-10-18 05:33:15', '2019-10-18 05:33:15'),
(159, 'admin/measurements/create', 'web-admin', '2019-10-18 05:33:15', '2019-10-18 05:33:15'),
(160, 'admin/measurements/{measurement}', 'web-admin', '2019-10-18 05:33:15', '2019-10-18 05:33:15'),
(161, 'admin/measurements/{measurement}/edit', 'web-admin', '2019-10-18 05:33:15', '2019-10-18 05:33:15'),
(162, 'admin/orders', 'web-admin', '2019-10-18 05:33:15', '2019-10-18 05:33:15'),
(163, 'admin/orders/create', 'web-admin', '2019-10-18 05:33:15', '2019-10-18 05:33:15'),
(164, 'admin/orders/{order}', 'web-admin', '2019-10-18 05:33:16', '2019-10-18 05:33:16'),
(165, 'admin/orders/{order}/edit', 'web-admin', '2019-10-18 05:33:16', '2019-10-18 05:33:16'),
(166, 'admin/forward_order/{order_id}/{supplier_id}', 'web-admin', '2019-10-18 05:33:16', '2019-10-18 05:33:16'),
(167, 'admin/update_order_supplier/{id}', 'web-admin', '2019-10-18 05:33:16', '2019-10-18 05:33:16'),
(168, 'admin/order_detail/{order_id}', 'web-admin', '2019-10-18 05:33:16', '2019-10-18 05:33:16'),
(169, 'admin/supplier_location', 'web-admin', '2019-10-18 05:33:16', '2019-10-18 05:33:16'),
(170, 'admin/supplier_location/create', 'web-admin', '2019-10-18 05:33:16', '2019-10-18 05:33:16'),
(171, 'admin/supplier_location/{supplier_location}', 'web-admin', '2019-10-18 05:33:16', '2019-10-18 05:33:16'),
(172, 'admin/supplier_location/{supplier_location}/edit', 'web-admin', '2019-10-18 05:33:16', '2019-10-18 05:33:16'),
(173, 'admin/supplier_product', 'web-admin', '2019-10-18 05:33:16', '2019-10-18 05:33:16'),
(174, 'admin/supplier_product/create', 'web-admin', '2019-10-18 05:33:16', '2019-10-18 05:33:16'),
(175, 'admin/supplier_product/{supplier_product}', 'web-admin', '2019-10-18 05:33:16', '2019-10-18 05:33:16'),
(176, 'admin/supplier_product/{supplier_product}/edit', 'web-admin', '2019-10-18 05:33:16', '2019-10-18 05:33:16'),
(177, 'admin/supplier_product/{id}', 'web-admin', '2019-10-18 05:33:16', '2019-10-18 05:33:16'),
(178, 'admin/delete-orders', 'web-admin', '2019-10-18 05:33:16', '2019-10-18 05:33:16'),
(179, 'admin/filterSupplier', 'web-admin', '2019-10-18 05:33:16', '2019-10-18 05:33:16'),
(180, 'admin/filterCustomer', 'web-admin', '2019-10-18 05:33:16', '2019-10-18 05:33:16'),
(181, 'admin/filterOrder', 'web-admin', '2019-10-18 05:33:16', '2019-10-18 05:33:16'),
(182, 'admin/deleteVehicle/{id}', 'web-admin', '2019-10-18 05:33:16', '2019-10-18 05:33:16'),
(183, 'admin/deleteLicense/{id}', 'web-admin', '2019-10-18 05:33:16', '2019-10-18 05:33:16'),
(184, 'admin/push_notification', 'web-admin', '2019-10-18 05:33:16', '2019-10-18 05:33:16'),
(185, 'admin/push_notification/create', 'web-admin', '2019-10-18 05:33:17', '2019-10-18 05:33:17'),
(186, 'admin/push_notification/{push_notification}', 'web-admin', '2019-10-18 05:33:17', '2019-10-18 05:33:17'),
(187, 'admin/push_notification/{push_notification}/edit', 'web-admin', '2019-10-18 05:33:17', '2019-10-18 05:33:17'),
(188, 'admin/filterNotification', 'web-admin', '2019-10-18 05:33:17', '2019-10-18 05:33:17'),
(189, 'admin/service_fees', 'web-admin', '2019-10-18 05:33:17', '2019-10-18 05:33:17'),
(190, 'admin/service_fees/create', 'web-admin', '2019-10-18 05:33:17', '2019-10-18 05:33:17'),
(191, 'admin/service_fees/{service_fee}', 'web-admin', '2019-10-18 05:33:17', '2019-10-18 05:33:17'),
(192, 'admin/service_fees/{service_fee}/edit', 'web-admin', '2019-10-18 05:33:17', '2019-10-18 05:33:17'),
(193, 'admin/service_fees/getGasApp_fees', 'web-admin', '2019-10-18 05:33:17', '2019-10-18 05:33:17'),
(194, 'downloadExcel_orders/{type}', 'api', '2019-10-18 05:33:17', '2019-10-18 05:33:17'),
(195, 'downloadExcel_customers/{type}', 'api', '2019-10-18 05:33:17', '2019-10-18 05:33:17'),
(196, 'downloadExcel_suppliers/{type}', 'api', '2019-10-18 05:33:17', '2019-10-18 05:33:17'),
(197, 'downloadExcel_service/{type}', 'api', '2019-10-18 05:33:17', '2019-10-18 05:33:17'),
(198, 'downloadExcel_units/{type}', 'api', '2019-10-18 05:33:17', '2019-10-18 05:33:17'),
(199, 'downloadExcel_governorate/{type}', 'api', '2019-10-18 05:33:17', '2019-10-18 05:33:17'),
(200, 'downloadExcel_city/{type}', 'api', '2019-10-18 05:33:17', '2019-10-18 05:33:17'),
(201, 'downloadExcel_section/{type}', 'api', '2019-10-18 05:33:17', '2019-10-18 05:33:17'),
(202, 'logout', 'api', '2019-10-18 05:33:17', '2019-10-18 05:33:17'),
(203, 'lang/{locale}', 'api', '2019-10-18 05:33:17', '2019-10-18 05:33:17'),
(204, 'login', 'api', '2019-10-18 05:33:17', '2019-10-18 05:33:17'),
(205, 'dashboard', 'api', '2019-10-18 05:33:17', '2019-10-18 05:33:17'),
(206, 'services', 'api', '2019-10-18 05:33:17', '2019-10-18 05:33:17'),
(207, 'profile', 'api', '2019-10-18 05:33:17', '2019-10-18 05:33:17'),
(208, 'order-gas', 'api', '2019-10-18 05:33:17', '2019-10-18 05:33:17'),
(209, 'order', 'api', '2019-10-18 05:33:17', '2019-10-18 05:33:17'),
(210, 'save', 'api', '2019-10-18 05:33:17', '2019-10-18 05:33:17'),
(211, 'number', 'api', '2019-10-18 05:33:17', '2019-10-18 05:33:17'),
(212, 'order/{id}', 'api', '2019-10-18 05:33:17', '2019-10-18 05:33:17');

-- --------------------------------------------------------

--
-- Table structure for table `push_notifications`
--

CREATE TABLE `push_notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notification_type` enum('Text','Question') COLLATE utf8mb4_unicode_ci NOT NULL,
  `notification_text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_response` enum('Yes','NO') COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `registermodels`
--

CREATE TABLE `registermodels` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'super admin', 'web-admin', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(2, 'home customer', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(3, 'small business customer', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(4, 'gas supplier', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(5, 'lpg supplier', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(6, 'oil supplier', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(7, 'bottled water supplier', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(8, 'tanker water supplier', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(9, 'barrel fuel supplier', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(10, 'per liter fuel supplier', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(11, 'diesel supplier', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12'),
(12, 'driver', 'api', '2019-10-18 05:33:12', '2019-10-18 05:33:12');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(97, 1),
(98, 1),
(99, 1),
(100, 1),
(101, 1),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(106, 1),
(107, 1),
(108, 1),
(109, 1),
(110, 1),
(111, 1),
(112, 1),
(113, 1),
(114, 1),
(115, 1),
(116, 1),
(117, 1),
(118, 1),
(119, 1),
(120, 1),
(121, 1),
(122, 1),
(123, 1),
(124, 1),
(125, 1),
(126, 1),
(127, 1),
(128, 1),
(129, 1),
(130, 1),
(131, 1),
(132, 1),
(133, 1),
(134, 1),
(135, 1),
(136, 1),
(137, 1),
(138, 1),
(139, 1),
(140, 1),
(141, 1),
(142, 1),
(143, 1),
(144, 1),
(145, 1),
(146, 1),
(147, 1),
(148, 1),
(149, 1),
(150, 1),
(151, 1),
(152, 1),
(153, 1),
(154, 1),
(155, 1),
(156, 1),
(157, 1),
(158, 1),
(159, 1),
(160, 1),
(161, 1),
(162, 1),
(163, 1),
(164, 1),
(165, 1),
(166, 1),
(167, 1),
(168, 1),
(169, 1),
(170, 1),
(171, 1),
(172, 1),
(173, 1),
(174, 1),
(175, 1),
(176, 1),
(177, 1),
(178, 1),
(179, 1),
(180, 1),
(181, 1),
(182, 1),
(183, 1),
(184, 1),
(185, 1),
(186, 1),
(187, 1),
(188, 1),
(189, 1),
(190, 1),
(191, 1),
(192, 1),
(193, 1);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_kr` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_kr` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_ar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` text COLLATE utf8mb4_unicode_ci,
  `unit` enum('liter','quantity','set','barrel') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `defualt_qty` int(11) DEFAULT '0',
  `params` text COLLATE utf8mb4_unicode_ci,
  `state` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0->Unactive, 1->Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `image`, `name`, `image_kr`, `name_kr`, `image_ar`, `name_ar`, `location`, `unit`, `parent_id`, `defualt_qty`, `params`, `state`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'order-gas.png', 'order gas', 'order-gas.png', 'gazê', 'order-gas.png', 'طلب الغاز', '', NULL, 0, 0, NULL, '0', '2019-10-19 05:59:29', '2019-10-19 05:59:29', NULL),
(2, 'order-fuel.png', 'order fuel', 'order-fuel.png', 'firotin', 'order-fuel.png', 'طلب الوقود', '', NULL, 0, 0, NULL, '0', '2019-10-19 05:59:29', '2019-10-19 05:59:29', NULL),
(3, 'order-lpg.png', 'order lpg', 'order-lpg.png', 'lpg', 'order-lpg.png', 'أجل غاز البترول المسال', '', NULL, 0, 0, NULL, '0', '2019-10-19 05:59:29', '2019-10-19 05:59:29', NULL),
(4, 'order-bottled-water.png', 'order bottled', 'order-bottled-water.png', 'şîrîn', 'order-bottled-water.png', 'المعبأة في زجاجات ترتيب ', '', NULL, 0, 0, NULL, '0', '2019-10-19 05:59:29', '2019-10-19 05:59:29', NULL),
(5, 'order-diesel-fuel.png', 'order diesel', 'order-diesel-fuel.png', 'birêvebirinê', 'order-diesel-fuel.png', 'طلب الديزل', '', NULL, 0, 0, NULL, '0', '2019-10-19 05:59:29', '2019-10-19 05:59:29', NULL),
(6, 'order-fuel-barel.png', 'order fuel barel', 'order-fuel-barel.png', 'birêkirina bermîlê', 'order-fuel-barel.png', 'طلب باريل الوقود', '', NULL, 0, 0, NULL, '0', '2019-10-19 05:59:29', '2019-10-19 05:59:29', NULL),
(7, 'order-oil.png', 'order oil', 'order-oil.png', 'petrolê', 'order-oil.png', 'طلب النفط', '', NULL, 0, 0, NULL, '0', '2019-10-19 05:59:29', '2019-10-19 05:59:29', NULL),
(8, 'order-water.png', 'order water', 'order-water.png', 'avê', 'order-water.png', 'طلب الماء', '', NULL, 0, 0, NULL, '0', '2019-10-19 05:59:29', '2019-10-19 05:59:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `support_models`
--

CREATE TABLE `support_models` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `designation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_number_tmp` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `otp` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_token` text COLLATE utf8mb4_unicode_ci,
  `params` text COLLATE utf8mb4_unicode_ci,
  `status` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0->Unactive, 1->Active',
  `state` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0->Unactive, 1->Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_addresses`
--

CREATE TABLE `user_addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `governorate_id` int(10) UNSIGNED DEFAULT NULL,
  `city_id` int(10) UNSIGNED DEFAULT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `default` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0->Yes, 1->no',
  `params` text COLLATE utf8mb4_unicode_ci,
  `state` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0->Unactive, 1->Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_customers`
--

CREATE TABLE `user_customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_drivers`
--

CREATE TABLE `user_drivers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `parent_user_id` int(10) UNSIGNED NOT NULL,
  `vehicle_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `license_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` text COLLATE utf8mb4_unicode_ci,
  `state` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0->Unactive, 1->Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_suppliers`
--

CREATE TABLE `user_suppliers` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `vehicle_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `license_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `latitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `longitude` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `governorate_id` int(10) UNSIGNED DEFAULT NULL,
  `city_id` int(10) UNSIGNED DEFAULT NULL,
  `section_ids` int(11) DEFAULT NULL,
  `params` text COLLATE utf8mb4_unicode_ci,
  `state` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0->Unactive, 1->Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_supplier_favourites`
--

CREATE TABLE `user_supplier_favourites` (
  `id` int(10) UNSIGNED NOT NULL,
  `supplier_id` int(10) UNSIGNED NOT NULL,
  `customer_id` int(10) UNSIGNED NOT NULL,
  `params` text COLLATE utf8mb4_unicode_ci,
  `state` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0->Unactive, 1->Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_supplier_service_rates`
--

CREATE TABLE `user_supplier_service_rates` (
  `id` int(10) UNSIGNED NOT NULL,
  `supplier_id` int(10) UNSIGNED NOT NULL,
  `service_id` int(10) UNSIGNED NOT NULL,
  `rate_cards` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `params` text COLLATE utf8mb4_unicode_ci,
  `state` enum('0','1') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0' COMMENT '0->Unactive, 1->Active',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admin_users_email_unique` (`email`);

--
-- Indexes for table `configurations`
--
ALTER TABLE `configurations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gas_app_fees`
--
ALTER TABLE `gas_app_fees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gas_notifications`
--
ALTER TABLE `gas_notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gas_notifications_user_id_index` (`user_id`);

--
-- Indexes for table `how_to_models`
--
ALTER TABLE `how_to_models`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `measurement_units`
--
ALTER TABLE `measurement_units`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `measurement_units_units_unique` (`units`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_user_customer_id_index` (`user_customer_id`),
  ADD KEY `orders_user_customer_address_id_index` (`user_customer_address_id`),
  ADD KEY `orders_service_id_index` (`service_id`),
  ADD KEY `orders_user_supplier_id_index` (`user_supplier_id`);

--
-- Indexes for table `order_children`
--
ALTER TABLE `order_children`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_children_order_id_index` (`order_id`),
  ADD KEY `order_children_service_id_index` (`service_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `push_notifications`
--
ALTER TABLE `push_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registermodels`
--
ALTER TABLE `registermodels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `support_models`
--
ALTER TABLE `support_models`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `support_models_mobile_number_unique` (`mobile_number`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_mobile_number_unique` (`mobile_number`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_addresses`
--
ALTER TABLE `user_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_addresses_user_id_index` (`user_id`),
  ADD KEY `user_addresses_governorate_id_index` (`governorate_id`),
  ADD KEY `user_addresses_city_id_index` (`city_id`),
  ADD KEY `user_addresses_section_id_index` (`section_id`);

--
-- Indexes for table `user_customers`
--
ALTER TABLE `user_customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_drivers`
--
ALTER TABLE `user_drivers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_drivers_vehicle_number_unique` (`vehicle_number`),
  ADD UNIQUE KEY `user_drivers_license_number_unique` (`license_number`),
  ADD KEY `user_drivers_user_id_index` (`user_id`),
  ADD KEY `user_drivers_parent_user_id_index` (`parent_user_id`);

--
-- Indexes for table `user_suppliers`
--
ALTER TABLE `user_suppliers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_suppliers_user_id_foreign` (`user_id`);

--
-- Indexes for table `user_supplier_favourites`
--
ALTER TABLE `user_supplier_favourites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_supplier_favourites_supplier_id_index` (`supplier_id`),
  ADD KEY `user_supplier_favourites_customer_id_index` (`customer_id`);

--
-- Indexes for table `user_supplier_service_rates`
--
ALTER TABLE `user_supplier_service_rates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_supplier_service_rates_supplier_id_index` (`supplier_id`),
  ADD KEY `user_supplier_service_rates_service_id_index` (`service_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `configurations`
--
ALTER TABLE `configurations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `gas_app_fees`
--
ALTER TABLE `gas_app_fees`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `gas_notifications`
--
ALTER TABLE `gas_notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `how_to_models`
--
ALTER TABLE `how_to_models`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `measurement_units`
--
ALTER TABLE `measurement_units`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_children`
--
ALTER TABLE `order_children`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=213;
--
-- AUTO_INCREMENT for table `push_notifications`
--
ALTER TABLE `push_notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `registermodels`
--
ALTER TABLE `registermodels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `support_models`
--
ALTER TABLE `support_models`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_addresses`
--
ALTER TABLE `user_addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_customers`
--
ALTER TABLE `user_customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_drivers`
--
ALTER TABLE `user_drivers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_suppliers`
--
ALTER TABLE `user_suppliers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_supplier_favourites`
--
ALTER TABLE `user_supplier_favourites`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_supplier_service_rates`
--
ALTER TABLE `user_supplier_service_rates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `gas_notifications`
--
ALTER TABLE `gas_notifications`
  ADD CONSTRAINT `gas_notifications_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_user_customer_address_id_foreign` FOREIGN KEY (`user_customer_address_id`) REFERENCES `user_addresses` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_user_customer_id_foreign` FOREIGN KEY (`user_customer_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `orders_user_supplier_id_foreign` FOREIGN KEY (`user_supplier_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `order_children`
--
ALTER TABLE `order_children`
  ADD CONSTRAINT `order_children_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `order_children_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_addresses`
--
ALTER TABLE `user_addresses`
  ADD CONSTRAINT `user_addresses_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_addresses_governorate_id_foreign` FOREIGN KEY (`governorate_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_addresses_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `locations` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_addresses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_drivers`
--
ALTER TABLE `user_drivers`
  ADD CONSTRAINT `user_drivers_parent_user_id_foreign` FOREIGN KEY (`parent_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_drivers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_suppliers`
--
ALTER TABLE `user_suppliers`
  ADD CONSTRAINT `user_suppliers_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_supplier_favourites`
--
ALTER TABLE `user_supplier_favourites`
  ADD CONSTRAINT `user_supplier_favourites_customer_id_foreign` FOREIGN KEY (`customer_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_supplier_favourites_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `user_supplier_service_rates`
--
ALTER TABLE `user_supplier_service_rates`
  ADD CONSTRAINT `user_supplier_service_rates_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_supplier_service_rates_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
