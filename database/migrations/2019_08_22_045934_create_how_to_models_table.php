<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHowToModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('how_to_models', function (Blueprint $table) {
            $table->increments('id');
            $table->text('howto_customer_en');
            $table->text('howto_customer_ar');
            $table->text('howto_customer_kr');
            $table->text('howto_supplier_en');
            $table->text('howto_supplier_ar');
            $table->text('howto_supplier_kr');
            $table->text('ads_customer_en');
            $table->text('ads_customer_ar');
            $table->text('ads_customer_kr');
            $table->text('ads_supplier_en');
            $table->text('ads_supplier_ar');
            $table->text('ads_supplier_kr');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('how_to_models');
    }
}
