<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigurationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('configurations', function (Blueprint $table) {
            $table->increments('id');
            $table->text('tnc')->nullable();
            $table->text('tnc_kr')->nullable();
            $table->text('tnc_ar')->nullable();
            $table->text('howto')->nullable();
            $table->text('howto_kr')->nullable();
            $table->text('howto_ar')->nullable();
            \App\Helpers\DbExtender::defaultParams($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('configurations');
    }

}
