<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_customer_id')->unsigned()->index();
            $table->foreign('user_customer_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('user_customer_address_id')->unsigned()->index();
            $table->foreign('user_customer_address_id')->references('id')->on('user_addresses')->onDelete('cascade');
            $table->integer('service_id')->unsigned()->index();
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');
            $table->integer('is_child')->default(0);
//            $table->string('service_unit')->nullable();
            $table->string('service_quantity')->nullable();
            $table->float('service_price')->default(0)->nullable();
            $table->integer('user_supplier_id')->unsigned()->index();
            $table->foreign('user_supplier_id')->references('id')->on('users')->onDelete('cascade');
            $table->enum('request_status', ['not accepted', 'canceled', 'processing', 'completed', 'rejected'])->default('not accepted');
            $table->enum('review_customer', ['0', '1', '2', '3', '4', '5'])->default('0');
            $table->enum('review_supplier', ['0', '1', '2', '3', '4', '5'])->default('0');
            \App\Helpers\DbExtender::defaultParams($table);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('orders');
    }

}
