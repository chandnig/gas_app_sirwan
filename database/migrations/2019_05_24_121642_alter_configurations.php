<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterConfigurations extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::table('configurations', function (Blueprint $table) {

            $table->text('ads')->nullable()->after('howto_ar');
            $table->text('ads_kr')->nullable()->after('howto_ar');
            $table->text('ads_ar')->nullable()->after('howto_ar');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
