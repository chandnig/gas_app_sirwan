<?php

use Illuminate\Database\Seeder;

class servicesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('services')->insert([
            [
                'id' => '1',
                'image' => 'order-gas.png',
                'name' => 'order gas',
                'image_kr' => 'order-gas.png',
                'name_kr' => 'gazê',
                'image_ar' => 'order-gas.png',
                'name_ar' => 'طلب الغاز',
                'location' => '',
            ],
            [
                'id' => '2',
                'image' => 'order-fuel.png',
                'name' => 'order fuel',
                'image_kr' => 'order-fuel.png',
                'name_kr' => 'firotin',
                'image_ar' => 'order-fuel.png',
                'name_ar' => 'طلب الوقود',
                'location' => '',
            ],
            [
                'id' => '3',
                'image' => 'order-lpg.png',
                'name' => 'order lpg',
                'image_kr' => 'order-lpg.png',
                'name_kr' => 'lpg',
                'image_ar' => 'order-lpg.png',
                'name_ar' => 'أجل غاز البترول المسال',
                'location' => '',
            ],
            [
                'id' => '4',
                'image' => 'order-bottled-water.png',
                'name' => 'order bottled',
                'image_kr' => 'order-bottled-water.png',
                'name_kr' => 'şîrîn',
                'image_ar' => 'order-bottled-water.png',
                'name_ar' => 'المعبأة في زجاجات ترتيب ',
                'location' => '',
            ],
            [
                'id' => '5',
                'image' => 'order-diesel-fuel.png',
                'name' => 'order diesel',
                'image_kr' => 'order-diesel-fuel.png',
                'name_kr' => 'birêvebirinê',
                'image_ar' => 'order-diesel-fuel.png',
                'name_ar' => 'طلب الديزل',
                'location' => '',
            ],
            [
                'id' => '6',
                'image' => 'order-fuel-barel.png',
                'name' => 'order fuel barel',
                'image_kr' => 'order-fuel-barel.png',
                'name_kr' => 'birêkirina bermîlê',
                'image_ar' => 'order-fuel-barel.png',
                'name_ar' => 'طلب باريل الوقود',
                'location' => '',
            ],
            [
                'id' => '7',
                'image' => 'order-oil.png',
                'name' => 'order oil',
                'image_kr' => 'order-oil.png',
                'name_kr' => 'petrolê',
                'image_ar' => 'order-oil.png',
                'name_ar' => 'طلب النفط',
                'location' => '',
            ],
            [
                'id' => '8',
                'image' => 'order-water.png',
                'name' => 'order water',
                'image_kr' => 'order-water.png',
                'name_kr' => 'avê',
                'image_ar' => 'order-water.png',
                'name_ar' => 'طلب الماء',
                'location' => '',
        ]]);
    }

}
