<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermistionTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');
        $collection = \Route::getRoutes();
        $routes = [];

        foreach (\App\Helpers\GasApplication::getUserRoles() as $id => $roleName):
            try {
                $roleUi = Role::where('name', $roleName)->first();
                if ($roleUi == null):
                    if ((strpos($roleName, 'admin') !== false))
                        $guard_name = 'web-admin';
                    else
                        $guard_name = 'api';
                    Role::create(['id' => $id, 'name' => $roleName, 'guard_name' => $guard_name]);
                endif;
            } catch (\Exception $ex) {
                echo 'There is already role ' . $roleName ;
            }
        endforeach;
        foreach ($collection as $route) {
            if ((strpos($route->uri(), 'laravel-filemanager') !== false))
                continue;
//            if ($route->uri() == null)
//                continue;

            if ((strpos($route->uri(), 'admin') !== false))
                $guard_name = 'web-admin';
            else
                $guard_name = 'api';

            try {
                Permission::create(['name' => $route->uri(), 'guard_name' => $guard_name]);
                $roleU = Role::where('name', \App\Helpers\GasApplication::getUserRoles('1'))->first();
                $roleU->givePermissionTo($route->uri());
                $roleU = Role::where('name', \App\Helpers\GasApplication::getUserRoles('4'))->first();
                $roleU->givePermissionTo($route->uri());
                $roleU = Role::where('name', \App\Helpers\GasApplication::getUserRoles('3'))->first();
                $roleU->givePermissionTo($route->uri());
                $routes[] = $route->uri();
                echo '<br> Permistion granted to ' . $route->uri() . '<br> ';
            } catch (\Exception $ex) {
                echo '<br>' . $ex->getMessage();
                continue;
            }
        }
    }

}
