@extends('layouts.admin-login')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="login-card card-block">
                @include('includes.flash_msg')
                <form class="form-horizontal md-float-material" role="form" method="POST" action="{{ route('admin.auth') }}">
                    {!! csrf_field() !!}
                    <h3 class="text-center txt-primary">
                        Sign In to your account
                    </h3>
                    <div class="md-input-wrapper form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <!--<label>E-Mail</label>-->
                        <input type="email" placeholder="E-Mail" class="md-form-control" name="email" value="{{ old('email') }}">
                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="md-input-wrapper form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <!--                        <label>Password</label>-->
                        <input type="password" placeholder="Password" class="md-form-control" name="password">
                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-xs-10 offset-xs-1">
                            <button type="submit" class="btn btn-primary btn-md btn-block waves-effect text-center m-b-20">LOGIN</button>
                        </div>
                    </div>
                    <!--a class="text-danger" href="{{route('admin.reset')}}">Forget Password ?</a-->
                </form>
            </div>
        </div>
    </div>
</div>
@stop
