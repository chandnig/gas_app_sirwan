@extends('layouts.admin-data')
@section('content')
<div class="content-wrapper">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <!-- Row Starts -->
        <div class="row">
            <div class="col-sm-9 p-0">
                <div class="main-header">
                    <h4>{{$title}}</h4>
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li  class="breadcrumb-item">
                            <i class="icon-home"></i>
                            <a href="{{url('/admin')}}">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li class="breadcrumb-item">
                            <i class="icon-users"></i>
                            <span> {{$title}}</span>
                        </li>
                        <li class="breadcrumb-item">
                            <i class="icon-users"></i>
                            <span> {{$type}}</span>
                        </li>
                    </ol>
                </div>
            </div>
            <div class="col-sm-3" style="margin-top:2%">
                    <a href="how_to"><button class="btn btn-secondary">Customer</button></a>
                    <button class="btn btn-primary">Supplier</button>
            </div>

            <div class="col-sm-12">
                    <div class="card">
                        <div class="portlet-body card-block">
                            <form role="form" id="frmDataEdit" action="{{$action}}">
                                {{csrf_field()}}
                                <div class="row">
                                    <!-- start col-lg-9 -->
                                    <div class="col-xl-12 col-lg-8">
                                        <!-- Nav tabs -->
                                        <div class="tab-header">
                                            <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab" href="#english" role="tab">English</a>
                                                    <div class="slide"></div>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#kurdish" role="tab">Kurdish</a>
                                                    <div class="slide"></div>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#arabic" role="tab">Arabic</a>
                                                    <div class="slide"></div>
                                                </li>
                                            </ul>
                                        </div>
                                        <!-- end of tab-header -->
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="english" role="tabpanel">
                                                <div class="form-group">
                                                    <label for="howto">How To </label>
                                                    <textarea id="howto_supplier_en" class="form-control my-editor" rows="5" name='howto_supplier_en'><?= isset($data->howto_supplier_en) ? $data->howto_supplier_en : '' ?></textarea>
                                                    <p class=" text-danger hidden"></p>
                                                </div>
                                            </div>
                                            <div class="tab-pane" id="kurdish" role="tabpanel">
                                                <div class="form-group">
                                                    <label for="howto_supplier_kr">How To kr</label>
                                                    <textarea id="howto_supplier_kr"class="form-control my-editor" rows="5" name='howto_supplier_kr'><?= isset($data->howto_supplier_kr) ? $data->howto_supplier_kr : '' ?></textarea>
                                                      <p class=" text-danger hidden"></p>
                                                </div>

                                            </div>

                                            <div class="tab-pane" id="arabic" role="tabpanel">
                                                <div class="form-group">
                                                    <label for="howto_ar">How To ar</label>
                                                    <textarea id="howto_supplier_ar"class="form-control my-editor" rows="5"name='howto_supplier_ar'><?= isset($data->howto_supplier_ar) ? $data->howto_supplier_ar : '' ?></textarea>
                                                      <p class=" text-danger hidden"></p>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <br>
                                <br>
                                <div class="bt">
                                    <button type="button" class="btn btn-primary" id="btnUpdate"><i class="glyphicon glyphicon-save"></i>&nbsp;Save</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>
        </div>
    </div>
</div>
<script type="text/javascript" charset="utf-8" async defer>
    $('#btnUpdate').on('click', function (e) {
    e.preventDefault();
    tinyMCE.triggerSave();
    var url = $('#frmDataEdit').attr('action');
    var frm = $('#frmDataEdit');
    var type = $('#id').val() == '' ? 'POST' : 'PUT';
    // console.log(frm.serialize());
    $.ajax({
    type: type,
    url: url,
    dataType: 'json',
    data: frm.serialize(),
    success: function (data) {

    //console.log(data);
    $('#frmDataEdit .text-danger').each(function () {
    $(this).addClass('hidden');
    });
    if (data.errors) {
    $.each(data.errors, function (index, value) {
    $('#frmDataEdit #' + index).parent().find('.text-danger').text(value);
    $('#frmDataEdit #' + index).parent().find('.text-danger').removeClass('hidden');
    });
    }
    if (data.success == true) {

    swal({title: "Success!", text: "{{$actionMessage}}", type: "success"},

    function () {
    //                                $('#cancel')[0].click();
    }
    );
    }
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
    console.log(jqXHR);
    console.log(textStatus);
    console.log(errorThrown);
    alert('Please Reload to read Ajax');
    }
    });
    });





 tinymce.init({
  selector: 'textarea.form-control',
  menubar: false,
 plugins: [
    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
    "searchreplace wordcount visualblocks visualchars code fullscreen",
    "insertdatetime media nonbreaking save table contextmenu directionality",
    "emoticons template paste textcolor colorpicker textpattern image code"
    ],
  toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
  // enable title field in the Image dialog
  image_title: true,
  // enable automatic uploads of images represented by blob or data URIs
  automatic_uploads: true,
  // add custom filepicker only to Image dialog
  file_picker_types: 'image',
  file_picker_callback: function(cb, value, meta) {
    var input = document.createElement('input');
    input.setAttribute('type', 'file');
    input.setAttribute('accept', 'image/*');

    input.onchange = function() {
      var file = this.files[0];
      var reader = new FileReader();

      reader.onload = function () {
        var id = 'blobid' + (new Date()).getTime();
        var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
        var base64 = reader.result.split(',')[1];
        var blobInfo = blobCache.create(id, file, base64);
        blobCache.add(blobInfo);

        // call the callback and populate the Title field with the file name
        cb(blobInfo.blobUri(), { title: file.name });
      };
      reader.readAsDataURL(file);
    };

    input.click();
  }
});
</script>
@endsection





