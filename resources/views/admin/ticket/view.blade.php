@extends('layouts.admin-data')
@section('content')
<div class="content-wrapper">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <!-- Row Starts -->
        <div class="row">
            <div class="col-sm-12 p-0">
                <div class="main-header">
                    <h4>Ticket List</h4>
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li class="breadcrumb-item">
                            <a href="<?= url(config('app.admin_prefix') . '/dashboard') ?>">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="<?= url(config('app.admin_prefix') . '/orders') ?>">Ticket List</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">List</a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <!--<div class="card-header"><h5 class="card-header-text">Simple Table</h5></div>-->
                    <div class="card-block">
                        <div class="data_table_main">
                            <!-- <br>
                            <a href="{{ URL::to('downloadExcel_orders/xlsx')}}" class="btn btn-danger pull-sm-right" id="btnd" style="margin: -17px 0 0 0;">download</a>
                            <br>
                            <div class="row">
                                &nbsp;&nbsp;
                                <select type="text" class="col-md btn btn-outline-primary" placeholder="Governorate" name="service_type" id="service_type">
                                   <option value="" >Select Service Type</option>
                                   @foreach($service_data as $service)
                                   <option value="{{ $service->id }}">{{ $service->name }}</option>
                                   @endforeach
                               </select>&nbsp;&nbsp;

                               <input type="text" id="start_date" class="btn  btn-outline-primary col-md" placeholder="From Date">
                               <input type="text" id="end_date" class="btn  btn-outline-primary col-md" placeholder='To Date'>&nbsp;


                               <select type="text" class="col-md btn btn-outline-primary" id="request_status">
                                    <option value="">Select Request Status</option>
                                    <option value="Fraud" >Fraud</option>
                                    <option value="Cancelled">Cancelled</option>
                                    <option value="Deleted By GasApp">Deleted By GasApp</option>
                                    <option value="Rejected">Rejected</option>
                                    <option value="Processing">Processing</option>
                                    <option value="Closed">Closed</option>
                               </select>&nbsp;&nbsp;
                               <button class="btn btn-success btn-sm" id="filterOrder" onclick=filterOrder()>Filter</button>
                               <a href="{{ orders }}"><i class="icon-refresh text-primary bold " aria-hidden="true"></i></a>
                            </div>
                           <br>
                           <br>
                            <br> -->
                        <table class="table dt-responsive table-striped table-bordered nowrap table-responsive tblData" id="tblData" cellspacing="0" width="100%">
                            {{csrf_field()}}
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <?php foreach ($fields as $field): ?>
                                        <th>{{$field}}</th>
                                    <?php endforeach; ?>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('admin.commonForm.notification',['way' => 'Per Customer'])
@endsection
@section('scripts')
<script type="text/javascript" charset="utf-8" async defer>

//ajax header need for deleted and updating data

    var table;
//datatables serverSide
    $('document').ready(function () {


        $("#start_date").datepicker({
            format: 'yyyy-mm-dd'

        });
         $("#end_date").datepicker({
            format: 'yyyy-mm-dd'

        });

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        table = $('#tblData').DataTable({
                stateSave: true,
                responsive: true,
                processing: true,
                serverSide: true,
                order: [0, 'desc'],
                ajax: window.location.href,
                columns: [
                {data: 'id', name: 'id'},
                <?php foreach ($fields as $field1): ?>
                                    {data: "{{$field1}}", name: "{{$field1}}"},
                <?php endforeach; ?>
                {data: 'action', name: 'action', orderable: false, searchable: true}
                ], "drawCallback": function (settings) {
        $('.change-state').bootstrapToggle();
        }
    });
    // table.draw(false);


//deleting data
    $('#tblData').on('click', '.btnDelete[data-remove]', function (e) {
        e.preventDefault();
        var url = $(this).data('remove');
        swal({
            title: "Are you sure want to remove this item?",
            text: "Data will be Temporary Deleted!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Confirm",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false,
        },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: url,
                            type: 'DELETE',
                            dataType: 'json',
                            data: {method: '_DELETE', submit: true},
                            success: function (data) {
                                if (data == 'Success') {
                                    swal("Deleted!", "Category has been deleted", "success");
                                    table.ajax.reload(null, false);
                                }
                            }
                        });
                    } else {

                        swal("Cancelled", "You Cancelled", "error");
                    }

                });
    });

    $('#tblData').on('change', '.change-state', function (e) {
    e.preventDefault();
            var url = "<?= url(config('app.admin_prefix') . '/user/changestate/') ?>" + '/' + $(this).attr('data-id');
            var checked = $('#change-state-' + $(this).attr('data-id')).prop('checked');
            swal({
            title: "Are you sure want to change Vote status",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonClass: "btn-info",
                    confirmButtonText: "Confirm",
                    cancelButtonText: "Cancel",
                    closeOnConfirm: true,
                    closeOnCancel: true
            },
                    function (isConfirm) {
                    if (isConfirm) {
                    $.ajax({
                    url: url,
                            type: 'GET',
                            datatype: 'json',
                            data: {checked: checked},
                            success: function (data) {
                            console.log(data);
                                    table.ajax.reload(null, false);
                            }

                    });
                    }
                    });
            });
    });

    function filterOrder()
    {

        if ($.fn.DataTable.isDataTable("#tblData")) {
            $('#tblData').DataTable().clear().destroy();
          }

        var service_type      =   $("#service_type").val();
        var start_date        =   $("#start_date").val();
        var end_date          =   $("#end_date").val();
        var request_status    =   $("#request_status").val();


        table = $('#tblData').DataTable({
                    stateSave: true,
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    retrieve: true,
                    order: [0, 'desc'],
                    ajax: {
                    url: "filterOrder",
                    type: 'post',
                    data: function (d) {
                        d.start_date = $('#start_date').val();
                        d.end_date = $('#end_date').val();
                        d.service_type = $('#service_type').val();
                        d.request_status = $('#request_status').val();
                    }
                },
                    columns: [
                        {data: 'id', name: 'id'},
                        <?php foreach ($fields as $filter_field): ?>
                                            {data: "{{$filter_field}}", name: "{{$filter_field}}"},
                        <?php endforeach; ?>
                        {data: 'action', name: 'action', orderable: false, searchable: true}
                        ],"drawCallback": function (settings) {
                            $('.change-state').bootstrapToggle();
                    }
        });
    }

    $('#filterOrder').click(function(){
        $('#tblData').DataTable().draw(true);
    });



</script>
@endsection

