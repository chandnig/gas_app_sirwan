@extends('layouts.admin-data')
@section('content')
<div class="content-wrapper">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <!-- Row Starts -->
        <div class="row">
            <div class="col-sm-12 p-0">
                <div class="main-header">
                    <h4>{{$title}}</h4>
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li  class="breadcrumb-item">
                            <i class="icon-home"></i>
                            <a href="{{url('/admin')}}">Home</a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li class="breadcrumb-item">
                            <i class="icon-users"></i>
                            <a class="ajaxify" href="{{$listUrl}}">List {{$title}}</a>
                        </li>
                        <li class="breadcrumb-item">
                            <i class="icon-users"></i>
                            <span>Edit {{$title}}</span>
                        </li>
                    </ol>
                </div>
            </div>
              @if(!empty($errors->first()))
            <div class="col-lg-12">
                <div class="alert alert-danger">
                    <span>{{ $errors->first() }}</span>
                </div>
            </div>
            @endif

            <div class="col-sm-12">
                <div class="card" id="dropDown_View">

                    <div class="portlet-body card-block">
                            <button class="btn btn-danger" style="float:right" onclick=searchByPhone() >Search By Phone Number</button>
                            <br/>
                            <label for"supplier forward">Select Supplier to Assign Order</label>
                            <br/>
                            <input type="hidden" id ="order_id" name="order_id" value="{{ $order_id }}" />

                            <select class="form-control" name="user_supplier_id" id="user_supplier_id" class="user_supplier_id">
                                <option>Select Supplier</option>
                                @foreach ($data as $item)
                                  <option value="{{ $item->id }}"  > {{ $item->name }} {{ $item->last_name }}  </option>
                                @endforeach
                            </select>
                            <br>
                            <br>
                            <div class="bt">
                                <a href="{{$listUrl}}" class="ajaxify btn btn-default" id="cancel">Cancel</a>
                                <button  class="btn btn-primary"  onclick=saveForward() id="btnUpdate"><i class="glyphicon glyphicon-save"></i>&nbsp;Save</button>
                            </div>
                    </div>
                </div>
                <div class="card" id="phoneNo_View" style="display:none">
                    <div class="portlet-body card-block">
                            <button class="btn btn-danger" style="float:right" onclick=availableSupplier() >Get Available Supplier</button>
                            <br/>
                            <label for"supplier forward">Search Supplier By Phone No</label>
                            <br/>
                            <input type="hidden" id ="order_id" name="order_id" value="{{ $order_id }}" />
                            <input type="text" class="form-control" placeholder="Enter Supplier's phone no" id="phone_no" />
                            <br/><br/>
                            <div class="" id="supplier_data">

                            </div>
                            <br>
                            <br>
                            <div class="bt">
                                <a href="{{$listUrl}}" class="ajaxify btn btn-default" id="cancel">Cancel</a>
                                <button  class="btn btn-primary"  onclick=saveForward_Phone() id="btnUpdate"><i class="glyphicon glyphicon-save"></i>&nbsp;Save</button>
                            </div>
                    </div>
                </div>


                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" charset="utf-8" async defer>

    //ajax header need for deleted and updating data

        var table;
    //datatables serverSide
        $('document').ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            table = $('#tblData').DataTable({
            stateSave: true,
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    order: [0, 'desc'],
                    ajax: window.location.href,
                    columns: [
                    {data: 'id', name: 'id'},
                    <?php foreach ($fields as $field): ?>
                                        {data: "{{$field}}", name: "{{$field}}"},
                    <?php endforeach; ?>
                                    {data: 'action', name: 'action', orderable: false, searchable: false}
                                    ], "drawCallback": function (settings) {
                            $('.change-state').bootstrapToggle();
                            }
        });

    });

    function saveForward(){
        var order_id = $("#order_id").val();
        var user_supplier_id = $("#user_supplier_id").val();

       $.post("/gasApp/admin/update_order_supplier/"+order_id,{user_supplier_id:user_supplier_id},function(data){
        if (data.success == true) {
            swal({title: "Success!", text: "Order Forwarded Successfully", type: "success"},
                    function () {
                     window.location.href='/gasApp/admin/orders';
                    }
            );
        }else{
            swal({title: "Warning!", text: "Oops! Something went wrong ", type: "warning"},
            function () {
              location.reload(true);
            }
        );
        }

        });
    }
    function saveForward_Phone(){
        var order_id = $("#order_id").val();
        var user_supplier_id = $("#supplier_id").val();

       $.post("/gasApp/admin/update_order_supplier/"+order_id,{user_supplier_id:user_supplier_id},function(data){
        if (data.success == true) {
            swal({title: "Success!", text: "Order Forwarded Successfully", type: "success"},
                    function () {
                     window.location.href='/gasApp/admin/orders';
                    }
            );
        }else{
            swal({title: "Warning!", text: "Oops! Something went wrong ", type: "warning"},
            function () {
              location.reload(true);
            }
        );
        }

        });
    }

    function searchByPhone()
    {
        $("#dropDown_View").hide();
        $("#phoneNo_View").show();

        $("#phone_no").keyup(function() {
           var phone_no = $("#phone_no").val();
            $.post("/gasApp/api/supplier/searchSupplierByNumber",{phone_no:phone_no},function(data){
                if (data.success == true) {
                    var supplier = data.data;
                    $("#supplier_data").html("<div>"+supplier.name+" "+supplier.last_name+"</div><input type='hidden' id='supplier_id'  value="+supplier.id+" >");
                }else{
                    $("#supplier_data").html("No Supplier Found");
                }

                });

          });

    }
    function availableSupplier()
    {
        $("#dropDown_View").show();
        $("#phoneNo_View").hide();
    }


    </script>
@endsection
