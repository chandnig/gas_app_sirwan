@extends('layouts.admin-data')
@section('content')
<div class="content-wrapper">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <!-- Row Starts -->
        <div class="row">
            <div class="col-sm-12 p-0">
                <div class="main-header">
                    <h4>{{$title}}</h4>
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li class="breadcrumb-item">
                            <a href="<?= url(config('app.admin_prefix') . '/dashboard') ?>">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="<?= url(config('app.admin_prefix') . '/customer') ?>">{{$title}}</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">List</a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <!--<div class="card-header"><h5 class="card-header-text">Simple Table</h5></div>-->
                    <div class="card-block">
                        <div class="data_table_main">
                            <a href="<?= $createUrl ?>" class="btn btn-primary" id="btnAdd">Add New</a>
                            <br>

                            {!! Form::open(array('route' => 'import-supplier-csv-excel','method'=>'POST','files'=>'true')) !!}
                            <div class="pull-lg-right" style="margin: -40px 0 0 0;">
                                {!! Form::file('sample_file', array('class' => '','required')) !!}
                                {!! $errors->first('sample_file', '<p class="alert alert-danger">:message</p>') !!}
                                {!! Form::submit('Import Supplier',['class'=>'btn btn-primary']) !!}
                            </div>
                        </div>
                        {!! Form::close() !!}
                        <br>
                             <a href="{{ URL::to('downloadExcel_suppliers/xlsx')}}"  class="btn btn-danger pull-sm-right" id="btnd" style="margin: -17px 0 0 0;">download</a>
                            <br>
                            <div class="row">
                                    &nbsp;&nbsp;<select type="text" class="col-md btn btn-outline-primary" placeholder="Governorate" name="governorate_id" id="governorate_id">
                                       <option value="" >Select Governorate</option>
                                       <option value="" >All</option>
                                       @foreach($location_data as $governorate_id)
                                       <option value="{{ $governorate_id->id }}">{{ $governorate_id->name }}</option>
                                       @endforeach
                                   </select>&nbsp;&nbsp;

                                   <select type="text" class="col-md btn btn-outline-primary"  id="city_id">
                                       <option value="" city_id>Select City</option>

                                   </select>&nbsp;&nbsp;
                                   <select type="text" class="col-md btn btn-outline-primary " id="service_id">
                                       <option value="">Select Supplier Of</option>
                                       <option value="">All</option>
                                       @foreach($services as $service)
                                       <option value="{{ $service->id }}">{{ $service->name }}</option>
                                       @endforeach
                                   </select>&nbsp;&nbsp;
                                   <select type="text" class="col-md btn btn-outline-primary " id="service_status">
                                       <option value="">Select Supplier Status</option>
                                       <option value="">All</option>
                                       <option value="1">ON</option>
                                       <option value="0">OFF</option>
                                   </select>&nbsp;&nbsp;
                                   <button class="btn btn-success btn-sm" id="filterSupplier" onclick=filterSupplier()>Filter</button>&nbsp;
                                   <a href="{{ suppliers }}"><i class="icon-refresh text-primary bold " aria-hidden="true"></i></a>
                               </div>

                               <br>
                               <br>
                            <br>
                            <table class="table dt-responsive table-responsive table-striped table-bordered nowrap" id="tblData" cellspacing="0" width="100%">
                                {{csrf_field()}}
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <?php foreach ($fields as $field): ?>
                                            <th>{{$field}}</th>
                                        <?php endforeach; ?>
                                        <th>Action
                                            <button type="button" name="check" id="checkid_suppliers"  style="margin-left:250px" class="btn btn-danger btn-xs ml-5"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                            <span><input type="checkbox" id="selectall_suppliers">&nbsp;select all</span>
                                        </th>
                                        <th>
                                            <button type="button" name="check_notify" id="notify_suppliers" class="btn btn-danger btn-xs ml-5">Notify</button>
                                            <span><input type="checkbox" id="selectall_notify">&nbsp;select all</span>
                                        </th>
                                        <th>
                                            <button type="button" name="check_fav" id="fav_suppliers" class="btn btn-danger btn-xs ml-5">Favourit Supplier</button>
                                            <span><input type="checkbox" id="selectall_fav">&nbsp;select all</span>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" tabindex="-1" role="dialog" id="notificationModel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Notification send To Suppliers</h4>
                </div>
                <div class="modal-body">
                    <form role="form" id="frmDataEdit">
                        <label>Body *</label>
                        <select class="form-control">
                            <option>Text</option>
                            <option>Question</option>
                        </select>
                        <br/>
                        <label> Title *</label>
                        <input type"text" class="form-control">
                        <br/>
                        <label>Body *</label>
                        <input type"text" class="form-control">
                        <br/>
                        <br/>
                    </form>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="sendNotify"><i class="glyphicon glyphicon-save"></i>&nbsp;Send</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@include('admin.commonForm.notification',['way' => 'Per Supplier'])
@endsection
@section('scripts')

<script type="text/javascript" charset="utf-8" async defer>

//ajax header need for deleted and updating data

    var tables;
//datatables serverSide
    $('document').ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        table = $('#tblData').DataTable({
        stateSave: true,
                responsive: true,
                processing: true,
                serverSide: true,
                order: [0, 'desc'],
                ajax: window.location.href,
                columns: [
                {data: 'id', name: 'id'},
                <?php foreach ($fields as $field): ?>
                                    {data: "{{$field}}", name: "{{$field}}"},
                <?php endforeach; ?>
                {data: 'action', name: 'action', orderable: false, searchable: false},
                {data: 'notify_checkbox', name: 'notify_checkbox', orderable: false, searchable: false},
                {data: 'checkbox', name: 'checkbox', orderable: false, searchable: false},

                ], "drawCallback": function (settings) {
        $('.change-state').bootstrapToggle();
        }
    });
    // table.draw(false);

//deleting data
    $('#tblData').on('click', '.btnDelete[data-remove]', function (e) {
        e.preventDefault();
        var url = $(this).data('remove');
        swal({
            title: "Are you sure want to remove this item?",
            text: "Data will be Temporary Deleted!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Confirm",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false,
        },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: url,
                            type: 'DELETE',
                            dataType: 'json',
                            data: {method: '_DELETE', submit: true},
                            success: function (data) {
                                if (data == 'Success') {
                                    swal("Deleted!", "Category has been deleted", "success");
                                    table.ajax.reload(null, false);
                                }
                            }
                        });
                    } else {

                        swal("Cancelled", "You Cancelled", "error");
                    }

                });
    });

    $('#tblData').on('change', '.change-state', function (e) {
    e.preventDefault();
            var url = "<?= url(config('app.admin_prefix') . '/user/changestate/') ?>" + '/' + $(this).attr('data-id');
            var checked = $('#change-state-' + $(this).attr('data-id')).prop('checked');

            swal({
            title: "Are you sure want to change Vote status",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonClass: "btn-info",
                    confirmButtonText: "Confirm",
                    cancelButtonText: "Cancel",
                    closeOnConfirm: true,
                    closeOnCancel: true
            },
                    function (isConfirm) {
                    if (isConfirm) {
                    $.ajax({
                    url: url,
                            type: 'GET',
                            datatype: 'json',
                            data: {checked: checked},
                            success: function (data) {
                            console.log(data);
                                    table.ajax.reload(null, false);
                            }
                    });
                    }
                    });
    });

    $('#checkid_suppliers').on('click', function (e) {
        e.preventDefault();

        var id = [];
        $.each($(".delete-suppliers:checkbox:checked"), function () {
            id.push($(this).attr('id'));
        });
        if (id.length == 0) {
            alert('please select one row ');
            return;
        }
        var url = "<?= url(config('app.admin_prefix') . '/multipledestroy_customers') ?>";
        var checked = id;
        //alert($('#checkid' + $(this).attr('id')).prop('checked'));
        swal({
            title: "Are you sure want to change status",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-info",
            confirmButtonText: "Confirm",
            cancelButtonText: "Cancel",
            closeOnConfirm: true,
            closeOnCancel: true
        },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: url,
                            type: 'POST',
                            datatype: 'json',
                            data: {checked: checked},
                            success: function (data) {
                                console.log(data);
                                table.ajax.reload(null, false);
                            }

                        });
                    }
                });
    });


    $("#selectall_suppliers").click(function(){
        $('.delete-suppliers').not(this).prop('checked', this.checked);
    });

//=================================SET AS FAVOURITE==================================//
    $('#fav_suppliers').on('click', function (e) {
        e.preventDefault();
        var id = [];
        $.each($(".set-favsupplier:checkbox:checked"), function () {
            id.push($(this).attr('id'));
        });
        if (id.length == 0) {
            alert('Please Select One Row ');
            return;
        }
        var url = "<?= url(config('app.admin_prefix') . '/setGasAppFav_Supplier') ?>";
        var checked = id;
        swal({
            title: "Are you sure want to set Supplier(s) as Favourite",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-info",
            confirmButtonText: "Confirm",
            cancelButtonText: "Cancel",
            closeOnConfirm: true,
            closeOnCancel: true
        },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: url,
                            type: 'POST',
                            datatype: 'json',
                            data: {checked: checked},
                            success: function (data) {
                                console.log(data);
                                table.ajax.reload(null, false);
                            }

                        });
                    }
                });
    });

    $("#selectall_fav").click(function(){
        $('.set-favsupplier').not(this).prop('checked', this.checked);
    });


//=================================SEND NOTIFICATION TO MULTIPLE STARTS HERE==================================//
    $('#notify_suppliers').on('click', function (e) {
        e.preventDefault();
        var id = [];
        $.each($(".send_notification:checkbox:checked"), function () {
            id.push($(this).attr('id'));
        });
        if (id.length == 0) {
            alert('Please Select One Row ');
            return;
        }
        $('#notificationModel').modal('show');


    });

    $("#selectall_notify").click(function(){
        $('.send_notification').not(this).prop('checked', this.checked);
    });


    $('select[name=governorate_id]').change(function(){
        var locationId = $(this).val();
        if(locationId){
            var options = "<option value='' >Select City</option>  <option value=''>All</option>";
              $.post("/api/location/getCity",{locale:"en",location_id:locationId},function(data){
                var status = data.success;
                if(status)

                if(status){
                    var obj = data.data;
                    if(obj.length>0)
                    {
                        for(var k=0;k< obj.length;k++)
                        {
                            if(locationId == obj[k].id)
                            {
                            options = options + '<option value="'+obj[k].id+'" selected> '+obj[k].name+'</option>';
                            }
                            else
                            {
                            options = options + '<option value="'+obj[k].id+'"> '+obj[k].name+'</option>';
                            }
                        }
                    }
                  else{
                    options = options+"<option value=''>No City Found</option>";
                  }
                }
                else{
                    options = options+"<option value=''>No City Found</option>";
                }
                $("#city_id").html(options);
            })

          }

       });

       $(function () {
        $('.color').minicolors();
    });
    });

    function filterSupplier()
    {
        if ($.fn.DataTable.isDataTable("#tblData")) {
            $('#tblData').DataTable().clear().destroy();
          }

        table = $('#tblData').DataTable({
            stateSave: true,
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    order: [0, 'desc'],
                    ajax: {
                        url: "filterSupplier",
                        type: 'post',
                        data: function (d) {
                            d.governorate_id   =   $("#governorate_id").val();
                            d.city_id          =   $("#city_id").val();
                            d.service_id       =   $("#service_id").val();
                            d.service_status   =   $("#service_status").val();
                        }
                    },
                    columns: [
                    {data: 'id', name: 'id'},
                    <?php foreach ($fields as $filter_field): ?>
                                        {data: "{{$filter_field}}", name: "{{$filter_field}}"},
                    <?php endforeach; ?>
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    {data: 'notify_checkbox', name: 'notify_checkbox', orderable: false, searchable: false},
                    {data: 'checkbox', name: 'checkbox', orderable: false, searchable: false},
                    ], "drawCallback": function (settings) {
            $('.change-state').bootstrapToggle();
            }
        });
    }

    $('#filterSupplier').click(function(){
        $('#tblData').DataTable().draw(true);
    });


</script>
@endsection
