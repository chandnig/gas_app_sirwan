@extends('layouts.admin-data')
@section('content')
<?php $a='1'?>
<?php $index=1?>
<div class="content-wrapper">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>{{$title}}</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li  class="breadcrumb-item">
                        <i class="icon-home"></i>
                        <a href="{{url('/admin')}}">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li class="breadcrumb-item">
                        <i class="icon-users"></i>
                        <a class="ajaxify" href="{{$listUrl}}">List {{$title}}</a>
                    </li>
                    <li class="breadcrumb-item">
                        <i class="icon-users"></i>
                        <span>View {{$title}}</span>
                    </li>
                </ol>
            </div>
        </div>
        <!-- Header end -->
        <div class="row">
            <!-- start col-lg-9 -->
            <div class="col-xl-12 col-sm-12 col-lg-8">
                <!-- Nav tabs -->
                <div class="tab-header">
                    <ul class="nav nav-tabs md-tabs tab-timeline" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#personal" role="tab">Personal Info</a>
                            <div class="slide"></div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " data-toggle="tab" href="#location_vehicle" role="tab">Location&Vehicle</a>
                            <div class="slide"></div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " data-toggle="tab" href="#products" role="tab">Products</a>
                            <div class="slide"></div>
                        </li>
                        @if($data->user_type != 'driver')
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#project" role="tab">Drivers</a>
                            <div class="slide"></div>
                        </li>
                        @endif
                    </ul>
                </div>
                <!-- end of tab-header -->
                <div class="tab-content">
                    <div class="tab-pane active" id="personal" role="tabpanel">
                        <div class="card">
                            <div class="card-header"><h5 class="card-header-text">About Supplier</h5>
                                <a id="edit-btn" href="{{ $editUrl}}" type="button" class="btn btn-primary waves-effect waves-light f-right" >
                                    <i  class="icofont icofont-edit"></i>
                                </a>
                            </div>
                            <div class="card-block">
                                <div class="view-info">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="general-info">
                                                <div class="row">
                                                    <div class="col-lg-12 col-xl-6">
                                                        <table class="table m-0">
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row">Name</th>
                                                                    <td>{{$data->name}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Email</th>
                                                                    <td>{{$data->email}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Mobile Number</th>
                                                                    <td>{{$data->mobile_number}}</td>
                                                                </tr>
                                                                {{--  <tr>
                                                                    <th scope="row">User Role</th>
                                                                    <td>{{implode(',',$data->getRoleNames()->toArray())}}</td>
                                                                </tr>  --}}
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- end of row -->
                                            </div>
                                            <!-- end of general info -->
                                        </div>
                                        <!-- end of col-lg-12 -->
                                    </div>
                                    <!-- end of row -->
                                </div>
                            </div>
                            <!-- end of card-block -->
                        </div>
                        <!-- end of card-->
                        <!-- end of row -->
                    </div>
                    <div class="tab-pane " id="location_vehicle" role="tabpanel">
                        <div class="card">
                            <div class="card-header"><h5 class="card-header-text">About Supplier</h5>

                                @if(count($supplier_location)>0)
                                <a id="edit-btn" href="{{ $editUrl_location}}" type="button" class="btn btn-primary waves-effect waves-light f-right" >
                                    EDIT Information
                                 </a>
                                @else
                                    <a type="button" href="{{ route('supplier_location.create') }}" class="btn btn-primary waves-effect waves-light f-right">
                                        + ADD Information </a>
                                @endif;

                            </div>
                            <div class="card-block">
                                <div class="view-info">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="general-info">
                                                <div class="row">
                                                    <div class="col-lg-12 col-xl-6">
                                                        <table class="table m-0">
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row">Governorate</th>
                                                                    <td>{{ $supplier_location[0]->governorate_name }}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">City</th>
                                                                    <td>{{ $supplier_location[0]->city_name}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Section</th>
                                                                    <td>{{ $supplier_location[0]->section_name}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Vehicle Number</th>
                                                                    <td>{{ $supplier_location[0]->vehicle_number}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Vehicle Number Image</th>
                                                                    <td>
                                                                        @if($supplier_location[0]->vehicle_image!="")
                                                                            <a href="{{  url('/').'/uploads/supplier'.'/'. $supplier_location[0]->vehicle_image}}" target="new">
                                                                                <img src="{{  url('/').'/uploads/supplier'.'/'. $supplier_location[0]->vehicle_image}}" height="100px" width"100px"/>
                                                                            </a>
                                                                         <button class="btn btn-danger btn-sm" onclick=DeleteVehicleNumber(); >Delete Vehicle Image</button>
                                                                         @endif

                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">License Number</th>
                                                                    <td>{{ $supplier_location[0]->license_number}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">License Number Image</th>
                                                                    <td>
                                                                         @if($supplier_location[0]->license_image!="")
                                                                            <a href="{{  url('/').'/uploads/supplier'.'/'. $supplier_location[0]->license_image}}" target="new">
                                                                                <img src="{{  url('/').'/uploads/supplier'.'/'. $supplier_location[0]->license_image}}" height="100px" width"100px"/>
                                                                            </a>
                                                                            <button class="btn btn-danger btn-sm" onclick=DeleteLicenseNumber(); >Delete License Image</button>
                                                                        @endif

                                                                    </td>
                                                                </tr>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- end of row -->
                                            </div>
                                            <!-- end of general info -->
                                        </div>
                                        <!-- end of col-lg-12 -->
                                    </div>
                                    <!-- end of row -->
                                </div>
                            </div>
                            <!-- end of card-block -->
                        </div>
                        <!-- end of card-->
                        <!-- end of row -->
                    </div>

                    <div class="tab-pane " id="products" role="tabpanel">
                        <div class="card">
                            <div class="card-header"><h5 class="card-header-text">About Supplier Products</h5>
                            </div>
                            <div class="card-block">
                                <div class="view-info">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="general-info">
                                                <div class="row">
                                                    <div class="col-lg-12 col-xl-10">
                                                        <table class="table m-5">
                                                            <tbody id="AddInput">

                                                                <tr>
                                                                    <input type="hidden" id ="supplier_id" value={{ $data->id }} />
                                                                    <th scope="row" class='col-md-1' ></th>
                                                                    <th scope="row" class='col-md-7'>Product Name</th>
                                                                    <th scope="row" class='col-md-3'>Product Price</th>
                                                                </tr>

                                                                @if(count($supplier_product) > 0)

                                                                @foreach(json_decode($supplier_product['rate_cards'], true) as $index=>$d)
                                                                <tr id="row_{{ $index+1 }}">
                                                                    <th scope="row" class='col-md-1'><input type="text" class="form-control" id="{{ $index+1}}"  value="{{ $index+1}}" hidden/></th>
                                                                    <th scope="row" class='col-md-7'><input type="text" class="form-control" id="product_{{$index+1 }}" value="{{ $d['product_name'] }}"/></th>
                                                                    <th scope="row" class='col-md-3'> <input type="text" class="form-control" id="price_{{ $index+1 }}" value="{{ $d['price']}}"/></th>
                                                                    <th scope="row" class='col-md-1 text-danger btn'  onclick=removeInput({{ $index+1 }})>Delete</th>
                                                                </tr>
                                                                @endforeach
                                                                <p hidden>
                                                                 {{ $index=$index+1 }}
                                                                </p>
                                                                @else
                                                                <tr id="row_{{ $a }}">
                                                                    <th scope="row" class='col-md-1'><input type="text" class="form-control" id="{{ $a  }}" value="{{ $a  }}" hidden/></th>
                                                                    <th scope="row" class='col-md-7'><input type="text" class="form-control" id="product_{{ $a }}"/></th>
                                                                    <th scope="row" class='col-md-3'> <input type="text" class="form-control" id="price_{{ $a }}"/></th>
                                                                    <th scope="row" class='col-md-1 text-danger btn' onclick=removeInput({{ $a }})>Delete</th>
                                                                </tr>

                                                                @endif

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- end of row -->
                                            </div>
                                            <!-- end of general info -->
                                        </div>
                                        <!-- end of col-lg-12 -->
                                    </div>
                                    <!-- end of row -->
                                </div><br/>
                                <button onclick=addMoreInput()  id="addNewLocation" class="btn btn-primary waves-effect waves-light f-right" >
                                        Add More
                                </button>
                                &nbsp;&nbsp;&nbsp;
                                @if(count($supplier_product) > 0)
                                <button  onclick=updateProduct() class="btn btn-success waves-effect waves-light f-right" >
                                    Update
                                </button>
                                @else
                                <button  onclick=saveProduct() class="btn btn-success waves-effect waves-light f-right" >
                                        Save
                                </button>
                                @endif
                            </div>

                            <!-- end of card-block -->
                        </div>
                        <!-- end of card-->
                        <!-- end of row -->
                    </div>

                    <div class="tab-pane" id="project" role="tabpanel">
                        <div class="card">
                            <div class="card-header">
                                <h5 class="card-header-text">Drivers Details</h5>
                                <a type="button" href="{{ route('drivers.create') }}" class="btn btn-primary waves-effect waves-light f-right">
                                    + ADD Drivers</a>
                            </div>
                            <!-- end of card-header  -->
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="project-table">
                                        <div class="table-responsive">
                                            <table class="table dt-responsive table-striped table-bordered nowrap" id="tblData" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>ID</th>
                                                        <?php foreach ($driver_fields as $field): ?>
                                                            <th class="text-center txt-primary">{{$field}}</th>
                                                        <?php endforeach; ?>
                                                        <th class="text-center txt-primary">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody class="text-center">

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- Container-fluid ends -->
</div>
<style type="text/css">
    .md-tabs .nav-item {
        width: calc(100%/4);
    }

    .nav-tabs .slide {
        width: calc(100% /4);
    }
    .table-responsive {
        padding: 25px;
    }
</style>
<script type="text/javascript" charset="utf-8" async defer>

var n = <?php echo $index ?>;

function addMoreInput(){
    n = n+1;
    var input = '<tr id ="row_'+n+'"><th scope="row" class="col-md-1"><input type="text" id='+n+' value='+n+' class="form-control" hidden/></th>'+
                '<th scope="row" class="col-md-7"><input type="text"  id ="product_'+n+'" class="form-control"/></th>'+
                '<th scope="row" class="col-md-3"> <input type="text" id ="price_'+n+'" class="form-control"/></th>'+
                '<th scope="row" class="col-md-1 text-danger btn" onclick=removeInput('+n+')>Delete</th></tr>'
                $('#AddInput').append(input);
}
function removeInput(id)
{

    $("#row_"+id).remove();
}


function saveProduct(){
    var productArray=[];
   var supplier_id = $("#supplier_id").val();

   for(i=1;i<=n;i++){

       var item= $("#product_"+i).val();
       var price= $("#price_"+i).val();

       var newArray = {
           'product_name':item,
           'price':price
       };
       if(newArray.product_name!=" " ||  newArray.product_name!=null)
       {
        productArray.push(newArray);
       }

   }

   if(productArray.length == 0 )
   {
        swal({title: "Warning!", text: "Please add alteast one product to save", type: "warning"});
        return false;

   }
   $.post("/admin/supplier_product",{supplier_id:supplier_id,rate_cards:JSON.stringify(productArray)},function(data){
    if (data.success == true) {
        swal({title: "Success!", text: "Products Saved Successfully", type: "success"},
                function () {
                  location.reload(true);
                }
        );
    }else{
        swal({title: "Warning!", text: "Oops! Something went wrong ", type: "warning"},
        function () {
          location.reload(true);
        });
    }

    });
}

function updateProduct(){

    var updateproductArray=[];
    var supplier_id = $("#supplier_id").val();

   for(i=1;i<=n;i++){

       var item= $("#product_"+i).val();
       var price= $("#price_"+i).val();

       var newArray = {
           'product_name':item,
           'price':price
       };
       if(newArray.product_name!=" "|| newArray.product_name!=null)
       {
        updateproductArray.push(newArray);
       }
   }

   $.post("/admin/supplier_product/"+supplier_id,{supplier_id:supplier_id,rate_cards:JSON.stringify(updateproductArray)},function(data){
    if (data.success == true) {
        swal({title: "Success!", text: "Products Updated Successfully Successfully", type: "success"});
    }else{
        swal({title: "Warning!", text: "Oops! Something went wrong ", type: "warning"},
        function () {
          location.reload(true);
        }
    );
    }
    });
}


function DeleteVehicleNumber()
{
    var supplier_id = $("#supplier_id").val();

    $.post("/admin/deleteVehicle/"+supplier_id,function(data){

        location.reload(true);
        });
}
function DeleteLicenseNumber()
{
    var supplier_id = $("#supplier_id").val();

    $.post("/admin/deleteLicense/"+supplier_id,function(data){
        location.reload(true);
        });
}
//ajax header need for deleted and updating data

var table;
//datatables serverSide
    $('document').ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        table = $('#tblData').DataTable({
        stateSave: true,
                responsive: true,
                processing: true,
                serverSide: true,
                order: [0, 'desc'],
                ajax: '{{route("drivers.index")}}',
                columns: [
                {data: 'id', name: 'id'},
        <?php foreach ($driver_fields as $field): ?>
                            {data: "{{$field}}", name: "{{$field}}"},
        <?php endforeach; ?>
                {data: 'action', name: 'action', orderable: false, searchable: false}
                ], "drawCallback": function (settings) {
        $('.change-state').bootstrapToggle();
        }
    });
    // table.draw(false);

//deleting data
    $('#tblData').on('click', '.btnDelete[data-remove]', function (e) {
        e.preventDefault();
        var url = $(this).data('remove');
        swal({
            title: "Are you sure want to remove this item?",
            text: "Data will be Temporary Deleted!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Confirm",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false,
        },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: url,
                            type: 'DELETE',
                            dataType: 'json',
                            data: {method: '_DELETE', submit: true},
                            success: function (data) {
                                if (data == 'Success') {
                                    swal("Deleted!", "Category has been deleted", "success");
                                    table.ajax.reload(null, false);
                                }
                            }
                        });
                    } else {

                        swal("Cancelled", "You Cancelled", "error");
                    }

                });
    });


    $('#tblData').on('change', '.change-state', function (e) {
    e.preventDefault();
            var url = "<?= url(config('app.admin_prefix') . '/user/changestate/') ?>" + '/' + $(this).attr('data-id');
            var checked = $('#change-state-' + $(this).attr('data-id')).prop('checked');
//            alert($('#change-state-' + $(this).attr('data-id')).prop('checked'));
            swal({
            title: "Are you sure want to change Vote status",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonClass: "btn-info",
                    confirmButtonText: "Confirm",
                    cancelButtonText: "Cancel",
                    closeOnConfirm: true,
                    closeOnCancel: true
            },
                    function (isConfirm) {
                    if (isConfirm) {
                    $.ajax({
                    url: url,
                            type: 'GET',
                            datatype: 'json',
                            data: {checked: checked},
                            success: function (data) {
                            console.log(data);
                                    table.ajax.reload(null, false);
                            }

                    });
                    }
                    });
    });
    }
    );

</script>
@endsection
