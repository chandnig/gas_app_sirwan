@extends('layouts.admin-dashboard')
@section('content')
<div class="content-wrapper">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 p-0">
                <div class="main-header">
                    <h4>Dashboard</h4>
                </div>
            </div>
        </div>
        <!-- 1-1 blocks row start -->
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-block">
                        <div class="media d-flex order-counter">
                            <div class="media-left media-middle">
                                <div class="new-orders">
                                    <i class="icofont icofont-users bg-warning"></i>
                                </div>
                            </div>
                            <div class="media-body">
                                <h1 class="text-center"><b>Customers</b></h1>
                            </div>
                        </div>
                        <ul>
                            <li class="new-users">
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-block">
                        <div class="media d-flex order-counter">
                            <div class="media-left media-middle">
                                <div class="new-orders">
                                    <i class="icofont icofont-users bg-warning"></i>
                                </div>
                                </div>
                                 <div class="media-body">
                                  <h1 class="text-center"><b>Supplier</b></h1>
                                </div>

                        </div>
                        <ul>
                            <li class="new-users">
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!-- 1-1-blocks row end -->
        <!-- 2nd row start -->

        <div class="col-xl-6 col-sm-12 grid-item">
            <div class="row">
                <div class="col-sm-6">
                    <div class="basic-widget basic-widget-purple ">
                        <i class="icofont icofont-briefcase"></i>
                        <h5>Total Customers</h5>
                        <h3>{{ $totalcustomers }}</h3>
                        <div class="counter-txt">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="basic-widget basic-widget-green m-r-0">
                        <i class="icofont icofont-briefcase"></i>
                        <h5>Today Customers</h5>
                        <h3>{{ $todaycustomers }}</h3>
                        <div class="counter-txt">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="basic-widget basic-widget-green m-r-0">
                        <h5>Blocked Customers</h5>
                        <h3>{{ $blockedcustomers }}</h3>
                        <div class="counter-txt">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-6 col-sm-12 grid-item">
            <div class="row">
                <div class="col-sm-6">
                    <div class="basic-widget basic-widget-purple ">
                        <i class="icofont icofont-user"></i>
                        <h5>Total Supplier</h5>
                        <h3>{{ $totalsuppliers }}</h3>
                        <div class="counter-txt">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="basic-widget basic-widget-green m-r-0">
                        <i class="icofont icofont-user"></i>
                        <h5>Today supplier </h5>
                        <h3>{{ $todaysuppliers }}</h3>
                        <div class="counter-txt">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="basic-widget m-r-0">
                        <h5>Pending suppliers </h5>
                        <h3>{{ $pendingsuppliers }}</h3>
                        <div class="counter-txt">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="basic-widget m-r-0">
                        <h5>Active suppliers </h5>
                        <h3>{{ $activesuppliers }}</h3>
                        <div class="counter-txt">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-block">
                    <div class="media d-flex order-counter">
                        <div class="media-left media-middle">
                            <div class="new-orders">
                                <i class="icofont icofont-users bg-warning"></i>
                            </div>
                            </div>
                             <div class="media-body">
                              <h1 class="text-center"><b>Service/Products</b></h1>
                            </div>

                    </div>
                    <ul>
                        <li class="new-users">
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-xl-12 col-sm-12 grid-item">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="basic-widget basic-widget-purple ">
                            <h5>Active Products per Governorate</h5>
                        </div>
                        <div style="padding-bottom: 10px"> 
                            <table border="1px" width="100%">
                                <tr>
                                    <th>Governorate</th>
                                    <th>Service</th>
                                </tr>
                                @foreach($govData as $data)
                                <tr>
                                    <td>{{$data['govName']}}</td>
                                    <td>
                                    @foreach($data['services'] as $service)
                                        <p>{{$service}}</p>
                                    @endforeach
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div>    
                    </div>
                </div>
            </div>    
        </div>
        
    </div>
</div>
<!-- Contact card start -->
<!-- Contact card End -->

<!-- 3rd row start -->
<!--div class="row">
    <div class="col-xl-6 col-sm-12 grid-item">
        <div class="row">
                                <div class="col-sm-6">
                                    <div class="basic-widget basic-widget-purple ">
                                        <i class="icofont icofont-ui-user"></i>
                                        <h5>Today Entries</h5>
                                        <h3></h3>
                                        <div class="counter-txt">

                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="basic-widget basic-widget-green m-r-0">
                                        <i class="icofont icofont-briefcase"></i>
                                        <h5>Today Group Entries</h5>
                                        <h3></h3>
                                        <div class="counter-txt">

                                        </div>
                                    </div>
                                </div>
        </div>
    </div>
    <div>
        <div class="col-xl-6 col-sm-12 grid-item">
            <div class="row">
                <div class="col-sm-6">
                    <div class="basic-widget basic-widget-purple ">
                        <i class="icofont icofont-users-alt-3"></i>
                        <h5>Groups Voting</h5>
                        <h3></h3>
                        <div class="counter-txt">
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="basic-widget basic-widget-green m-r-0">
                        <i class="icofont icofont-users-alt-3"></i>
                        <h5>Today Groups Voting</h5>
                        <h3></h3>
                        <div class="counter-txt">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
<!-- Contact card start -->
<!-- Contact card End -->

<!-- Container-fluid ends -->
@stop
