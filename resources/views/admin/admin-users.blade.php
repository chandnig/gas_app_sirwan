@extends('layouts.admin-data')
@section('content')
<div class="content-wrapper">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <!-- Row Starts -->
        <div class="row">
            <div class="col-sm-12 p-0">
                <div class="main-header">
                    <h4>Administrators</h4>
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li class="breadcrumb-item">
                            <a href="{{url('fastway/dashboard')}}">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="{{url('fastway/adminuser')}}">Users</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">List</a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <!--<div class="card-header"><h5 class="card-header-text">Simple Table</h5></div>-->
                    <div class="card-block">
                        <div class="data_table_main">
                            <button type="button" class="btn btn-primary" id="btnAdd">Add New</button>
                            <br>
                            <br>
                            <table class="table dt-responsive table-striped table-bordered nowrap" id="tblData" cellspacing="0" width="100%">
                                {{csrf_field()}}
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>email</th>
                                        <th>created_at</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
<!-- start addmodal-->
<div class="modal fade" tabindex="-1" role="dialog" id="mdlAddData">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Form</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="frmDataAdd">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="name" class="control-label">
                            Name<span class="required">*</span>
                        </label>
                        <input type="text" class="form-control" id="name" name="name">
                        <p class="errorName text-danger hidden"></p>
                    </div>
                    <div class="form-group">
                        <label for="email" class="control-label">
                            Email<span class="required">*</span>
                        </label>
                        <input type="text" class="form-control" id="email" name="email">
                        <p class="errorEmail text-danger hidden"></p>
                    </div>
                    <div class="form-group">
                        <label for="password" class="control-label">
                            Password<span class="required">*</span>
                        </label>
                        <input class="form-control" id="password" name="password" >
                        <p class="errorPassword text-danger hidden"></p>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btnSave"><i class="glyphicon glyphicon-save"></i>&nbsp;Save</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- start endmodal-->

<!-- start editmodal-->
<div class="modal fade" tabindex="-1" role="dialog" id="mdlEditData">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Edit Form</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="frmDataEdit">
                    <div class="form-group">
                        <label for="edit_ID" class="control-label">
                            ID
                        </label>
                        <input type="text" class="form-control" id="edit_ID" name="edit_ID" disabled>
                    </div>
                    <div class="form-group">
                        <label for="edit_name" class="control-label">
                            Name<span class="required">*</span>
                        </label>
                        <input type="text" class="form-control" id="edit_name" name="edit_name">
                        <p class="edit_errorName text-danger hidden"></p>
                    </div>
                    <div class="form-group">
                        <label for="edit_email" class="control-label">
                            email<span class="required">*</span>
                        </label>
                        <input type="text" class="form-control" id="edit_email" name="edit_email">
                        <p class="edit_errorEmail text-danger hidden"></p>
                    </div>
                    <div class="form-group">
                        <label for="edit_address" class="control-label">
                            password<span class="required">*</span>
                        </label>
                        <input type="password" class="form-control" id="edit_password" name="edit_password">
                        <p class="edit_errorPassword text-danger hidden"></p>
                    </div>
                    <div class="form-group">
                        <label for="role" class="control-label">
                            Role<span class="required">*</span>
                        </label>
                        <select class="form-control" name="role" id="role">
                            <option value="">Please select Role</option>
                            <?php foreach ($roles as $role): ?>
                                <option value="<?= $role->name ?>" <?= isset($data) ? $data->hasRole($role->name) ? 'selected="selected"' : '' : '' ?>><?= $role->name ?></option>
                            <?php endforeach; ?>
                        </select>
                        <br>
                        <p class="errorRole text-danger hidden"></p>
                    </div>

                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btnUpdate"><i class="glyphicon glyphicon-save"></i>&nbsp;Save</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end editmodal-->
@endsection

@section('scripts')
<script type="text/javascript" charset="utf-8" async defer>

//ajax header need for deleted and updating data

    var table;

//datatables serverSide
    $('document').ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        table = $('#tblData').DataTable({
            stateSave: true,
            responsive: true,
            processing: true,
            serverSide: true,
            order: [0, 'desc'],
            ajax: '{{route("adminuser.index")}}',
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'created_at', name: 'created_at'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });
        // table.draw(false);
//calling add modal
        $('#btnAdd').click(function (e) {
            $('#mdlAddData').modal('show');
        });

//Adding new data
        $('#btnSave').click(function (e) {
            e.preventDefault();
            var frm = $('#frmDataAdd');
            $.ajax({
                url: '<?= url(config('app.admin_prefix') . '/adminuser') ?>',
                type: 'POST',
                dataType: 'json',
                data: $('#frmDataAdd').serializeArray(),
                success: function (data) {
                    $('#frmDataAdd .text-danger').each(function () {
                        $(this).addClass('hidden');
                    });

                    if (data.errors) {
                        $.each(data.errors, function (index, value) {
                            $('#' + index).parent().find('.text-danger').text(value);
                            $('#' + index).parent().find('.text-danger').removeClass('hidden');
                        });
                    }
                    if (data.success == true) {
                        $('#mdlAddData').modal('hide');
                        frm.trigger('reset');
                        table.ajax.reload(null, false);
                        swal('success!', 'Successfully Added', 'success');

                    }
                }
            });
        });

//calling edit modal and id info of data

        $('#tblData').on('click', '.btnEdit[data-edit]', function (e) {
            e.preventDefault();
            var url = $(this).data('edit');
            swal({
                title: "Are you sure want to Edit this item?",
                type: "info",
                showCancelButton: true,
                confirmButtonClass: "btn-info",
                confirmButtonText: "Confirm",
                cancelButtonText: "Cancel",
                closeOnConfirm: true,
                closeOnCancel: true
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url: url,
                                type: 'GET',
                                datatype: 'json',
                                success: function (data) {
                                    $('#edit_ID').val(data.id);
                                    $.each(data, function (index, value) {
                                        $('#frmDataEdit #edit_' + index).val(value);
                                    });
                                    $('#frmDataEdit .text-danger').each(function () {
                                        $(this).addClass('hidden');
                                    });
                                    $('#mdlEditData').modal('show');
                                }

                            });
                        }
                    });
        });

// updating data infomation
        $('#btnUpdate').on('click', function (e) {
            e.preventDefault();
            var url = "<?= url(config('app.admin_prefix') . '/adminuser') . '/' ?>" + $('#edit_ID').val();
            var frm = $('#frmDataEdit');
            $.ajax({
                type: 'PUT',
                url: url,
                dataType: 'json',
                data: frm.serialize(),
                success: function (data) {
                    // console.log(data);
                    $('#frmDataEdit .text-danger').each(function () {
                        $(this).addClass('hidden');
                    });
                    if (data.errors) {
                        $.each(data.errors, function (index, value) {
                            $('#frmDataEdit #' + index).parent().find('.text-danger').text(value);
                            $('#frmDataEdit #' + index).parent().find('.text-danger').removeClass('hidden');
                        });
                    }
                    if (data.success == true) {
                        // console.log(data);
                        frm.trigger('reset');
                        $('#mdlEditData').modal('hide');
                        swal('Success!', 'Data Updated Successfully', 'success');
                        table.ajax.reload(null, false);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Please Reload to read Ajax');
                }
            });
        });

//deleting data
        $('#tblData').on('click', '.btnDelete[data-remove]', function (e) {
            e.preventDefault();
            var url = $(this).data('remove');
            swal({
                title: "Are you sure want to remove this item?",
                text: "Data will be Temporary Deleted!",
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: "Confirm",
                cancelButtonText: "Cancel",
                closeOnConfirm: false,
                closeOnCancel: false,
            },
                    function (isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url: url,
                                type: 'DELETE',
                                dataType: 'json',
                                data: {method: '_DELETE', submit: true},
                                success: function (data) {
                                    if (data == 'Success') {
                                        swal("Deleted!", "Category has been deleted", "success");
                                        table.ajax.reload(null, false);
                                    }
                                }
                            });

                        } else {

                            swal("Cancelled", "You Cancelled", "error");

                        }

                    });
        });

    });
</script>
@endsection
