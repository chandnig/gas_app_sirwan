@extends('layouts.admin-data')
@section('content')
<?php $a='1'?>
<?php $index=1?>
<div class="content-wrapper">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <div class="row">
            <div class="main-header">
                <h4>{{$title}}</h4>
                <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                    <li  class="breadcrumb-item">
                        <i class="icon-home"></i>
                        <a href="{{url('/admin')}}">Home</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li class="breadcrumb-item">
                        <i class="icon-users"></i>
                        <a class="ajaxify" href="{{$listUrl}}">List {{$title}}</a>
                    </li>
                    <li class="breadcrumb-item">
                        <i class="icon-users"></i>
                        <span>View {{$title}}</span>
                    </li>
                </ol>
            </div>
        </div>
        <!-- Header end -->
        <div class="row">
            <!-- start col-lg-9 -->
            <div class="col-xl-12 col-sm-12 col-lg-8">
                <!-- Nav tabs -->
                <div class="tab-header">
                        <div class="row">&nbsp;&nbsp;
                                 <select type="text" class="col-md btn btn-outline-primary" placeholder="" name="governorate_id" id="governorate_id">
                                    <option value="">Select Governorate</option>
                                    @foreach($location_data as $loc)
                                    <option value="{{ $loc->id }}" {{($loc->id == $govId) ? 'selected' : ''}}> {{ $loc->name }}</option>
                                    @endforeach
                                </select>&nbsp;&nbsp;
                                <input type="hidden" value="" />
                                <button class="btn btn-success btn-sm" id="filterSupplier" onclick=SearchGasAppFee()>Search</button>&nbsp;
                                    <a href="{{ service_fees }}"><i class="icon-refresh text-primary bold " aria-hidden="true"></i></a>
                        </div>
                </div>
                
                @if($service)
                    <div class="bg-white feeModel" style="padding-left:20px"><br/>
                        @foreach($service as $s) 
                        <form>
                                <input type="hidden"  value="{{ $s['id'] }}" class="service_id"/>
                                <h6>{{ $s['name'] }}
                                    <input type="text" class="btn btn-outline-primary service_price" value="{{$s['service_price']}}" placeholder=""/> IQD</h6>
                                <br/>
                                <h6>{{ $s['name'] }}, if order more than
                                    <input type="text" class="btn btn-outline-primary service_quantity" value="{{$s['service_quantity']}}" placeholder="" /> Set the price to
                                    <input type="text" placeholder="" value="{{$s['service_price_many']}}"  class="btn btn-outline-primary service_price_many"/>IQD</h6>
                                <hr/>
                                <br/>
                            </form>
                        @endforeach
                        <button  class="btn btn-block btn-primary" id="saveAdj"> Save Data </button>
                        <br/>
                        <br/>
                    </div>
                @endif
            </div>
        </div>

    </div>
    <!-- Container-fluid ends -->
</div>

<script type="text/javascript" charset="utf-8" async defer>
    $('document').ready(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });


    // $('select[name=governorate_id]').change(function(){
    //     var governorate_id = $(this).val();
    //         alert(governorate_id);
    //     $.post("service_fees/getGasApp_fees",{governorate_id:governorate_id},function(data){

    //     });


    //    });


  $('#saveAdj').click(function() {
    var url = '<?php echo getenv("ADMIN_URL"); ?>'
    var dataArray=[];
    var governorate_id            =   $("#governorate_id").val();
    if(governorate_id=="")
    {
        alert("Please Select Governorate");
        return false;
    }
    $('.feeModel form').each(function() {
        var thisForm = this;
        var service_quantity         =  $('.service_quantity', thisForm).val();
        var service_price            =  $('.service_price', thisForm).val();
        var service_price_many          =  $('.service_price_many', thisForm).val();
        var service_id               =  $('.service_id', thisForm).val();
        var governorate_id              =   $("#governorate_id").val();
        dataArray.push({
            governorate_id      : governorate_id,
            service_quantity    : service_quantity,
            service_price       : service_price,
            service_price_many  : service_price_many,
            service_id          : service_id,
        });
    });
    $.post(url+"service_fees",{governorate_id:governorate_id,dataArray:dataArray},function(data){
        if (data=="Success") {
            swal({title: "Success!", text: "Service Fee Added Successfully", type: "success"});
            location.reload(true);
        }else{
            swal({title: "Warning!", text: "Oops! Something went wrong ", type: "warning"},
            function () {
              location.reload(true);
            }
        );
        }
    });
  });



function SearchGasAppFee()
{
  var url = '<?php echo getenv("ADMIN_URL"); ?>'
  var governorate_id = $("#governorate_id").val();
  if(governorate_id=="")
  {
      alert("Please Select Governorate");
      return false;
  }
  else {
    window.location.href = url+"service_fees/"+governorate_id 
  }
  
    //   $.get("service_fees/"+governorate_id+"/edit",function(data){
    //       console.log(data);
    //   });
}

</script>
@endsection
