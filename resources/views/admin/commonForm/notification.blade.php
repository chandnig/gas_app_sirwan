@section('modal')
<!-- start editmodal-->
<div class="modal fade" tabindex="-1" role="dialog" id="mdlEditData">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Notification send {{$way}}</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="frmDataEdit">
                    @include('admin.commonForm.Cform',['inputTypeTexts' => \App\Http\Controllers\Admin\NotificationController::rules()])

                    <br/>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="sendNotify"><i class="glyphicon glyphicon-save"></i>&nbsp;Send</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end editmodal-->
@endsection

<script type="text/javascript" charset="utf-8" async defer>
    $('document').ready(function () {
        var frm = $('#frmDataEdit');
        $('#tblData').on('click', '.notify', function (e) {
            e.preventDefault();
            $("#mdlEditData").modal();
            frm.attr('action', $(this).attr('href'));
        });
        $('body').on('click', '#sendNotify', function (e) {
            e.preventDefault();
            var url = frm.attr('action');
            $.ajax({
                type: 'POST',
                url: url,
                dataType: 'json',
                data: frm.serialize(),
                success: function (data) {
                    // console.log(data);
                    $('#frmDataEdit .text-danger').each(function () {
                        $(this).addClass('hidden');
                    });
                    if (data.errors) {
                        $.each(data.errors, function (index, value) {
                            $('#frmDataEdit #' + index).parent().find('.text-danger').text(value);
                            $('#frmDataEdit #' + index).parent().find('.text-danger').removeClass('hidden');
                        });
                    }
                    if (data.success == true) {
                        // console.log(data);
                        frm.trigger('reset');
                        $('#mdlEditData').modal('hide');
                        swal('Success!', 'Sent Successfully', 'success');
                        table.ajax.reload(null, false);
                    }
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Please Reload to read Ajax');
                }
            });
        });
    });
</script>



