{{csrf_field()}}
<div class="form-group">
    <label for="id" class="control-label">
        ID
    </label>
    <input type="text" class="form-control" id="id" name="id" disabled  value="<?= isset($data->id) ? $data->id : '' ?>">
</div>

<?php
//dd(\Request()->path());

foreach ($inputTypeTexts as $input => $inputValue):
    $label = ucwords(str_replace('_', ' ', $input));
    ?>
    <div class="form-group">
        <label for="<?= $input ?>" class="control-label">
            <?= $label ?>
            <?= (strpos($inputValue, 'required') !== false) ? '<span class="required">*</span>' : '' ?>
        </label>
        <?php
        if (strpos($inputValue, 'in') !== false):
            $inputValue = (strpos($inputValue, 'required|') !== false) ? str_replace('required|', '', $inputValue) : $inputValue;
            $inputValueAr = explode(',', str_replace('in:', '', $inputValue));
        ?>

            <?php if ($input == 'customer_type'): ?>
                <div class="form-control form-radio" id="<?= $input ?>"  >
                    <?php foreach ($inputValueAr as $ar): ?>
                        <div class="radio radiofill">
                            <label>
                                <input type="radio" name="<?= $input ?>" value="<?= $ar ?>" <?= isset($data) ? (($data->customer_type) ? 'checked="checked"' : '') : '' ?>>
                                <i class="helper"></i><?= $ar ?>
                            </label>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php elseif ($input == 'role'): ?>
                <div class="form-control form-radio" id="<?= $input ?>"  >
                    <?php foreach ($inputValueAr as $ar): ?>
                        <div class="radio radiofill">
                            <label>
                                <input type="radio" name="<?= $input ?>" value="<?= $ar ?>" <?= isset($data) ? (($data->hasRole($ar)) ? 'checked="checked"' : '') : '' ?>>
                                <i class="helper"></i><?= $ar ?>
                            </label>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php elseif ($input == 'role_id'):  ?>
                <div class="form-control form-radio" id="<?= $input ?>"  >
                    <?php
                    $inputValueAr = \Spatie\Permission\Models\Role::where('guard_name', 'api')->where('name', 'like', '%Supplier%')->get();
                    foreach ($inputValueAr as $ar):
                        ?>
                        <div class="radio radiofill">
                            <label>
                                <input type="radio" name="<?= $input ?>" value="<?= $ar->id ?>" <?= isset($data) ? (($data->$input == $ar->id) ? 'checked="checked"' : '') : '' ?>>
                                <i class="helper"></i><?= $ar->name ?>
                            </label>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php elseif ($input == 'supplier_role'): ?>
                <div class="form-control" id="<?= $input ?>"  >
                    <?php foreach (App\Models\Service::where('parent_id','=','0')->get() as $ar):  ?>
                        <div class="checkbox-fade fade-in-success">
                            <label>
                                <input type="checkbox" name="<?= $input . '[]' ?>" value="<?= $ar->id?>" <?= isset($data) ? (($data->$input == $ar->id) ? 'checked="checked"' : '') : '' ?>>
                                <span class="cr"><i class="cr-icon fa fa-check txt-success"></i></span> <?= $ar->name ?>
                            </label>
                        </div>
                    <?php endforeach; ?>
                </div>

            <?php else: ?>
                <select class="form-control" id="<?= $input ?>" name="<?= $input ?>" >
                    <option value="" disabled="">Please Select <?= $label ?> </option>
                    <?php foreach ($inputValueAr as $ar): ?>
                        <option value="<?= $ar ?>" <?= isset($data->$input) ? ($data->$input == $ar) ? 'selected="selected"' : '' : '' ?> > <?= $ar ?></option>
                    <?php endforeach; ?>
                </select>
            <?php endif; ?>
            <?php elseif (strpos($inputValue, 'mime') !== false):
            if (isset($data->$input)):
                $img = (strpos($data->$input, 'http') !== false) ? $data->$input : 'uploads/services/' . $data->$input;
                echo $imgSrc = ($data->$input != '') ? '<img src="' . url($img) . '" class="logo"/>' : 'Not Found';
            endif;
            ?>
            <br/>
            <input type="file" class="form-contro" id="<?= $input ?>" name="<?= $input ?>" accept="image/x-png,image/gif,image/jpeg"/>


        <?php elseif (strpos($input, 'location') !== false): ?>
            <div class="form-control" id="<?= $input ?>"  >
                <?php foreach (App\Models\Location::where('type', 'governorate')->get() as $ar): ?>
                    <div class="checkbox-fade fade-in-success">
                        <label>
                            <input type="checkbox" name="<?= $input . '[]' ?>" value="<?= $ar->id ?>" <?= (isset($data->$input) && $data->$input != null) ? ((in_array($ar->id, json_decode($data->$input))) ? 'checked="checked"' : '') : '' ?>>
                            <span class="cr"><i class="cr-icon fa fa-check txt-success"></i></span> <?= $ar->name ?>
                        </label>
                    </div>
                <?php endforeach; ?>
            </div>

        <?php elseif (strpos($input, 'helpline_number') !== false): ?>
            <div class="" id="<?= $input ?>"  >
                <input type="text" class="form-control" id="<?= $input?>" name="<?= $input?>" value="<?= $data->helpline_number ?>">
                <p class=" text-danger hidden"></p>
            </div>
        <?php elseif (strpos($input, 'role_supplier_id') !== false): ?>
            <div class="form-control" id="<?= $input ?>"  >
                <?php foreach (App\Models\Service::where('parent_id', '0')->get() as $ar): ?>
                    <div class="checkbox-fade fade-in-success">
                        <label>

                            <input type="checkbox" name="<?= $input . '[]' ?>" value="<?= $ar->id ?>" <?= (isset($data->$input) && $data->$input != null) ? ((in_array($ar->id, json_decode($data->$input)))  ? 'checked="checked"' : '') : '' ?>>
                            <span class="cr"><i class="cr-icon fa fa-check txt-success"></i></span> <?= $ar->name.' Supplier' ?>
                        </label>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php elseif (strpos($input, 'service_id') !== false): ?>
            <select class="form-control" id="<?= $input ?>" name="<?= $input ?>" >
                <?php
                foreach (App\Models\Service::where('parent_id', '0')->get() as $ar):
                    ?>
                    <option value="<?= $ar->id ?>" <?= isset($data[0]->$input) ? ($data[0]->$input == $ar->id) ? 'selected="selected"' : '' : '' ?> > <?= $ar->name ?></option>
                <?php endforeach; ?>
            </select>
        <?php elseif (strpos($input, 'request_status') !== false): ?>
            <select class="form-control" id="<?= $input ?>" name="<?= $input ?>" >
                <option value="<?= $data[0]->request_status ?>" <?= isset($data[0]->$input) ? ($data[0]->$input == $data[0]->id) ? 'selected="selected"' : '' : '' ?> > <?= $data[0]->request_status ?></option>
                <option value="Fraud">Fraud</option>
                <option value="Cancelled">Cancelled</option>
                <option value="Deleted By GasApp">Deleted By GasApp</option>
                <option value="Rejected'">Rejected</option>
                <option value="Processing">Processing</option>
                <option value="Closed">Closed</option>
            </select>
        <?php elseif (strpos($input, 'service_quantity') !== false): ?>
            <div class="" id="<?= $input ?>"  >
                <input type="text" class="form-control" id="<?= $input?>" name="<?= $input?>" value="<?= $data[0]->service_quantity ?>">
                <p class=" text-danger hidden"></p>
            </div>

        <?php elseif (strpos($input, 'unit') !== false):  ?>
            <?php if (strpos(\Request()->path(), 'service') !== false): ?>
                <select class="form-control" id="<?= $input ?>" name="<?= $input ?>" >
                    <option value="" disabled>Please Select Unit</option>
                    <?php
                    foreach (App\Models\MeasurementUnit::all() as $mu):
                        ?>
                        <option value="<?= $mu->id ?>" <?= isset($data->$input) ? ($data->$input == $mu->id) ? 'selected="selected"' : '' : '' ?> > <?= $mu->measurement_unit ?></option>
                    <?php endforeach; ?>
                </select>
                <?php elseif(strpos($input, 'measurement_unit_ar') !== false):?>
                <div class="" id="<?= $input ?>"  >
                        <input type="text" class="form-control" id="<?= $input?>" name="<?= $input?>" value="<?= $data->measurement_unit_ar ?>">
                        <p class=" text-danger hidden"></p>
                    </div>
                <?php elseif(strpos($input, 'measurement_unit_kr') !== false):?>
                    <div class="" id="<?= $input ?>"  >
                            <input type="text" class="form-control" id="<?= $input?>" name="<?= $input?>" value="<?= $data->measurement_unit_kr ?>">
                            <p class=" text-danger hidden"></p>
                        </div>
                <?php elseif(strpos($input, 'measurement_unit') !== false):?>
                    <div class="" id="<?= $input ?>"  >
                            <input type="text" class="form-control" id="<?= $input?>" name="<?= $input?>" value="<?= $data->measurement_unit ?>">
                            <p class=" text-danger hidden"></p>
                        </div>

            <?php endif?>

        <?php elseif (strpos($input, 'parent_id') !== false):  ?>
            <?php if (strpos(\Request()->path(), 'service') !== false): ?>
                <select class="form-control" id="<?= $input ?>" name="<?= $input ?>" >
                    <option value="" disabled="">Please Select <?= $input ?> </option>
                    <option value="0" <?= isset($data->$input) ? (($data->$input == '0') ? 'selected="selected"' : '') : '' ?>>root</option>
                    <?php
                    foreach (App\Models\Service::where('parent_id', '0')->get() as $par):
                        ?>
                        <option value="<?= $par->id ?>" <?= isset($data->$input) ? ($data->$input == $par->id) ? 'selected="selected"' : '' : '' ?> > <?= $par->name ?></option>
                    <?php endforeach; ?>
                </select>

            <?php else: ?>
                <select class="form-control" id="<?= $input ?>" name="<?= $input ?>" >
                    <option value="" disabled="">Please Select <?= $type ?> </option>
                    <?php
                    $columnT = ($type == 'city') ? 'name_kr' : 'name';
                    foreach (App\Models\Location::where('type', $type)->get() as $location):
                        ?>
                        <option value="<?= $location->id ?>" <?= isset($data->$input) ? ($data->$input == $location->id) ? 'selected="selected"' : '' : '' ?> > <?= $location->$columnT ?></option>
                    <?php endforeach; ?>
                </select>
            <?php
            endif;
            elseif (strpos($inputValue, 'color') !== false):
            ?>
            <input type="text" class="form-control color" id="<?= $input ?>" name="<?= $input ?>"data-inline="true" value="<?= isset($data->$input) ? $data->$input : '' ?>"/>


            <?php elseif (strpos($input, 'governorate_id') !== false):  ?>
                    <select class="form-control" id="<?= $input ?>" name="<?= $input ?>" >
                        <option value="" >Please Select Governorate </option>
                        <?php foreach (App\Models\Location::where('parent_id', '0')->get() as $par):
                            ?>
                            <option value="<?= $par->id ?>" <?= isset($data->$input) ? ($data->$input == $par->id) ? 'selected="selected"' : '' : '' ?> > <?= $par->name ?></option>
                        <?php endforeach; ?>
                    </select>

            <?php elseif (strpos($input, 'city_id') !== false):  ?>
                <select class="form-control" id="<?= $input ?>" name="<?= $input ?>">
                </select>

            <?php elseif (strpos($input, 'section_id') !== false):  ?>

                <div id="<?=$input ?>" name="<?= $input ?>" >
                </div>


        <?php elseif (strpos($input, 'vehicle_number') !== false):?>
        <div class="row">
            <div class="col-md-6">
             <input type="text" class="form-control col-md-6" id="<?= $input ?>" name="<?= $input ?>"  value="<?= $data->vehicle_number ?>"/>
            </div>
            <div class="col-md-6">
              <?php  if (isset($data->vehicle_image)):
                   $img = 'uploads/supplier/';
                   echo $imgSrc ='<a href="' . url($img.$data->vehicle_image) . '" target="new"><img src="' . url($img.$data->vehicle_image) . '" class="logo" height="120px" height="120px"/></a>';
               endif;?>
            </div>
        </div>
           <br/>
        <?php elseif (strpos($input, 'license_number') !== false):?>
        <div class="row">
            <div class="col-md-6">
             <input type="text" class="form-control col-md-6" id="<?= $input ?>" name="<?= $input ?>"  value="<?= $data->license_number ?>"/>
            </div>
            <div class="col-md-6">
              <?php  if (isset($data->license_image)):
                   $img = 'uploads/supplier/';
                   echo $imgSrc ='<a href="' . url($img.$data->license_image) . '" target="new"><img src="' . url($img.$data->license_image) . '" class="logo" height="120px" height="120px"  alt="Image Not Available"/></a>';
                   endif;?>
            </div>
        </div>
           <br/>
            <?php elseif (strpos($input, 'user_id') !== false): ?>
                <input type="text"  class="form-control" id="<?= $input?>" name="<?= $input?>" value="<?= $user_id ?>">

            <?php elseif (strpos($input, 'tnc') !== false): ?>
            <textarea class="form-control color" id="<?= $input ?>" name="<?= $input ?>" data-inline="true" style="height: 120px;"><?= isset($data->$input) ? $data->$input : '' ?></textarea>
        <?php elseif (strpos($input, 'howto') !== false): ?>
            <textarea class="form-control color" id="<?= $input ?>" name="<?= $input ?>" data-inline="true" style="height: 120px;"><?= isset($data->$input) ? $data->$input : '' ?></textarea>
        <?php else: ?>
            <input type="text" class="form-control" id="<?= $input ?>" name="<?= $input ?>"  value="<?= isset($data->$input) ? $data->$input : old($input) ?>">
        <?php endif; ?>
        <p class=" text-danger hidden"></p>
    </div>
<?php endforeach; ?>

<script type="text/javascript">

        $('select[name=governorate_id]').change(function(){
        var locationId = $(this).val();
        if(locationId){
            var options = "<option value='' >Select City</option>";
              $.post("/api/location/getCity",{locale:"en",location_id:locationId},function(data){
                var status = data.success;
                if(status)

                if(status){
                    var obj = data.data;
                    if(obj.length>0)
                    {
                        for(var k=0;k< obj.length;k++)
                        {
                            if(locationId == obj[k].id)
                            {
                            options = options + '<option value="'+obj[k].id+'" selected> '+obj[k].name+'</option>';
                            }
                            else
                            {
                            options = options + '<option value="'+obj[k].id+'"> '+obj[k].name+'</option>';
                            }
                        }
                    }
                  else{
                    options = options+"<option value=''>No City Found</option>";
                  }
                }
                else{
                    options = options+"<option value=''>No City Found</option>";
                }
                $("#city_id").html(options);
            })

          }

       });


       $('select[name=city_id]').change(function(){
        var cityId = $(this).val();
        if(cityId){
            var section_options = "";
              $.post("/api/location/getSection",{locale:"kr",city_id:cityId},function(data){
                var status = data.success;
                if(status)

                if(status){
                    var obj = data.data;
                    if(obj.length>0)
                    {
                        for(var k=0;k< obj.length;k++)
                        {
                            if(cityId == obj[k].id)
                            {
                                section_options = section_options = '<div class="checkbox-fade fade-in-success"><label>'+
                                            '<input type="checkbox" name="section_id[]" value="'+obj[k].id+'" "checked">'+
                                            '<span class="cr"><i class="cr-icon fa fa-check txt-success"></i></span>'+obj[k].name_kr+'</label></div>'

                            }
                            else
                            {
                                section_options = section_options + '<div class="checkbox-fade fade-in-success"><label>'+
                                        '<input type="checkbox" name="section_id[]" value="'+obj[k].id+'" >'+
                                        '<span class="cr"><i class="cr-icon fa fa-check txt-success"></i></span>'+obj[k].name_kr+'</label></div>'
                            }
                        }
                    }
                  else{
                    section_options = section_options;
                  }
                }
                else{
                    section_options = section_options;
                }
                $("#section_id").html(section_options);
            })

          }

       });

       $(function () {
        $('.color').minicolors();
    });
</script>

