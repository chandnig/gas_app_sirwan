@extends('layouts.admin-data')
@section('content')
<div class="content-wrapper">
    <!-- Container-fluid starts -->
    <div class="container-fluid">
        <!-- Row Starts -->
        <div class="row">
            <div class="col-sm-12 p-0">
                <div class="main-header">
                    <h4>{{$title}}</h4>
                    <ol class="breadcrumb breadcrumb-title breadcrumb-arrow">
                        <li class="breadcrumb-item">
                            <a href="<?= url(config('app.admin_prefix') . '/dashboard') ?>">
                                <i class="icofont icofont-home"></i>
                            </a>
                        </li>
                        <li class="breadcrumb-item"><a href="<?= url(config('app.admin_prefix') . '/push_notification') ?>">{{$title}}</a></li>
                        <li class="breadcrumb-item"><a href="javascript:void(0);">List</a></li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <!--<div class="card-header"><h5 class="card-header-text">Simple Table</h5></div>-->
                    <div class="card-block">
                        <div class="data_table_main mt-2">
                            <br>
                            <div class="row">
                            <input type="text" id="start_date" class="btn  btn-outline-primary col-md" placeholder="From Date">
                               <input type="text" id="end_date" class="btn  btn-outline-primary col-md" placeholder='To Date'>&nbsp;
                                   <select type="text" class="col-md btn btn-outline-primary " id="service_id">
                                       <option value="">Select Supplier Of</option>
                                       <option value="">All</option>
                                       @foreach($services as $service)
                                       <option value="{{ $service->id }}">{{ $service->name }}</option>
                                       @endforeach
                                   </select>&nbsp;
                                   <select type="text" class="col-md btn btn-outline-primary " id="customer_type">
                                       <option value="">Customer Type</option>
                                       <option value="">All</option>
                                       <option value="home_customer">Home Customer</option>
                                       <option value="business_customer">Business Customer</option>
                                   </select>&nbsp;
                                   <select type="text" class="col-md btn btn-outline-primary " id="user_response">
                                       <option value="">User Response</option>
                                       <option value="">All</option>
                                       <option value="Yes">Yes</option>
                                       <option value="No">No</option>
                                   </select>&nbsp;
                                   <button class="btn btn-success btn-sm" id="filterNotification" onclick=filterNotification()>Filter</button>&nbsp;
                                   <a href="{{ push_notification }}"><i class="icon-refresh text-primary bold " aria-hidden="true"></i></a>
                               </div>
                            <br><br/>
                            <table class="table dt-responsive table-responsive table-striped table-bordered nowrap" id="tblData" cellspacing="0" width="100%">
                                {{csrf_field()}}
                                <thead>
                                    <tr>
                                        <th>ID</th>

                                        <?php foreach ($fields as $field): ?>
                                            <th>{{$field}}</th>
                                        <?php endforeach; ?>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script type="text/javascript" charset="utf-8" async defer>

//ajax header need for deleted and updating data

    var table;
//datatables serverSide
    $('document').ready(function () {


        $("#start_date").datepicker({
            format: 'yyyy-mm-dd'

        });
         $("#end_date").datepicker({
            format: 'yyyy-mm-dd'

        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        table = $('#tblData').DataTable({
        stateSave: true,
                responsive: true,
                processing: true,
                serverSide: true,
                order: [0, 'desc'],
                ajax: window.location.href,
                columns: [
                {data: 'id', name: 'id'},
                <?php foreach ($fields as $field): ?>
                                    {data: "{{$field}}", name: "{{$field}}"},
                <?php endforeach; ?>
                {data: 'action', name: 'action', orderable: false, searchable: false}
                ], "drawCallback": function (settings) {
        $('.change-state').bootstrapToggle();
        }
    });
    // table.draw(false);

//deleting data
    $('#tblData').on('click', '.btnDelete[data-remove]', function (e) {
        e.preventDefault();
        var url = $(this).data('remove');
        swal({
            title: "Are you sure want to remove this notification?",
            text: "Data will be Temporary Deleted!",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Confirm",
            cancelButtonText: "Cancel",
            closeOnConfirm: false,
            closeOnCancel: false,
        },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: url,
                            type: 'DELETE',
                            dataType: 'json',
                            data: {method: '_DELETE', submit: true},
                            success: function (data) {
                                if (data == 'Success') {
                                    swal("Deleted!", "Category has been deleted", "success");
                                    table.ajax.reload(null, false);
                                }
                            }
                        });
                    } else {

                        swal("Cancelled", "You Cancelled", "error");
                    }

                });
    });



    $('#tblData').on('change', '.change-state', function (e) {
    e.preventDefault();
            var url = "<?= url(config('app.admin_prefix') . '/location/governorate/changestate/') ?>" + '/' + $(this).attr('data-id');
            var checked = $('#change-state-' + $(this).attr('data-id')).prop('checked');
            swal({
            title: "Are you sure want to change status",
                    type: "info",
                    showCancelButton: true,
                    confirmButtonClass: "btn-info",
                    confirmButtonText: "Confirm",
                    cancelButtonText: "Cancel",
                    closeOnConfirm: true,
                    closeOnCancel: true
            },
                    function (isConfirm) {
                    if (isConfirm) {
                    $.ajax({
                    url: url,
                            type: 'GET',
                            datatype: 'json',
                            data: {checked: checked},
                            success: function (data) {
                            console.log(data);
                                    table.ajax.reload(null, false);
                            }

                    });
                    }
                });
    });



    $('#checkid_units').on('click', function (e) {
        e.preventDefault();
        var id = [];
        $.each($(".delete-units:checkbox:checked"), function () {
            id.push($(this).attr('id'));
        });
        if (id.length == 0) {
            alert('please select one row ');
            return;
        }
        var url = "<?= url(config('app.admin_prefix') . '/multipledestroy_units') ?>";
        var checked = id;
        //alert($('#checkid' + $(this).attr('id')).prop('checked'));
        swal({
            title: "Are you sure want to change status",
            type: "info",
            showCancelButton: true,
            confirmButtonClass: "btn-info",
            confirmButtonText: "Confirm",
            cancelButtonText: "Cancel",
            closeOnConfirm: true,
            closeOnCancel: true
        },
                function (isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: url,
                            type: 'POST',
                            datatype: 'json',
                            data: {checked: checked},
                            success: function (data) {
                                console.log(data);
                                table.ajax.reload(null, false);
                            }

                        });
                    }
                });

    });

    $("#selectall_units").click(function(){
        $('input:checkbox').not(this).prop('checked', this.checked);
    });
    });


    function filterNotification()
    {
        if ($.fn.DataTable.isDataTable("#tblData")) {
            $('#tblData').DataTable().clear().destroy();
          }

        table = $('#tblData').DataTable({
            stateSave: true,
                    responsive: true,
                    processing: true,
                    serverSide: true,
                    order: [0, 'desc'],
                    ajax: {
                        url: "filterNotification",
                        type: 'post',
                        data: function (d) {
                            d.start_date        =   $("#start_date").val();
                            d.end_date          =   $("#end_date").val();
                            d.service_id        =   $("#service_id").val();
                            d.customer_type     =   $("#customer_type").val();
                            d.user_response     =   $("#user_response").val();

                        }
                    },
                    columns: [
                    {data: 'id', name: 'id'},
                    <?php foreach ($fields as $filter_field): ?>
                                        {data: "{{$filter_field}}", name: "{{$filter_field}}"},
                    <?php endforeach; ?>
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    ], "drawCallback": function (settings) {
            $('.change-state').bootstrapToggle();
            }
        });
    }

    $('#filterNotification').click(function(){
        $('#tblData').DataTable().draw(true);
    });













</script>
@endsection
