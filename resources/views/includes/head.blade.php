<?php
/**
 * template_head.php
 *
 * Author:phoenixcoded
 *
 * The first block of code used in every page of the template
 *
 */
$template = config('adminNav.template');
//dd($template);die('ss');
?>
<!--<title><?php // echo $template['title']  ?></title>-->
<title> {{ config('app.name') }}</title>
<link rel="shortcut icon" href="{{ asset('Fw-icon.png') }}" type="image/png">
<!-- HTML5 Shim and Respond.js IE9 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

<!-- Meta -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="description" content="<?php echo $template['description'] ?>">
<meta name="keywords" content="<?php echo $template['keywords'] ?>">
<meta name="author" content="<?php echo $template['author'] ?>">
<!-- Favicon icon -->
<link rel="shortcut icon" href="{{ asset('favicon.png') }}" type="image/x-icon">
<link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
<meta name="csrf-token" content="{{csrf_token()}}">

<!-- Google font-->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">

<!-- iconfont -->
<link rel="stylesheet" type="text/css" href="{{ asset('admin/icon/icofont/css/icofont.css') }}">

<!-- simple line icon -->
<link rel="stylesheet" type="text/css" href="{{ asset('admin/icon/simple-line-icons/css/simple-line-icons.css') }}">

<!-- Required Fremwork -->
<link rel="stylesheet" type="text/css" href="{{ asset('admin/css/bootstrap.min.css') }}">

<!-- Style.css -->
<link rel="stylesheet" type="text/css" href="{{ asset('admin/css/main.css') }}">

<!-- Responsive.css-->
<link rel="stylesheet" type="text/css" href="{{ asset('admin/css/responsive.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>

<!-- Dark layout -->
<!-- Uncomment this line for Dark layout
<link rel="stylesheet" type="text/css" href="assets/css/color/inverse.css" id="color">
-->
