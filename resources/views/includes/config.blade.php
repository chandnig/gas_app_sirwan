<?php

/**
 * config.php
 *
 * Author: phoenixcoded
 *
 * Configuration file. It contains variables used in the template as well as the primary navigation array from which the navigation is created
 *
 */
/* Template variables */
$template = array(
    'title' => 'Able Pro Responsive Bootstrap 4 Admin Template by Phoenixcoded',
    'author' => 'Phoenixcoded',
    'description' => 'Phoenixcoded',
    'keywords' => ', Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app',
    'active_page' => basename($_SERVER['PHP_SELF'])
);
$primary_nav = array(
    array(
        'name' => 'Navigation',
        'url' => 'header',
    ),
    array(
        'name' => 'Dashboard',
        'icon' => 'icon-speedometer',
        'url' => 'index.php',
        'sub' => array(
            array(
                'name' => 'Dashboard 1',
                'url' => 'index.php'
            ),
            array(
                'name' => 'Dashboard 2',
                'url' => 'dashboard2.php'
            ),
            array(
                'name' => 'Dashboard 3',
                'url' => 'dashboard3.php'
            ),
            array(
                'name' => 'Dashboard 4',
                'url' => 'dashboard4.php'
            )
        )
    ),
    array(
        'name' => 'Widget',
        'opt' => '<span class="label label-warning menu-caption">33+</span>',
        'url' => 'widget.php',
        'icon' => 'icon-grid',
        'sub' => 0
    ),
    array(
        'name' => 'Page Layout',
        'icon' => 'icon-film',
        'sub' => array(
            array(
                'name' => 'Static Layout',
                'url' => 'menu-static.php'
            ),
            array(
                'name' => 'Header and Sidebar Fixed',
                'url' => 'menu-fixed.php'
            ),
            array(
                'name' => 'Sidebar Sticky',
                'url' => 'menu-sidebar-sticky.php'
            ),
            array(
                'name' => 'Header Fixed',
                'url' => 'menu-header-fixed.php'
            ),
            array(
                'name' => ' Footer Fixed',
                'url' => 'menu-footer-fixed.php'
            ),
            array(
                'name' => ' Bottom Menu Fixed',
                'url' => 'menu-bottom.php'
            ),
            array(
                'name' => 'Box Layout',
                'url' => 'box-layout.php'
            ),
        )
    ),
    array(
        'name' => 'Components',
        'url' => 'header',
    ),
    array(
        'name' => 'UI Elements',
        'icon' => 'icon-briefcase',
        'sub' => array(
            array(
                'name' => 'Accordion',
                'url' => 'accordion.php'
            ),
            array(
                'name' => 'Button',
                'url' => 'button.php'
            ),
            array(
                'name' => 'Button FAB',
                'url' => 'button-fab.php'
            ),
            array(
                'name' => 'Label Badge',
                'url' => 'label-badge.php'
            ),
            array(
                'name' => 'Grid system',
                'url' => 'bootstrap-ui.php'
            ),
            array(
                'name' => 'Box Shadow',
                'url' => 'box-shadow.php'
            ),
            array(
                'name' => 'Color',
                'url' => 'color.php'
            ),
            array(
                'name' => 'Draggable',
                'url' => 'draggable.php'
            ),
            array(
                'name' => 'Light Box',
                'url' => 'light-box.php'
            ),
            array(
                'name' => 'List',
                'url' => 'list.php'
            ),
            array(
                'name' => 'Nestable',
                'url' => 'nestable.php'
            ),
            array(
                'name' => 'Notification',
                'url' => 'notification.php'
            ),
            array(
                'name' => 'Panels-Wells',
                'url' => 'panels-wells.php'
            ),
            array(
                'name' => 'Preloader',
                'url' => 'preloader.php'
            ),
            array(
                'name' => 'Range-Slider',
                'url' => 'range-slider.php'
            ),
            array(
                'name' => 'Rating',
                'url' => 'rating.php'
            ),
            array(
                'name' => 'Slider',
                'url' => 'slider.php'
            ),
            array(
                'name' => 'Tabs',
                'url' => 'tabs.php'
            ),
            array(
                'name' => 'Tree View',
                'url' => 'treeview.php'
            ),
            array(
                'name' => 'Tour',
                'url' => 'tour.php'
            ),
            array(
                'name' => 'Tooltips',
                'url' => 'tooltips.php'
            ),
            array(
                'name' => 'Typography',
                'url' => 'typography.php'
            ),
            array(
                'name' => 'Card',
                'url' => 'card.php'
            ),
            array(
                'name' => 'Footer',
                'url' => 'footer.php'
            ),
            array(
                'name' => 'Footer Center',
                'url' => 'footer-center.php'
            ),
            array(
                'name' => 'Footer Right',
                'url' => 'footer-right.php'
            ),
            array(
                'name' => 'Other',
                'url' => 'other.php'
            ),
        )
    ),
    array(
        'name' => 'Theme UI',
        'icon' => 'icon-picture',
        'sub' => array(
            array(
                'name' => 'Contact Card',
                'url' => 'contact-card.php'
            ),
            array(
                'name' => 'Contact Details',
                'url' => 'contact-details.php'
            ),
            array(
                'name' => 'Animation',
                'url' => 'animation.php'
            ),
            array(
                'name' => 'Dynamic Grid',
                'url' => 'dynamic-grid.php'
            ),
            array(
                'name' => 'Generic Class',
                'url' => 'generic-class.php'
            ),
            array(
                'name' => 'Grid Stack',
                'url' => 'gridstack.php'
            ),
            array(
                'name' => 'Modal',
                'url' => 'modal.php'
            ),
            array(
                'name' => 'Portlets',
                'url' => 'portlets.php'
            ),
            array(
                'name' => 'Sticky',
                'url' => 'sticky.php'
            ),
            array(
                'name' => 'Icon',
                'icon' => 'icon-arrow-right',
                'sub' => array(
                    array(
                        'name' => 'Font-Awesome Icons',
                        'url' => 'font-awesome.php'
                    ),
                    array(
                        'name' => 'Material Design Icons',
                        'url' => 'material-design-icons.php'
                    ),
                    array(
                        'name' => 'Simple Line Icons',
                        'url' => 'simple-line-icons.php'
                    ),
                    array(
                        'name' => 'Ion Icons',
                        'url' => 'ion-icon.php'
                    ),
                    array(
                        'name' => 'Ico Fonts Icons',
                        'url' => 'icofonts.php'
                    ),
                    array(
                        'name' => 'Weather Icons',
                        'url' => 'weather-icons.php'
                    ),
                    array(
                        'name' => 'TypIcons',
                        'url' => 'typicons-icons.php'
                    ),
                    array(
                        'name' => 'Flags',
                        'url' => 'flags.php'
                    )
                )
            ),
        )
    ),
    array(
        'name' => ' Charts &amp; Maps',
        'opt' => '<span class="label label-success menu-caption">New</span>',
        'icon' => 'icon-chart',
        'sub' => array(
            array(
                'name' => 'E-Charts',
                'url' => 'echart.php'
            ),
            array(
                'name' => 'Chart Js',
                'url' => 'chartjs.php'
            ),
            array(
                'name' => 'List Charts',
                'url' => 'list-charts.php'
            ),
            array(
                'name' => 'Float Charts',
                'url' => 'float-chart.php'
            ),
            array(
                'name' => 'Knob Charts',
                'url' => 'knob-chart.php'
            ),
            array(
                'name' => 'Morris Charts',
                'url' => 'morris-chart.php'
            ),
            array(
                'name' => 'nvd3 Charts',
                'url' => 'nvd3-chart.php'
            ),
            array(
                'name' => 'Peity Charts',
                'url' => 'peity-chart.php'
            ),
            array(
                'name' => 'Radial Charts',
                'url' => 'radial-chart.php'
            ),
            array(
                'name' => 'Rickshaw Charts',
                'url' => 'rickshaw-chart.php'
            ),
            array(
                'name' => 'Sparkline Charts',
                'url' => 'sparkline-chart.php'
            ),
            array(
                'name' => 'c3Chart Charts',
                'url' => 'c3chart-chart.php'
            ),
            array(
                'name' => 'Map Google',
                'url' => 'map-google.php'
            ),
            array(
                'name' => 'Map Vector',
                'url' => 'map-vector.php'
            )
        )
    ),
    array(
        'name' => 'Forms',
        'icon' => 'icon-book-open',
        'sub' => array(
            array(
                'name' => 'Form Elements Bootstrap',
                'url' => 'form-elements-bootstrap.php'
            ),
            array(
                'name' => 'Form Elements Material',
                'url' => 'form-elements-materialize.php'
            ),
            array(
                'name' => 'Form Elements Advance',
                'url' => 'form-elements-advance.php'
            ),
            array(
                'name' => 'Form Wizard',
                'url' => 'forms-wizard.php'
            ),
            array(
                'name' => 'Form Masking',
                'url' => 'form-mask.php'
            ),
            array(
                'name' => 'Form Validation',
                'url' => 'forms-validation.php'
            ),
            array(
                'name' => 'X-Editable',
                'url' => 'x-editable.php'
            ),
            array(
                'name' => 'File Upload',
                'url' => 'file-upload.php'
            ),
            array(
                'name' => 'Image Cropper',
                'url' => 'image-cropper.php'
            ),
        )
    ),
    array(
        'name' => 'Tables',
        'icon' => 'icon-list',
        'sub' => array(
            array(
                'name' => 'Basic Tables',
                'url' => 'basic-table.php'
            ),
            array(
                'name' => ' Data Tables',
                'url' => 'data-table.php'
            ),
            array(
                'name' => 'Responsive Tables',
                'url' => 'responsive-table.php'
            ),
            array(
                'name' => 'Editable Tables',
                'url' => 'editable-table.php'
            ),
            array(
                'name' => 'Foo Tables',
                'url' => 'foo-tables.php'
            )
        )
    ),
    array(
        'name' => 'More',
        'url' => 'header',
    ),
    array(
        'name' => 'Pages',
        'icon' => 'icon-docs',
        'sub' => array(
            array(
                'name' => 'Authentication',
                'sub' => array(
                    array(
                        'name' => 'Register 1',
                        'url' => 'register1.php'
                    ),
                    array(
                        'name' => 'Register 2',
                        'url' => 'register2.php'
                    ),
                    array(
                        'name' => 'Sign In/Up with Modal',
                        'url' => 'signin-up-modal.php'
                    ),
                    array(
                        'name' => 'Login 1',
                        'url' => 'login1.php'
                    ),
                    array(
                        'name' => 'Login 2',
                        'url' => 'login2.php'
                    ),
                    array(
                        'name' => 'Forgot Password',
                        'url' => 'forgot-password.php'
                    )
                )
            ),
            array(
                'name' => 'Landing page',
                'url' => 'landing-page.php'
            ),
            array(
                'name' => 'Lock Screen',
                'url' => 'lock-screen.php'
            ),
            array(
                'name' => 'Error 400',
                'url' => '400.php'
            ),
            array(
                'name' => 'Error 403',
                'url' => '403.php'
            ),
            array(
                'name' => 'Error 404',
                'url' => '404.php'
            ),
            array(
                'name' => 'Error 500',
                'url' => '500.php'
            ),
            array(
                'name' => 'Error 503',
                'url' => '503.php'
            ),
            array(
                'name' => 'Gallery',
                'url' => 'gallery.php'
            ),
            array(
                'name' => 'Sample Page',
                'url' => 'sample-page.php'
            ),
            array(
                'name' => 'Invoice',
                'url' => 'invoice.php'
            ),
            array(
                'name' => 'Blog',
                'url' => 'blog.php'
            ),
            array(
                'name' => 'Blog Detail',
                'url' => 'blog-detail.php'
            ),
            array(
                'name' => 'Search Result 1',
                'url' => 'search-result.php'
            ),
            array(
                'name' => 'Search Result 2',
                'url' => 'search-result2.php'
            ),
        )
    ),
    array(
        'name' => 'Apps',
        'icon' => 'icon-social-dropbox',
        'sub' => array(
            array(
                'name' => 'Task',
                'sub' => array(
                    array(
                        'name' => 'Task List',
                        'url' => 'task-list.php'
                    ),
                    array(
                        'name' => 'Task Board',
                        'url' => 'task-board.php'
                    ),
                    array(
                        'name' => 'Task Detailed',
                        'url' => 'task-detailed.php'
                    ),
                    array(
                        'name' => 'Issue List',
                        'url' => 'issue-list.php'
                    ),
                )
            ),
            array(
                'name' => 'Email',
                'sub' => array(
                    array(
                        'name' => 'Inbox',
                        'url' => 'inbox.php'
                    ),
                    array(
                        'name' => 'Compose',
                        'url' => 'compose.php'
                    ),
                    array(
                        'name' => 'Read Mail',
                        'url' => 'read-mail.php'
                    )
                )
            ),
            array(
                'name' => 'To Do',
                'url' => 'todo.php'
            ),
            array(
                'name' => 'Chat',
                'url' => 'chat.php'
            ),
            array(
                'name' => ' CK Editor',
                'url' => 'ck-editor.php'
            ),
            array(
                'name' => 'wysiwyg Editor',
                'url' => 'wysiwyg-editor.php'
            ),
            array(
                'name' => 'Ace Editor',
                'url' => 'ace-editor.php'
            ),
        )
    ),
    array(
        'name' => 'CRM',
        'icon' => 'icon-user-follow',
        'sub' => array(
            array(
                'name' => 'CRM-Dashbord',
                'url' => 'crm-dashboard.php'
            ),
            array(
                'name' => 'CRM-Contact',
                'url' => 'crm-contact.php'
            ),
        )
    ),
    array(
        'name' => 'E-Commerce',
        'icon' => 'icon-basket-loaded',
        'sub' => array(
            array(
                'name' => 'Product',
                'url' => 'product.php'
            ),
            array(
                'name' => 'Product Detail',
                'url' => 'product-detail.php'
            ),
            array(
                'name' => 'Product List',
                'url' => 'product-list.php'
            ),
            array(
                'name' => 'Product Edit',
                'url' => 'product-edit.php'
            ),
        )
    ),
    array(
        'name' => 'Social',
        'icon' => 'icon-share',
        'sub' => array(
            array(
                'name' => 'Profile Social',
                'url' => 'profile-social.php'
            ),
            array(
                'name' => 'Timeline Social',
                'url' => 'timeline-social.php'
            ),
            array(
                'name' => 'Wall',
                'url' => 'wall.php'
            ),
            array(
                'name' => 'Message',
                'url' => 'message.php'
            ),
        )
    ),
    array(
        'name' => 'Extras',
        'icon' => 'icon-present',
        'sub' => array(
            array(
                'name' => 'Email Template',
                'opt' => '<span class="label label-primary menu-arrow-caption">4</span>',
                'sub' => array(
                    array(
                        'name' => 'Email Confirm',
                        'url' => 'email-confirm.php'
                    ),
                    array(
                        'name' => 'Email Order Track',
                        'url' => 'email-order-track.php'
                    ),
                    array(
                        'name' => 'Email OTP',
                        'url' => 'email-otp.php'
                    ),
                    array(
                        'name' => 'Email Signup',
                        'url' => 'email-signup.php'
                    ),
                )
            ),
            array(
                'name' => 'Timeline',
                'url' => 'timeline.php'
            ),
            array(
                'name' => 'Maintainace',
                'url' => 'maintainance.php'
            ),
            array(
                'name' => 'Coming Soon',
                'url' => 'coming-soon.php'
            ),
            array(
                'name' => 'Profile',
                'url' => 'profile.php'
            ),
            array(
                'name' => 'Pricing',
                'url' => 'pricing.php'
            ),
            array(
                'name' => 'Full Calender',
                'url' => 'full-calender.php',
                'opt' => '<span class="label label-danger menu-caption">Hot</span>',
            ),
        )
    ),
    array(
        'name' => 'Menu Level',
        'url' => 'header',
    ),
    array(
        'name' => 'Menu Level 1',
        'icon' => 'icofont icofont-company',
        'sub' => array(
            array(
                'name' => 'Level Two'
            ),
            array(
                'name' => 'Level Two',
                'sub' => array(
                    array(
                        'opt' => '',
                        'name' => 'Level Three',
                    ),
                    array(
                        'opt' => 'Level Three',
                        'name' => 'Level Three',
                        'sub' => array(
                            array(
                                'name' => 'Level Four'
                            ),
                            array(
                                'name' => 'Level Four'
                            )
                        )
                    )
                )
            )
        )
    )
);
?>