<?php
/**
 * script.php
 *
 * Author: phoenixcoded
 *
 * All vital JS scripts are included here
 *
 */
?>
<script src="{{ asset('admin/js/jquery-3.1.1.min.js') }}"></script>
<script src="{{ asset('admin/js/jquery-ui.min.js') }}"></script>
<script src="{{ asset('admin/js/tether.min.js') }}"></script>

<!-- Required Fremwork -->
<script src="{{ asset('admin/js/bootstrap.min.js') }}"></script>

<!-- waves effects.js -->
<script src="{{ asset('admin/plugins/waves/js/waves.min.js') }}"></script>

<!-- Scrollbar JS-->
<script src="{{ asset('admin/plugins/slimscroll/js/jquery.slimscroll.js') }}"></script>
<script src="{{ asset('admin/plugins/slimscroll/js/jquery.nicescroll.min.js') }}"></script>

<!--classic JS-->
<script src="{{ asset('admin/plugins/search/js/classie.js') }}"></script>

<!-- notification -->
<script src="{{ asset('admin/plugins/notification/js/bootstrap-growl.min.js') }}"></script>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>

<!-- custom js -->
<script type="text/javascript" src="{{ asset('admin/js/main.js') }}"></script>
<script type="text/javascript" src="{{ asset('admin/pages/elements.js') }}"></script>
<!--<script src="{{ asset('admin/js/menu.js') }}"></script>-->

