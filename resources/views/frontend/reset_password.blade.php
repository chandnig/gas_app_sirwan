@extends('frontend.layouts.forget-head')
@section('content')
<style>
        .mastercard{
        background-size: cover;
        text-align: center;
        display: flex;
        align-items: center;
        }
</style>
        <div class="col-md-4 bg-white mx-auto px-0" style="border: 1px solid #062d49" >
                <div class="py-3 mx-0 text-white text-center" style="height:10%;background-color: #062d49;{{__('login-page.inner_header_border')}}">
                        <h4> {{ __('login-page.change_my_password') }}</h4>
                </div>
                <form method="POST" action="{{ route('login.save_changes') }}">
                        {!! csrf_field() !!}
                    <div class="mastercard" style="height:80%;">
                            <div class="container">
                                <div class="col-md-12">
                                    <input type="text" name="password"class="form-control mobile {{ __('login-page.text') }}" placeholder="{{ __('login-page.new_password') }}">
                                </div><br/>
                                <div class="col-md-12">
                                    <input type="text" name="confirm_password" class="form-control mobile {{ __('login-page.text') }}" placeholder="{{ __('login-page.confirm_password') }}">
                                </div>
                                <input type="hidden" name="mobile_number" value="{{ $mobile_number }}">
                            </div>
                        </div>
                    <div class="py-3 mx-0 text-white  pb-2 row" style="height:10%;background-color:#062d49;bottom:0;{{ __('login-page.inner_footer_border') }}">
                    <div class="col-md-12   text-center">
                        <input type="submit" class="text-white" style="background-color:#062d49;border:none;font-size:24px" value="{{ __('login-page.save_changes') }}">
                        {{--  <a href="{{ route('login.user')}}" style="color:white"><h4>{{ __('login-page.save_changes') }}</h4></a>  --}}
                    </div>
                </form>
        </div>
@stop

