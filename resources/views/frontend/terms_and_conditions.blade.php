@extends('frontend.layouts.services-head')
@section('content')
<div id="site-wrapper">
    <div id="site-canvas">
        <header>
            <!-- <div class="row header py-4">
                <h4 class="col text-center">{{__('home-head.term_condition')}}</h4>
            </div> -->
            <div class="py-4 mx-0 mt-0 text-white text-center" style="height:10%;
            background-color: #062d49;{{__('login-page.inner_header_border')}}">
                        <h4>{{__('home-head.term_condition')}}</h4>
            </div>
        </header>
        <?php
            $name = 'tnc';
            if (app()->getLocale() == 'ar'):
                $name .= '_ar';
            endif;
            if (app()->getLocale() == 'ar'):
            $image .= '_ar';
            endif;
            if (app()->getLocale() == 'kr'):
            $name .= '_kr';
            endif;
        ?>

        <div class="container-fluid home">

                <div class=" pt-3 container product-details" style="line-height:30px;text-align:justify">
                        <div class="" >
                           {{ strip_tags($terms->$name) }}
                        </div>
                </div>
            </div>

        </div>

    </div>
    <div class="row">

            <div class="col-8 mx-auto">
                <a type="button" href="{{ URL::previous() }}" class="btn back_btn_profile py-2 btn-block text-white">{{ __('login-page.back') }}</a>
            </div>


    </div>
</div>

<br/><br/>


@stop



