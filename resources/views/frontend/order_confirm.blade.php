@extends('frontend.layouts.order-gas')
@section('content')
<style>


    .inputGroup {
        background-color: #fff !important;
        display: block !important;
        margin: 10px 0 !important;
        position: relative !important;
        left: 30px;
          top: 6px;
      }
      .inputGroup_arabic {
              background-color: #fff !important;
              display: block !important;
              margin: 10px 0 !important;
              position: relative !important;
              left: 60px;
              float: left;
              top: 4px;

      }
      .inputGroup input {
        width: 32px;
        height: 32px;
        order: 1;
        z-index: 2;
        position: absolute;
        right: 30px;
        top: 50%;
        transform: translateY(-50%);
        cursor: pointer;
      }
      .inputGroup_arabic input {
        width: 32px;
        height: 32px;
        order: 1;
        z-index: 2;
        position: absolute;
        right: 30px;
        top: 50%;
        transform: translateY(-50%);
        cursor: pointer;
      }


      {{--  [type="radio"]:not(:checked) {
          border: 2px solid red;
        border-radius: 25px;
        background-color: red;
        border-color:green;

      }  --}}

      .order_border{
          border:2px solid #d9d9d9;
          padding:10px;
          border-radius:10px;
      }
      /* Create a custom radio button */

      input[type='radio'] {
          -webkit-appearance: none;
          width: 20px;
          height: 20px;
          border-radius: 50%;
          outline: none;
          border: 4px solid lightgreen;
          padding: 12px;
          top: -4px;
      }

      input[type='radio']:before {
          content: '';
          display: block;
          width: 60%;
          height: 60%;
          margin: 20% auto;
          border-radius: 50%;
      }

      input[type="radio"]:checked:before {
          background: green;

      }

      input[type="radio"]:checked {
          border-color: lightgreen;
          padding: 11px;
          background: lightgreen;
      }

      .role {
          margin-right: 80px;
          margin-left: 20px;
          font-weight: normal;
      }

      .checkbox label {
          margin-bottom: 20px !important;
      }

      .roles {
          margin-bottom: 40px;
      }
</style>
  <?php
               $name = 'name';
               $last_name = 'last_name';

               if (app()->getLocale() == 'ar'):
               $name .= '_ar';
               $last_name .= '_ar';
               endif;
               if (app()->getLocale() == 'kr'):
               $name .= '_kr';
               $last_name .= '_kr';
               endif;
               ?>
<div id="site-wrapper">
   <div id="site-canvas" class="order-by-quantity order-gas-quant">
      <header>
         <div class="py-4 mx-0 mt-0 text-white text-center" style="background-color:#062d49;{{__('login-page.inner_header_border')}}">
            <h4>{{__('order-gas.order')}} {{ $service_name }}</h4>
        </div>
      </header>

      {{-- =================SUPPLIER PHONE NO AND NAME ============================= --}}
      <div class="col-md-12 order-quantity mt-4 {{ __('login-page.text') }}">
         <span style="font-size:1.0rem">{{ __('order-gas.supplier_phone_no') }}</span>
         <div class="form-group  mt-3 ">
            <form method="POST" action="{{route('order.search_supplier')}}">
               {!! csrf_field() !!}
               <input type="hidden" value="{{ $service_name }}" name="service_name" />
               <div class="row">
                @if (app()->getLocale() == 'kr' || app()->getLocale() == 'ar')
                    <div class="col-2">
                        <input type="submit" class=" order_border btn btn-warning" value="{{ __('order-gas.search') }}"/>
                     </div>
                    <div class="col-10">
                        <input type="number" name="supplier_mobile_no" class="order_border form-control text-right supplier {{ ('login-page.text') }}" placeholder="{{ __('order-gas.enter_supplier_number') }}" min="0" value="{{ $draft_mobile_number }}"/>
                        <input type="hidden" value="{{$address_id}}" name="address_id">
                        <input type="hidden" value="{{$quantity}}"   name="quantity">
                        <input type="hidden" value="{{$service_id}}" name="service_id">
                    </div>
                @else
                    <div class="col-8">
                        <input type="number" name="supplier_mobile_no" class=" order_border form-control supplier {{ ('login-page.text') }}" placeholder="{{ __('login-page.phone_number') }}" min="0" value="{{ $draft_mobile_number }}"/>
                        <input type="hidden" value="{{$address_id}}" name="address_id">
                        <input type="hidden" value="{{$quantity}}" name="quantity">
                        <input type="hidden" value="{{$service_id}}" name="service_id">
                     </div>
                     <div class="col-4">
                        <input type="submit" class=" order_border btn btn-warning" value="{{ __('order-gas.search') }}"/>
                     </div>
                @endif
               </div>
            </form>
         </div>
         <!-- Full form submit -->
         <form method="POST" action="{{route('order.submit_confirm')}}">
            {!! csrf_field() !!}
            <div class="form-group input-group">
               <div class="" style="width:100%">
                  @if(count($supplier_exist) > 0 )
                  <h6 class="text-success">{{ __('order-gas.supplier_found_success') }} </h6>
                  <input type="text" class="order_border form-control supplier bg-white {{ __('login-page.text') }}" placeholder="Supplier Name" disabled value="{{$supplier_exist->name}}">
                  <input type="hidden" name="supplier_id" value="{{$supplier_exist->id}}"/>
                  @else
                  <h6 class="text-danger">{{ __('order-gas.no_supplier_selected') }}</h6>
                  <input type="text" class=" order_border form-control supplier bg-white {{ __('login-page.text') }}" placeholder="{{ __('order-gas.supplier_name') }}" disabled value="{{$supplier_exist->name}}">
                  @endif
                  <input type="hidden" value="{{$address_id}}" name="confirm_address">
                  <input type="hidden" value="{{$quantity}}" name="confirm_quantity">
                  <input type="hidden" value="{{$service_id}}" name="confirm_service_id">
               </div>
            </div>
      </div>

    @if(count($supplier_exist) > 0 )
        <div class="col-md-12 order-quantity profile {{ __('login-page.text') }}" hidden>
    @else
        <div class="col-md-12 order-quantity profile {{ __('login-page.text') }}">
    @endif

    @if(count($supplierArray) > 0)
        <span style="font-size:1.0rem">{{ __('order-gas.available_supplier') }}</span>
    @else
        <span style="font-size:1.0rem">{{ __('order-gas.no_available_supplier') }}</span>
    @endif

      @foreach($supplierArray as $supplier)
      <ul class="container mb-3 profile order_border mt-3" >
        <li class="row">
            <div class="col-12" style="display:flex;">
            @if (app()->getLocale() == 'kr' || app()->getLocale() == 'ar')
                <div class="col-3" style="text-align: center;">
                    <div class="inputGroup_arabic ">
                        <input id="radio1" value="{{$supplier->user_id}}" name="supplier_id" type="radio"/>
                    </div>
                </div>
                    <div class="col-9 py-1">
                        <a href=""><span style="font-size:0.9rem;color: gray;line-height: 1.2;font-weight:500;">{{ $supplier->$name }} {{ $supplier->$last_name }}</span> </a>
                    </div>
                @else
                <div class="col-9 py-1">
                    <a href="">
                        <span style="font-size:0.9rem;color: gray;line-height: 1.2;font-weight:500;">{{ $supplier->$name }} {{ $supplier->$last_name }}</span>
                    </a>
                </div>
                <div class="col-3" style="text-align: right;">
                    <div class="inputGroup">
                        <input id="radio1" value="{{$supplier->user_id}}" name="supplier_id" type="radio"/>
                    </div>
                </div>
            @endif
            </div>
        </li>
      </ul>
      @endforeach
      </div>
        <div class="col-md-6 mx-auto  mt-4 row">
          <div class="col-12">
            <input type="submit" class="order_border form-control btn text-white" value=" {{ __('order-gas.confirm') }} " style="background-color:#4a6ee2" />
          </div>
        </div>
      </form>

   </div><br/><br/>
   <div style="position: sticky;  bottom:0%;  z-index: 999; text-align:right;{{ __('home-head.green_back_rotate') }}" class="pr-4 pb-2">
        <a href="{{ route('home.dashboard')}}"><img src="{{asset('images/right.png')}}" height="50px" width="50px"></a>
    </div>
</div>


@stop
