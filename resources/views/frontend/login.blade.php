@extends('frontend.layouts.login-head')
@section('content')
<?php
if(app()->getLocale() == 'kr' || app()->getLocale() == 'ar'):
$container_fluid = "container-fluid_ar";
else:
$container_fluid = "container-fluid";
endif;
?>
<div class="{{ $container_fluid }}" >
    <div class="col-md-12 row pt-2">
        <div class="col-md-7 col-sm-7">

        </div>
        <div class="col-md-5 col-sm-5" >
               <a href="lang/ar"> <img src="{{ asset('images/ar_sm.png')}}"/></a>
               <a href="lang/kr"><img src="{{ asset('images/ku_sm.png')}}"/></a>
               <a href="lang/en"><img src="{{ asset('images/en_sm.png')}}"/></a>
        </div>
    </div>
    <header class="center">
        <img src="{{ asset('images/logo-dark.png')}}">
        <h5 style="color:#91abb9">{{__('login-page.login_to_account')}}</h5>
    </header>
    @if ($errors->any())
    @foreach ($errors->all() as $error)
        <div class="alert alert-danger h6 text-center">{{$error}}</div>
    @endforeach
   @endif

    <form class="login" id="frmDataEdit" role="form" method="POST" action="{{ route('login.user.auth') }}">
        {!! csrf_field() !!}
        <div class="wrap">
            <div>
                <div class="form-group {{ $errors->has('mobile_number') ? ' has-error' : '' }}">
                    <label for="mobile_number" style="right:{{ __('login-page.right_shift') }}">{{__('login-page.phone_number')}}</label>
                    <input id="mobile_number" type="text" name="mobile_number" class="cool {{ __('login-page.text') }}" autocomplete="off" required >
                    <p class=" text-danger hidden"></p>
                </div>
            </div>
            <div>
                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" style="right:{{ __('login-page.right_shift') }}">{{__('login-page.password')}}</label>
                    <input id="password" type="password" name="password" class="cool {{__('login-page.text')}}" autocomplete="off" required>
                    <p class=" text-danger hidden"></p>
                </div>
            </div>
            <div class="{{ __('login-page.forgot_shift') }}">
            <h6><a style="color:#91abb9;border-bottom: 1px solid #91abb9;text-decoration:none" class="pb-2" href="{{ route('login.forgot')}}" >{{ __('login-page.forgot_password') }}</a></h6>
            </div>
            {{-- <button type="button" id="login" class="btn btn-primary sign">{{__('login-page.signin')}}</button> --}}
            <div class="col-md-12">
                <input type="submit" id="login"  class="w-100 p-2 btn text-white" style="background-color:#4a6de1" value="{{ __('login-page.signin') }}" />
            </div>
        </div>
    </form>
</div>
@stop

