@extends('frontend.layouts.order-gas')
@section('content')
<style>
    #loading{
        display:none;
    }
    .header {
    border-radius: 0 0 83px 0;
    }

    .supplier
    {
    border: 2px solid #d9d9d9 !important;
    height: 50px !important;
    border-radius: 10px !important;
    color:#707070;
    }
.inputGroup {
  background-color: #fff !important;
  display: block !important;
  margin: 10px 0 !important;
  position: relative !important;
  left: 30px;
    top: 6px;
}
.inputGroup_arabic {
        background-color: #fff !important;
        display: block !important;
        margin: 10px 0 !important;
        position: relative !important;
        left: 60px;
        float: left;
        top: 4px;

}
.inputGroup input {
  width: 32px;
  height: 32px;
  order: 1;
  z-index: 2;
  position: absolute;
  right: 30px;
  top: 50%;
  transform: translateY(-50%);
  cursor: pointer;
}
.inputGroup_arabic input {
  width: 32px;
  height: 32px;
  order: 1;
  z-index: 2;
  position: absolute;
  right: 30px;
  top: 50%;
  transform: translateY(-50%);
  cursor: pointer;
}


{{--  [type="radio"]:not(:checked) {
    border: 2px solid red;
  border-radius: 25px;
  background-color: red;
  border-color:green;

}  --}}

.order_border{
    border:2px solid #d9d9d9;
    padding:10px;
    border-radius:10px;
}
/* Create a custom radio button */

input[type='radio'] {
    -webkit-appearance: none;
    width: 20px;
    height: 20px;
    border-radius: 50%;
    outline: none;
    border: 4px solid lightgreen;
    padding: 12px;
    top: -4px;
}

input[type='radio']:before {
    content: '';
    display: block;
    width: 60%;
    height: 60%;
    margin: 20% auto;
    border-radius: 50%;
}

input[type="radio"]:checked:before {
    background: green;

}

input[type="radio"]:checked {
    border-color: lightgreen;
    padding: 11px;
    background: lightgreen;
}

.role {
    margin-right: 80px;
    margin-left: 20px;
    font-weight: normal;
}
.checkbox label {
    margin-bottom: 20px !important;
}
.roles {
    margin-bottom: 40px;
}
</style>
    <?php
        $name = 'name';
        $measurement_unit = 'measurement_unit';
        if (app()->getLocale() == 'ar'):
            $name .= '_ar';
            $measurement_unit .= '_ar';
        endif;
        if (app()->getLocale() == 'kr'):
            $name .= '_kr';
            $measurement_unit .= '_kr';
        endif;
     ?>
<div id="site-wrapper">
    <div id="site-canvas" class="order-by-quantity order-gas-quant">
            <header>
                <!-- <div class="row header py-4">
                    <h4 class="col">{{ __('order-gas.order') }} {{ $service_data[0]->$name}}</h4>
                </div> -->
                <div class="py-4 mx-0 mt-0 text-white text-center" style="height:10%;background-color:#062d49;{{__('login-page.inner_header_border')}}">
                        <h4>{{ __('order-gas.order') }} {{ $service_data[0]->$name}}</h4>
                </div>
            </header>
            <form method="POST" role="form"  action="{{ route('place_order.step1') }}">
                    {!! csrf_field() !!}
                <div class="col-md-12 order-quantity profile mt-5 {{ __('login-page.text') }}">
                    <span style="font-size:1.0rem">{{ $service_data[0]->$name }}</span>
                        <div class="row mt-3">
                                <input type="hidden" name="service_name" value="{{ $service_data[0]->$name }}"/>
                                @if (app()->getLocale() == 'kr' || app()->getLocale() == 'ar')
                                    <div class="col-8 text-center ">
                                                <input type="text" class="form-control supplier {{ __('login-page.text') }}" style="font-size:1.2rem" value=" {{ $service_data[0]->$measurement_unit}} {{ $service_data[0]->$name}} "   disabled style="background-color:#f8f8f8"/>
                                        </div>
                                        <div class="col-4 text-center px-3">
                                                <input type="number"  name="quantity"  min="{{ $service_data[0]->default_qty}}" class="form-control supplier {{ __('login-page.text') }}" value="{{ $service_data[0]->default_qty}}"
                                                style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);"/>
                                        </div>

                                @else
                                    <div class="col-8 text-center">
                                            <input type="text" class="form-control supplier {{ __('login-page.text') }}"  style="font-size:1.2rem" value=" {{ $service_data[0]->$name}}  {{ $service_data[0]->$measurement_unit}}"  disabled style="background-color:#f8f8f8"/>
                                    </div>
                                    <div class="col-4 px-3">
                                            <input type="number" name="quantity" min="{{ $service_data[0]->default_qty}}" class="form-control supplier text-right" value="{{ $service_data[0]->default_qty}}"
                                            style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);"/>
                                    </div>
                                @endif
                        </div>
                </div>
                <div class="col-md-12 order-quantity profile {{ __('login-page.text') }} ">
                        <span style="font-size:1.0rem">{{ __('order-gas.deliver_to_address') }}</span>

                        @foreach($user_data as $address)
                            <ul class="container mb-3 profile order_border mt-3" >
                                <li class="row">
                                    <div class="col-12" style="display:flex;">
                                        @if (app()->getLocale() == 'kr' || app()->getLocale() == 'ar')
                                            <div class="col-3">
                                                <div class="inputGroup_arabic">
                                                    <input id="radio1" value="{{ $address->id }}" name="address" type="radio" checked/>
                                                </div>
                                            </div>
                                            <div class="col-9 py-1">
                                                <a href=""><span style="font-size:0.9rem;color: gray;line-height: 1.2;font-weight:500;">{{ $address->name }}</span> </a>
                                            </div>
                                        @else
                                            <div class="col-9 py-1">
                                                <a href=""><span style="font-size:0.9rem;color: gray;line-height: 1.2;font-weight:500;">{{ $address->name }}</span> </a>
                                            </div>
                                            <div class="col-3">
                                                <div class="inputGroup">
                                                    <input id="radio1"  value="{{ $address->id }}" name="address" type="radio" checked/>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                </li>
                            </ul>
                        @endforeach
                </div>
                 <input type="hidden" name="service_id" value="{{ $service_data[0]->id}}">
                <div class="col-md-12 order-quantity profile">
                    <ul class="container btn text-white s py-0 profile order_border mt-2" style="background-color:#4a6ee2">
                      <input type="submit" style="width:100%;outline:none" value=" {{ __('login-page.next') }}" class="text-white" />
                    </ul>
                </div>
            </form>
    </div>

    <div style="position: sticky;  bottom:0%;  z-index: 999; text-align:right;{{ __('home-head.green_back_rotate') }}" class="pr-4 pb-2">
            <a href="{{ route('home.dashboard')}}"><img src="{{asset('images/right.png')}}" height="50px" width="50px"></a>
    </div>
</div>

@stop

