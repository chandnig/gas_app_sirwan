
<!-- https://codepen.io/ncerminara/pen/quJpi -->
<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="shortcut icon" href="{{ asset('images/logo.png')}}" type="image/x-icon"/>

        <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/bootstrap.min.css')}}">
	 <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/custom-styles.css')}}">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<title>title text</title>
</head>
<body>
     @yield('content')
     </body>
     {{-- <!-- Modal -->
     <div class="modal fade" id="myModal" role="dialog">
               <div class="modal-dialog">
                  <div id="modal-body">
                  </div>
     </div> --}}
   <footer>
        {{-- <script src="{{ asset('js/frontend/jquery-3.4.0.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('js/frontend/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('js/frontend/scripts.js')}}"></script> --}}
    </footer>



