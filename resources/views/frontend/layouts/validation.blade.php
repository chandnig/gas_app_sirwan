<html lang="{{ app()->getLocale() }}">
    <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
            <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
          <style>
                .mastercard{
                background-size: cover;
                text-align: center;
                display: flex;
                align-items: center;
                height: 20%;
                }
                .mobile{
                border:2px solid #bce0fd;
                background-color: #f1f9ff;
                height:60px;
                border-radius:20px;
                }
                .box {
             border: 1px solid #bce0fd;
              position: relative;
              background-color: #f1f9ff;
              top: 13px;
              z-index: 1;
              color: #6c6b7d;
              left: 112px;
             padding: 2px 10px 3px 10px;
                }
                .footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
                color: white;
                text-align: center;
                }

                    /* Extra small devices (phones, 600px and down) */
    @media only screen and (max-width: 600px) {
        .box{
                left:85px;
            }
    }

    /* Small devices (portrait tablets and large phones, 600px and up) */
     /* @media only screen and (min-width: 600px) {
        .box{
                left:65px;
        } */

    }

    /* Medium devices (landscape tablets, 768px and up)*/
    /* @media only screen and (min-width: 768px) {
        .box{git
                left:65px;
            }
    }   */

    /* Large devices (laptops/desktops, 992px and up) */
    @media only screen and (min-width: 992px) {

    }

    /* Extra large devices (large laptops and desktops, 1200px and up) */
    @media only screen and (min-width: 1200px) {

    }


    @media screen(-webkit-min-device-pixel-ratio: 2),(min-resolution: 192dpi)and (device-          width: 1440px)and (orientation: portrait){
        {
            .box{
                left:70px;
            }
        }
             </style>
        <!--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js"></script>-->

        <script src="{{ asset('js/frontend/jquery-3.4.0.min.js')}}"></script>
    </head>
    <body class="signin">
        @yield('content')
    </body>


    <footer>
            {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> --}}
        {{-- <script type="text/javascript" src="{{ asset('js/frontend/scripts.js')}}"></script> --}}
    </footer>

</html>
