
<!-- https://codepen.io/ncerminara/pen/quJpi -->
<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" href="{{ asset('images/logo-dark.png')}}" type="image/x-icon"/>
        <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/frontend/custom-styles.css')}}">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <div id="site-wrapper">
            @if( app()->getLocale() == "ar" || app()->getLocale() == "kr")
                <div id="site-canvas_ar">
            @else
                <div id="site-canvas">
            @endif
                <header>
                    <div id="site-menu" style="{{ __('home-head.site_menu_right') }}">
                        <div class="nav-logo" style="{{__('home-head.side_menu_border')}};height:23%" >
                                <!-- <a href="#" class="toggle-nav"><i class="fa fa-times"></i></a> -->
                            <a href=""><img class="logo" src="{{asset('images/logo-dark.png')}}" height="100px"></a>
                        </div>
                            @if( app()->getLocale() == "en")
                            <ul>
                                <li><a href="{{route('home.profile')}}"><img src="{{asset('images/profile.png')}}">{{__('home-head.my_profile')}}</a></li>
                                <li><a href="{{route('home.service')}}"><img src="{{asset('images/order.png')}}">{{__('home-head.my_order')}}</a></li>
                                <li><a href="{{route('home.terms_and_condtions')}}"><img src="{{asset('images/terms.png')}}">{{__('home-head.term_condition')}}</a></li>
                                <li><a href="{{ route('user.logout')}}"><img src="{{asset('images/profile.png')}}"> {{__('home-head.logout')}}</a></li>
                            </ul>
                            @elseif(app()->getLocale() == "ar" || app()->getLocale() == "kr")
                            <ul style="" class="pr-4 text-right">
                                <li ><a href="{{route('home.profile')}}">{{__('home-head.my_profile')}} <img src="{{asset('images/profile.png')}}"></a></li>
                                <li><a href="{{route('home.service')}}">{{__('home-head.my_order')}} <img src="{{asset('images/order.png')}}"></a></li>
                                <li><a href="{{ route('home.terms_and_condtions')}}">{{__('home-head.term_condition')}} <img src="{{asset('images/terms.png')}}"></a></li>
                                <li><a href="{{ route('user.logout')}}"> {{__('home-head.logout')}} <img src="{{asset('images/profile.png')}}"></a></li>
                            </ul>
                            @endif
                        <div class="nav-footer" style="">
                            <ul>
                                <li><a href="https://www.facebook.com/gasapplication/?modal=admin_todo_tour"><img src="{{asset('images/fb.png')}}"></a></li>
                                <li><a href="https://www.instagram.com/gasapplication/?hl=en"><img src="{{asset('images/instagram.png')}}"></a></li>
                                <li><a href="https://www.youtube.com/channel/UCOCwbGB70Jn6y0qyRmoHKWw?view_as=subscriber"><img src="{{asset('images/youtube.png')}}"></a></li>
                            </ul>
                        </div>
                    </div>
                    @if(app()->getLocale() == "ar" || app()->getLocale() == "kr")
                    <div class="row py-4 mx-0 mt-0 text-white" style="background-color: #062d49;border-radius: 0px 0px 0px 56px;">
                    <div class="col-8 text-right"><a href=""><img src="{{asset('images/logo-dark.png')}}" height="100px"></a></div>
                        <div class="col-4 text-right" ><a href="#" class="toggle-nav" id="nav-toggle"><i class="fa fa-bars"></i></a></div>
                    </div>
                    @else
                    <div class="row py-4 mx-0 mt-0 text-white" style="background-color: #062d49;border-radius: 0px 0px 56px 0px;">
                        <div class="col-4" ><a href="#" class="toggle-nav" id="nav-toggle"><i class="fa fa-bars"></i></a></div>
                        <div class="col-8"><a href=""><img src="{{asset('images/logo-dark.png')}}" height="100px"></a></div>
                    </div>
                    @endif
                </header>
                @yield('content')
            </div>
        </div>
<!--        <div class="modal fade" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">

                     Modal Header
                    <div class="modal-header">
                        <h4 class="modal-title">Notifications</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                     Modal body
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-8" p-l:5px>
                                <p>Supplier of 2 Ltr Petrol in your section.</p>
                                <button class="btn btn-success">View Detail</button>
                            </div>
                            <div class="col-xs-4">
                                <span class="badge btn btn-danger">2</span>
                                <img src="{{asset('images/map.png')}}">
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xs-8">
                                <p>Supplier of 2 Ltr Petrol in your section.</p>
                                <button class="btn btn-success">View Detail</button>
                            </div>
                            <div class="col-xs-4">
                                <span class="badge btn btn-danger">2</span>
                                <img src="{{asset('images/map.png')}}">
                            </div>
                        </div>
                        <hr>

                    </div>
                </div>
            </div>
        </div>-->
    </body>
    <footer>
        <script src="{{ asset('js/frontend/jquery-3.4.0.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('js/frontend/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('js/frontend/scripts.js')}}"></script>
    </footer>

</html>
