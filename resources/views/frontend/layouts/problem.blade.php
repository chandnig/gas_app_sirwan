
<!-- https://codepen.io/ncerminara/pen/quJpi -->
<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script> --}}

	<title>title text</title>
</head>
<body>
     @yield('content')
     </body>
   <footer>
        {{-- <script src="{{ asset('js/frontend/jquery-3.4.0.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('js/frontend/bootstrap.min.js')}}"></script>
        <script type="text/javascript" src="{{ asset('js/frontend/scripts.js')}}"></script> --}}
    </footer>
