@extends('frontend.layouts.profile-head')
@section('content')
<?php
        $name = 'name';
        $image="image";
        $measurement_unit = 'measurement_unit';
        if(app()->getLocale() == 'ar'):
             $name .= '_ar';
             $image .= '_ar';
        endif;
        if(app()->getLocale() == 'kr'):
            $name .= '_kr';
            $image .= '_kr';
        endif;
        ?>
 <div id="site-wrapper">
	<div id="site-canvas" class="order-by-quantity">
		<header>
			
                        <div class="py-4 mx-0 mt-0 text-white text-center" style="height:10%;background-color:#062d49;{{__('login-page.inner_header_border')}}">
                        <h4>{{__('order-water.title')}}</h4>
                </div>  
                </header>
<div class="orders">
<div class="container">
    <form class="" role="form" method="POST" action="{{ route('order.multiple_order') }}">
	    <div class="row">
            @foreach($child_services as $sub_service)
            @if(app()->getLocale() == 'en' )
            <div class="col-md-12 order-quantity">
                
                    <div class="row">
                       
                            {!! csrf_field() !!}
                            <div class="col-6 text-center">
                                    <h5>{{ $sub_service->$name }}</h5> <br/>
                                    <button type="button" class="btn btn-default">{{ $sub_service->$measurement_unit }}</button>
                            </div>
                            <div class="col-3 text-center">
                            <input type="checkbox"  id="{{ $sub_service->$name }}" value="{{$sub_service->id}}" name="id[]"/> <label for="{{ $sub_service->$name }}"></label>
                                <div class="edit-quantity ml-1"><input type="text" name="quantity[]" placeholder="0"></div>
                            </div>
                            <div class="col-3 pl-4 pt-3 text-center">
                                    <img src="{{ asset('uploads/services/'.$sub_service->$image)}}">
                            </div>
                    </div>

     
            </div>
            @elseif(app()->getLocale() == 'ar' ||app()->getLocale() == 'kr')
                     <div class="col-md-12 order-quantity">
                        <form class="" role="form" method="POST" action="{{ route('order.delete_order') }}">
                            <div class="row">
                            
                                    {!! csrf_field() !!}
                                <div class="col-3 text-center">
                                            <input type="checkbox"  id="{{ $sub_service->$name }}" value="{{$sub_service->id}}" name="service_id[]"/> <label for="{{ $sub_service->$name }}"></label>
                                                <div class="edit-quantity ml-1"><input type="text" name="quantity[]" placeholder="0"></div>
                                </div>
                                <div class="col-6 text-center">
                                        <h5>{{ $sub_service->$name }}</h5> <br/>
                                        <button type="button" class="btn btn-default">{{ $sub_service->$measurement_unit }}</button>
                                </div>
                                <div class="col-3 pl-4 pt-3 text-center">
                                        <img src="{{ asset('uploads/services/'.$sub_service->$image)}}">
                                </div>  
                            </div>
                        </form>  
                        
                    </div>
            @endif

            @endforeach
		</div>

        </div>
        <div class="w-100">
                <input type="submit" class="back btn w-100 btn-primary" value="{{__('order-water.confirm_order')}}">
        </div>
</form> 
</div>

</div>


</div>
</div>



@stop

