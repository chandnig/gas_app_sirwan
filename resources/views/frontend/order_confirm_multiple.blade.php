@extends('frontend.layouts.order-gas')
@section('content')
<style>


    .inputGroup {
        background-color: #fff !important;
        display: block !important;
        margin: 10px 0 !important;
        position: relative !important;
        left: 30px;
          top: 6px;
      }
      .inputGroup_arabic {
              background-color: #fff !important;
              display: block !important;
              margin: 10px 0 !important;
              position: relative !important;
              left: 60px;
              float: left;
              top: 4px;

      }
      .inputGroup input {
        width: 32px;
        height: 32px;
        order: 1;
        z-index: 2;
        position: absolute;
        right: 30px;
        top: 50%;
        transform: translateY(-50%);
        cursor: pointer;
      }
      .inputGroup_arabic input {
        width: 32px;
        height: 32px;
        order: 1;
        z-index: 2;
        position: absolute;
        right: 30px;
        top: 50%;
        transform: translateY(-50%);
        cursor: pointer;
      }


      {{--  [type="radio"]:not(:checked) {
          border: 2px solid red;
        border-radius: 25px;
        background-color: red;
        border-color:green;

      }  --}}

      .order_border{
          border:2px solid #d9d9d9;
          padding:10px;
          border-radius:10px;
      }
      /* Create a custom radio button */

      input[type='radio'] {
          -webkit-appearance: none;
          width: 20px;
          height: 20px;
          border-radius: 50%;
          outline: none;
          border: 4px solid lightgreen;
          padding: 12px;
          top: -4px;
      }

      input[type='radio']:before {
          content: '';
          display: block;
          width: 60%;
          height: 60%;
          margin: 20% auto;
          border-radius: 50%;
      }

      input[type="radio"]:checked:before {
          background: green;

      }

      input[type="radio"]:checked {
          border-color: lightgreen;
          padding: 11px;
          background: lightgreen;
      }

      .role {
          margin-right: 80px;
          margin-left: 20px;
          font-weight: normal;
      }

      .checkbox label {
          margin-bottom: 20px !important;
      }

      .roles {
          margin-bottom: 40px;
      }
</style>
<?php
   $name = 'name';
   $last_name = 'last_name';
   if (app()->getLocale() == 'ar'):
   $name .= '_ar';
   $last_name .= '_ar';
   endif;
   if (app()->getLocale() == 'kr'):
   $name .= '_kr';
   $last_name .= '_kr';
   endif;
?>
<div id="site-wrapper">
   <div id="site-canvas" class="order-by-quantity order-gas-quant">
      <header>
         <div class="py-4 mx-0 mt-0 text-white text-center" style="background-color:#062d49;{{__('login-page.inner_header_border')}}">
            <h4>{{__('order-gas.order')}} {{ $service_name }}</h4>
        </div>
      </header>
      {{-- =================SUPPLIER PHONE NO AND NAME ============================= --}}
      <div class="col-md-12 order-quantity mt-4 {{ __('login-page.text') }}">
         <span style="font-size:1.0rem">{{ __('order-gas.supplier_phone_no') }}</span>
         <div class="form-group  mt-3 ">
            <form method="POST" action="{{route('order.search_supplier_multiple')}}">
               {!! csrf_field() !!}
               <div class="col-md-12 row">
                @if (app()->getLocale() == 'kr' || app()->getLocale() == 'ar')
                  <div class="col-10">
                     <input type="number" name="supplier_mobile_no" class="text-center form-control supplier {{ ('login-page.text') }}" placeholder="{{ ('order-gas.supplier_phone_no') }}" value="{{$supplier_exist->mobile_number}}"/>
                     <input type="hidden" value="{{$address_id}}" name="address_id">
                     @foreach($serviceId_quantity as $key=>$value)
                     <input type="hidden" name="quantity[]" value="{{$value}}">
                     <input type="hidden" name="service_id[]" value="{{$key}}">
                     @endforeach
                  </div>
                  <div class="col-2">
                     <input type="submit" class="btn btn-warning" value="Search"/>
                  </div>
                @else
                    <div class="col-2">
                        <input type="submit" class="btn btn-warning" value="Search"/>
                     </div>
                    <div class="col-10">
                        <input type="number" name="supplier_mobile_no" class="form-control supplier {{ ('login-page.text') }}" placeholder="{{ ('order-gas.supplier_phone_no') }}" value="{{$supplier_exist->mobile_number}}"/>
                        <input type="hidden" value="{{$address_id}}" name="address_id">
                        @foreach($serviceId_quantity as $key=>$value)
                        <input type="hidden" name="quantity[]" value="{{$value}}">
                        <input type="hidden" name="service_id[]" value="{{$key}}">
                        @endforeach
                     </div>

                @endif
               </div>
            </form>
         </div>
         <!-- Full form submit -->
         <form method="POST" action="{{route('order.submit_confirm_multiple')}}">
            {!! csrf_field() !!}
            <div class="form-group input-group">
               <div class="col-md-12">
                  @if(count($supplier_exist) > 0 )
                  <h6 class="text-success">Supplier found successfully </h6>
                  <input type="text" class="form-control supplier bg-white {{ __('login-page.text') }}" placeholder="Supplier Name" disabled value="{{$supplier_exist->name}}">
                  <input type="hidden" name="supplier_id" value="{{$supplier_exist->id}}"/>
                  @else
                  <h6 class="text-danger">No supplier Selected</h6>
                  <input type="text" class="form-control supplier bg-white {{ __('login-page.text') }}" placeholder="Supplier Name" disabled value="{{$supplier_exist->name}}">
                  @endif
                  <input type="hidden" value="{{$address_id}}" name="confirm_address">
                  @foreach($serviceId_quantity as $key=>$value)
                  <input type="hidden" name="confirm_quantity[]" value="{{$value}}">
                  <input type="hidden" name="confirm_service_id[]" value="{{$key}}">
                  @endforeach
               </div>
            </div>
      </div>
      {{-- ==================AVAILABLE SUPPLIER======================================= --}}
        @if(count($supplier_exist) > 0 )
        <div hidden class="col-md-12 order-quantity profile {{ __('login-page.text') }} ">
        @else
        <div class="col-md-12 order-quantity profile {{ __('login-page.text') }} ">
        @endif
        <span style="font-size:1.0rem">{{ __('order-gas.available_supplier') }}</span>
      @foreach($supplierArray as $supplier)
      <ul class="container mb-3 profile order_border mt-3" >
        <li class="row">
            <div class="col-12" style="display:flex;">
            @if (app()->getLocale() == 'kr' || app()->getLocale() == 'ar')
                <div class="col-3" style="text-align: center;">
                    <div class="inputGroup_arabic ">
                        <input id="radio1" value="{{$supplier->id}}" name="supplier_id" type="radio"/>
                    </div>
                </div>
                    <div class="col-9 py-1">
                        <a href=""><span style="font-size:0.9rem;color: gray;line-height: 1.2;font-weight:500;">{{ $supplier->$name }} {{ $supplier->$last_name }}</span> </a>
                    </div>
                @else
                <div class="col-9 py-1">
                    <a href="">
                        <span style="font-size:0.9rem;color: gray;line-height: 1.2;font-weight:500;">{{ $supplier->$name }} {{ $supplier->$last_name }}</span>
                    </a>
                </div>
                <div class="col-3" style="text-align: right;">
                    <div class="inputGroup">
                        <input id="radio1" value="{{$supplier->id}}" name="supplier_id" type="radio"/>
                    </div>
                </div>
            @endif
            </div>
        </li>
      </ul>
      @endforeach
      </div>
      <div class="col-md-12 order-quantity profile">
        <ul class="container btn text-white mb-3 py3 profile order_border mt-3" style="background-color:#4a6ee2">
            <input type="submit" value=" {{ __('order-gas.confirm') }} " />
        </ul>
      </div>
      </form>
   </div>
</div>
@stop
