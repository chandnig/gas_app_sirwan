@extends('frontend.layouts.home-head')
@section('content')
<div class="container-fluid home">
    <div class="container product-details">
        <div class="row">
            <div class="col-md-12">
                <ul class="product-list container">
                    <?php
                    $name = 'name';
                    if (app()->getLocale() == 'ar'):
                         $name .= '_ar';
                    endif;
                    $image = 'image';
                    if (app()->getLocale() == 'ar'):
                       $image .= '_ar';
                    endif;

                    if (app()->getLocale() == 'kr'):
                        $name .= '_kr';
                    endif;

                    if (app()->getLocale() == 'kr'):
                        $image .= '_kr';
                    endif;
                    ?>

                    @if($error)
                    <h6 class="text-danger alter-warning">{{ $error }}</h6>
                    @endif

                    @foreach ($services as $service)
                    @if(app()->getLocale() == 'kr' || app()->getLocale() == 'ar')
                    <?php $route = (count($service->childern) > 0) ? route('home.order',[$service->id]) : route('order.show', [$service->id]) ?>
                    <li class="row">
                            <div class="col-12" style="border-bottom: 1px solid #cccccc;display: flex;">
                                    <div class="col-8 text-right pr-4 pt-4 pb-4">
                                    <a href="{{$route}}"><span>  {{ __('home-head.home_order') }} {{  $service->$name}} </span></a>
                                </div>
                                <div class="col-4 pb-2">
                                    <a href="{{$route}}">
                                        <span class="product-icon">
                                            <img src="{{ asset('uploads/services/'.$service->$image)}}">
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </li>
                    @elseif(app()->getLocale() == 'en' )
                    <?php $route = (count($service->childern) > 0) ? route('home.order',[$service->id]) : route('order.show', [$service->id]) ?>
                        <li class="row">
                            <div class="col-12" style="border-bottom: 1px solid #cccccc;display: flex;">
                                <div class="col-4 pb-2">
                                    <a href="{{ $route }}">
                                        <span class="product-icon">
                                            <img src="{{ asset('uploads/services/'.$service->$image)}}">
                                        </span>
                                    </a>
                                </div>
                                    <div class="col-8 text-center pt-4 pb-4">
                                            <a href="{{$route}}"><span> {{ __('home-head.home_order') }}  {{  $service->$name}}</span> </a>
                                    </div>
                            </div>
                        </li>
                    @endif
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>
@stop

