@extends('frontend.layouts.problem')
@section('content')
<style>
        .mastercard{
            background-size: cover;
            text-align: center;
            display: flex;
            align-items: center;
            height: 80%;
            }
            .mobile{
            border:2px solid #bce0fd;
            background-color: #f1f9ff;
            height:60px;
            border-radius:20px;
            }
      </style>
<div class="container-fluid">
        <div class="row">
           <div class="col-md-12 py-1 mb-5 text-white text-center" style="background-color: #062d49;{{__('login-page.inner_header_border')}}">
              <h4>{{ __('my-order.submit_problem') }}</h4>
           </div>
        </div>
</div>
<div class="col-12 text-center text-success">
        @if(count($store) > 0)
            <h6>Your problem submitted successfully.</h6>
        @endif
        @if ($error)
            <div>{{$error}}</div>
       @endif
</div>

     <div class="mastercard mt-5">
        <div class="container">
            <form action="{{ route('home.problem_submit') }}" method="POST">

                    {!! csrf_field() !!}
                <div class="row">
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-8">
                        <textarea required rows="7" class="form-control mobile p-2" id="problem_data" name="problem_data"> </textarea>

                        <input type="hidden" value="{{ $order_id }}" id="order_id" name="order_id"/>
                        <input type="hidden" value="{{ $supplier_id }}" id="supplier_id" name="supplier_id"/>
                        <input type="hidden" value="{{ $service_id }}" id="service_id" name="service_id"/>
                    </div>
                    <div class="col-md-2">
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-4">
                       <input type="submit" class="back btn w-100 btn-primary" value="{{ __('my-order.submit') }}">
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
                <div class="row mt-2">
                    <div class="col-md-4">
                    </div>
                    <div class="col-md-4">
                         <a href="{{ route('order_history.show',[$service_id]) }}">
                             <button type="button" class="btn  btn-dark w-100 text-white">Back</button>
                         </a>
                    </div>
                    <div class="col-md-4">
                    </div>
                </div>
            </form>
        </div>
     </div>
@stop
