@extends('frontend.layouts.services-head')
@section('content')
<div id="site-wrapper">
    <div id="site-canvas">
        <header>
            <!-- <div class="row header py-4">
                <h4 class="col text-center">{{__('my-order.title')}}</h4>
            </div> -->
            <div class="py-4 mx-0 mt-0 text-white text-center" style="background-color: #062d49;{{__('login-page.inner_header_border')}};">
                <h4>{{__('my-order.title')}}</h4>
            </div>
        </header>
        <div class="container-fluid home">
                <div class="container product-details">
                    <div class="row">
                        <div class="col-md-12">
                                <?php
                                $name = 'name';
                                if (app()->getLocale() == 'ar'):
                                     $name .= '_ar';
                                endif;
                                if (app()->getLocale() == 'ar'):
                                $image .= '_ar';
                                endif;
                                if (app()->getLocale() == 'kr'):
                                $name .= '_kr';
                                endif;
                                ?>
                            <div class="row pt-3 text-center mb-4" style="display:block">
                                <small>{{ __('my-order.see_order_history') }} </small>
                            </div>

                            @if(app()->getLocale()=='en')
                             @foreach($services as $service)
                                <ul class="container mb-3" style="border: 2px solid #e3c8ad;padding: 10px;border-radius: 8px;background: #f1f9ff;">
                                        <li class="row">
                                            <div class="col-12" style="display: flex;">
                                                <div class="col-9 pt-1">
                                                    <a href=""><h4 style="font-size: 1.2rem;color: gray;line-height: 1.2;font-weight: 500;">{{ $service->$name}}</h4> </a>
                                                </div>
                                                <div class="col-3" style="text-align: right;">
                                                    <a href="{{ route('order_history.show', [$service->id]) }}"> <img src="{{asset('images/right.png')}}"></a>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                @endforeach

                            @elseif(app()->getLocale()=='kr' || app()->getLocale() == 'ar')

                            @foreach($services as $service)
                            <ul class="container mb-3" style="border: 2px solid #e3c8ad;padding: 10px;border-radius: 8px;background: #f1f9ff;">
                                    <li class="row">
                                        <div class="col-12" style="display: flex;">
                                            <div class="col-3">
                                                <a href="{{ route('order_history.show', [$service->id]) }}"> <img src="{{asset('images/left.png')}}"></a>
                                            </div>
                                            <div class="col-9 pt-1">
                                                    <a href=""><h4 style="font-size: 1.2rem;color: gray;line-height: 1.2;font-weight: 500;text-align: right;">{{ $service->$name}}</h4> </a>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                @endforeach
                            @endif
                            {{--  <div class="mt-3">
                                    <a type="button" class=" btn btn-block btn-primary" style="background-color:#022e48" href="{{ route('home.dashboard')}}">{{__('my-order.back')}}</a>
                            </div>  --}}

                        </div>
                        <br/><br/>
                        <a type="button" href="{{ route('home.dashboard')}}" class="btn back_btn_profile py-2  mx-3 mt-2 btn-block text-white"> {{__('profile.back')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

