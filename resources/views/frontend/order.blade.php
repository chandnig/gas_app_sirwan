<style>
        body{
            overflow: hidden!important;
        }
        #site-wrapper {

            overflow: scroll!important;

        }
      .box {
        width: 40%;
        margin: 0 auto;
        background: rgba(255,255,255,0.2);
        padding: 35px;
        border: 2px solid #fff;
        border-radius: 20px/50px;
        background-clip: padding-box;
        text-align: center;
      }

      .button {
        font-size: 1em;
        padding: 10px;
        color: #fff;
        border: 2px solid #06D85F;
        border-radius: 20px/50px;
        text-decoration: none;
        cursor: pointer;
      }
      .button:hover {
        background: #06D85F;
      }

      .overlay {
        position: fixed;
        top: 0%;
        bottom: 0;
        left: 0;
        right: 0;
        background: rgba(0, 0, 0, 0.7);
        transition: opacity 500ms;
        visibility: hidden;
        opacity: 0;
      }
      .overlay:target {
        visibility: visible;
        opacity: 1;
      }

      .popup {
        margin: 0px auto;
        padding: 20px;
        background: #fff;
        border-radius: 5px;
        width: 35%;
        position: relative;
        top:35%;
      }


      .popup h2 {
        margin-top: 0;
        color: #333;
        font-family: Tahoma, Arial, sans-serif;
      }
      .popup .close {
        position: absolute;
        top: 20px;
        right: 30px;
        transition: all 200ms;
        font-size: 30px;
        font-weight: bold;
        text-decoration: none;
        color: #333;
      }
      .popup .close:hover {
        color: #06D85F;
      }
      .popup .content {
        max-height: 30%;
      }

      @media screen and (max-width: 700px){
        .box{
          width: 100%;
        }
        .popup{
          width: 100%;
        }
      }
</style>

@extends('frontend.layouts.order-head')
@section('content')
<div id="site-wrapper">
   <div id="site-canvas">
      <header>
         <!-- <div class="row header py-4">
            <h4 class="col text-center">{{__('my-order.title')}}</h4>
         </div> -->
         <div class="py-4 mx-0 mt-0 text-white text-center"
               style="background-color: #062d49;{{__('login-page.inner_header_border')}}">
                <h4>{{__('my-order.title')}}</h4>
            </div>
      </header>

      <?php
      $id="";
         $name = 'name';
         $unit_name = 'measurement_unit';
         $service_name = 'service_name';
         if (app()->getLocale() == 'ar'):
         $name .= '_ar';
         $unit_name .= '_ar';
         $service_name .= '_ar';
         endif;
         if (app()->getLocale() == 'kr'):
         $name .= '_kr';
         $unit_name .= '_kr';
         $service_name .= '_kr';
         endif;
         ?>
      <div class="container-fluid orders">
         <div class="container pt-4">
              <div style="position: sticky;  top: 85%;  z-index: 999; text-align:right;{{ __('home-head.green_back_rotate') }}">
                 <a href="{{ route('home.service')}}"><img src="{{asset('images/right.png')}}" height="50px" width="50px"></a>
              </div>
            @if(app()->getLocale() == 'kr' ||app()->getLocale()=='ar')
             <h5 style="text-align:right">{{ $order_list[0]->$service_name }}   <span style="float:right" class="px-1"> {{ __('my-order.order') }} </span></h5>
            @else
            <h5> {{ $order_list[0]->$service_name }}  {{ __('my-order.order') }} </h5>
            @endif
            <div class="row">
               @foreach($order_list as $orders)
               @if (app()->getLocale() == 'ar' || app()->getLocale() == 'kr' )
               <div class="col-md-12 my-order">
                  <h6 class="text-center">{{ __('my-order.order_id') }} {{ $orders->order_id }}</h6>
                  <div class="order-inner">
                     <p>
                        <span> {{ $orders->time }} | {{ $orders->day }} <i class="fa fa-clock-o" aria-hidden="true"></i></span>
                        <span> {{ $orders->date_new }} <i class="fa fa-calendar" aria-hidden="true"></i></span>
                     </p>
                     <p><strong> {{ $orders->$name }}</strong>&nbsp;&nbsp;{{__('my-order.supplier_name')}}</p>
                     <button type="button" class="w-100 btn btn-light text-right mb-3"> {{ $orders->$unit_name }}  <span style="float:right" class="px-1"> {{ $orders->service_quantity }}</span> </button>
                        <div style="width:100%;text-align:center">
                                <a  href="#popup1/{{ $orders->id }}">
                                    <span class=" delete" style="padding: 7px 12px;">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </span></a>
                        </div>
                  </div>
                  <a href="#popup2/{{ $orders->id }}">
                    <button class="w-70 btn btn-outline-danger">{{ __('my-order.problem') }}</button>
                  </a>
               </div>
               @elseif(app()->getLocale() == 'en' )
               <div class="col-md-12 my-order">
                  <h6 class="text-center">{{ __('my-order.order_id') }} {{ $orders->order_id }}</h6>
                  <div class="order-inner">
                     <p>
                         <span><i class="fa fa-calendar" aria-hidden="true"></i>{{ $orders->date_new }}</span>
                          <span><i class="fa fa-clock-o" aria-hidden="true"></i> {{ $orders->time }} | {{ $orders->day }}</span></p>
                     <p>{{__('my-order.supplier_name')}}&nbsp;&nbsp;<strong>{{ $orders->$name }}</strong></p>
                     <button type="button" class="w-100 btn btn-light text-left mb-3">{{ $orders->service_quantity }}{{ $orders->$unit_name }}</button>
                     <div style="width:100%;text-align:center">

                     <a  href="#popup1/{{ $orders->id }}">
                        <span class="delete" style="padding: 7px 12px;">
                           <i class="fa fa-trash" aria-hidden="true"></i>
                        </span>
                    </a>
                     </div>
                  </div>

                 <a href="#popup2/{{ $orders->id }}">
                     <button  class="w-70 btn btn-outline-danger">{{__('my-order.problem')}}</button></a>
               </div>
               @endif
               @endforeach
               <div class="w-100">
                  <a type="button" class="back btn w-100 btn-primary" href="{{ route('home.service')}}">{{__('my-order.back')}}</a>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@foreach($order_list as $orders)

<div id="popup1/{{$orders->id}}" class="overlay">
   <div class="popup" style="background-color: #084264;padding:35px">
         <a class="close" href="#">&times;</a>
            <div class="content">
                     <h6 style="color:white;margin-top:30px" class="text-center">
                            {{ __('my-order.delete_popup_msg') }}
                     </h6>
                     <div class="row" style="margin-top:40px ">
                @if(app()->getLocale()=='ar' || app()->getLocale()=='kr')
                     <div class="col-12  text-center ">
                           <form class="" role="form" method="POST" action="{{ route('order.delete_order') }}">
                                {!! csrf_field() !!}
                            <input type="hidden"  value="{{ $orders->id }}" name="order_id">
                              <input type="hidden"   value="{{ $service_id }}"  name="service_id">
                              <input type="submit" style="background-color: #32d27c;color:white;border-radius:10px;line-height:30px" class="btn px-4" value="{{ __('my-order.delete') }}"/>
                           </form>
                     </div>

                @else
                <div class="col-12  text-center ">
                        <form class="" role="form" method="POST" action="{{ route('order.delete_order') }}">
                             {!! csrf_field() !!}
                         <input type="hidden"  value="{{ $orders->id }}" name="order_id">
                           <input type="hidden"   value="{{ $service_id }}"  name="service_id">
                           <input type="submit" style="background-color: #32d27c;color:white;border-radius:10px;line-height:30px" class="btn px-4" value="{{ __('my-order.delete') }}"/>
                        </form>
                  </div>
                @endif
                  </div>
            </div>
   </div>
</div>
@endforeach


@foreach($order_list as $orders)

<div id="popup2/{{ $orders->id }}" class="overlay">
      <div class="popup" style="background-color: #084264;padding:45px">
            <a class="close" href="#">&times;</a>
               <div class="content">
                        <h6 style="color:white" class="text-center pt-3">
                              {{ __('my-order.problem_popup_msg') }}
                        </h6>
                        <div class="row" style="margin-top:40px">
                        @if(app()->getLocale()=='ar' || app()->getLocale()=='kr')
                                <div class="col-6 text-center">
                                    <a href="#"> <button type="button" style="background-color: #aa080a;color:white;border-radius:10px; line-height:30px" class="btn px-5"><span>{{ __('my-order.no') }}</span></button></a>
                                </div>
                                <div class="col-6  text-center ">
                                        {{--  <form class="" role="form" method="POST" action="{{ route('home.problem') }}">  --}}
                                                {!! csrf_field() !!}
                                                <input type="hidden"  value="{{ $orders->user_supplier_id}}"  name="supplier_id">
                                                <input type="hidden"  value="{{ $orders->id }}"  name="order_id">
                                                <input type="hidden"  value="{{ $service_id }}"  name="service_id">
                                                {{--  <a href="#"  <input type="submit" style="background-color: #32d27c;color:white;border-radius:10px;line-height:30px" class="btn px-5"  value="Yes"></a>  --}}
                                                <a href="#"> <button type="button" style="background-color: #32d27c;color:white;border-radius:10px; line-height:30px" class="btn px-5"><span>{{ __('my-order.yes') }}</span></button></a>
                                        {{--  </form>  --}}
                                </div>
                        @else
                            <div class="col-6  text-center ">
                                    {{--  <form class="" role="form" method="POST" action="{{ route('home.problem') }}">  --}}
                                            {!! csrf_field() !!}
                                            <input type="hidden"  value="{{ $orders->user_supplier_id}}"  name="supplier_id">
                                            <input type="hidden"  value="{{ $orders->id }}"  name="order_id">
                                            <input type="hidden"  value="{{ $service_id }}"  name="service_id">
                                            {{--  <a href="#"  <input type="submit" style="background-color: #32d27c;color:white;border-radius:10px;line-height:30px" class="btn px-5"  value="Yes"></a>  --}}
                                            <a href="#"> <button type="button" style="background-color: #32d27c;color:white;border-radius:10px; line-height:30px" class="btn px-5"><span>{{ __('my-order.yes') }}</span></button></a>
                                    {{--  </form>  --}}
                            </div>
                            <div class="col-6 text-center">
                                <a href="#"> <button type="button" style="background-color: #aa080a;color:white;border-radius:10px; line-height:30px" class="btn px-5"><span>{{ __('my-order.no') }}</span></button></a>
                            </div>
                        @endif
                     </div>
               </div>
      </div>
</div>
@endforeach

@stop

