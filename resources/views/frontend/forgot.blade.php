@extends('frontend.layouts.forget-head')
@section('content')
<style>
    .mastercard{
    background-size: cover;
    text-align: center;
    display: flex;
    align-items: center;
    }
</style>
        <div class="col-md-4 bg-white mx-auto px-0" style="border: 1px solid #062d49" >
                <div class="py-3 mx-0 text-white text-center" style="height:10%;background-color: #062d49;{{__('login-page.inner_header_border')}}">
                        <h4> {{ __('login-page.mobile_number') }}</h4>
                </div>

                <form method="POST" action="{{ route('login.validation')}}" >
                        {!! csrf_field() !!}
                        <div class="" >
                                @if($error==true)
                                <h6 class="text-danger text-center pt-5">Please enter valid mobile number</h6>
                                @endif
                        </div>
                    <div class="mastercard mt-0" style="height:80%;">
                            <div class="container">
                                <div class="row">
                                <div class="col-xs-12 mx-auto text-center">
                                    <div class="box">
                                    {{ __('login-page.enter_mobile_number') }}
                                    </div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-md-12">
                                    <input type="text" name="mobile_number" class="form-control mobile {{ __('login-page.text') }}">
                                </div>
                                </div>
                            </div>
                    </div>
                    <div class="py-3 mx-0 text-white  pb-2 row" style="height:10%;background-color:#062d49;bottom:0;{{ __('login-page.inner_footer_border') }}">
                    <div class="col-md-12 pl-5 py-2">
                        @if(app()->getLocale() == 'ar' || app()->getLocale() == 'kr' )
                            <span>
                                <input type="submit" style="color:white;background-color:#062d49;border:none" value="{{ __('login-page.next') }}">
                            </span>
                            <span style="float:right" class="pr-3">
                                <a href="{{ route('login.user')}}" style="color:white">{{ __('login-page.back') }}</a>
                            </span>
                        @else
                            <span>
                                <a href="{{ route('login.user')}}" style="color:white">{{ __('login-page.back') }}</a>
                            </span>
                            <span style="float:right" class="pr-3">
                                <input type="submit" style="color:white;background-color:#062d49;border:none" value="{{ __('login-page.next') }}">
                            </span>
                        @endif
                    </div>
                    </div>
                </form>
        </div>

@stop

