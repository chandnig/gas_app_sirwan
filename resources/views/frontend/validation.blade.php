@extends('frontend.layouts.forget-head')
@section('content')
<style>
        .mastercard{
        background-size: cover;
        text-align: center;
        display: flex;
        align-items: center;
        }
</style>

        <div class="col-md-4 bg-white mx-auto px-0" style="border: 1px solid #062d49" >
                <div class="py-3 mx-0 text-white text-center" style="height:10%;background-color: #062d49;{{__('login-page.inner_header_border')}}">
                        <h4>{{ __('login-page.verification_code') }}</h4>
                </div>
                <div class="mastercard" style="height:30%">
                        <div class="container">
                           <div class="row">
                              <div class="col-md-12 text-center";style="color: #06466c">
                                 <p class="padding-top:50px">{{ __('login-page.soon_recive_verification_code') }}</p>
                              </div>
                           </div>
                        </div>
                </div>
                <form method="POST" action="{{ route('login.reset')}}">
                        {!! csrf_field() !!}
                    <div class="mastercard" style="height:20%">
                            <div class="container">
                                    <div class="row">
                                    <div class="col-xs-12 mx-auto text-center">
                                        <div class="box">
                                            {{ __('login-page.enter_verification_code') }}
                                        </div>
                                    </div>
                                    </div>
                                    <div class="row">
                                    <div class="col-md-12">
                                        <input type="text" name="verification_code" class="form-control mobile {{ __('login-page.text') }}">
                                        <input type="hidden" name="mobile_number" value="{{ $exist->mobile_number }}">
                                        <input type="hidden" name="db_otp" value="{{ $exist->otp }}">
                                    </div>
                                    </div>
                                </div>
                    </div>

                    <div class="mastercard" style="height:30%" >
                        <div class="container">
                       <div class="row">
                          <div class="col-md-2">
                          </div>
                          <div class="col-md-8">
                             <button class="btn" style="background-color:#26d37c;color:white;text-align: center;color: white;width: 220px;height: 43px;border-radius: 30px">
                                 {{ __('login-page.resend_code') }}
                             </button>
                          </div>
                          <div class="col-md-2">
                          </div>
                       </div>
                        </div>
                    </div>

                    <div class="py-3 mx-0 text-white  pb-2 row" style="height:10%;background-color:#062d49;bottom:0;{{ __('login-page.inner_footer_border') }}">
                            <div class="col-md-12 pl-5 py-2">
                            @if(app()->getLocale() == 'ar' || app()->getLocale() == 'kr')
                                <span >
                                    <input type="submit" style="color:white;background-color:#062d49;border:none" value="{{ __('login-page.next') }}">
                                </span>
                                <span style="float:right" class="pr-3">
                                    <a href="{{ route('login.forgot')}}" style="color:white">{{ __('login-page.back') }}</a>
                                </span>

                            @else
                                <span>
                                    <a href="{{ route('login.forgot')}}" style="color:white">{{ __('login-page.back') }}</a>
                                </span>
                                <span style="float:right" class="pr-3">
                                    <input type="submit" style="color:white;background-color:#062d49;border:none" value="{{ __('login-page.next') }}">
                                </span>
                            @endif
                        </div>
                    </div>
            </form>
@stop

