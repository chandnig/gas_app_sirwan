@extends('frontend.layouts.profile-head')
@section('content')
<style>

        .topnav {
            overflow: hidden;
            background-color: transparent;
          }

          .topnav a {
            float: left;
            display: block;
            color: #f2f2f2;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
            font-size: 17px;
          }


          .topnav .icon {
            display: none;
          }

          .dropdown1 {
            float: left;
            overflow: hidden;
          }

          .dropdown1 .dropbtn1 {
            font-size: 17px;
            border: none;
            outline: none;
            color: black;
            background-color: inherit;
            font-family: inherit;
            margin: 0;
            width: 200px;
            background: url(../../images/arrow.png) no-repeat right #ddd;
            -webkit-appearance: none;
            background-position-x: 90px;
            height: 35px;
            font-weight: 500;
            background-color:transparent;
            border:none;
          }

          .dropdown-content1 {
            display: none;
            position: absolute;
            min-width: 160px;
            box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
            z-index: 1;
            background: white;
          }

          .dropdown-content1 a {
            float: none;
            color: black;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
            text-align: left;
          }

          .dropdown1:hover .dropdown-content1 {
            display: block;
          }

         </style>
<div id="site-wrapper">
    <div id="site-canvas" class="profile">
        <header>
            <!-- <div class="row header py-4">
                <div class='col-md-12 col-lg-12 text-center'>
                    <h4 class="col"></h4>
                </div>

                <div class='col-md-10 col-lg-10'>
                </div>
            </div> -->
            <div class="py-4 mx-0 mt-0 text-white text-center" style="background-color: #062d49;{{__('login-page.inner_header_border')}}">
                        <h4>{{__('profile.my_profile')}}</h4>
            </div>
        </header>
        <div class="orders">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <form class="login" id="frmDataEdit" role="form" method="POST" action="">
                            {!! csrf_field() !!}
                            <div class="container">
                                <div class="row py-4 form-group {{ $errors->has('name') ? ' has-error' : '' }}" style="border-bottom:1px solid #d9d9d9;">
                                    @if (app()->getLocale() == 'ar' || app()->getLocale() == 'kr' )
                                        <div class="col-5 text-right" style="color: #064163;"> {{$users->name}} {{ $users->last_name }}</div>
                                        <div class="col-7 text-right" style="color: #adc0cb;"> {{ __('profile.full_name') }}</div>
                                    @else
                                        <div class="col-5" style="color: #adc0cb;"> {{ __('profile.full_name') }}</div>
                                        <div class="col-7" style="color: #064163;"> {{$users->name}} {{$users->last_name}}</div>
                                    @endif

                                </div>
                            </div>
                            <div class=" py-4 form-group {{ $errors->has('name') ? ' has-error' : '' }}" style="border-bottom:1px solid #d9d9d9;">
                                @if(app()->getLocale() == 'ar' || app()->getLocale() == 'kr' )
                                    <label for="fname" style="color: #adc0cb;float:right" class="pl-2 pb-2">{{ __('profile.address') }}</label>
                                    @foreach($address as $ad)
                                            <input type="text" class="form-control pl-3 address_profile bg-white text-right" id="name" name="name" value='{{ $ad->name }} {{ $ad->address }}' disabled >
                                    @endforeach
                                    <p class=" text-danger hidden"></p>
                                @else
                                <label for="fname" style="color: #adc0cb;" class="pl-2 pb-2">{{ __('profile.address') }}</label>
                                    @foreach($address as $ad)
                                            <input type="text" class="form-control pl-3 address_profile bg-white" id="name" name="name" value='{{ $ad->name }} {{ $ad->address }}' disabled >
                                    @endforeach
                                    <p class=" text-danger hidden"></p>
                                @endif
                            </div>

                            <div class="container">
                                <div class="row py-4 form-group {{ $errors->has('phone') ? ' has-error' : '' }}" style="border-bottom:1px solid #d9d9d9;">
                                    @if(app()->getLocale() == 'ar' || app()->getLocale() == 'kr' )
                                        <div class="col-5 text-right" style="color: #064163;"> {{ $users->mobile_number }}</div>
                                        <div class="col-7 text-right" style="color: #adc0cb;">{{ __('login-page.phone_number') }}</div>
                                    @else
                                        <div class="col-5" style="color: #adc0cb;">{{ __('login-page.phone_number') }}</div>
                                        <div class="col-7" style="color: #064163;"> {{ $users->mobile_number }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="container">
                                <div class="row py-4 form-group {{ $errors->has('language') ? ' has-error' : '' }}" style="border-bottom:1px solid #d9d9d9;">

                                    @if(app()->getLocale() == 'ar' || app()->getLocale() == 'kr' )
                                      <div class="col-5">


                                            <div class="topnav" id="">
                                                    <div class="dropdown1">
                                                    @php $locale = session()->get('locale');
                                                    if($locale=="en"){
                                                        $language ="English";
                                                    }
                                                    elseif($locale=="ar"){
                                                        $language="العربیة";
                                                    }
                                                    elseif($locale=="kr"){
                                                        $language="کوردی";
                                                    }
                                                    @endphp
                                                      <div class="dropbtn1 px-5 pt-2" style="background-position-x:0px!important;">{{ $language }}
                                                      </div>
                                                      <div class="dropdown-content1">
                                                        <a href="lang/en">English</a>
                                                        <a href="lang/ar">العربیة</a>
                                                        <a href="lang/kr">کوردی</a>
                                                      </div>
                                                    </div>
                                            </div>
                                        </div>
                                        <div class="col-7 text-right" style="color: #adc0cb;line-height: 2.5;">
                                            {{__('profile.language')}}
                                        </div>
                                        @else
                                            <div class="col-5" style="color: #adc0cb;line-height: 2.5;">{{__('profile.language')}}</div>
                                            <div class="col-7">
                                                <div class="topnav" id="">
                                                        <div class="dropdown1">
                                                        @php $locale = session()->get('locale');
                                                        if($locale=="en"){
                                                            $language ="English";
                                                        }
                                                        elseif($locale=="ar"){
                                                            $language="العربیة";
                                                        }
                                                        elseif($locale=="kr"){
                                                            $language="کوردی";
                                                        }
                                                        @endphp
                                                          <div class="dropbtn1 text-left pt-2">{{ $language }}
                                                          </div>
                                                          <div class="dropdown-content1">
                                                            <a href="lang/en">English</a>
                                                            <a href="lang/ar">العربیة</a>
                                                            <a href="lang/kr">کوردی</a>
                                                          </div>
                                                        </div>
                                                </div>
                                            </div>
                                        @endif
                                    </div>
                                    <br/><br/><br/>
                                    <a type="button" href="{{ route('home.dashboard')}}" class="btn back_btn_profile py-2 btn-block text-white"> {{__('profile.back')}}</a><br/><br/>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
                <div class="w-100">

                </div>
            </div>
        </div>

    </div>


</div>
{{-- <script type="text/javascript" charset="utf-8" async defer>
    $(function () {
    $('#login').on('click', function (e) {
    e.preventDefault();
    var url = $('#frmDataEdit').attr('action');
    var frm = $('#frmDataEdit');
    $.ajax({
    type: 'POST',
    url: url,
    dataType: 'json',
    data: frm.serialize(),
    success: function (data) {
    console.log(data);
    $('#frmDataEdit .text-danger').each(function () {
    $(this).addClass('hidden');
    });
    if (data.errors) {
    $.each(data.errors, function (index, value) {
    $('#frmDataEdit #' + index).parent().find('.text-danger').text(value);
    $('#frmDataEdit #' + index).parent().find('.text-danger').removeClass('hidden');
    });
    }
    if (data.success == true) {
     window.location.href = "http://localhost/gas-application/dashboard";

    }
    if (data.error == true) {
    alert('User not found');
    }
    },
    error: function (jqXHR, textStatus, errorThrown)
    {
    console.log(jqXHR);
    console.log(textStatus);
    console.log(errorThrown);
    alert('Please Reload to read Ajax');
    }
    });
    });
    });
</script> --}}
@stop

