 <?php
return [
    'my_profile' =>  'My Profile',
    'first_name' =>  'First Name',
    'last_name' =>  'Last Name',
    'phone' =>  'Phone',
    'language' =>  'Language',
    'save' =>  'save',
    'back' =>  'Back',
    'address' =>'My Address',
    'full_name' => 'Full Name',
];
