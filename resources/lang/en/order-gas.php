<?php

return [
    'title' => 'Order Gas',
    'order' => 'Order',
    'button' => 'Cylinder Quantity',
    'heading' => 'Deliver the Order to Address',
    'supplier' => 'Supplier Phone Number',
    'level1' => 'Home Panaton Address line-1',
    'level2' => 'Home Panaton Address line-2',
    'level3' => 'Your Supplier Phone No.',
    'confirm' => 'Confirm Order',
    'deliver_to_address'=>'Deliver the order to address',
    'available_supplier'=>'Currently following suppliers are work in your Section',
    'supplier_phone_no' =>'Supplier Phone No',
    'back'  =>'Back',
    'no_available_supplier' =>'No available supplier',
    'order'=>'Order',
    'no_supplier_selected'=>'No Supplier Selected',
    'supplier_name'=>'Supplier name',
    'supplier_found_success' =>'Supplier Found Successfully',
    'search'=>'Search',
    'order_confirmed_success'=>'Your order submitted successfully',
    'ok'=>'Home',

];
