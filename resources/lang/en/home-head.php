 <?php
return [
    'my_profile' => 'My Profile',
    'my_order' => 'My Orders',
    'term_condition' => 'Terms and Conditions',
    'logout' => 'Logout',
    'font-family' =>'Arabic',
    'home_order' =>'Order',
    'green_back_rotate' =>'transform: scaleX(-1)',
    'site_menu_right' =>'left: -304px;',
    'side_menu_border'=>' border-radius: 0 0 80px 0;',

];
