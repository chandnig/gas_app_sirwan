<?php

return [
    'title_order_pending' => 'Order Received',
    'message_order_pending' => 'Order Received, Kindly Accept the order.',
    'title_order_canceled' => 'Order Canceled ',
    'message_order_canceled' => 'Order Canceled Automatically, As you not accepted order in TIME.',
    'title_order_processing' => 'Order Accepted',
    'message_order_processing' => 'Order Accepeted Successfully, Please Process the order and deliver in time.',
    'title_order_completed' => 'Order Completed ',
    'message_order_completed' => 'Order Completed Successfully, Yeah !!! You have Completed the order.',
    'title_order_rejected' => 'Order Rejected',
    'message_order_rejected' => 'Order Rejected, You have rejected order, If you are unavailable please change status to off so that you cant receive more order.'
];
