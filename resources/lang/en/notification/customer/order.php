<?php

return [
    'title_order_pending' => 'Order Submitted',
    'message_order_pending' => 'Order Submitted Successfully, waiting for your supplier to accept order',
    'title_order_canceled' => 'Order Canceled ',
    'message_order_canceled' => 'Order Canceled Automatically as supplier not accepted the order, Please choose some other supplier',
    'title_order_processing' => 'Order Accepted',
    'message_order_processing' => 'Order Accepeted Successfully, Your order placed please check order deatils for more detail',
    'title_order_completed' => 'Order Completed ',
    'message_order_completed' => 'Order Completed Successfully, Please rate services by Supplier',
    'title_order_rejected' => 'Order Rejected',
    'message_order_rejected' => 'Order Rejected, Your order rejected by supplier Please choose some other supplier'
];
