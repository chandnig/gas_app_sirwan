 <?php
return [
    'title' =>  'My Orders',
    'order' =>  'Orders',
    'supplier_name' =>  'Supplier Name',
    'problem' =>  'I have problem with this product',
    'back' =>  'Back',
    'order_id'=>'Order Id',
    "see_order_history"=>'To show a history of the order please select a product',
    "confirm_order"=>'Confirm Order',
    'problem_popup_msg'=>'Do you want to open a ticket about this issue and send it back to the supplier',
    'delete_popup_msg'=>'Are you sure to delete this order?',
    'yes'=>'Yes',
    'no'=>'No',
    'delete'=>'Delete',
    'cancel' =>'Cancel',
];
