 <?php
return [
    'my_profile' =>  'حسابي',
    'first_name' =>  'الاسم الکامل',
    'last_name' =>  'الكنية',
    'phone' =>  'رقم الموبایل',
    'language' =>  'تغییر اللغة',
    'save' =>  'الرجوع',
    'back' =>  'الرجوع',
    'address' =>  'عنوان',
    'full_name' =>'الاسم الکامل',
];
