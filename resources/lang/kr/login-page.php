<?php
return [
    'login_to_account' => 'چوونەژورەوە بۆ ناو هەژمارەکەت',
    'text'=>'text-right',
    'right_shift'=>'0',
    'forgot_shift'=>'text-left',
    'inner_header_border'=>"border-radius: 0px 0px 0px 34px;",
    'inner_footer_border'=>"border-radius: 0px 34px 0px 0px;",
    'phone_number'=>'ژمارەی مۆبایل',
    'password'=>'ووشەی نهێنی',
    'signin'=>'چوونەژورەوە',
    'back'=>'گەڕانەوە',
    'next'=>'دواتر',
    'verification_code'=>'کۆدی دووپاتکردنە',
    'enter_verification_code'=>' کۆدی دووپاتکردنەوە بنوسە ',
    'soon_recive_verification_code'=>' لە زووترین کاتدا کۆدی دووپاتکردنەوە بە کورتە نامەیەک پێت دەگات ',
    'resend_code'=>'دووبارە ناردنەوەی کۆد',
    'mobile_number'=>'ژمارەی مۆبایل',
    'enter_mobile_number'=>' تکایە لێرە ژمارەی مۆبایلەکەت تۆماربکە ',
    'forgot_password'=>' ووشەی نهێنم بیرجۆتەوە ',
    'new_password'=>'وشەی نهێنی تازە',
    'confirm_password'=>'دووپاتکردنەوەی',
    'change_my_password' =>'گۆڕینی وشەی نهێنی',
    'save_changes'=>'پاشەکەوت',

];

