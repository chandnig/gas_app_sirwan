 <?php
return [
    'title' => 'داواکردنی',
    'gas' => 'Xaz',
    'button' => 'Çelê Cylinder',
    'heading' => 'ژ.مۆبایلی بریکارەکەت لێرە بنوسە ئەگەر بریکارێک دەناسی',
    'supplier' => 'ژمارەی مۆبایلی بریکار',
    'level1' => 'Navnîşa Panaton Serûpel-1',
    'level2' => 'Navnîşa Panaton Serûpel-2',
    'level3' => 'Telefon No Supplier',
    'address' => 'محمد احمد علی',
    'confirm' => 'دووپاتکردنەوەی داواکاریەکە',
    'deliver_to_address'=>' داواکاریەکەم بۆ بێنە بۆ ئەم ناونیشانە ',
    'available_supplier'=>'ئەم بریکارانە بەردەستن لە دەوروبەرتان',
    'supplier_phone_no' =>'ژ.مۆبایلی بریکارەکەت لێرە بنوسە ئەگەر بریکارێک دەناسی',
    'back'=>'گەڕانەوە',
    'no_available_supplier' =>'بریکاری داواکراو نەدۆزرایەوە',
    'no_supplier_selected' =>'هیچ بریکارێك هەڵنەبژێردراوە',
    'order'=>'داواکردنی',
    'enter_supplier_number' =>'ژمارەی مۆبایلی بریکار',
    'search'=>'گەڕان',
    'supplier_name'=>'ناوی بریکار',
    'supplier_found_success'=>'بریکاری مەبەست دۆزرایەوە',
    'order_confirmed_success'=>' داواکاریەکەت بە سەرکەوتووی ئەنجامدرا',
    'ok'=>'سەرەکی',
];
