<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Passport\HasApiTokens;


class User extends Authenticatable {

    use Notifiable;
    use HasRoles;
    use HasApiTokens;

//  use SoftDeletes;
//  protected $table = 'admin_users';

    protected $guard_name = 'api'; // or whatever guard you want to use

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'last_name', 'email', 'password', 'mobile_number','otp','user_language','user_type','customer_type','gasApp_fav',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    protected $guarded = [];

    public function driver() {
        return $this->hasOne('App\Models\UserDriver', 'user_id', 'id')->select('user_id', 'vehicle_number', 'license_number');
    }

    public function addresses() {
        return $this->hasMany('App\Models\UserAddress', 'user_id', 'id');
    }

    public function address() {
        return $this->hasMany('App\Models\UserAddress', 'user_id', 'id')->where('default', '1');
    }

    public function supplierAddress() {
        return $this->hasOne('App\Models\UserSupplier', 'user_id', 'id')->select('user_id', 'latitude', 'longitude');
    }

    public function supplierRating() {
        return $this->hasOne('App\Models\Order', 'user_supplier_id', 'id')->where('request_status', 'completed')->select('user_supplier_id', \DB::raw('avg(review_supplier) as review_supplier'))->groupBy('user_supplier_id');
    }

    public function supplierRateCard() {
        return $this->hasOne('App\Models\UserSupplierServiceRate', 'supplier_id', 'id')->select('supplier_id', 'service_id', 'rate_cards');
    }

}
