<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class GasNotification extends BaseModel {

    use SoftDeletes;
}
