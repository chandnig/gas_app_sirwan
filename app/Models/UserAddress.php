<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class UserAddress extends BaseModel {

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'address', 'user_id', 'latitude', 'longitude','comment','state','address_status','default','governorate_id','city_id','section_id'
    ];

    //
    public function governorate() {
        return $this->hasOne('App\Models\Location', 'id', 'governorate_id');
    }

    public function city() {
        return $this->hasOne('App\Models\Location', 'id', 'city_id');
    }

    public function section() {
        return $this->hasOne('App\Models\Location', 'id', 'section_id');
    }

}
