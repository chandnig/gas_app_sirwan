<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Feedback extends BaseModel {

    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id','rating'
    ];


}
