<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class UserDriver extends BaseModel {

    use SoftDeletes;

      /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'driver_name', 'mobile_number', 'user_id','parent_user_id','vehicle_number','license_number','row_status'
    ];
    //
}
