<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Service extends BaseModel {

    use SoftDeletes;

    //
    public function childern() {
        return $this->hasMany('App\Models\Service', 'parent_id', 'id')->where('state','1');
    }

}
