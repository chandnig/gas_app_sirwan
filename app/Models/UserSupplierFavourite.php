<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSupplierFavourite extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'supplier_id', 'customer_id', 'row_status'
    ];
    //
}
