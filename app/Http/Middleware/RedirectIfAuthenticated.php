<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null) {
        Auth::user();
        Auth::logout();
        if (Auth::guard($guard)->check()) {
            return redirect(config('app.admin_prefix') . '/login');
        }

        return $next($request);
    }

}
