<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
use Twilio\Rest\Client;



class UserLoginController extends Controller {

    public function __construct() {
//        $this->middleware('guest', ['except' => 'logout']);
    }


//     /**
//  * Sends sms to user using Twilio's programmable sms client
//  * @param String $message Body of sms
//  * @param Number $recipients string or array of phone number of recepient
//  */
// private function sendMessage($message, $recipients)
// {
//     $account_sid = getenv("TWILIO_SID");
//     $auth_token = getenv("TWILIO_AUTH_TOKEN");
//     $twilio_number = getenv("TWILIO_NUMBER");
//     $client = new Client($account_sid, $auth_token);
//     $client->messages->create($recipients,
//             ['from' => $twilio_number, 'body' => $message] );
// }

    public function Login() {
        if (\Auth::user())
            return redirect('dashboard');
        else
            return view('frontend.login');
    }

    public function Auth(Request $request) {
        $validator = Validator::make(Input::all(), ['mobile_number' => 'required', 'password' => 'required']);
        if ($validator->fails())
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));

        if (\Auth::attempt(['mobile_number' => request('mobile_number'), 'password' => request('password')])) {
            Auth::login(\Auth::user(), true);
          //$user = \Auth::user();

          return redirect()->route('home.dashboard');

        } else {
            return back()->withErrors(['message'=>'User not found']);
           // return response()->json(["errors" => ['mobile_number'=>'User not found']]);
        }
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request) {

        \Auth::logout();
        return redirect('/');
    }

    public function forgot(Request $request)
    {

        return view('frontend.forgot');

        }

    public function validation(Request $request)
    {

       $mobile_number =  $request->mobile_number;

       $exist = User::where('mobile_number',$mobile_number)->first();

       if($exist)
       {
        $str_result = '0123456789';
        $otp=substr(str_shuffle($str_result), 0, 5);
        $number='+919877108532';
        $account_sid = getenv("TWILIO_SID");
        $auth_token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_number = getenv("TWILIO_NUMBER");
        $client = new Client($account_sid, $auth_token);
        $message=$client->messages->create($number,
            ['from' => $twilio_number, 'body' => $otp] );

        $exist->otp = $otp;
        $exist->save();
        if($exist->save())
        {
            return view('frontend.validation',compact('exist'));
        }
        else{
            $error =true;
            return view('frontend.forgot',compact('error'));
        }
       }
       else{
        $error =true;
        return view('frontend.forgot',compact('error'));
       }

    }
    public function reset_password(Request $request)
    {

        $db_otp         =   $request->db_otp;
        $user_otp       =   $request->verification_code;
        $mobile_number  =   $request->mobile_number;


        if($db_otp == $user_otp)
        {
            return view('frontend.reset_password',compact('mobile_number'));
        }
        else{
            return view('frontend.forgot');
        }

    }

    public function save_changes(Request $request)
    {

        $mobile_number      =   $request->mobile_number;
        $password           =   $request->password;
        $confrim_password   =   $request->confirm_password;

        if($password != $confrim_password)
        {
            return redirect()->route('login.user');

        }
        else{

            $exist = User::where('mobile_number',$mobile_number)->first();
            $exist->password =  bcrypt($request->password);
            $exist->save();
            if($exist->save())
            {
                $success = true;
                return redirect()->route('login.user');
            }
            else
            {
                $success=false;
                return redirect()->route('login.user');
            }
        }
    }
}
