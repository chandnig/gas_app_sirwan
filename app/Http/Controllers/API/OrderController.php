<?php


namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\User;
use App\Models\Order;
use App\Models\Service;
use App\Models\UserAddress;
use App\Models\Feedback;
use Illuminate\Support\Facades\Auth;
use Validator;
use Carbon\Carbon;
use App\Models\UserSupplier;
use DB;
use App\Models\UserSupplierServiceRate;
use DateTime;


class OrderController extends BaseController
{
    /** service list api
    *
    * @return \Illuminate\Http\Response
    */
    public function getServiceList(Request $request)
    {
     $image_path = url('/').'/uploads/services';

     $validator = Validator::make($request->all(), [
             'locale' => 'required|in:en,ar,ku',
         ]);

         if($validator->fails()){
             return $this->sendError('Validation Error.', $validator->errors());
         }


         $user = Auth::user();
         if($user){
             if($request['locale']=='en'){
                 $serviceData = DB::table('services as s')
                                ->leftJoin('measurement_units as mu','s.unit','=','mu.id')
                                ->select('s.id','s.name','s.image','s.default_qty','mu.measurement_unit','s.created_at')->where('state','=','1')->where('parent_id','=','0')
                                ->whereNull('s.deleted_at')
                                ->get();

             }elseif($request['locale']=='ar'){
                 $serviceData = DB::table('services as s')
                                ->leftJoin('measurement_units as mu','s.unit','=','mu.id')
                                ->select('s.id','s.name_ar as name','s.image_ar as image','mu.measurement_unit_ar as measurement_unit','s.default_qty','s.created_at')
                                ->where('state','=','1')
                                ->where('parent_id','=','0')
                                ->whereNull('s.deleted_at')->get();
             }elseif($request['locale']=='ku'){
                 $serviceData =  DB::table('services as s')
                                ->leftJoin('measurement_units as mu','s.unit','=','mu.id')
                                ->select('s.id','s.name_kr as name','s.image_kr as image','mu.measurement_unit_kr as measurement_unit','s.default_qty','s.created_at')
                                ->where('state','=','1')->where('parent_id','=','0')
                                ->whereNull('s.deleted_at')->get();
             }


             $locale=$request['locale'];
             if(count($serviceData) >0){
                 foreach($serviceData as $service ){
                        $service->baseUrl= $image_path;
                        $sub_data = $this->getSubServices($service->id,$locale);
                        if(count($sub_data) > 0 ){
                            $service->has_child = "yes";
                        }
                        else{
                            $service->has_child = "no";
                        }
                        $service->child = $sub_data;

                 }
                 return $this->sendResponse($serviceData, 'Service List Found Successfully');
             }
             else{
                 $error =  "Empty service data ";
                 return $this->sendError($error, 'Empty Service Data');
             }

         }
           else{

            $error =  "Invalid Token ";
             return $this->sendError($error, 'Error in getting user data');
           }
    }

//========================SERVICES ACCORDGIN TO LOCAATION================================//

    public function servicesByLocation(Request $request)
    {
        $image_path = url('/').'/uploads/services';
        $validator = Validator::make($request->all(), [
            'governorate_id' => 'required',
            'locale' => 'required|in:en,ar,ku',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        if($request['locale']=='en'){
            $serviceData = DB::table('services as s')
                           ->leftJoin('measurement_units as mu','s.unit','=','mu.id')
                           ->where('s.location','like', '%"'. $request->governorate_id .'"%')
                           ->select('s.id','s.name','s.image','s.default_qty','mu.measurement_unit','s.created_at')
                           ->where('state','=','1')
                           ->where('parent_id','=','0')
                           ->whereNull('s.deleted_at')
                           ->get();

        }elseif($request['locale']=='ar'){
            $serviceData = DB::table('services as s')
                           ->leftJoin('measurement_units as mu','s.unit','=','mu.id')
                           ->select('s.id','s.name_ar as name','s.image_ar as image','mu.measurement_unit_ar as measurement_unit','s.default_qty','s.created_at')
                           ->where('state','=','1')
                           ->where('parent_id','=','0')
                           ->where('s.location','like', '%"'. $request->governorate_id .'"%')
                           ->whereNull('s.deleted_at')->get();

        }elseif($request['locale']=='ku'){
            $serviceData =  DB::table('services as s')
                           ->leftJoin('measurement_units as mu','s.unit','=','mu.id')
                           ->select('s.id','s.name_kr as name','s.image_kr as image','mu.measurement_unit_kr as measurement_unit','s.default_qty','s.created_at')
                           ->where('state','=','1')->where('parent_id','=','0')
                           ->where('s.location','like', '%"'. $request->governorate_id .'"%')
                           ->whereNull('s.deleted_at')->get();
        }

        $locale=$request['locale'];
             if(count($serviceData) >0){
                 foreach($serviceData as $service ){
                        $service->baseUrl= $image_path;
                        $sub_data = $this->childService($service->id,$locale);
                        if(count($sub_data) > 0 ){
                            $service->has_child = "yes";
                        }
                        else{
                            $service->has_child = "no";
                        }
                        $service->child = $sub_data;

                 }
                 return $this->sendResponse($serviceData, 'Service List Found Successfully');
             }
             else{
                 $error =  "Empty service data ";
                 return $this->sendError($error, 'Empty Service Data');
             }
    }
    public function getSubServices(Request $request){
        $id = $request->id;
        $locale = $request->locale;
        $childService = $this->childService($id,$locale);
        if(count($childService) > 1) {
            return $this->sendResponse($childService, 'Service Child List Found Successfully');
        }
        else{
            $error =  "Empty service data ";
            return $this->sendError($error, 'Empty Service Data');
        }

    }
    protected function childService($id,$locale) 
    {
        if($locale=='en'){
            $sub_cat_data = DB::table('services as s')
                           ->leftJoin('measurement_units as mu','s.unit','=','mu.id')
                           ->where('parent_id','=',$id)
                           ->whereNull('s.deleted_at')
                           ->select('s.id','s.name','s.image','s.default_qty','mu.measurement_unit as quantity_type','s.created_at')->where('state','=','1')->get();

        }elseif($locale=='ar'){
            $sub_cat_data = DB::table('services as s')
                           ->leftJoin('measurement_units as mu','s.unit','=','mu.id')
                           ->where('parent_id','=',$id)
                           ->whereNull('s.deleted_at')
                           ->select('s.id','s.name_ar as name','s.image_ar as image','mu.measurement_unit_ar as quantity_type','s.default_qty','s.created_at')->where('state','=','1')->get();
        }elseif($locale=='ku'){
            $sub_cat_data =  DB::table('services as s')
                           ->leftJoin('measurement_units as mu','s.unit','=','mu.id')
                           ->where('parent_id','=',$id)
                           ->whereNull('s.deleted_at')
                           ->select('s.id','s.name_kr as name','s.image_kr as image','mu.measurement_unit_kr as quantity_type','s.default_qty','s.created_at')->where('state','=','1')->get();
        }

    return $sub_cat_data;
    }

    /** Create Order By Customer API
    *
    * @return \Illuminate\Http\Response
    */
   public function placeOrder(Request $request)
   {
    $image_path = url('/').'/uploads/services';

    $validator = Validator::make($request->all(), [
            'serviceArray'  =>'required',
            'address_id'    => 'required',
            'supplier_id'   => 'required',
            'customer_id'   =>'required',

        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $user_customer = Auth::user();
        $serviceArray = $request->serviceArray;

        $data = array();




       $dataArray = json_decode($serviceArray);

        foreach($dataArray as $service)
        {
            $current_timestamp = Carbon::now()->timestamp;
            // return $current_timestamp;

            $order_id = substr($current_timestamp, 2);
            $input = [
                'user_customer_id' => $request->customer_id,
                'user_supplier_id' => $request->supplier_id,
                'user_customer_address_id' => $request->address_id,
                'service_quantity' => $service->quantity,
                'service_id' => $service->service_id,
                'request_status' => "",
                'order_id' => $order_id,

            ];

            array_push($data, $input);
        }

        // return $data;

            $order = Order::insert($data);

            if($order){

                return $this->sendResponse($order, 'Order placed Successfully');
            }

            else{
                $error =  "Something went wrong";
                return $this->sendError($error, 'Error in Placing Order.Please Try Again');
            }

   }


   /** Find Supplier By Phone number
    *
    * @return \Illuminate\Http\Response
    */
   public function findSupplierByPhoneNumber(Request $request)
   {
    $image_path = url('/').'/uploads/services';

    $validator = Validator::make($request->all(), [
            'supplier_mobile_no'    => 'required',

        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $supplier = User::where('mobile_number',$request->supplier_mobile_no)
                        ->where('state','1')->where('user_type','supplier')->first();




            if(count($supplier)>0){

                return $this->sendResponse($supplier, 'Supplier Found Successfully');
            }

            else{
                $error =  "Supplier Doesn't Exist";
                return $this->sendError($error, 'Supplier Not Found');
            }


   }

   /** list of nearby supplier
    *
    * @return \Illuminate\Http\Response
    */
   public function findNearBySupplier(Request $request)
   {
        $validator = Validator::make($request->all(), [
            'address_id'      => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $customer_address = UserAddress::find($request->address_id);

        $customer_lat =$customer_address->latitude;
        $customer_long =$customer_address->longitude;

        $supplier = DB::table('user_suppliers as us')
                    ->leftJoin('users as u','us.user_id','=','u.id')
                    ->select('us.*','u.name','u.last_name','u.mobile_number')
                    ->get();

        foreach($supplier as $s)
        {
            $average = Order::where('user_supplier_id',$s->user_id)->avg('rating');

            if(count($average)>0)
            {
                $rating_average = $average;
            }else{
                $rating_average = 0;
            }
            $productData = $this->getSupplierProductArray($s->user_id);
            $s->productList = $productData;
            $s->supplier_rating = $rating_average;

        }

        $supplierArray=array();

        foreach($supplier as $index=>$d)
        {
            $supplier_lat 		= $d->latitude;
            $supplier_long  	= $d->longitude;

            $distance = $this->distance($customer_lat,$customer_long,$supplier_lat,$supplier_long,'K');


           if($distance<10)
            {
                $supplier[$index]->total_distance = $distance;
                array_push($supplierArray,$supplier[$index]);
            }
        }

        if(count($supplierArray) > 0){

            return $this->sendResponse($supplierArray, 'Nearby Suppliers found Successfully');
        }

        else{
            $error =  "Something went wrong";
            return $this->sendError($error, 'No NearBy Supplier Available');
        }
   }

    /**order cancelled by customer
    *
    * @return \Illuminate\Http\Response
    */
public function cancelOrder_Cusotmer(Request $request)
   {
        $validator = Validator::make($request->all(), [
            'order_id'    => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $order_data = Order::find($request->order_id);
        if($order_data)
        {
            $current_time       =  date("Y-m-d H:i:s");
            $diff_in_minutes = $order_data->created_at->diffInMinutes($current_time);

            if($diff_in_minutes<5){
                $order_data->request_status = "Cancelled";
                $order_data->save();
                if($order_data->save()){
                    return $this->sendResponse($order_data, 'Order Cancelled Successfully');
                }
                else{
                    $error =  "Somethng went Wrong";
                    return $this->sendError($error, 'Error in cancellation');
                }
             }
             else{
                $error =  "Cannot cancel order";
                return $this->sendError($error, 'Cancellation time exceeded(cancel within 5 min)');
             }
        }
        else{
            $error =  "Order doesn't exist";
            return $this->sendError($error, 'Order Doesnt exist');
        }
}


public function distance($lat1, $lon1, $lat2, $lon2, $unit) {

    if (($lat1 == $lat2) && ($lon1 == $lon2)) {
      return 0;
    }
    else {
      $theta = $lon1 - $lon2;


    //   echo  "lon1 = > ".$lon1." lat1 => ".$lat1."  lon2 => ".$lon2." lat2 => ".$lat2;

      $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
      $dist = acos($dist);
      $dist = rad2deg($dist);
      $miles = $dist * 60 * 1.1515;
      $unit = strtoupper($unit);

      if ($unit == "K") {
        return ($miles * 1.609344);
      } else if ($unit == "N") {
        return ($miles * 0.8684);
      } else {
        return $miles;
      }
    }

}


public function getSupplierProductArray($id)
{
    $supplier_product = UserSupplierServiceRate::where('supplier_id','=',$id)->select('rate_cards')->first();
    return $supplier_product;
}



public function orderList(Request $request){

    $image_path = url('/').'/uploads/services';

    $validator = Validator::make($request->all(), [
            'locale' => 'required|in:en,ar,ku',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }


    $user = User::find(auth()->user()->id);
    $user_id = $user->id;

    $order_his = Order::where("user_customer_id",'=',$user_id)
                ->where('request_status','<>','Cancelled')
                ->select('service_id')
                ->groupBy('service_id')
                ->get();

        if(count($order_his) > 0)
                {
                    foreach($order_his as $order)
                    {
                        if($request['locale']=='en'){
                            $Order = Service::where('services.id','=',$order->service_id)->where('state','=','1')->select('services.id','name','parent_id','image','default_qty','measurement_unit')
                            ->leftJoin('measurement_units', 'services.unit','=','measurement_units.id')->first();
                        }
                        elseif($request['locale']=='ar'){
                            $Order = Service::where('services.id','=',$order->service_id)->where('state','=','1')->select('services.id','name_ar as name','parent_id','image_ar as image','default_qty','measurement_unit_ar as measurement_unit')
                            ->leftJoin('measurement_units', 'services.unit','=','measurement_units.id')->first();
                        }
                        elseif($request['locale']=='ku'){
                            $Order = Service::where('services.id','=',$order->service_id)->where('state','=','1')->select('services.id','name_kr as name','parent_id','image_kr as image','default_qty','measurement_unit_kr as measurement_unit')
                            ->leftJoin('measurement_units', 'services.unit','=','measurement_units.id')->first();
                        }

                        if($Order->parent_id==0)
                        {
                            $Order->image_base_url = $image_path;
                            $Order->has_child = "no";
                            $order_history[] =$Order;
                        }
                        else {
                            $parent_id= $Order->parent_id;
                            // $order_data =Service::find($parent_id);
                            if($request['locale']=='en'){
                                $order_data = Service::where('services.id','=',$parent_id)->where('state','=','1')->select('services.id','name','parent_id','image','default_qty','measurement_unit')
                                ->leftJoin('measurement_units', 'services.unit','=','measurement_units.id')->first();
                            }
                            if($request['locale']=='ar'){
                                $order_data = Service::where('id','=',$parent_id)->where('state','=','1')->select('id','name_ar as name','parent_id','image_ar as image','default_qty','measurement_unit_ar as measurement_unit')
                                ->leftJoin('measurement_units', 'services.unit','=','measurement_units.id')->first();
                            }
                            if($request['locale']=='ku'){
                                $order_data = Service::where('id','=',$parent_id)->where('state','=','1')->select('id','name_kr as name','parent_id','image_kr as image','default_qty','measurement_unit_kr as measurement_unit')
                                ->leftJoin('measurement_units', 'services.unit','=','measurement_units.id')->first();
                            }
                            $order_data->image_base_url = $image_path;
                            $order_data->has_child = "yes";
                            $order_history[] = $order_data;
                        }
                }

                $order_history_unique = $this->array_multi_unique($order_history);


                 if(count($order_history_unique) > 0)
                     return $this->sendResponse($order_history_unique, 'Order History found Successfully');

                 else{
                     $error =  "Something went wrong";
                     return $this->sendError($error, 'No History Found');
                 }
        }
        else{
            $error =  "Order doesn't exist";
            return $this->sendError($error, 'Order Doesnt exist');
        }

}

public function array_multi_unique($multiArray){

    $uniqueArray = array();

    foreach($multiArray as $subArray){

      if(!in_array($subArray, $uniqueArray)){
        $uniqueArray[] = $subArray;
      }
    }
    return $uniqueArray;
  }

  public function createOrder(Request $request)
  {
    $validator = Validator::make($request->all(), [
        'serviceArray'  =>'required',
        'address_id'    => 'required',
        'supplier_id'   => 'required',
        //'customer_id'   =>'required',
        "is_child"      => 'required'
    ]);
    if($validator->fails()){
        return $this->sendError('Validation Error.', $validator->errors());
    }
    $user_customer = Auth::user();
    $serviceArray = $request->serviceArray;
    //dd($user_customer['id']);
    $data = array();
    foreach($serviceArray as $service)
    {
      $current_timestamp = Carbon::now()->timestamp;
      $order_id = substr($current_timestamp, 2);
      $input = [
          'user_customer_id' => $user_customer['id'],
          'user_supplier_id' => $request->supplier_id,
          'user_customer_address_id' => $request->address_id,
          'is_child' => $request->is_child,
          'service_quantity' => $service['quantity'],
          'service_id' => $service['service_id'],
          'request_status' => "",
          'order_id' => $order_id,
      ];
      array_push($data, $input);
    }
    $order = Order::insert($data);
    if($order){

        return $this->sendResponse($order, 'Order placed Successfully');
    }
    else{
        $error =  "Something went wrong";
        return $this->sendError($error, 'Error in Placing Order.Please Try Again');
    }
  }

  public function createOrder1(Request $request)
  {
    $validator = Validator::make($request->all(), [
              'service_id'  =>'required',
              'address_id'    => 'required',
              'supplier_id'   => 'required',
              'customer_id'   =>'required',
              'service_quantity'   =>'required',
          ]);
    if($validator->fails()){
        return $this->sendError('Validation Error.', $validator->errors());
    }
    $user_customer = Auth::user();
    $current_timestamp = Carbon::now()->timestamp;
    $order_id = substr($current_timestamp, 2);
    $input = [
        'user_customer_id' => $request->customer_id,
        'user_supplier_id' => $request->supplier_id,
        'user_customer_address_id' => $request->address_id,
        'service_quantity' => $request->service_quantity,
        'service_id' => $request->service_id,
        'request_status' => "",
        'order_id' => $order_id,

    ];
    //dd($input);
    $order = Order::insert($input);
    if($order) {
        return $this->sendResponse($order, 'Order placed Successfully');
    }
    else {
        $error =  "Something went wrong";
        return $this->sendError($error, 'Error in Placing Order.Please Try Again');
    }
  }

  public function updateFeedback(Request $request)
  {
    $validator = Validator::make($request->all(), [
      'order_id'  =>'required',
      'rating'    => 'required',
    ]);
    if($validator->fails()){
    return $this->sendError('Validation Error.', $validator->errors());
    }
    $user_customer = Auth::user();
    $input = [
      'order_id' => $request->order_id,
      'rating'=> $request->rating

    ];
    //dd($input);
    $feedback = Feedback::insert($input);
    if($feedback) {
        return $this->sendResponse($feedback, 'Feedback updated Successfully');
    }
    else {
        $error =  "Something went wrong";
        return $this->sendError($error, 'Error in Placing Order.Please Try Again');
    }
    
  }
}

