<?php


namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Order;
use App\Models\Service;
use App\Models\UserSupplier;
use App\Models\UserDriver;
use App\Models\UserSupplierFavourite;
use App\Models\HowToModel;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;


class SupplierController extends BaseController
{

  /** supplier service list api
  *
  * @return \Illuminate\Http\Response
  */
   public function getSupplierServiceList(Request $request)
   {
        $image_path = url('/').'/uploads/services';
        $validator = Validator::make($request->all(), [
                'locale' => 'required|in:en,ar,ku',
            ]);

            if($validator->fails()){
                return $this->sendError('Validation Error.', $validator->errors());
            }

                if($request['locale']=='en'){
                    $serviceData = DB::table('supplier_roles')->select('id','name','image','created_at')->get();
                }elseif($request['locale']=='ar'){
                    $serviceData = DB::table('supplier_roles')->select('id','name_ar as name','image_ar as image','created_at')->get();
                }elseif($request['locale']=='ku'){
                    $serviceData = DB::table('supplier_roles')->select('id','name_kr as name','image_kr as image','created_at')->get();
                }

                 if(count($serviceData)>0){
                     foreach($serviceData as $service ){
                         $service->baseUrl= $image_path;
                     }
                     return $this->sendResponse($serviceData, 'Service List Found Successfully');
                 }
                 else{
                     $error =  "Empty service data ";
                     return $this->sendError($error, 'Empty Service Data');
                 }
    }

    /** supplier registration
    *
    * @return \Illuminate\Http\Response
    */
   public function supplierSignUp(Request $request)
   {
        $validator = Validator::make($request->all(), [

            'name' => 'required',
            'last_name' => 'required',
            'mobile_number' => 'required|unique:users',
            'password' => 'required|min:6',
            'user_language' => 'required',
            'role_id'=>'required',
            'governorate_id'=>'required',
            'city_id'=>'required',
            'section_ids'=>'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $input = ([
            'name' => $request->name,
            'last_name' => $request->last_name,
            'mobile_number' => $request->mobile_number,
            'password' => bcrypt($request->password),
            'user_language' => $request->user_language,
            'user_type' => 'supplier',

        ]);


        $user = User::create($input);
        if($user){
            $location_details = ([
                'user_id'           =>  $user->id,
                'governorate_id'    =>  $request->governorate_id,
                'city_id'           =>  $request->city_id,
                'section_ids'       =>  $request->section_ids,
                // 'role_id'           =>  $request->role_id,
            ]);

            $location_data = UserSupplier::create($location_details);

            // return $location_data;
            $success['id'] =$user->id;
            $success['token'] = $user->createToken('MyApp')->accessToken;
            $success['name'] =  $user->name;
            $success['last_name'] =  $user->last_name;
            $success['mobile_number'] =  $user->mobile_number;
            $success['user_language'] =  $user->user_language;
            // $success['otp'] =  $user->otp;

            return $this->sendResponse($success, 'User register successfully.');
        }
        else{
            $error =  "Something went wrong";
            return $this->sendError($error, 'Error in regestering user');
        }
    }


    // Create Supplier 
   
    /** update vehicle/liscence
    *
    * @return \Illuminate\Http\Response
    */

    public function updateSupplier(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_language' => 'required|in:en,kr,ar',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $user = User::find(auth()->user()->id);
        if($user){
            $user->user_language = $request->user_language;
            $user->save();
            if($user->save()){

                if($request->vehicle_image!=""){
                    $vehicle_image = $request->vehicle_image;
                    $imageName = $user['id'].time().'vehicle.'.$vehicle_image->getClientOriginalExtension();
                    $destinationPath = public_path('/supplier/vehicle');
                    $vehicle_image->move($destinationPath, $imageName);
                    $vehicle_img = $imageName;
                }
                if($request->license_image!=""){
                    $license_image = $request->license_image;
                    $imageName2 = $user['id'].time().'license.'.$license_image->getClientOriginalExtension();
                    $destinationPath = public_path('/supplier/license');
                    $license_image->move($destinationPath, $imageName2);
                    $license_img = $imageName2;
                }

                $user_supplier = UserSupplier::where('user_id','=',$user['id'])
                ->update(['vehicle_image' => $vehicle_img,
                'license_image' => $license_img]);

                if($request->productList!="")
                {
                    $user_supplier = UserSupplier::where('user_id','=',$user['id'])
                    ->update(['rate_cards' => $request->productList]);
                }

                return $this->sendResponse($user, 'Supplier Updated Successfully');
            }
            else{
                $error =  "Something went wrong ";
                return $this->sendError($error, 'Error in updating Supplier');
            }
        }
        else{
            $error =  "Invalid Token ";
            return $this->sendError($error, 'Error in updating Profile');
        }

    }

    /** add location(latitude longitude) of Supplier
    *
    * @return \Illuminate\Http\Response
    */

    public function addLocation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longitude' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $user = User::find(auth()->user()->id);
        if(count($user)>0){
            $data = UserSupplier::where('user_id','=',$user['id'])
                                ->update(['latitude' => $request->latitude, 'longitude' => $request->longitude]);

            return $this->sendResponse($data, 'Supplier Location Added Successfully');

        }
        else{
            $error =  "Invalid Token ";
            return $this->sendError($error, 'Error in adding Location');
        }

    }




    /** Update Supplier's Sections
    *
    * @return \Illuminate\Http\Response
    */

    public function updateSupplierSection(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'section_ids' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $user = User::find(auth()->user()->id);

        if(count($user)>0){
            if($user['user_type']=="suppplier"){
                $data = UserSupplier::where('user_id','=',$user['id'])
                                    ->update(['section_ids' => $request->section_ids]);

                return $this->sendResponse($user, 'Supplier Section Updated Successfully');
            }
            else{
                $error =  "Invalid Token ";
                return $this->sendError($error, 'Supplier Doesnt Exist');
            }
        }
        else{
            $error =  "Invalid Token ";
            return $this->sendError($error, 'Error in Updating Sections');
        }
    }

    public function createSupplier(Request $request)
    {
         $validator = Validator::make($request->all(), [
 
             'first_name' => 'required',
             'last_name' => 'required',
             'mobile_number' => 'required|unique:users',
             'password' => 'required|min:6',
             'governorate_id'=>'required',
             'city_id'=>'required',
             'section_ids'=>'required',
             'service_id' =>'required'
         ]);
 
         if($validator->fails()){
             return $this->sendError('Validation Error.', $validator->errors());
         }
 
         $input = ([
             'name' => $request->first_name,
             'last_name' => $request->last_name,
             'mobile_number' => $request->mobile_number,
             'password' => bcrypt($request->password),
             'user_language' => 'en',
             'user_type' => 'supplier',
 
         ]);
 
 
         $user = User::create($input);
         if($user){
             $location_details = ([
                 'user_id'           =>  $user->id,
                 'governorate_id'    =>  $request->governorate_id,
                 'city_id'           =>  $request->city_id,
                 'section_ids'       =>  json_encode($request->section_ids),
                 'role_supplier_id'  =>  json_encode($request->service_id),
             ]);
 
             $location_data = UserSupplier::create($location_details);
 
             // return $location_data;
             $success['id'] =$user->id;
             $success['token'] = $user->createToken('MyApp')->accessToken;
             $success['name'] =  $user->name;
             $success['last_name'] =  $user->last_name;
             $success['mobile_number'] =  $user->mobile_number;
             //$success['user_language'] =  $user->user_language;
             // $success['otp'] =  $user->otp;
 
             return $this->sendResponse($success, 'User register successfully.');
         }
         else{
             $error =  "Something went wrong";
             return $this->sendError($error, 'Error in regestering user');
         }
     }

     //Update Supplier Profile
     public function updateSupplierProfile(Request $request)
     {
          $validator = Validator::make($request->all(), [
  
              'first_name' => 'required',
              'last_name' => 'required',
              'mobile_number' => 'required|unique:users',
              'password' => 'required|min:6',
              'governorate_id'=>'required',
              'city_id'=>'required',
              'section_ids'=>'required',
              'service_id' =>'required'
          ]);
  
          if($validator->fails()){
              return $this->sendError('Validation Error.', $validator->errors());
          }
  
          $input = ([
              'name' => $request->first_name,
              'last_name' => $request->last_name,
              'mobile_number' => $request->mobile_number,
              'password' => bcrypt($request->password),
              'user_language' => 'en',
              'user_type' => 'supplier',
              'email' => $request->email,
          ]);
  
  
          $user = User::create($input);
          if($user){
              $location_details = ([
                  'user_id'           =>  $user->id,
                  'governorate_id'    =>  $request->governorate_id,
                  'city_id'           =>  $request->city_id,
                  'section_ids'       =>  json_encode($request->section_ids),
                  'role_supplier_id'  =>  json_encode($request->service_id),
                  'vehicle_number'    => $request->vehicle_number,
                  'vehicle_image'     => $request->vehicle_image,
                  'license_number'    => $request->license_number,
                  'license_image'     => $request->license_image,
              ]);
  
              $location_data = UserSupplier::create($location_details);
  
              // return $location_data;
              $success['id'] =$user->id;
              $success['token'] = $user->createToken('MyApp')->accessToken;
              $success['name'] =  $user->name;
              $success['last_name'] =  $user->last_name;
              $success['mobile_number'] =  $user->mobile_number;
              //$success['user_language'] =  $user->user_language;
              // $success['otp'] =  $user->otp;
  
              return $this->sendResponse($success, 'User register successfully.');
          }
          else{
              $error =  "Something went wrong";
              return $this->sendError($error, 'Error in regestering user');
          }
      }

      public function getProfile(Request $request) {
        $userId = auth()->user()->id;
        $serviceData = DB::table('users as s')
        ->leftJoin('user_suppliers as us','s.id','=','us.user_id')
        ->leftJoin('user_supplier_service_rates as product', 's.id','=','product.supplier_id' )
        ->select('s.id','s.name','s.email','s.mobile_number','us.vehicle_number','us.license_number','us.governorate_id','us.city_id','us.section_ids','us.vehicle_image','us.license_image','product.rate_cards as products')
        ->where('s.id',$userId)
        ->orWhere('s.parent_supplier_id',$userId)
        ->whereNull('s.deleted_at')
        ->get();
        if($serviceData) {
            return $this->sendResponse($serviceData, 'Profile Detail.');
        }
        else{
            $error =  "Something went wrong";
            return $this->sendError($error, 'Error in regestering user');
        }
      }




//==================================== DRIVERS MODULE===============================//
    /** add Driver by Supplier
    *
    * @return \Illuminate\Http\Response
    */

    public function addDriver(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'driver_name' => 'required',
            'mobile_number' => 'required',
            'vehicle_number' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $user = User::find(auth()->user()->id);

        if(count($user)>0){

            $input = ([
                'driver_name' => $request->driver_name,
                'vehicle_number' => $request->vehicle_number,
                'mobile_number' => $request->mobile_number,
                'user_id' => $user['id'],
                'parent_user_id' => $user['id'],
                'row_status'=>'active',
            ]);

            $driver = UserDriver::create($input);
            return $this->sendResponse($driver, 'Driver Added Successfully');
        }
        else{
            $error =  "Invalid Token ";
            return $this->sendError($error, 'Error in adding Driver');
        }
    }

    /** get Supplier Drivers
    *
    * @return \Illuminate\Http\Response
    */

    public function getSupplierDriver(Request $request)
    {
        $user = User::find(auth()->user()->id);
        if(count($user)>0){
            $data = UserDriver::where('parent_user_id','=',$user['id'])->where('row_status','active')->get();
            if(count($data)>0){
                return $this->sendResponse($data, 'Drivers Found Successfully');
            }
            else{
            $error =  "Empty Driver List ";
            return $this->sendError($error, 'Empty Driver List');
            }
        }
        else{
            $error =  "Invalid Token ";
            return $this->sendError($error, 'Error in adding Location');
        }

    }

    /** delete drive (by supplier)
    *
    * @return \Illuminate\Http\Response
    */
    public function deleteSupplierDriver(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'driver_id' => 'required',

        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $data = UserDriver::find($request->driver_id);

        if($data){
            $data->row_status = 'delete';
            $data->save();
                if($data->save()){
                    return $this->sendResponse($data, 'Driver Deleted Successfully');
                }
                else{
                    $error = "error in deleting address";
                    return $this->sendError($error, 'Empty Driver Data');
                }
        }
        else{
            $error = "No data found";
            return $this->sendError($error, 'Drive does not exist');
        }

    }


//===========================SUPPLIER FAV CUSTOMER===============================//


    /** get Supplier fav Customer
    *
    * @return \Illuminate\Http\Response
    */

    public function getSupplierFavCustomer(Request $request)
    {
        $user = User::find(auth()->user()->id);
        if(count($user)>0){
            // $data = UserSupplierFavourite::where('supplier_id','=',$user['id'])->where('row_status','active')->get();
        $data = DB::table('user_supplier_favourites as usf')
                ->leftJoin('users as u', 'usf.customer_id', '=', 'u.id')
                ->leftJoin('user_addresses as ua', 'ua.user_id', '=', 'u.id')->where('ua.default','=','1')
                ->select('usf.*','u.name','u.mobile_number','ua.address','ua.name as user_address_type')
                ->get();

            if(count($data)>0){
                return $this->sendResponse($data, 'Fav Customer Found Successfully');
            }
            else{
            $error =  "Empty Fav Customer List ";
            return $this->sendError($error, 'Empty Fav Customer List');
            }
        }
        else{
            $error =  "Invalid Token ";
            return $this->sendError($error, 'Error in getting fav customer');
        }

    }


    /** add fav customer by Supplier
    *
    * @return \Illuminate\Http\Response
    */

    public function addFavCustomer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $user = User::find(auth()->user()->id);

        if(count($user)>0){

            $db_fav = UserSupplierFavourite::where('customer_id','=',$request->customer_id)->where('supplier_id',$user['id'])
                                            ->where('row_status','active')->get();

            if(count($db_fav) >0){
                $error =  "Already Favourited ";
                return $this->sendError($error, 'Already Selected as Favourite');
            }

            $input = ([
                'supplier_id' =>  $user['id'],
                'customer_id' => $request->customer_id,
                'row_status'=>'active',
            ]);

            $favourit_customer = UserSupplierFavourite::create($input);
            return $this->sendResponse($favourit_customer, 'Added as Favourite Successfully');
        }
        else{
            $error =  "Invalid Token ";
            return $this->sendError($error, 'Error in adding fav customer');
        }

    }

        /** add fav customer by Supplier
    *
    * @return \Illuminate\Http\Response
    */

    public function deleteFavCustomer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $user = User::find(auth()->user()->id);

        if(count($user)>0){

            $db_fav = UserSupplierFavourite::where('customer_id','=',$request->customer_id)->where('supplier_id',$user['id'])
                                            ->where('row_status','active')->first();
           $db_fav->row_status = "delete";
           $db_fav->update();

            return $this->sendResponse($favourit_customer, 'Removed from Favourite');
        }
        else{
            $error =  "Invalid Token ";
            return $this->sendError($error, 'Error in removing fav customer');
        }
    }

//====================SUPPLIER AVAILABLE /UNAVAILABLE STATUS===============================//

    /** supplier available/unavailable status
    *
    * @return \Illuminate\Http\Response
    */
    public function supplierStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'supplier_status' => 'required|in:active,inactive',

        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $user = User::find(auth()->user()->id);

        if($user){
            if($request->supplier_status=='active'){
                $supplier_status='1';
            }
            else{
                $supplier_status='0';
            }
            $user->state = $supplier_status;   //supplier is available for taking orders
            $user->save();
                if($user->save()){
                    return $this->sendResponse($user, 'Status Changed Successfully');
                }
                else{
                    $error = "error in deleting address";
                    return $this->sendError($error, 'Empty Updating Status');
                }
        }
        else{
            $error = "No data found";
            return $this->sendError($error, 'User does not exist');
        }

    }


//========================GET LIST OF ORDERS ARRIVED (supplier)=============================//

    /**get orders received by supplier
    *
    * @return \Illuminate\Http\Response
    */

    public function getSupplierOrders(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'supplier_current_lat' => 'required',
            'supplier_current_long' => 'required',
            'locale'=>'required|in:en,ar,ku',

        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $user = User::find(auth()->user()->id);

        if($request->locale=='en')
        {

            $order_list =  DB::table('orders as o')
            ->leftJoin('users as u', 'o.user_customer_id', '=', 'u.id')
            ->leftJoin('user_addresses as a', 'a.id', '=', 'o.user_customer_address_id')
            ->leftJoin('services as s','s.id','=','o.service_id')
            ->leftJoin('measurement_units as mu','mu.id','=','s.unit')
            ->select('o.*','u.mobile_number as customer_mobile','u.name as customer_name',
                    'a.address as customer_address','a.name as address_type','a.latitude','a.longitude','o.service_quantity','mu.measurement_unit','s.name')
            ->where('user_supplier_id',$user['id'])
            ->where('o.request_status',"")
            ->get();
        }
        elseif($request->locale=='ku')
        {
            $order_list =  DB::table('orders as o')
            ->leftJoin('users as u', 'o.user_customer_id', '=', 'u.id')
            ->leftJoin('user_addresses as a', 'a.id', '=', 'o.user_customer_address_id')
            ->leftJoin('services as s','s.id','=','o.service_id')
            ->leftJoin('measurement_units as mu','mu.id','=','s.unit')
            ->select('o.*','u.mobile_number as customer_mobile','u.name as customer_name',
                    'a.address as customer_address','a.name as address_type','a.latitude','a.longitude','o.service_quantity','mu.measurement_unit_kr as measurement_unit','s.name_kr as name')
            ->where('user_supplier_id',$user['id'])
            ->where('o.request_status',"")
            ->get();
        }
        elseif($request->locale=='ar')
        {
            $order_list =  DB::table('orders as o')
            ->leftJoin('users as u', 'o.user_customer_id', '=', 'u.id')
            ->leftJoin('user_addresses as a', 'a.id', '=', 'o.user_customer_address_id')
            ->leftJoin('services as s','s.id','=','o.service_id')
            ->leftJoin('measurement_units as mu','mu.id','=','s.unit')
            ->select('o.*','u.mobile_number as customer_mobile','u.name as customer_name',
                    'a.address as customer_address','a.name as address_type','a.latitude','a.longitude','o.service_quantity','mu.measurement_unit_ar as measurement_unit','s.name_ar as name')
            ->where('user_supplier_id',$user['id'])
            ->where('o.request_status',"")
            ->get();
        }


        $orderArray=array();
            foreach($order_list as $index=>$order_arrived)
            {
                $customer_lat 		= $order_arrived->latitude;
                $customer_long  	= $order_arrived->longitude;

                $distance = $this->distance($request->supplier_current_lat,$request->supplier_current_long,$customer_lat,$customer_long,'K');
                $order_arrived->total_distance = $distance;
                array_push($orderArray,$order_arrived);
            }

        if(count($orderArray) > 0){

            return $this->sendResponse($orderArray, 'Orders List Found Successfully');
        }
        else{
            $error =  "Empty order list";
            return $this->sendError($error, 'Empty Order List');
        }

    }

    public function distance($lat1, $lon1, $lat2, $lon2, $unit) {

        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
        return 0;
        }
        else {
        $theta = $lon1 - $lon2;


        //   echo  "lon1 = > ".$lon1." lat1 => ".$lat1."  lon2 => ".$lon2." lat2 => ".$lat2;

        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
        }

    }

//=============================CHANGE ORDER STATUS (by supplier)================================//
    public function changeOrderStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'order_status' => 'required|in:Processing,Rejected,Closed',
            'order_id'       => 'required',
        ]);
        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $user = User::find(auth()->user()->id);
        $user_id = $user['id'];

        $order_data = Order::where('id','=',$request->order_id)->where('user_supplier_id','=',$user_id)->first();
        if($order_data)
        {
            $order_data->request_status = $request->order_status;
            $order_data->save();
            if($order_data->save())
            {
                return $this->sendResponse($order_data, 'Orders Status Changed Successfully');
            }
            else{
                $error =  "Something Went Worng";
                return $this->sendError($error, 'Unable to change  order status');
            }
        }
        else{
            $error =  "Order Doesn't exit";
            return $this->sendError($error, 'No such order exist');
        }
    }

//========================LIST OF ORDERS' ACCEPTED BY SUPPLIER==============================//
public function supplierAcceptedOrders(Request $request)
{
    $validator = Validator::make($request->all(), [
        'locale'=>'required|in:en,ar,ku',
        'supplier_current_lat'=>'required',
        'supplier_current_long'=>'required',
    ]);
    if($validator->fails()){
        return $this->sendError('Validation Error.', $validator->errors());
    }

    $user = User::find(auth()->user()->id);

    if($request->locale=='en')
    {
        $order_list =  DB::table('orders as o')
                        ->leftJoin('users as u', 'o.user_customer_id', '=', 'u.id')
                        ->leftJoin('user_addresses as a', 'a.id', '=', 'o.user_customer_address_id')
                        ->leftJoin('services as s','s.id','=','o.service_id')
                        ->leftJoin('measurement_units as mu','mu.id','=','s.unit')
                        ->select('o.*','u.mobile_number as customer_mobile','u.name as customer_name',
                                'a.address as customer_address','a.name as address_type','a.latitude','a.longitude','o.service_quantity','mu.measurement_unit','s.name')
                        ->where('user_supplier_id',$user['id'])
                        ->where('o.request_status',"Processing")
                        ->get();
    }
    elseif($request->locale=='ku')
    {
        $order_list =  DB::table('orders as o')
        ->leftJoin('users as u', 'o.user_customer_id', '=', 'u.id')
        ->leftJoin('user_addresses as a', 'a.id', '=', 'o.user_customer_address_id')
        ->leftJoin('services as s','s.id','=','o.service_id')
        ->leftJoin('measurement_units as mu','mu.id','=','s.unit')
        ->select('o.*','u.mobile_number as customer_mobile','u.name as customer_name',
                'a.address as customer_address','a.name as address_type','a.latitude','a.longitude','o.service_quantity','mu.measurement_unit_kr as measurement_unit','s.name_kr as name')
        ->where('user_supplier_id',$user['id'])
        ->where('o.request_status',"Processing")
        ->get();
    }
    elseif($request->locale=='ar')
    {
        $order_list =  DB::table('orders as o')
        ->leftJoin('users as u', 'o.user_customer_id', '=', 'u.id')
        ->leftJoin('user_addresses as a', 'a.id', '=', 'o.user_customer_address_id')
        ->leftJoin('services as s','s.id','=','o.service_id')
        ->leftJoin('measurement_units as mu','mu.id','=','s.unit')
        ->select('o.*','u.mobile_number as customer_mobile','u.name as customer_name',
                'a.address as customer_address','a.name as address_type','a.latitude','a.longitude','o.service_quantity','mu.measurement_unit_ar as measurement_unit','s.name_ar as name')
        ->where('user_supplier_id',$user['id'])
        ->where('o.request_status',"Processing")
        ->get();
    }

    $orderArray=array();

    foreach($order_list as $index=>$order_arrived)
    {
        $customer_lat 		= $order_arrived->latitude;
        $customer_long  	= $order_arrived->longitude;

        $distance = $this->distance($request->supplier_current_lat,$request->supplier_current_long,$customer_lat,$customer_long,'K');
        $order_arrived->total_distance = $distance;
        array_push($orderArray,$order_arrived);
    }

    if(count($order_list) > 0){
        foreach($order_list as $orders_data)
        {
            $orders_data->view_type = '1';
        }
        return $this->sendResponse($order_list, 'Accepted Orders List Found Successfully');
    }
    else{
        $error =  "Empty order list";
        return $this->sendError($error, 'Empty Accepted Order List');
    }
}

//=============================SEARCH SUPPLIER BY PHONE NO================================//
public function searchSupplierByNumber(Request $request)
{

    $validator = Validator::make($request->all(), [
        'phone_no' => 'required',

    ]);

    if($validator->fails()){
        return $this->sendError('Validation Error.', $validator->errors());
    }

    $user = User::where('mobile_number',$request->phone_no)->where('user_type','supplier')->first();

    if(count($user)){

        return $this->sendResponse($user, 'Supplier Found Successfully');
    }
    else{
        $error =  "Empty supplier list";
        return $this->sendError($error, 'No supplier found under this phone no');
    }
}



}
