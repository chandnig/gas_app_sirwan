<?php


namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\API\BaseController as BaseController;
use App\User;
use App\Models\Configuration;
use App\Models\UserAddress;
use App\Models\Order;
use App\Models\UserSupplier;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use App\Models\Service;
use App\Models\HowToModel;
use Carbon\Carbon;
use DateTime;




class CustomerController extends BaseController
{


//===========================ADD SUPPLIER RATING==========================//
    /**
     * Rate Supplier
     *
     * @return \Illuminate\Http\Response
     */
    public function rateSupplier(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'rate' => 'required|in:0,1,2,3,4,5',
            'order_id'=>'required',

        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $orderdata = Order::find($request->order_id);
        if(count($orderdata) > 0){
            $orderdata->rating = $request->rate;
            $orderdata->save();
            if($orderdata->save()){
                return $this->sendResponse($orderdata, 'Rated Successfully');
            }
            else{
                $error =  "Something went wrong";
                return $this->sendError($error, 'Error in rating');
            }
        }
        else{
            $error =  "Empty Order list";
            return $this->sendError($error, 'Order does not Exist');
        }
    }



    //===========================CUSTOMER ORDER LIST==========================//
    /**
     * customer order list
     *
     * @return \Illuminate\Http\Response
     */
    public function customerOrderList(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'service_id' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }


        $user = User::find(auth()->user()->id);

        $order_list =   DB::table('orders as o')
        ->leftJoin('users as u', 'o.user_supplier_id', '=', 'u.id')
        ->select('o.*','u.mobile_number as supplier_mobile','u.name as first_name','u.last_name as last_name')
        ->where('user_customer_id',$user['id'])
        ->where('service_id',$request->service_id)
        ->where('o.request_status','<>','Cancelled')
        ->get();

        foreach($order_list as $order){

            $date   = $order->created_at;
            $d      = new DateTime($date);
            $day    = $d->format('l');
            $time   = date("g:i A", strtotime($date));
            $date_new = explode(" ", $date);
            $order->day = $day;
            $order->time = $time;
            $order->date_new = $date_new[0];
        }

        if(count($order_list) >0 ){
            return $this->sendResponse($order_list, 'Orders Found Successfully');
        }
        else{
            $error =  "Empty Order list";
            return $this->sendError($error, 'Empty Order List');
        }


    }

    // Customer Orders List
    public function customerOrdersList(Request $request)
    {
        $orderList = array();
        $user = User::find(auth()->user()->id);

        $order_list =   DB::table('orders as o')
        ->leftJoin('users as u', 'o.user_supplier_id', '=', 'u.id')
        ->leftJoin('feedback as f','o.order_id', '=', 'f.order_id')
        ->select('o.*','f.rating','u.name as first_name','u.last_name as last_name')
        ->where('user_customer_id',$user['id'])
        //->where('o.request_status','<>','Cancelled')
        ->get();
        
        foreach($order_list as $key=> $order){

            $date   = $order->created_at;
            $d      = new DateTime($date);
            $day    = $d->format('l');
            $time   = date("g:i A", strtotime($date));
            $date_new = explode(" ", $date);
            $orderList[$key]['id'] = $order->id;
            $orderList[$key]['date'] = $date_new[0];
            $orderList[$key]['time'] = $time;
            $orderList[$key]['day'] = $day;
            $orderList[$key]['supplier_name'] = $order->first_name ." ".$order->last_name;
            $orderList[$key]['feedback'] = $order->rating;
            // $order->day = $day;
            // $order->time = $time;
            // $order->date_new = $date_new[0];
        }
        $orderLists = $this->array_multi_unique($orderList);
        if(count($orderLists) >0 ){
            return $this->sendResponse($orderLists, 'Orders List');
        }
        else{
            $error =  "Empty Order list";
            return $this->sendError($error, 'Empty Order List');
        }


    }
    public function array_multi_unique($multiArray){

        $uniqueArray = array();
    
        foreach($multiArray as $subArray){
    
          if(!in_array($subArray, $uniqueArray)){
            $uniqueArray[] = $subArray;
          }
        }
        return $uniqueArray;
      }

      
    //===========================MY Calculation (GET TOTAL ORDER COUNT)==========================//
    /**
     * customer order list
     *
     * @return \Illuminate\Http\Response
     */
    public function myCalculation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from_date'    => 'required',
            'to_date'      =>'required',
            'service_id'   =>'required',

        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $user = User::find(auth()->user()->id);
        $from_date = date("$request->from_date");
        $to_date = date("$request->to_date");

        $child_ids = DB::table("services as s")
                    ->leftJoin("measurement_units as mu",'s.unit','=','mu.id')
                    ->where('s.parent_id',$request->service_id)
                    ->select("s.id","mu.measurement_unit as name")->get();


        if(count($child_ids) > 0)
        {
            $setArray =array();
            $sum_quantity = array();
            $total_value=0;
            $total_value1=0;
            foreach($child_ids as $ids)
            {
                $setArray =  Order::where('service_id','=',$ids->id)
                            ->where('request_status','Closed')
                            ->whereBetween('created_at', [$from_date,$to_date])
                            ->where('user_customer_id','=',$user['id'])
                            ->sum('service_quantity');

                if(($ids->name) == 'set' || ($ids->name) == 'Set')
                {
                     $total_value =  $total_value + $setArray;

                     $sum_quantity[0] = array($ids->name =>  $total_value);
                }
                elseif(($ids->name) == 'Quantity' || ($ids->name) == 'quantity')
                {
                     $total_value1 =  $total_value1 + $setArray;

                     $sum_quantity[1] = array($ids->name =>  $total_value1);
                }
                else{
                    $newArray = array($ids->name => $setArray);
                    array_push($sum_quantity , $newArray);
                }

            }
        }
        else{
            $setArray = Order::where('service_id','=',$request->service_id)
                        ->where('request_status','Closed')
                        ->whereBetween('created_at', [$from_date,$to_date])
                        ->where('user_customer_id','=',$user['id'])
                        ->sum('service_quantity');

            $sum_quantity[] = array("Quantity" => $setArray);
            $emptyArray = array("Set" => "");
            array_push($sum_quantity,$emptyArray);

        }

        if(count($sum_quantity)>0){
                return $this->sendResponse($sum_quantity, 'Data Found Successfully');
        }
        else{
            $error =  "Empty Quantity Number";
            return $this->sendError($error, 'Empty Quantity Number');
        }

    }

    public function getSubService($id,$locale){

        // $sub_cat_data = DB::table('services as s')
        //     ->leftJoin('measurement_units as mu','s.unit','=','mu.id')
        //     ->where('parent_id','=',$id)
        //     ->select('s.id','s.name','s.image','s.default_qty','mu.measurement_unit','s.created_at')->where('state','=','1')->get();

            if($locale=='en'){
                $sub_cat_data = DB::table('services as s')
                               ->leftJoin('measurement_units as mu','s.unit','=','mu.id')
                               ->where('parent_id','=',$id)
                               ->select('s.id','s.name','s.image','s.default_qty','mu.measurement_unit as quantity_type','s.created_at')->where('state','=','1')->get();

            }elseif($locale=='ar'){
                $sub_cat_data = DB::table('services as s')
                               ->leftJoin('measurement_units as mu','s.unit','=','mu.id')
                               ->where('parent_id','=',$id)
                               ->select('s.id','s.name_ar as name','s.image_ar as image','mu.measurement_unit_ar as quantity_type','s.default_qty','s.created_at')->where('state','=','1')->get();
            }elseif($locale=='ku'){
                $sub_cat_data =  DB::table('services as s')
                               ->leftJoin('measurement_units as mu','s.unit','=','mu.id')
                               ->where('parent_id','=',$id)
                               ->select('s.id','s.name_kr as name','s.image_kr as image','mu.measurement_unit_kr as quantity_type','s.default_qty','s.created_at')->where('state','=','1')->get();
            }

        return $sub_cat_data;

        }


//==========================GET SUPPLIER/CUSTOMER HOWTO======================================//

    /** get howto and ads
    *
    * @return \Illuminate\Http\Response
    */

    public function getHowTo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'locale' => 'required|in:en,ar,ku',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $user = User::find(auth()->user()->id);

        $how_to="";
        if($user->user_type =="customer")
        {
                if($request['locale']=='en'){
                    $how_to = HowToModel::select('howto_customer_en as how_to')->first();
                }elseif($request['locale']=='ar'){
                    $how_to = HowToModel::select('howto_customer_ar as how_to')->first();
                }elseif($request['locale']=='ku'){
                    $how_to = HowToModel::select('howto_customer_kr as how_to')->first();
                }
        }

        elseif($user->user_type =="supplier"){
            if($request['locale']=='en'){
                $how_to = HowToModel::select('howto_supplier_en as how_to')->first();
            }elseif($request['locale']=='ar'){
                $how_to = HowToModel::select('howto_supplier_ar as how_to')->first();
            }elseif($request['locale']=='ku'){
                $how_to = HowToModel::select('howto_supplier_kr as how_to')->first();
            }
        }


        if($how_to){

            return $this->sendResponse($how_to, 'How To data found successfully');
        }

        else{
            $error =  "Enter valid locale";
            return $this->sendError($error, 'Error in getting How To data');
        }
    }


//==========================GET SUPPLIER/CUSTOMER ADS======================================//

    /** get howto and ads
    *
    * @return \Illuminate\Http\Response
    */
    public function getAds(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'locale' => 'required|in:en,ar,ku',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $user = User::find(auth()->user()->id);

        $ads_data="";

        if($user->user_type =="customer")
        {
            if($user->customer_type=="home_customer")
            {
                if($request['locale']=='en'){
                    $ads_data = HowToModel::select('ads_customer_en as ads')->first();
                }elseif($request['locale']=='ar'){
                    $ads_data = HowToModel::select('ads_customer_ar as ads')->first();
                }elseif($request['locale']=='ku'){
                    $ads_data = HowToModel::select('ads_customer_kr as as')->first();
                }
            }
            elseif($user->customer_type=="business_customer")
            {
                if($request['locale']=='en'){
                    $ads_data = HowToModel::select('ads_business_customer_en as ads')->first();
                }elseif($request['locale']=='ar'){
                    $ads_data = HowToModel::select('ads_business_customer_ar as ads')->first();
                }elseif($request['locale']=='ku'){
                    $ads_data = HowToModel::select('ads_business_customer_kr as ads')->first();
                }
            }

        }

        elseif($user->user_type =="supplier"){
            if($request['locale']=='en'){
                $ads_data = HowToModel::select('ads_supplier_en as ads')->first();
            }elseif($request['locale']=='ar'){
                $ads_data = HowToModel::select('ads_supplier_ar as ads')->first();
            }elseif($request['locale']=='ku'){
                $ads_data = HowToModel::select('ads_supplier_kr as ads')->first();
            }
        }

        if($ads_data){

            return $this->sendResponse($ads_data, 'Ad data found successfully');
        }

        else{
            $error =  "Enter valid locale";
            return $this->sendError($error, 'Error in getting Ad data');
        }
    }
}
