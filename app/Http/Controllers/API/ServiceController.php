<?php


namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Service;
use App\Models\GasAppFee;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;


class ServiceController extends BaseController
{
  public function gasServiceFee($govId)
  {
    $service = [];
    if($govId)
    {
      $serviceData = Service::where('state',"1")->get();
      //dd($serviceData);
      $serviceArray = array();
      foreach($serviceData as $key => $service)
      {
          if(strpos($service['location'],$govId)) {
              $serviceId =  $service['id'];
              // echo $serviceId."<br>";
              $gasApp = GasAppFee::where('governorate_id','=',$govId)->where('service_id','=',$serviceId)->first();
              $serviceArray[$key]['id'] = $serviceId;
              $serviceArray[$key]['govId'] = $govId;
              $serviceArray[$key]['name'] = $service['name'];
              $serviceArray[$key]['service_price'] = $gasApp['service_price'];
              $serviceArray[$key]['service_price_many'] = $gasApp['service_price_many'];
              $serviceArray[$key]['service_quantity'] = $gasApp['service_quantity'];
          }
      }
      $service = $serviceArray;
      return $this->sendResponse($service, 'Gas Fee');
    }
    else {
      $error =  "Something went wrong";
      return $this->sendError($error, 'Enter GovernorateId');
    }
         
  }
}  
