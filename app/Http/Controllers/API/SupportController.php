<?php


namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Order;
use App\Models\Service;
use App\Models\UserSupplier;
use App\User;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;


class SupportController extends BaseController
{

  /** supplier service list api
  *
  * @return \Illuminate\Http\Response
  */
   public function getSupplierServiceList(Request $request)
   {
        $image_path = url('/').'/uploads/services';

        $serviceData =  DB::table('roles')
            ->where('guard_name','api')
            ->where('guard_name','api')
            ->select('*')
            ->get();

        if(count($serviceData)>0){
            // foreach($serviceData as $service ){
            //     $service->baseUrl = $image_path;
            // }
            return $this->sendResponse($serviceData, 'Service List Found Successfully');
        }
        else{
            $error =  "Empty service data ";
            return $this->sendError($error, 'Empty Service Data');
        }
    }


        /** supplier registration
    *
    * @return \Illuminate\Http\Response
    */
   public function supplierSignUp(Request $request)
   {
        $validator = Validator::make($request->all(), [

            'name' => 'required',
            'last_name' => 'required',
            'mobile_number' => 'required|unique:users',
            'password' => 'required|min:6',
            'user_language' => 'required',
            'role_id'=>'required',
            'governorate_id'=>'required',
            'city_id'=>'required',
            'section_ids'=>'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $digits = 4;
        $otp = str_pad(rand(0, pow(10, $digits)-1), $digits, '0', STR_PAD_LEFT);

        $input = ([
            'name' => $request->name,
            'last_name' => $request->last_name,
            'mobile_number' => $request->mobile_number,
            'password' => bcrypt($request->password),
            'user_language' => $request->user_language,
            'user_type' => 'supplier',
            //'otp' =>  $otp,
        ]);


        $user = User::create($input);
        if($user){
            $location_details = ([
                'user_id'           =>  $user->id,
                'governorate_id'    =>  $request->governorate_id,
                'city_id'           =>  $request->city_id,
                'section_ids'       =>  $request->section_ids,
            ]);

            $location_data = UserSupplier::create($location_details);

            // return $location_data;
            $success['id'] =$user->id;
            $success['token'] = $user->createToken('MyApp')->accessToken;
            $success['name'] =  $user->name;
            $success['last_name'] =  $user->last_name;
            $success['mobile_number'] =  $user->mobile_number;
            $success['user_language'] =  $user->user_language;
            // $success['otp'] =  $user->otp;

            return $this->sendResponse($success, 'User register successfully.');
        }
        else{
            $error =  "Something went wrong";
            return $this->sendError($error, 'Error in regestering user');
        }
    }


    /** update vehicle/liscence
    *
    * @return \Illuminate\Http\Response
    */

    public function updateSupplier(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'vehicle_number' => 'required',
            'license_number' => 'required',
            'user_language' => 'required|in:en,kr,ar',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $user = User::find(auth()->user()->id);
        if($user){
            $user->user_language = $request->user_language;
            $user->save();
            if($user->save()){

                $user_supplier = UserSupplier::where('user_id','=',$user['id'])->update(['vehicle_number' => $request->vehicle_number, 'license_number' => $request->license_number]);

                return $this->sendResponse($user, 'Supplier Updated Successfully');
            }
            else{
                $error =  "Something went wrong ";
                return $this->sendError($error, 'Error in updating Supplier');
            }
        }
        else{
            $error =  "Invalid Token ";
            return $this->sendError($error, 'Error in updating Profile');
        }

    }


    /** add location(latitude longitude) of Supplier
    *
    * @return \Illuminate\Http\Response
    */

    public function addLocation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'latitude' => 'required',
            'longitude' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $user = User::find(auth()->user()->id);
        if(count($user)>0){
            $data = UserSupplier::where('user_id','=',$user['id'])->  $user_supplier = UserSupplier::where('user_id','=',$user['id'])
                                ->update(['latitude' => $request->latitude, 'longitude' => $request->longitude]);;

            return $this->sendResponse($data, 'Supplier Location Added Successfully');


        }
        else{
            $error =  "Invalid Token ";
            return $this->sendError($error, 'Error in adding Location');
        }

    }


}
