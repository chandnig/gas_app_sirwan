<?php


namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\API\BaseController as BaseController;
use App\User;
use App\Models\Configuration;
use App\Models\UserAddress;
use App\Models\UserSupplier;
use Illuminate\Support\Facades\Auth;
use Validator;
use DB;
use Twilio\Rest\Client;


class RegisterController extends BaseController
{


//===========================CUSTOMER REGISTRATION/LOGIN APIS==========================//
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
       
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'last_name' => 'required',
            'mobile_number' => 'required|unique:users',
            'password' => 'required',
            'user_language' => 'required|in:en,kr,ar',
            'user_type' => 'required|in:customer,supplier',

        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        if($request->user_type == "customer"){
            $validator = Validator::make($request->all(), [
               'customer_type' =>'required|in:home_customer,business_customer',

            ]);

            if($validator->fails()){
                return $this->sendError('Validation Error.', $validator->errors());
            }
        }
        if($request->user_type == "supplier"){
            $validator = Validator::make($request->all(), [
                'role_id'=>'required',
                'governorate_id'=>'required',
                'city_id'=>'required',
                'section_ids'=>'required',
            ]);

            if($validator->fails()){
                return $this->sendError('Validation Error.', $validator->errors());
            }
        }

        $input = ([
            'name' => $request->name,
            'last_name' => $request->last_name,
            'mobile_number' => $request->mobile_number,
            'password' => bcrypt($request->password),
            'user_language' => $request->user_language,
            'customer_type' => $request->customer_type,
            'user_type' => $request->user_type,
        ]);

        $user = User::create($input);

        if($request->user_type == "supplier"){
            $location_details = ([
                'user_id'           =>  $user->id,
                'role_supplier_id'  =>  $request->role_id,
                'governorate_id'    =>  $request->governorate_id,
                'city_id'           =>  $request->city_id,
                'section_ids'       =>  $request->section_ids,
            ]);

            $location_data = UserSupplier::create($location_details);
        }
        if($user){
          $success['id'] =$user->id;
          $success['token'] =  $user->createToken('MyApp')->accessToken;
          $success['name'] =  $user->name;
          $success['last_name'] =  $user->last_name;
          $success['mobile_number'] =  $user->mobile_number;
          $success['user_language'] =  $user->user_language;
          return $this->sendResponse($success, 'User register successfully.');
        }

        else{
            $error =  "Something went wrong";
            return $this->sendError($error, 'Error in regestering user');
        }
    }

    /**
     * Terms and Condition
     * @return \Illuminate\Http\Response
     */
    public function termsAndCondtion(Request $request){

        $validator = Validator::make($request->all(), [
            'locale' => 'required|in:en,ar,ku',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $terms_and_conditon="";

        if($request['locale']=='en'){
            $terms_and_conditon = Configuration::select('tnc','created_at')->first();
        }elseif($request['locale']=='ar'){
            $terms_and_conditon = Configuration::select('tnc_ar','created_at')->first();
        }elseif($request['locale']=='ku'){
            $terms_and_conditon = Configuration::select('tnc_kr','created_at')->first();
        }

        if($terms_and_conditon){

            return $this->sendResponse($terms_and_conditon, 'Terms and Condition found successfully');
        }

        else{
            $error =  "Enter valid locale";
            return $this->sendError($error, 'Error in getting terms and condtion');
        }
    }



    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request){

        $mobile_number = $request['mobile_number'];
        $pass = $request['password'];
        if(Auth::attempt(['mobile_number' => $mobile_number, 'password' => $pass])){
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')->accessToken;
            $success['user_type'] =  $user->user_type;
            $success['mobile_number'] =  $user->mobile_number;

            return $this->sendResponse($success, 'User Login successfully.');
            // return response()->json(['success' => $success], $this-> successStatus);
        }
        else{
            $error =  "Invalid Credential. Please try again";
            return $this->sendError($error, 'Error in login user');
        }
    }


    /** changePassword
    *
    * @return \Illuminate\Http\Response
    */
    public function changePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'new_password' => 'required',
            'confirm_password' => 'required|same:new_password',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }
        $data = $request->all();
        $user = User::find(auth()->user()->id);
        // $user = Auth::user();
            // if($user){
        if($user){
             if (Hash::check($request->old_password,  $user['password'])) {
                    $user->password = bcrypt($request->new_password);
                    $user->save();
                    return $this->sendResponse($user, 'Password Updated Successfully');
                   }

                else{
                    $error =  "Old password doesn't match";
                    return $this->sendError($error, 'Error in Changigng Password');
                }
            }

        else{
            $error =  "Invalid Token or Incorrect Old Password";
            return $this->sendError($error, 'Error in changigng password');
        }

    }


    /** forgetPassword
    *
    * @return \Illuminate\Http\Response
    */
    public function forgotPassword(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'mobile_number'     => 'required',
            'password'          => 'required',
            'confirm_password'  => 'required|same:password',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $user = User::where('mobile_number',$request->mobile_number)->first();

            if($user){
                $user->password = bcrypt($request->password);
                $user->save();
                if($user->save())
                {
                    return $this->sendResponse($user,'Password Updated Successfully');
                }
                else{
                    $error =  "Invalid Token or Incorrect Old Password";
                    return $this->sendError($error, 'Error in changing password');
                }
            }
            else{
                $error =  "Invalid Token or Incorrect Old Password";
                return $this->sendError($error, 'Error in changigng password');
            }

    }
//=====================================LOGOUT =========================================//

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }


//=========================================USER PROFILE DETAIL APS========================//
    /** details api
    *
    * @return \Illuminate\Http\Response
    */
    public function userDetails()
    {
        $user = Auth::user();
            if($user){
                if($user['user_type']=="supplier")
                {
                    $supplier_data = DB::table('user_suppliers as us')
                                    ->leftJoin('user_supplier_service_rates as usr' ,'usr.supplier_id','=','us.user_id')
                                    ->where('us.user_id','=',$user['id'])
                                    ->select('us.vehicle_number','us.license_number','us.role_supplier_id','usr.rate_cards')->first();


                    $user->vehicle_number =$supplier_data->vehicle_number;
                    $user->license_number =$supplier_data->license_number;
                    $user->role_supplier_id =$supplier_data->role_supplier_id;
                    $user->ProductList =$supplier_data->rate_cards;
                }
                return $this->sendResponse($user, 'User Login successfully.');
            }
            else{
                $error =  "Invalid token";
                return $this->sendError($error, 'Error in login user');
            }
    }

    /** editProfile
    *
    * @return \Illuminate\Http\Response
    */
    public function editProfile(Request $request)
    {
        $user = User::find(auth()->user()->id);

        $validator = Validator::make($request->all(), [
            'first_name'    => 'required',
            'last_name'     => 'required',
            'mobile_number' => 'required|unique:users,mobile_number,' . $user->id,
            'user_language' => 'required|in:en,kr,ar',
            'customer_type' => 'required|in:home_customer,business_customer',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        if($user){
            $user->name           =   $request->first_name;
            $user->last_name      =   $request->last_name;
            $user->mobile_number  =   $request->mobile_number;
            $user->user_language  =   $request->user_language;
            $user->customer_type  =   $request->customer_type;
            $user->save();
            return $this->sendResponse($user, 'Profile Updated Successfully');
        }
        else{
            $error =  "Invalid Token ";
            return $this->sendError($error, 'Error in updating Profile');
        }

    }



    //===============================ADDRESS MODULE STARTS HERE=======================//
    /** add user Address
    *
    * @return \Illuminate\Http\Response
    */
    public function addAddress(Request $request)
    {
        //return $this->sendResponse($request->all(), 'Address Added successfully.');
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'address' => 'required',
            'latitude'=>'required',
            'longitude'=>'required',
            'governorate_id'=>'required',
            'city_id'=>'required',
            'section_id'=>'required'
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $user = Auth::user();

        $total_address = UserAddress::where('user_id','=',$user['id'])->where('address_status','active')->get();

        $default_address = UserAddress::where('user_id','=',$user['id'])
                    ->where('address_status','active')->where('default','=',0)->get();
        $input_var='0';

        if(count($default_address)<1){

            $input_var  = '1' ;
        }

        if(count($total_address) >=3){
            $error =  "Add Address limit exceeds";
            return $this->sendError($error, 'You can add only 3 address');
        }

        $input = ([
            'user_id'           =>  $user['id'],
            'name'              =>  $request->name,
            'address'           =>  $request->address,
            'latitude'          =>  $request->latitude,
            'longitude'         =>  $request->longitude,
            'address_status'    =>  'active',
            'comment'           =>  $request->comment,
            'default'           =>  $input_var,
            'governorate_id'    =>  $request->governorate_id,
            'city_id'    =>  $request->city_id,
            'section_id'    =>  $request->section_id,
        ]);

        $user = UserAddress::create($input);

        if($user){
            $success['id']          =  $user->id;
            $success['name']        =  $user->name;
            $success['address']     =  $user->address;
            $success['latitude']    =  $user->latitude;
            $success['longitude']   =  $user->longitude;
            $success['comment']     =  $user->comment;
            $success['governorate_id']  =  $user->governorate_id;
            $success['city_id']  =  $user->city_id;
            $success['section_id']  =  $user->section_id;

        return $this->sendResponse($success, 'Address Added successfully.');
        }

        else{
            $error =  "Something went wrong";
            return $this->sendError($error, 'Error in regestering user');
        }

    }

    /** get user Address
    *
    * @return \Illuminate\Http\Response
    */
    public function getAddress(Request $request)
    {

        $user = Auth::user();

        $UserAddress = UserAddress::where('user_id','=',$user['id'])->where('address_status','active')->get();

        if(count($UserAddress)>0){
            return $this->sendResponse($UserAddress, 'User Address Found Successfully');
        }
        else{
            $error = "Empty User Address ";
            return $this->sendError($error, 'Empty Address Data');
        }

    }

    /** delete user Address
    *
    * @return \Illuminate\Http\Response
    */
    public function deleteAddress(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'address_id' => 'required',

        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $data = UserAddress::find($request->address_id);

        if($data){
            $data->address_status = 'delete';
            $data->save();
                if($data->save()){
                    return $this->sendResponse($data, 'Address Deleted Successfully');
                }
                else{
                    $error = "error in deleting address";
                    return $this->sendError($error, 'Empty Address Data');
                }
        }
        else{
            $error = "No data found";
            return $this->sendError($error, 'Address does not exist');
        }

    }

    /** update user Address
    *
    * @return \Illuminate\Http\Response
    */
    public function updateAddress(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'address_id' => 'required',
            'name' => 'required',
            'address' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'governorate_id'=>'required',
            'city_id'=>'required',
            'section_id'=>'required'

        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $data = UserAddress::find($request->address_id);

        if($data){
            $data->name = $request->name;
            $data->address = $request->address;
            $data->latitude = $request->latitude;
            $data->longitude = $request->longitude;
            $data->governorate_id= $request->governorate_id;
            $data->city_id= $request->city_id;
            $data->section_id= $request->section_id;
            $data->save();
            return $this->sendResponse($data, 'Address Updated Successfully');
        }
        else{
            $error = "No data found";
            return $this->sendError($error, 'Address does not exist');
        }

    }

     /** mobile number exist
    *
    * @return \Illuminate\Http\Response
    */
    public function mobileAlreadyExist(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile_number' => 'required',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $mobile_exist = User::where('mobile_number',$request->mobile_number)->get();
        if(count($mobile_exist) > 0) {

            $error ="Mobile number already exist";

            return $this->sendError($error, 'Mobile number already exist');
        }
        else{
            $error ="No data ";
            return $this->sendResponse($error, 'Mobile number doesnt exist' );
        }
    }

    public function generateOtp(Request $request)
    {

        $str_result = '0123456789';
        $otp=substr(str_shuffle($str_result), 0, 6);

        $number='+919877108532';
        $account_sid = getenv("TWILIO_SID");
        $auth_token = getenv("TWILIO_AUTH_TOKEN");
        $twilio_number = getenv("TWILIO_NUMBER");
        $client = new Client($account_sid, $auth_token);
        $message=$client->messages->create($number,
            ['from' => $twilio_number, 'body' => $otp] );

        if($client) {
            return $this->sendResponse($otp, 'Otp found successfully' );
        }
        else{
            $error ="Something went wrong";
            return $this->sendError($error, 'Otp not found');
        }
    }




}
