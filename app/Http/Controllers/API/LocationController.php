<?php


namespace App\Http\Controllers\API;


use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Order;
use App\Models\Location;
use Illuminate\Support\Facades\Auth;
use Validator;


class LocationController extends BaseController
{
    /** location list api
    *
    * @return \Illuminate\Http\Response
    */
   public function getLocation(Request $request)
   {
    $validator = Validator::make($request->all(), [
        'locale' => 'required|in:en,ar,kr',
    ]);

    if($validator->fails()){
        return $this->sendError('Validation Error.', $validator->errors());
    }
    $locationData='';
    if($request['locale']=='en'){
        $locationData = Location::select('id','name','type','code','location','helpline_number','created_at')
                        ->where('parent_id','=',0)->where('type','governorate')->get();
    }elseif($request['locale']=='ar'){
        $locationData = Location::select('id','name_ar','type','code','location','helpline_number','created_at')
        ->where('type','governorate')->where('parent_id','=',0)->get();
    }elseif($request['locale']=='kr'){
        $locationData = Location::select('id','name_kr','type','code','location','helpline_number','created_at')
        ->where('type','governorate')->where('parent_id','=',0)->get();
    }
    if(count($locationData)>0){
        return $this->sendResponse($locationData, 'Location Governate Found Successfully');
    }
    else{
        $error =  "Empty location data ";
        return $this->sendError($error, 'Empty Location Data');
    }
   }

    /** city list api
    *
    * @return \Illuminate\Http\Response
    */
    public function getCity(Request $request)
    {
     $validator = Validator::make($request->all(), [
             'location_id' => 'required',
             'locale' => 'required|in:en,kr,ar',
         ]);

         if($validator->fails()){
             return $this->sendError('Validation Error.', $validator->errors());
         }

         $location_id = $request->location_id;

        $cityData="";
         if($request['locale']=='en'){
             $cityData = Location::select('id','name','type','code','location','created_at')
                             ->where('type','city')->where('parent_id','=',$location_id)->get();
         }elseif($request['locale']=='ar'){
             $cityData = Location::select('id','name_ar','type','code','location','created_at')
                            ->where('type','city')->where('parent_id','=',$location_id)->get();
         }elseif($request['locale']=='kr'){
             $cityData = Location::select('id','name_kr','type','code','location','created_at')
                            ->where('type','city')->where('parent_id','=',$location_id)->get();
         }

         if(count($cityData)>0){
             return $this->sendResponse($cityData, 'City Found Successfully');
         }
         else{
             $error =  "Empty city data ";
             return $this->sendError($error, 'Empty City Data');
         }
    }
    /** section list api
    *
    * @return \Illuminate\Http\Response
    */
    public function getSection(Request $request)
    {
     $validator = Validator::make($request->all(), [
             'city_id' => 'required',
             'locale' => 'required|in:en,kr,ar',
         ]);

         if($validator->fails()){
             return $this->sendError('Validation Error.', $validator->errors());
         }

         $city_id = $request->city_id;

        $sectionData="";
         if($request['locale']=='en'){
             $sectionData = Location::select('id','name','type','code','location','created_at')
                             ->where('type','section')->where('parent_id','=',$city_id)->get();
         }elseif($request['locale']=='ar'){
             $sectionData = Location::select('id','name_ar','type','code','location','created_at')
                            ->where('type','section')->where('parent_id','=',$city_id)->get();
         }elseif($request['locale']=='kr'){
             $sectionData = Location::select('id','name_kr','type','code','location','created_at')
                            ->where('type','section')->where('parent_id','=',$city_id)->get();
         }

         if(count($sectionData)>0){
             return $this->sendResponse($sectionData, 'Section Found Successfully');
         }
         else{
             $error = "Empty city data ";
             return $this->sendError($error, 'Section City Data');
         }
    }




}
