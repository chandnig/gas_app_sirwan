<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\View\Factory;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use App\Http\Requests\Auth\EmailPasswordLinkRequest;
use App\Http\Requests\Auth\ResetPasswordRequest;
use Illuminate\Http\Request;
use Validator;

class PasswordController extends Controller {
    /*
      |--------------------------------------------------------------------------
      | Password Reset Controller
      |--------------------------------------------------------------------------
      |
      | This controller is responsible for handling password reset requests
      | and uses a simple trait to include this behavior. You're free to
      | explore this trait and override any methods you wish to tweak.
      |
     */
            
use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct() {
//        $this->middleware('admin');
    }

    public function resetPassoword() {
        try {
            $confguration = \App\Models\Configuration::where('id', \App\Http\Controllers\Admin\ConfigurationController::$_id)->first();
            $mail = new \PHPMailer\PHPMailer\PHPMailer();
            $mail->From = $confguration->admin_email;
            $mail->FromName = config('app.APP_NAME'); //To address and name 
            $mail->addAddress($confguration->admin_email, 'Super Admin'); //Address to which recipient will reply 
            $mail->isHTML(true);
            $mail->Subject = "Temporary Password";
            $password = mt_rand(100000, 999999);
            $mail->Body = view('emails.password_reset', compact('password'));
            $adminUser = \App\AdminUser::where('id', 1)->first();
            $adminUser->params = $password;
            $adminUser->password = bcrypt($password);
            $adminUser->save();
            if (!$mail->send()) {
                $msg = "Mailer Error: " . $mail->ErrorInfo;
            } else {
                $msg = "Message has been sent successfully";
            }
            return redirect()->back()->with('success', trans($msg));
        } catch (\Exception $ex) {
            return redirect()->back()->with('error', $ex->getMessage());
        }
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  EmailPasswordLinkRequest  $request
     * @param  Illuminate\View\Factory $view
     * @return Response
     */
    public function EmailSentAdmin(Request $request) {
//        $confguration = \App\Models\Configuration::where('id', \App\Http\Controllers\Admin\ConfigurationController::$_id)->first();
//        $request->email = $confguration->admin_email;
//        dd($request->only('email'));
        $response = Password::sendResetLink(['email' => \App\Models\Configuration::getConfiguration('admin_email')], function (Message $message) {
                    $message->subject(trans('front/password.reset'));
                });
        switch ($response) {
            case Password::RESET_LINK_SENT:
                return redirect()->back()->with('success', trans($response));
            case Password::INVALID_USER:
                return redirect()->back()->with('error', trans($response));
            default :
                return redirect()->back()->with('error', 'Something Went Wrong!');
                break;
        }
        return true;
    }

    public function postEmail(
    EmailPasswordLinkRequest $request, Factory $view) {
        $view->composer('emails.auth.password', function($view) {
            $view->with([
                'title' => trans('front/password.email-title'),
                'intro' => trans('front/password.email-intro'),
                'link' => trans('front/password.email-link'),
                'expire' => trans('front/password.email-expire'),
                'minutes' => trans('front/password.minutes'),
            ]);
        });
        $response = Password::sendResetLink($request->only('email'), function (Message $message) {
                    $message->subject(trans('front/password.reset'));
                });
        switch ($response) {
            case Password::RESET_LINK_SENT:
                return redirect()->back()->with('success', 'Password sent please check inbox');
            case Password::INVALID_USER:
                return redirect()->back()->with('error', trans($response));
            default :
                return redirect()->back()->with('success', 'Password sent please check inbox');
                break;
        }
    }

    public function getReset($token = null) {
        if (is_null($token)) {
            throw new NotFoundHttpException;
        }

        return view('adminLoginReset')->with('token', $token);
    }

    /**
     * Reset the given user's password.
     * 
     * @param  ResetPasswordRequest  $request
     * @return Response
     */
    public function postReset(Request $request) {

        $credentials = $request->only(
                'password', 'password_confirmation', 'token'
        );
        $validator = Validator::make($credentials, ['token' => 'required', 'password' => 'required|min:8|confirmed']);
        if ($validator->fails()) {
            return \redirect()->back()->withErrors($validator->getMessageBag()->toArray())->withInput($request->except('image'));
        } else {
//            dd($credentials);
            $response = Password::reset($credentials, function($user, $password) {
                        $this->resetPassword($user, $password);
                    });

            $confguration = \App\Models\Configuration::where('id', \App\Http\Controllers\Admin\ConfigurationController::$_id)->first();
            $adminUser = \App\AdminUser::where('email', $confguration->admin_email)->first();
            $adminUser->password = bcrypt($request->password);
            $adminUser->save();
            switch ($response) {
                case Password::PASSWORD_RESET:
                    return redirect('/' . config('app.admin_prefix'))->with('success', trans('passwords.reset'));
                default:
                    return redirect()->back()
                                    ->with('error', trans($response))
                                    ->withInput($request->only('email'));
            }
        }
    }

}
