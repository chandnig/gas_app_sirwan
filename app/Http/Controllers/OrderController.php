<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Models\Order;
use Yajra\Datatables\Datatables;
use App\Models\UserAddress;
use Validator;
use Auth;
use DB;

class OrderController extends Controller {

    public function __construct() {
//        $this->middleware('guest', ['except' => 'logout']);
    }

    protected static function baseURL() {
        return config('app.admin_prefix');
    }
    protected $baseVIEW = 'admin.order';

    public function index(Request $request) {
        if (!Auth::guard('admin')->check()) {
            return redirect()->route('admin.login');
            throw UnauthorizedException::notLoggedIn();
        }
        else{
            if (request()->ajax()) {
            $selected =  DB::table('orders as o')
                             ->Join('users as u','u.id','=','o.user_customer_id')
                             ->Join('users as us','us.id','=','o.user_supplier_id')
                             ->Join('services as s','s.id','=','o.service_id')
                             ->select('o.*','u.name as Customer Name','u.last_name','us.name as Supplier Name','s.name as Service')
                             ->where('row_status','active')
                             ->get();

            //    echo"<pre>";
            //    print_r($total_orders);
                return DataTables::of($selected)->addColumn('action', function($selected) {
                    return '<a class="ajaxify btn btn-info btn-sm btnEdit" href="' . url(self::baseURL()) . '/order_detail'.'/'. $selected->id . '">View Order Details</a>
                    <a class="ajaxify btn btn-secondary btn-sm btnEdit" href="' . url(self::baseURL()) .'/forward_order'.'/'. $selected->id . '">Forward</a>
                    <a class="ajaxify btn btn-danger btn-sm btnEdit" href="' . url(self::baseURL()) . '/'. $selected->id . '">Delete</a>';
                })
                ->rawColumns(['action'])->make(true);
            }
                return view('admin.order.view', ['title' => 'Admin User', 'fields' => ['Customer Name','Supplier Name','Service','service_quantity','request_status','created_at']]);

        //  return view('admin.order.view',compact(['total_orders']));
        }
    }

    public function orderDetail($id) {
        if (!Auth::guard('admin')->check()) {
            return redirect()->route('admin.login');
            throw UnauthorizedException::notLoggedIn();
        }
        else{
            if (request()->ajax()) {
                $selected =  DB::table('orders as o')
                 ->Join('users as u','u.id','=','o.user_customer_id')
                 ->Join('users as us','us.id','=','o.user_supplier_id')
                 ->Join('services as s','s.id','=','o.service_id')
                 //  ->select('ti.*','ci.country_name','ri.role')
                 ->select('o.*','u.name','u.last_name','us.name as supplier_name','s.name as service')
                 ->where('o.id','=',$id)
                 ->get();

            return DataTables::of($selected)
                            ->addColumn('action', function($selected) {
                                return '';
                            })
                            ->rawColumns(['action'])->make(true);
                        }
            return view('admin.order.orderDetail', ['title' => 'Admin User', 'fields' => ['rating']]);
       }
    }


    public function forward_view($id) {
        if (!Auth::guard('admin')->check()) {
            return redirect()->route('admin.login');
            throw UnauthorizedException::notLoggedIn();
        }
        else{
            $selected =  DB::table('users')
            ->select("u.*")->where('user_type','supplier')
            ->get();
            return view('admin.order.forward_view', ['title' => 'Forward Order to Other Supplier', 'data'=>$selected]);
        }
    }

    public function multipleOrderPlace(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'id'  =>'required',
            'quantity'   => 'required'
        ]);

        $error="Field is required";
        $user = \Auth::user();
        $service_id             =    $request->id;;
        $quantity               =    $request->quantity;

        if($validator->fails()){
            return view('frontend.order-quantity',compact('service_id','quantity','error'));
        }

        $service_id = array_filter($service_id);
        $quantity   = array_filter($quantity);
        $serviceId_quantity=array_combine($service_id,$quantity);

        $user_data = UserAddress::where('user_id','=',$user['id'])->get();
        return view('frontend.order-gas-multiple', compact('user_data','serviceId_quantity'));

    }






}




