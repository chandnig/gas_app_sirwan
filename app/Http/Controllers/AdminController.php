<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Location;
use App\Models\Service;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller{

    public function __construct() {
        $roles = \Spatie\Permission\Models\Role::where('guard_name','web-admin')->get()->pluck('name')->toArray();
//        dd(implode(',', $roles));
        $this->middleware(['role:'.implode(',', $roles)]);
//        dd(Auth::guard('admin')->user());
//        dd(auth()->guard('admin')->user());
    }

    public function dashboard() {
        //$totalcustomers = User::select('*')->role(\GasApplication::getUserRoles('customer', 'ctg'))->count();
        $blockedcustomers = User::select('*')->where('user_type','customer')->where('status',0)->count();
        
        $totalcustomers = User::select('*')->where('user_type','customer')->count();
        $todaycustomers = User::select('*')->where('user_type','customer')->whereDate('created_at',Carbon::today())->count();
       // dd($todaycustomers);
        $totalsuppliers = User::select('*')->where('user_type','customer')->count();

        $todaysuppliers = User::select('*')->where('user_type','supplier')->whereDate('created_at',Carbon::today())->count();
           //dd($todaysuppliers);
        $pendingsuppliers = User::select('*')->where('user_type','supplier')->where('state',0)->count();
        $activesuppliers = User::select('*')->where('user_type','supplier')->where('state',1)->count();

         $activeproducts = Location::select('*')->where('type', 'governorate')->get();
         $Service = Service::select('*')->get();
         $govArr = array();
         
         foreach ($activeproducts as $key => $location) {
            $govId =  $location['id'];
            $serviceArr = array();
            foreach ($Service as  $value) {
                //echo "=====".$govId."=====";
                $loc =  trim($value['location'],'[]'); 
                $b = explode(",",$loc);

                for ($i=0; $i < count($b) ; $i++) { 
                    if($govId == trim($b[$i],'""'))
                    {
                        array_push($serviceArr,$value['name']);
                    }
                }
                $govArr[$key]['govName'] = $location['name'];
                $govArr[$key]['services'] = $serviceArr;
            }
         }
        // dd($govArr);
        return view('admin.dashboard', ['totalcustomers' => $totalcustomers, 'todaycustomers' => $todaycustomers, 'totalsuppliers' => $totalsuppliers, 'todaysuppliers' => $todaysuppliers,'blockedcustomers'=>$blockedcustomers,'pendingsuppliers'=>$pendingsuppliers,'activesuppliers'=>$activesuppliers,'activeproducts'=>$activeproducts,'govData'=>$govArr]);
    }

    public function index() {
        return view('admin.admin-users');
    }

    public function deleteAll($model) {
        \DB::table($model)->truncate();
        return response()->json(['status' => true]);
    }

}
