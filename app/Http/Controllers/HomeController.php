<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Validator;
use Auth;
use DB;
use App\Models\Service;
use App\Models\UserAddress;
use App\Models\ProblemModel;
use App\Models\Order;
use App\User;
use Carbon\Carbon;
use DateTime;
use App\Models\SupplierSearchModel;
use App\Models\CustomerSearchDraftModel;


class HomeController extends Controller {

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware(function ($request, $next) {
            //dd(\Auth::user());
            if (\Auth::user() === null)
                return redirect('/login');
            return $next($request);
        });
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard() {

        $services = \App\Models\Service::where('state', '1')->where('parent_id', '0')->with('childern')->get();
//         dd($services->toArray());
        return view('frontend.home', compact('services'));
    }

    public function getservice(Request $request) {

            $user = User::find(auth()->user()->id);
            $user_id = $user->id;
            // $services= array();

            $order_his = Order::where("user_customer_id",'=',$user_id)
                        ->where('request_status','<>','Cancelled')
                        ->select('service_id')
                        ->groupBy('service_id')
                        ->get();


                if(count($order_his) > 0)
                        {
                            foreach($order_his as $order)
                            {
                                $Order = Service::where('services.id','=',$order->service_id)->where('state','=','1')
                                ->select('services.id','name','name_ar','name_kr','parent_id','image','default_qty','measurement_unit','measurement_unit_ar','measurement_unit_kr')
                                ->leftJoin('measurement_units', 'services.unit','=','measurement_units.id')
                                ->whereNull('deleted_at')->first();

                                if($Order->parent_id==0)
                                {
                                    $Order->has_child = "no";
                                    $order_history[] = $Order;
                                }
                                else {
                                    $parent_id= $Order->parent_id;
                                    // $order_data =Service::find($parent_id);
                                    $order_data = Service::where('services.id','=',$parent_id)->where('state','=','1')
                                    ->select('services.id','name','parent_id','image','name_ar','name_kr','default_qty','measurement_unit','measurement_unit_kr','measurement_unit_ar')
                                    ->leftJoin('measurement_units', 'services.unit','=','measurement_units.id')->first();

                                    $order_data->image_base_url = $image_path;
                                    $order_data->has_child = "yes";
                                    $order_history[] = $order_data;
                                }
                        }

                        $services = $this->array_multi_unique($order_history);



                         if(count($services) > 0)
                                return view('frontend.services',compact('services'));
                         else{
                            return view('frontend.services',compact('services'));
                         }
                }
                else{
                    return view('frontend.services',compact('services'));
                }

        // $services = \App\Models\Service::where('state', '1')->where('parent_id', '0')->get();

        // return view('frontend.services',compact('services'));
    }



    public function profile() {
        $users = \Auth::user();

        $user_address = UserAddress::where('user_id',$users['id'])->get();



        return view('frontend.profile', ['users' => $users,'address'=>$user_address]);
    }

    public function profilesave(Request $request) {
        $rules = Admin\UserCustomerController::rules();
        unset($rules['role_customer']);
        unset($rules['password']);
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            try {
                $model = \App\User::find(\Auth::user()->id);
                foreach (array_keys($rules) as $index):
                    if ($index == 'role_customer')
                        continue;
                    if ($index == 'password')
                        $model->$index = Hash::make($request->$index);
                    else
                        $model->$index = $request->$index;
                endforeach;
                $model->save();
                 return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }



//    public function ordergas() {
//        $currentuser = \App\User::where('id', \Auth::user()->id)->with('addresses')->first();
//        return view('frontend.order-gas', compact('currentuser'));
//    }

    public function getphonenumber(Request $request) {
        $data = \App\User::where('mobile_number', 'like', '%' . $request->search . '%')->get();
//        dd(count($data));
        return (count($data) == 0) ? ['errors' => 'Supplier Not Found with this Mobile Number'] : $data->first();
    }

    public function ordershow($id) {
        // $orderlist = \App\Models\Service::findOrFail($id);
        // $message = trans('passwords.type');
        $user = \Auth::user();
        // $currentuser = \App\User::where('id', \Auth::user()->id)->with('addresses')->first();
        $service_data = DB::table('services as s')
                        ->leftJoin('measurement_units as mu','mu.id','=','unit')
                        ->where('s.id','=',$id)
                        ->select('s.*','measurement_unit','measurement_unit_ar','measurement_unit_kr')
                        ->get();

        $user_data = UserAddress::where('user_id','=',$user['id'])->get();

        return view('frontend.order-gas', compact('service_data','user_data'));
    }


//SHOW CHILD SERVICES OF SPECIFIC SERVICES
public function orderquantity($id) {

    $child_services = DB::table('services as s')
                    ->leftJoin('measurement_units as mu','s.unit','=','mu.id')
                    ->where('parent_id','=',$id)
                    ->select('s.*','mu.measurement_unit','mu.measurement_unit_ar','mu.measurement_unit_kr')->get();

    return view('frontend.order-quantity',compact('child_services'));
}

//MY ORDER MENU
    public function order_historyshow($id) {

        $service_id = $id;
        $order_list =   DB::table('orders as o')
        ->leftJoin('users as u', 'o.user_supplier_id', '=', 'u.id')
        ->leftJoin('services as s','s.id','=','o.service_id')
        ->leftJoin('measurement_units as mu','mu.id','=','s.unit')
        ->select('o.*','s.name as service_name','s.name_kr as service_name_kr','s.name_ar as service_name_ar',
                'u.mobile_number','u.name','u.last_name','u.name_ar',
                'u.last_name_ar','u.name_kr','u.last_name_kr','mu.measurement_unit','mu.measurement_unit_ar','mu.measurement_unit_kr')
        ->where('user_customer_id',\Auth::user()->id)
        ->where('service_id',$id)
        ->where('request_status', '<>' , 'Cancelled')
        ->orderBy('o.created_at','desc')
        ->get();

        foreach($order_list as $order)
        {
            $date   = $order->created_at;
            $d      = new DateTime($date);
            $day    = $d->format('l');
            $time   = date("g:i A", strtotime($date));
            $date_new = explode(" ", $date);

            $order->day = $day;
            $order->time = $time;
            $order->date_new = $date_new[0];
        }


        return view('frontend.order', compact('order_list','service_id'));
    }

//TERMS and CONDITIONS
    public function terms_and_conditions()
    {

        $terms = \App\Models\Configuration::first();

        return view('frontend.terms_and_conditions', compact('terms'));
    }


    public function problem(Request $request)
    {

        $order_id       =    $request->order_id;
        $supplier_id    =    $request->supplier_id;
        $service_id     =    $request->service_id;

        return view('frontend.problem',compact('order_id','supplier_id','service_id'));
    }


    public function submitProblem(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'service_id'    =>  'required',
            'supplier_id'   =>  'required',
            'order_id'      =>  'required',
            'problem_data'  =>  'required'

        ]);

        $error="Field is required";

        $user = \Auth::user();

        $order_id           =    $request->order_id;
        $supplier_id        =    $request->supplier_id;
        $customer_id        =    $user['id'];
        $problem_data       =    $request->problem_data;
        $service_id         =    $request->service_id;

        if($validator->fails()){
            return view('frontend.problem',compact('order_id','supplier_id','error','service_id'));
        }

        $store = new ProblemModel();
        $store->order_id        =    $order_id;
        $store->customer_id     =    $customer_id;
        $store->supplier_id     =    $supplier_id;
        $store->problem_data    =    $problem_data;
        $store->problem_type    =    "text";
        $store->save();
        if($store->save())
        {
            return view('frontend.problem',compact('order_id','supplier_id','store','service_id'));
        }
        else{
            return view('frontend.problem',compact('order_id','supplier_id','store','service_id'));
        }
    }

    public function orderDelete_Customer(Request $request)
    {
        $user = \Auth::user();
        $order_id           =    $request->order_id;
        $service_id         =    $request->service_id;
        // date_default_timezone_set('Asia/Kolkata');
        $current_time       =  date("Y-m-d H:i:s");

        $year = $carbon->year;


        $order_data = Order::find($order_id);

        $diff_in_minutes = $order_data->created_at->diffInMinutes($current_time);

        // return "Current Date.->". $current_time .
        // "Diff In Minutes:->".$diff_in_minutes.
        // "Order Id:->$order_id " ;

         if($diff_in_minutes<5){
            $order_data->request_status = "Cancelled";
            $order_data->save();

            if($order_data->save()){

                return redirect()->route('order_history.show',array('id' => $service_id));
            }
            else{
                return redirect()->route('order_history.show',array('id' => $service_id));
            }
         }
         else{
            return redirect()->route('order_history.show',array('id' => $service_id));
         }

    }


    public function placeOrder_Step1(Request $request) {

        $user = \Auth::user();
        $quantity           =    $request->quantity;
        $address_id         =    $request->address;
        $service_id         =    $request->service_id;
        $service_name         =    $request->service_name;

        $draft_data = CustomerSearchDraftModel::where('user_id','=',$user['id'])
        ->where('service_id','=',$service_id)->first();

         $draft_mobile_number = $draft_data->last_serched_mobile_no;

        $customer_address = UserAddress::find($address_id);
        $customer_lat =$customer_address->latitude;
        $customer_long =$customer_address->longitude;


        $supplier = DB::table('user_suppliers as us')
                    ->leftJoin('users as u','us.user_id','=','u.id')
                    ->select('us.*','u.name','u.last_name','u.mobile_number','u.name_kr','u.name_ar','u.last_name_ar','u.last_name_kr')
                    ->get();


        $supplierArray=array();

        foreach($supplier as $index=>$d)
        {
            $supplier_lat 		= $d->latitude;
            $supplier_long  	= $d->longitude;

            $distance = $this->distance($customer_lat,$customer_long,$supplier_lat,$supplier_long,'K');


           if($distance<10)
            {
                $supplier[$index]->total_distance = $distance;
                array_push($supplierArray,$supplier[$index]);
            }
        }

        if($supplierArray){
            return view('frontend.order_confirm',compact('supplierArray','quantity','address_id','service_id','service_name','draft_mobile_number'));
        }
        else{
            return view('frontend.order_confirm',compact('supplierArray','quantity','address_id','service_id','service_name','draft_mobile_number'));
        }


    }

    public function placeOrder_Step1Multiple(Request $request) {

        $user = \Auth::user();
        $quantity           =    $request->quantity;
        $address_id         =    $request->address;
        $service_id         =    $request->service_id;



        $customer_address = UserAddress::find($address_id);

        $customer_lat =$customer_address->latitude;
        $customer_long =$customer_address->longitude;

        $supplier = DB::table('user_suppliers as us')
        ->leftJoin('users as u','us.user_id','=','u.id')
        ->select('us.*','u.name','u.last_name','u.mobile_number','u.name_kr','u.name_ar','u.last_name_ar','u.last_name_kr')
        ->get();


        $supplierArray=array();


        foreach($supplier as $index=>$d)
        {
            $supplier_lat 		= $d->latitude;
            $supplier_long  	= $d->longitude;

            $distance = $this->distance($customer_lat,$customer_long,$supplier_lat,$supplier_long,'K');



           if($distance<10)
            {
                $supplier[$index]->total_distance = $distance;
                array_push($supplierArray,$supplier[$index]);
            }
        }


        $service_id = array_filter($service_id);
        $quantity   = array_filter($quantity);
        $serviceId_quantity=array_combine($service_id,$quantity);
        if($supplierArray){
            return view('frontend.order_confirm_multiple',compact('supplierArray','serviceId_quantity','address_id'));
        }
        else{
            return view('frontend.order_confirm_multiple',compact('supplierArray','serviceId_quantity','address_id'));
        }



    }
    public function distance($lat1, $lon1, $lat2, $lon2, $unit) {

        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
          return 0;
        }
        else {
          $theta = $lon1 - $lon2;


        //   echo  "lon1 = > ".$lon1." lat1 => ".$lat1."  lon2 => ".$lon2." lat2 => ".$lat2;

          $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
          $dist = acos($dist);
          $dist = rad2deg($dist);
          $miles = $dist * 60 * 1.1515;
          $unit = strtoupper($unit);

          if ($unit == "K") {
            return ($miles * 1.609344);
          } else if ($unit == "N") {
            return ($miles * 0.8684);
          } else {
            return $miles;
          }
        }

    }


    public function search_supplier(Request $request)
    {
       $supplier_mobile = $request->supplier_mobile_no;
       $quantity = $request->quantity;
       $address_id = $request->address_id;
       $service_id = $request->service_id;
       $service_name = $request->service_name;

       $user = \Auth::user();
       $customer_address = UserAddress::find($address_id);
       $customer_lat =$customer_address->latitude;

       $customer_long =$customer_address->longitude;

       //Save last insert phone number
       $customer_draft = CustomerSearchDraftModel::where('user_id','=',$user['id'])
                         ->where('service_id','=',$service_id)->first();

        if($customer_draft)
        {
            $customer_draft->last_serched_mobile_no = $supplier_mobile;
            $customer_draft->save();
        }
        else{
            $customer_draft = new CustomerSearchDraftModel();
            $customer_draft->user_id = $user['id'];
            $customer_draft->service_id = $service_id;
            $customer_draft->last_serched_mobile_no = $supplier_mobile;
            $customer_draft->save();
        }

        $draft_mobile_number = $customer_draft->last_serched_mobile_no;

        //SUPPLIER EXIST
       $supplier_exist = User::where('mobile_number',$supplier_mobile)->first();
       if(!$supplier_exist)
       {
           $supplier_search = new SupplierSearchModel();
           $supplier_search->customer_id            =  $user['id'];
           $supplier_search->supplier_number    =  $supplier_mobile;
           $supplier_search->service_id         =  $service_id;
           $supplier_search->save();
       }

       $supplier = DB::table('user_suppliers as us')
                   ->leftJoin('users as u','us.user_id','=','u.id')
                   ->select('us.*','u.name','u.last_name','u.mobile_number',
                        'u.name_kr','u.name_ar','u.last_name_ar','u.last_name_kr')
                   ->get();

       $supplierArray=array();
       foreach($supplier as $index=>$d)
       {
           $supplier_lat 		= $d->latitude;
           $supplier_long  	= $d->longitude;

           $distance = $this->distance($customer_lat,$customer_long,$supplier_lat,$supplier_long,'K');

          if($distance<10)
           {
               $supplier[$index]->total_distance = $distance;
               array_push($supplierArray,$supplier[$index]);
           }
       }

       return view('frontend.order_confirm',compact('supplier_exist','supplier_mobile','supplierArray','quantity',
                    'address_id','service_id','service_name','draft_mobile_number'));

    }

    public function search_supplier_multiple(Request $request)
    {
       $supplier_mobile = $request->supplier_mobile_no;
       $quantity = $request->quantity;
       $address_id = $request->address_id;
       $service_id = $request->service_id;



       $user = \Auth::user();
       $customer_address = UserAddress::find($address_id);
       $customer_lat =$customer_address->latitude;
       $customer_long =$customer_address->longitude;

       $supplier = DB::table('user_suppliers as us')
                   ->leftJoin('users as u','us.user_id','=','u.id')
                   ->select('us.*','u.name','u.last_name','u.mobile_number','u.name_kr','u.name_ar','u.last_name_ar','u.last_name_kr')
                   ->get();
       $supplierArray=array();
       foreach($supplier as $index=>$d)
       {
           $supplier_lat 		= $d->latitude;
           $supplier_long  	= $d->longitude;

           $distance = $this->distance($customer_lat,$customer_long,$supplier_lat,$supplier_long,'K');


          if($distance<10)
           {
               $supplier[$index]->total_distance = $distance;
               array_push($supplierArray,$supplier[$index]);
           }
       }

       $supplier_exist = User::where('mobile_number',$supplier_mobile)->first();
       $service_id = array_filter($service_id);
       $quantity   = array_filter($quantity);
       $serviceId_quantity=array_combine($service_id,$quantity);

       return view('frontend.order_confirm_multiple',compact('supplier_exist','supplier_mobile','supplierArray','address_id','serviceId_quantity'));

    }

    public function placeOrderFinalStep(Request $request)
    {

        $user = \Auth::user();
        $customer_id = $user['id'];
        $supplier_id = $request->supplier_id;
        $addresss_id= $request->confirm_address;
        $quantity = $request->confirm_quantity;
        $service_id = $request->confirm_service_id;

        if($customer_id=="" || $supplier_id=="" || $addresss_id=="" || $quantity=="" || $service_id=="" )
        {
            $error="Order not placed. Please enter appropriate details for placing order";
            $services = \App\Models\Service::where('state', '1')->where('parent_id', '0')->with('childern')->get();
             return view('frontend.home',compact('error','services'));
        }

        $current_timestamp = Carbon::now()->timestamp;

        $order_id = substr($current_timestamp, 2);


        $store = new Order();
        $store->user_customer_id            = $customer_id;
        $store->user_customer_address_id    = $addresss_id;
        $store->service_quantity            = $quantity;
        $store->user_supplier_id            = $supplier_id;
        $store->service_id                  = $service_id;
        $store->order_id                    = $order_id;
        $store->save();
        if($store->save())
        {
            // return redirect()->route('order_history.show',array('id' => $service_id));
            return view('frontend.confirm_success',compact('service_id'));
        }
        else{
            return view('frontend.confirm_success',compact('service_id'));
        }

    }


    public function placeOrderFinalStepMultiple(Request $request)
    {

        $user = \Auth::user();
        $customer_id = $user['id'];
        $supplier_id = $request->supplier_id;
        $addresss_id= $request->confirm_address;
        $quantity = $request->confirm_quantity;
        $service_id = $request->confirm_service_id;
        $current_timestamp = Carbon::now()->timestamp;
        $order_id = substr($current_timestamp, 2);



        $service_id = array_filter($service_id);
        $quantity   = array_filter($quantity);
        $serviceId_quantity=array_combine($service_id,$quantity);
        foreach($serviceId_quantity as $key=>$value)
        {
            $store = new Order();
            $store->user_customer_id            = $customer_id;
            $store->user_customer_address_id    = $addresss_id;
            $store->service_quantity            = $value;
            $store->user_supplier_id            = $supplier_id;
            $store->service_id                  = $key;
            $store->order_id                    = $order_id;
            $store->save();
        }
        return redirect()->route('home.dashboard');
        // if($store->save())
        // {
        //     return redirect()->route('order_history.show',array('id' => $service_id));
        // }
        // else{
        //     return redirect()->route('order_history.show',array('id' => $service_id));
        // }

    }

    public function array_multi_unique($multiArray){

        $uniqueArray = array();

        foreach($multiArray as $subArray){

          if(!in_array($subArray, $uniqueArray)){
            $uniqueArray[] = $subArray;
          }
        }
        return $uniqueArray;
      }
}

