<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\User;
use App\Models\UserSupplier;
use App\Models\UserSupplierServiceRate;
use Auth;
use DB;
use App\Models\Order;
use App\Models\Service;
use App\Models\Location;


class UserSupplierController extends \App\Http\Controllers\AdminController {

    public static function rules() {
        return ['name' => 'required|max:100', 'last_name' => 'required|max:100','name_kr' => 'required|max:100', 'last_name_kr' => 'required|max:100',
        'name_ar' => 'required|max:100', 'last_name_ar' => 'required|max:100','email' => '', 'mobile_number' => 'required|unique:users,mobile_number', 'password' => '',      'user_language'=>'required|in:en,kr,ar', 'role_supplier_id' => 'required'];
    }

    protected static function baseURL() {
        return config('app.admin_prefix') . '/suppliers';
    }

    protected static function baseURL_location() {
        return config('app.admin_prefix') . '/supplier_location';
    }
    protected $baseVIEW          =  'admin.supplier';
    protected $baseVIEW_location =  'admin.supplier_location';
    protected $baseVIEW_product  =  'admin.supplier_product';


public function index() {
    $location_data = Location::select('*')->where('parent_id','=','0')->get();
    $services = Service::select('*')->where('parent_id','=','0')->get();

        if (request()->ajax()) {
            $selected = User::select('*')->where('user_type','supplier')->get();
            return DataTables::of($selected)->editColumn('created_at', function ($data) {
                                return $data->created_at->toDayDateTimeString();
                            })
                            ->editColumn('updated_at', function ($data) {
                                return $data->updated_at->toDayDateTimeString();
                            })
                            ->addColumn('action', function($selected) {
                                return '<a class="ajaxify btn btn-success btn-sm btnEdit" href="' . url(self::baseURL()) . '/' . $selected->id . '">Manage Profile</a>&nbsp;<a class="ajaxify btn btn-info btn-sm btnEdit" href="' . url(self::baseURL()) . '/' . $selected->id . '/edit">Edit</a>
                                <button type="submit" class="btn btn-warning btn-sm btnDelete" data-remove="' . url(self::baseURL()) . '/' . $selected->id . '">Delete</button>
                                <a class="ajaxify btn btn-primary btn-sm notify" href = "' . url(config('app.admin_prefix') . '/notification/supplier/' . $selected->id) . '">Notify</a>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input class="delete-suppliers ml-5" type="checkbox" id="' . $selected->id . '" name="chk">';
                            })
                            ->addColumn('status', function($selected) {
                                $checkbox = ($selected->state == '1') ? 'checked' : '';
                                return '<input class="change-state" id="change-state-' . $selected->id . '" data-id="' . $selected->id . '" type="checkbox" ' . $checkbox . ' data-toggle="toggle">';
                            })
                            ->addColumn('notify_checkbox', function ($selected) {
                                return '<input class="send_notification" type="checkbox" id="' . $selected->id . '"  name="chk_notify">';
                            })
                            ->addColumn('checkbox', function ($selected) {
                                $checkbox = ($selected->gasApp_fav == 'yes') ? 'checked' : '';
                                return '<input class="set-favsupplier" type="checkbox" id="' . $selected->id . '" '. $checkbox .' name="chk_fav">';
                            })
                            ->rawColumns(['action', 'status', 'status','notify_checkbox','checkbox'])->make(true);
        }
        return view($this->baseVIEW . '.index', ['title' => 'Supplier', 'createUrl' => url(self::baseURL() . '/create'), 'fields' => ['name', 'mobile_number', 'user_type','status'],'location_data'=>$location_data,'services'=>$services]);
}


 public function indexSearch(Request $request) { 
        if (request()->ajax()) {
            if (count($request->search['value']))
                $selected = User::select('*')->where('user_type','supplier')->get();
            else
                $selected = [];
            return DataTables::of($selected)->editColumn('created_at', function ($data) {
                                return $data->created_at->toDayDateTimeString();
                            })
                            ->editColumn('updated_at', function ($data) {
                                return $data->updated_at->toDayDateTimeString();
                            })
                            ->addColumn('action', function($selected) {
                                return '<a class="ajaxify btn btn-success btn-sm btnEdit" href="' . url(self::baseURL()) . '/' . $selected->id . '">Manage Profile</a>&nbsp;<a class="ajaxify btn btn-info btn-sm btnEdit" href="' . url(self::baseURL()) . '/' . $selected->id . '/edit">Edit</a>
                                <button type="submit" class="btn btn-warning btn-sm btnDelete" data-remove="' . url(self::baseURL()) . '/' . $selected->id . '">Delete</button>
                                <a class="ajaxify btn btn-info btn-sm notify" href="' . url(config('app.admin_prefix') .'/notification/customer/' . $selected->id) . '">Notify</a>';
                                
                            })
                            ->addColumn('status', function($selected) {
                                $checkbox = ($selected->state == '1') ? 'checked' : '';
                                return '<input class="change-state" id="change-state-' . $selected->id . '" data-id="' . $selected->id . '" type="checkbox" ' . $checkbox . ' data-toggle="toggle">';
                            })
                            ->addColumn('notify_checkbox', function ($selected) {
                                //return '<input class="send_notification_customer" type="checkbox" id="' . $selected->id . '"  name="chk_notify">';
                                return '<input class="delete-customers ml-5" type="checkbox" id="' . $selected->id . '" name="chk">';
                            })
                            ->rawColumns(['action', 'status','notify_checkbox'])->make(true);
        }
        return view($this->baseVIEW . '.supplier_search', ['title' => 'Supplier', 'createUrl' => url(self::baseURL() . '/create'), 'fields' => ['name', 'mobile_number', 'status']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view($this->baseVIEW . '.form', ['title' => 'Create Supplier', 'action' => url(self::baseURL()), 'actionMessage' => 'Supplier Created Successfully', 'listUrl' => url(self::baseURL()), 'inputTypeTexts' => self::rules()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {


        $rules = self::rules();
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            try {
                $model = new User();
                $model->name                =    $request->name;
                $model->last_name           =    $request->last_name;
                $model->name_ar             =    $request->name_ar;
                $model->last_name_ar        =    $request->last_name_ar;
                $model->name_kr             =    $request->name_kr;
                $model->last_name_kr        =    $request->last_name_kr;
                $model->email               =    $request->email;
                $model->mobile_number       =    $request->mobile_number;
                $model->password            =    bcrypt($request->password);
                $model->user_type           =    "supplier";
                $model->email               =    $request->email;
                $model->user_language       =    $request->user_language;
                $model->save();

                $user_supplier = new UserSupplier();
                $user_supplier->user_id = $model->id;
                $user_supplier->role_supplier_id = json_encode($request->role_supplier_id);
                $user_supplier->save();

                return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $model
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request) {
        $data = User::where('id', $id)->first();
        // $supplier_location = UserSupplier::where('user_id', $id)->get();

        $supplier_location =   DB::table('user_suppliers as us')
            ->leftJoin('locations as l1', 'us.city_id', '=', 'l1.id')
            ->leftJoin('locations as l2', 'us.section_ids', '=', 'l2.id')
            ->leftJoin('locations as l3', 'us.governorate_id', '=', 'l3.id')
            ->select('us.*','l1.name as city_name','l2.name as section_name','l3.name as governorate_name')
            ->where('us.user_id',$id)
            ->get();

        $supplier_product = UserSupplierServiceRate::where('supplier_id',$id)->first();

        return view($this->baseVIEW . '.view', ['title' => 'Supplier', 'actionMessage' => 'Supplier List', 'data' => $data, 'listUrl' => url(self::baseURL()), 'editUrl' => url(self::baseURL()) . '/' . $id . '/edit', 'editUrl_location' => url(self::baseURL_location()) . '/' . $id . '/edit',
        'driver_fields' => ['name', 'mobile_number', 'status'], 'product_fields' => ['product_name', 'price', 'status'],'supplier_location'=>$supplier_location,'supplier_product'=>$supplier_product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $model
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        // $data = User::find($id);
        $data =  DB::table('users as u')->where('u.id','=',$id)
                ->leftJoin('user_suppliers as us' , 'us.user_id','=','u.id')
                ->select('u.*','us.role_supplier_id')
                ->first();


        $data->password = '';
        return view($this->baseVIEW . '.form', ['title' => 'Edit Supplier', 'action' => url(self::baseURL()) . '/' . $id, 'actionMessage' => 'Supplier Updated Successfully', 'data' => $data, 'listUrl' => url(self::baseURL()), 'inputTypeTexts' => self::rules()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $model
     * @return \Illuminate\Http\Response
     */
    public function changeState(Request $request, $id) {
        if ($model = User::find($id)) {
            $model->state = $request->checked == 'true' ? '1' : '0';
            $model->save();
            $data = 'Success';
        } else {
            $data = 'Failed';
        }
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $model
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $rules = self::rules();
        $rules['mobile_number'] .= ',' . $id;

        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            try {
                $model = User::find($id);
                $model->name                =    $request->name;
                $model->last_name           =    $request->last_name;
                $model->name_ar             =    $request->name_ar;
                $model->last_name_ar        =    $request->last_name_ar;
                $model->name_kr             =    $request->name_kr;
                $model->last_name_kr        =    $request->last_name_kr;
                $model->email               =    $request->email;
                $model->mobile_number       =    $request->mobile_number;
                $model->password            =    bcrypt($request->password);
                $model->user_type           =    "supplier";
                $model->email               =    $request->email;
                $model->user_language       =    $request->user_language;
                $model->save();


                $user_supplier = UserSupplier::where('user_id',$id)->first();
                $user_supplier->role_supplier_id = json_encode($request->role_supplier_id);
                $user_supplier->save();

                return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $model
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (User::destroy($id)) {
            $data = 'Success';
        } else {
            $data = 'Failed';
        }
        return response()->json($data);
    }


       /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_location() {
        $url = explode('/', url()->previous());
        $supplier = end($url);
        return view($this->baseVIEW . '.form', ['title' => 'Create Driver', 'action' => url(self::baseURL()), 'actionMessage' => 'Driver Created Successfully', 'listUrl' => url(self::baseURL()), 'inputTypeTexts' => self::rules(), 'supplier' => $supplier]);
    }








}
