<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Validator;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class AdminRoleController extends \App\Http\Controllers\AdminController {

    protected $rules = ['name' => 'required|unique:roles,name', 'permission' => 'required'];

//    protected static baseURL() = 'fastway/roles';
    protected static function baseURL() {
        return config('app.admin_prefix') . '/admin-roles';
    }

    protected $baseVIEW = 'admin.roles';

    protected function defaultRoles() {
        return ['admin', 'admin/login', 'admin/dashboard'];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (request()->ajax()) {
            $selected = Role::select('*')->where('guard_name', 'web-admin');
            return DataTables::of($selected)->editColumn('created_at', function ($data) {
                                return $data->created_at->toDayDateTimeString();
                            })
                            ->editColumn('updated_at', function ($data) {
                                return $data->updated_at->toDayDateTimeString();
                            })
                            ->addColumn('action', function($selected) {
                                return '<a class="ajaxify btn btn-info btn-sm btnEdit" href="' . url(self::baseURL()) . '/' . $selected->id . '/edit">Edit</a>
                <button type="submit" class="btn btn-warning btn-sm btnDelete" data-remove="' . url(self::baseURL()) . '/' . $selected->id . '">Delete</button>';
                            })
                            ->make(true);
        }
        return view($this->baseVIEW . '.index', ['title' => 'Admin Users Roles', 'createUrl' => url(self::baseURL() . '/create'), 'datatableIndex' => 'admin-role-fetch.index']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $permissions =Permission::where('guard_name', 'web-admin')->get();

        return view($this->baseVIEW . '.form', ['title' => 'Create Admin User Roles', 'action' => url(self::baseURL()), 'actionMessage' => 'Admin User Created Successfully', 'listUrl' => url(self::baseURL()), 'permissions' => $permissions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make(Input::all(), $this->rules);
        if ($validator->fails()) {
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            $role = new Role();
            foreach (array_keys($this->rules) as $index):
                if ($index == 'permission')
                    continue;
                else
                    $role->$index = $request->$index;

            endforeach;
            $request->permission = array_merge($request->permission, $this->defaultRoles());
            $role->givePermissionTo($request->permission);
            $role->save();
            return response()->json(array("success" => true));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data = Role::find($id);
        $permission = Permission::where('guard_name', 'web-admin')->get();
        return view($this->baseVIEW . '.form', ['title' => 'Edit Admin User', 'action' => url(self::baseURL()) . '/' . $id, 'actionMessage' => 'Role User Updated Successfully', 'data' => $data, 'listUrl' => url(self::baseURL()), 'permissions' => $permission]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->rules['name'] = $this->rules['name'] . ',' . $id;
//        dd($this->rules);
        $Validator = Validator::make(Input::all(), $this->rules);
        if ($Validator->fails()) {
            return response()->json(array('errors' => $Validator->getMessageBag()->toArray()));
        } else {
            $role = Role::find($id);
            foreach (array_keys($this->rules) as $index):
                if ($index == 'permission')
                    continue;
                else
                    $role->$index = $request->$index;
            endforeach;
            $request->permission = array_merge($request->permission, $this->defaultRoles());
            $role->syncPermissions($request->permission);
            $role->save();
            return response()->json(array("success" => true));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (Role::destroy($id)) {
            $data = 'Success';
        } else {
            $data = 'Failed';
        }
        return response()->json($data);
    }

}
