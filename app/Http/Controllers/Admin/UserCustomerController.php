<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\User;
use App\Models\Location;
use Auth;
use DB;

class UserCustomerController extends \App\Http\Controllers\AdminController {

    public static function rules() {
        return ['name' => 'required|max:100', 'last_name' => 'required|max:100', 'email' => '',
        'mobile_number' => 'required|unique:users,mobile_number', 'password' => '','user_language'=>'required|in:en,ar,kr','customer_type' => 'required|in:home_customer,business_customer'];
    }

    protected static function baseURL() {
        return config('app.admin_prefix') . '/customers';
    }

    protected $baseVIEW = 'admin.customer';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        
        $location_data = Location::select('*')->where('parent_id','=','0')->get();

        if (request()->ajax()) {
//            dd(\GasApplication::getUserRoles('customer', 'ctg'));
            $selected = User::select('*')->where('user_type','customer')->get();

            return DataTables::of($selected)
            ->editColumn('created_at', function ($data) {
                                return $data->created_at->toDayDateTimeString();
                            })
                            ->editColumn('updated_at', function ($data) {
                                return $data->updated_at->toDayDateTimeString();
                            })
                            ->addColumn('action', function($selected) {
                                return '<a class="ajaxify btn btn-info btn-sm btnEdit" href="' . url(self::baseURL()) . '/' . $selected->id . '/edit">Edit</a>
                                        <button type="submit" class="btn btn-warning btn-sm btnDelete" data-remove="' . url(self::baseURL()) . '/' . $selected->id . '">Delete</button>
                                        <a class="ajaxify btn btn-info btn-sm notify" href="' . url(config('app.admin_prefix') .'/notification/customer/' . $selected->id) . '">Notify</a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                                    })
                            // ->addColumn('status', function($selected) {
                            //     $checkbox = ($selected->state == '1') ? 'checked' : '';
                            //     return '<input class="change-state" id="change-state-' . $selected->id . '" data-id="' . $selected->id . '" type="checkbox" ' . $checkbox . ' data-toggle="toggle">';
                            // })
                            ->addColumn('notify_checkbox', function ($selected) {
                                //return '<input class="send_notification_customer" type="checkbox" id="' . $selected->id . '"  name="chk_notify">';
                                return '<input class="delete-customers ml-5" type="checkbox" id="' . $selected->id . '" name="chk">';
                            })
                            ->rawColumns(['action', 'status','notify_checkbox'])->make(true);
                    }
            return view($this->baseVIEW . '.index', ['title' => 'Customer', 'createUrl' => url(self::baseURL() . '/create'), 'fields' => ['name', 'mobile_number', 'status'],'location_data'=>$location_data]);
    }


    public function indexSearch(Request $request) {
        if (request()->ajax()) {
//            dd(\GasApplication::getUserRoles('customer', 'ctg'));
            if (count($request->search['value']))
                $selected = User::select('*')->where('user_type','customer')->get();
            else
                $selected = [];
            return DataTables::of($selected)->editColumn('created_at', function ($data) {
                                return $data->created_at->toDayDateTimeString();
                            })
                            ->editColumn('updated_at', function ($data) {
                                return $data->updated_at->toDayDateTimeString();
                            })
                            ->addColumn('action', function($selected) {
                                return '<a class="ajaxify btn btn-info btn-sm btnEdit" href="' . url(self::baseURL()) . '/' . $selected->id . '/edit">Edit</a>
                                        <button type="submit" class="btn btn-warning btn-sm btnDelete" data-remove="' . url(self::baseURL()) . '/' . $selected->id . '">Delete</button>
                                        <a class="ajaxify btn btn-info btn-sm notify" href="' . url(config('app.admin_prefix') . '/notification/customer/' . $selected->id) . '">Notify</a>';
                            })
                            ->addColumn('status', function($selected) {
                                $checkbox = ($selected->state == '1') ? 'checked' : '';
                                return '<input class="change-state" id="change-state-' . $selected->id . '" data-id="' . $selected->id . '" type="checkbox" ' . $checkbox . ' data-toggle="toggle">';
                            })
                            ->rawColumns(['action', 'status'])->make(true);
        }

        return view($this->baseVIEW . '.customer_search', ['title' => 'Customer', 'createUrl' => url(self::baseURL() . '/create'), 'fields' => ['name', 'mobile_number', 'status']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        return view($this->baseVIEW . '.form', ['title' => 'Create Customer', 'action' => url(self::baseURL()), 'actionMessage' => 'Customer Created Successfully', 'listUrl' => url(self::baseURL()), 'inputTypeTexts' => self::rules()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $validator = Validator::make(Input::all(), self::rules());
        if ($validator->fails()) {
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            try {
                $model = new User();
                foreach (array_keys(self::rules()) as $key):
                    $index = str_replace('edit_', '', $key);
                    $model->user_type = "customer";
                    // if ($index == 'role_customer')
                    //     continue;
                    if ($index == 'password')
                        $model->$index = Hash::make($request->$key);
                    else
                        $model->$index = $request->$key;
                endforeach;
                $model->assignRole($request->role_customer);
                $model->state = '1';
                $model->save();
                return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $role
     * @return \Illuminate\Http\Response
     */
    public function show(User $role) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $role
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data = User::find($id);
        $data->password = '';
        return view($this->baseVIEW . '.form', ['title' => 'Edit Customer', 'action' => url(self::baseURL()) . '/' . $id, 'actionMessage' => 'Customer Updated Successfully', 'data' => $data, 'listUrl' => url(self::baseURL()), 'inputTypeTexts' => self::rules()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $role
     * @return \Illuminate\Http\Response
     */
    public function changeState(Request $request, $id) {
        if ($model = User::find($id)) {
            $model->state = $request->checked == 'true' ? '1' : '0';
            $model->save();
            $data = 'Success';
        } else {
            $data = 'Failed';
        }
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $rules = self::rules();
        $rules['mobile_number'] .= ',' . $id;
        $Validator = Validator::make(Input::all(), $rules);
        if ($Validator->fails()) {
            return response()->json(array('errors' => $Validator->getMessageBag()->toArray()));
        } else {
            try {
                $model = User::find($id);
                foreach (array_keys(self::rules()) as $key):
                    $index = str_replace('edit_', '', $key);

                    if ($index == 'password')
                        $model->$index = Hash::make($request->$key);
                    else
                        $model->$index = $request->$key;
                endforeach;
                $model->save();
                return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (User::destroy($id)) {
            $data = 'Success';
        } else {
            $data = 'Failed';
        }
        return response()->json($data);
    }

    //=========================DELETE MULTIPLE CUSTOMERS ==========================================
    public function multipledestroy_customers(Request $request) {
        try {
            foreach ($request->checked as $id):
                User::destroy($id);
            endforeach;
            $data = 'Success';
        } catch (Exception $ex) {
            $data = 'something went wrong';
        }
        return response()->json($data);
    }







}
