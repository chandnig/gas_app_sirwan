<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\User;
use App\Models\PushNotification;
use App\Models\Service  ;
use Auth;
use DB;

class PushNotificationController extends Controller
{

    protected static function baseURL() {
        return config('app.admin_prefix') .'/push_notification';
    }

    protected $baseVIEW = 'admin.push_notification';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = Service::select('*')->where('parent_id','=','0')->get();
        if (request()->ajax()) {
            $selected = DB::table('push_notifications as p')
                        ->leftJoin('users as u','u.id','=','p.user_id')
                        ->select('p.*','u.mobile_number','u.name as user_name','u.user_type')->get();

            return DataTables::of($selected)
                            ->addColumn('action', function($selected) {
                                return
                                // '<a class="ajaxify btn btn-info btn-sm btnEdit" href="' . url(self::baseURL()) . '/' . $selected->id . '/edit">Edit</a>
                                        '<button type="submit" class="btn btn-danger btn-sm btnDelete" data-remove="' . url(self::baseURL()) . '/' . $selected->id . '">Delete</button>';
                        })
                            ->rawColumns(['action'])->make(true);
        }
        return view($this->baseVIEW . '.index', ['title' => 'Notification History', 'fields' => ['user_name','notification_text','notification_type','user_response','user_type','created_at'],'services'=>$services]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (PushNotification::destroy($id)) {
            $data = 'Success';
        } else {
            $data = 'Failed';
        }
        return response()->json($data);
    }


    public function filterNotification(Request $request)
    {
        $start_date    =  date("$request->start_date");
        $end_date      =  date("$request->end_date");
        $service_id    =    $request->service_id    ;
        $customer_type =    $request->customer_type ;
        $user_response =    $request->user_response ;


        $services = Service::select('*')->where('parent_id','=','0')->get();

            $query = DB::table('push_notifications as p')
                ->leftJoin('users as u','u.id','=','p.user_id')
                ->leftJoin('user_suppliers as us' ,'us.user_id','=','u.id')
                ->select('u.name as user_name','u.user_type','u.mobile_number','p.*','us.role_supplier_id');

                if($customer_type!=""){
                    $query->where('u.customer_type','=',$customer_type);
                }
                if($user_response!="")
                {
                     $query->where('p.user_response','=',$user_response);
                }

                if($start_date!="" && $end_date!="")
                {
                     $query->whereBetween('p.created_at', [$start_date, $end_date]);
                }

                if($service_id!="")
                {
                     $query->where('us.role_supplier_id','like', '%"'. $service_id .'"%');
                }
                $selected = $query->get();



        return DataTables::of($selected)
                            ->addColumn('action', function($selected) {
                                return
                                        '<button type="submit" class="btn btn-danger btn-sm btnDelete" data-remove="' . url(self::baseURL()) . '/' . $selected->id . '">Delete</button>';
                        })
                            ->rawColumns(['action'])->make(true);

        return view($this->baseVIEW . '.index', ['title' => 'Notification History', 'fields' => ['user_name','notification_text','notification_type','user_response','user_type','created_at'],'services'=>$services]);



    }
}
