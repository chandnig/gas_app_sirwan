<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\User;
use App\Models\UserSupplier;
use Auth;
use DB;

class UserDriverController extends \App\Http\Controllers\AdminController {

    public static function rules() {
        return ['name' => 'required|max:100', 'last_name' => '','email' => '', 'mobile_number' => 'required|unique:users,mobile_number', 'password' => ''];
    }

    protected static function baseURL() {
        return config('app.admin_prefix') . '/drivers';
    }
    protected static function baseURL_supplier() {
        return config('app.admin_prefix') . '/suppliers';
    }

    protected $baseVIEW = 'admin.driver';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (request()->ajax()) {
            $url = explode('/', url()->previous());
            $supplier = end($url);
            $selected = User::where('parent_supplier_id', $supplier)->where('user_type','driver')->get();

            return DataTables::of($selected)->editColumn('created_at', function ($data) {
                        return $data->created_at->toDayDateTimeString();
                    })->editColumn('updated_at', function ($data) {
                        return $data->updated_at->toDayDateTimeString();
                    })->addColumn('action', function($selected) {
                        return '<a class="ajaxify btn btn-info btn-sm btnEdit" href="' . url(self::baseURL()) . '/' . $selected->id . '/edit">Edit</a>
                                <button type="submit" class="btn btn-warning btn-sm btnDelete" data-remove="' . url(self::baseURL()) . '/' . $selected->id . '">Delete</button>';
                    })->addColumn('status', function($selected) {
                        $checkbox = ($selected->state == '1') ? 'checked' : '';
                        return '<input class="change-state" id="change-state-' . $selected->id . '" data-id="' . $selected->id . '" type="checkbox" ' . $checkbox . ' data-toggle="toggle">';
                    })->rawColumns(['action', 'status'])->make(true);
        }
        return view($this->baseVIEW . '.index', ['title' => 'Driver', 'createUrl' => url(self::baseURL() . '/create'), 'fields' => ['name', 'mobile_number','status']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $url = explode('/', url()->previous());
        $supplier = end($url);

        $parent_supplier_role = UserSupplier::where('user_id','=',$supplier)->first();

        return view($this->baseVIEW . '.form', ['title' => 'Create Driver', 'action' => url(self::baseURL()), 'actionMessage' => 'Driver Created Successfully', 'listUrl' => url(self::baseURL_supplier()), 'inputTypeTexts' => self::rules(), 'supplier' => $supplier,'parent_supplier_role'=>$parent_supplier_role]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $rules = self::rules();
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            try {
                $model = new User();
                $model->name = $request->name;
                $model->last_name = $request->last_name;
                $model->email = $request->email;
                $model->mobile_number = $request->mobile_number;
                $model->password = bcrypt($request->password);
                $model->user_type = "driver";
                $model->parent_supplier_id = $request->parent_user_id;
                $model->save();

                $user_role_data             =   new UserSupplier();
                $user_role_data->user_id    =   $model->id;
                $user_role_data->role_supplier_id = $request->role_supplier_id;
                $user_role_data->save();

                return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $role
     * @return \Illuminate\Http\Response
     */
    public function show(User $role) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $role
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        // $data = User::where('id', $id)->where('user_type','driver')->first();
        $data = DB::table('users as u')
                ->leftJoin('user_suppliers as us','us.user_id','=','u.id')
                ->where('u.id', $id)
                ->select('u.*','us.vehicle_number','us.license_number')
                ->first();

        $data->password = '';
        $url = explode('/', url()->previous());
        $supplier = end($url);
        return view($this->baseVIEW . '.form', ['title' => 'Edit Driver', 'action' => url(self::baseURL()) . '/' . $id, 'actionMessage' => 'Driver Updated Successfully', 'data' => $data, 'listUrl' => url(self::baseURL()), 'inputTypeTexts' => self::rules(), 'supplier' => $supplier]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $role
     * @return \Illuminate\Http\Response
     */
    public function changeState(Request $request, $id) {
        if ($model = User::find($id)) {
            $model->state = $request->checked == 'true' ? '1' : '0';
            $model->save();
            $data = 'Success';
        } else {
            $data = 'Failed';
        }
        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $rules = self::rules();
        $rules['mobile_number'] .= ','.$id;
        $rules['email'] .= ','.$id;

        $Validator = Validator::make(Input::all(), $rules);
        if ($Validator->fails()) {
            return response()->json(array('errors' => $Validator->getMessageBag()->toArray()));
        } else {
            try {
                $model = User::find($id);
                $model->name = $request->name;
                $model->last_name = $request->last_name;
                $model->email = $request->email;
                $model->mobile_number = $request->mobile_number;
                $model->password = bcrypt($request->password);
                $model->user_type = "driver";
                $model->parent_supplier_id = $request->parent_user_id;
                $model->save();

                $user_role_data             =   UserSupplier::where('user_id','=',$id)->first();
                $user_role_data->user_id    =   $model->id;
                $user_role_data->role_supplier_id = $request->role_supplier_id;
                $user_role_data->save();

                return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (User::destroy($id)) {
            $data = 'Success';
        } else {
            $data = 'Failed';
        }
        return response()->json($data);
    }

}
