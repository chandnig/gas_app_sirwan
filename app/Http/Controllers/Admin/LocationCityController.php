<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\Models\Location;
use Auth;

class LocationCityController extends \App\Http\Controllers\AdminController {

    protected static function rules() {
        return ['name' => '','name_kr' => 'required|max:100','name_ar' => '', 'code' => '','parent_id'=>'required'];
    }

    protected static function baseURL() {
        return config('app.admin_prefix') . '/city';
    }

    protected $baseVIEW = 'admin.location.city';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        if (request()->ajax()) {
            $selected = Location::select('*')->where('type','city');
            return DataTables::of($selected)->editColumn('created_at', function ($data) {
                                return $data->created_at->toDayDateTimeString();
                            })
                            ->editColumn('updated_at', function ($data) {
                                return $data->updated_at->toDayDateTimeString();
                            })
                            ->editColumn('parent_id', function ($data) {
                                return Location::where('id',$data->parent_id)->first()->name_kr;
                            })
                            ->addColumn('action', function($selected) {
                                return '<a class="ajaxify btn btn-info btn-sm btnEdit" href="' . url(self::baseURL()) . '/' . $selected->id . '/edit">Edit</a>
                                <button type="submit" class="btn btn-warning btn-sm btnDelete" data-remove="' . url(self::baseURL()) . '/' . $selected->id . '">Delete</button>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <input class="delete-city ml-5" type="checkbox" id="' . $selected->id . '" name="chk">';

                            })
                            ->addColumn('status', function($selected) {
                                $checkbox = ($selected->state == '1') ? 'checked' : '';
                                return '<input class="change-state" id="change-state-' . $selected->id . '" data-id="' . $selected->id . '" type="checkbox" ' . $checkbox . ' data-toggle="toggle">';
                            })

                            ->rawColumns(['action', 'status'])->make(true);
        }
        return view($this->baseVIEW . '.index', ['title' => 'Location City', 'createUrl' => url(self::baseURL() . '/create'), 'fields' => ['name','name_kr', 'name_ar','code','parent_id', 'status']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {

        return view($this->baseVIEW . '.form', ['title' => 'Create Location City', 'action' => url(self::baseURL()), 'actionMessage' => 'Location City Created Successfully', 'listUrl' => url(self::baseURL()), 'inputTypeTexts' => self::rules()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $validator = Validator::make(Input::all(), self::rules());
        if ($validator->fails()) {
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
           // try {
                $model = new Location();
                foreach (array_keys(self::rules()) as $key):
                    $model->$key = $request->$key;
                endforeach;
                $model->type='city';
                $model->save();
                return response()->json(array("success" => true));
           // } catch (\Exception $ex) {
                dd($ex->getMessage());
           // }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Location  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Location $role) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Location  $role
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data = Location::find($id);
        $data->password = '';
        return view($this->baseVIEW . '.form', ['title' => 'Edit Location City', 'action' => url(self::baseURL()) . '/' . $id, 'actionMessage' => 'Location City Updated Successfully', 'data' => $data, 'listUrl' => url(self::baseURL()), 'inputTypeTexts' => self::rules()]);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Location  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $Validator = Validator::make(Input::all(), self::rules());
        if ($Validator->fails()) {
            return response()->json(array('errors' => $Validator->getMessageBag()->toArray()));
        } else {
            try {
                $model = Location::find($id);
                foreach (array_keys(self::rules()) as $key):
                    $model->$key = $request->$key;
                endforeach;
                $model->save();
                return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Location  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (Location::destroy($id)) {
            $data = 'Success';
        } else {
            $data = 'Failed';
        }
        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Location  $role
     * @return \Illuminate\Http\Response
     */
    public function multipledestroy(Request $request) {
        try {
            foreach ($request->checked as $id):
                Location::destroy($id);
            endforeach;
            $data = 'Success';
        } catch (Exception $ex) {
            $data = 'something went wrong';
        }
        return response()->json($data);
    }



}
