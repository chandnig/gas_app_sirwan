<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\Models\Service;
use App\Models\Order;
use App\User;
use Auth;
use DB;
use Excel;

class OrderController extends \App\Http\Controllers\AdminController
{
        //public function __construct() {
        //        $this->middleware('guest', ['except' => 'logout']);
        //    }
        protected static function rules() {
            return ['service_quantity'=>'required','request_status'=>'required','service_id'=>'required'];
        }
        protected static function baseURL() {
            return config('app.admin_prefix');
        }
        protected static function baseURL_forward() {
            return config('app.admin_prefix').'/tickting';
        }
        protected $baseVIEW = 'admin.order';
        protected $baseVIEW_supplier = 'admin.suppliers';
        protected $baseVIEW_customer = 'admin.customers';

        public function index(Request $request) {

            $service_data = Service::select('*')->where('parent_id','=','0')->get();
            if (request()->ajax()) {
            $selected =  DB::table('orders as o')
                         ->leftJoin('users as u','u.id','=','o.user_customer_id')
                         ->leftJoin('users as us','us.id','=','o.user_supplier_id')
                         ->leftJoin('services as s','s.id','=','o.service_id')
                         ->select('o.*','u.name as Customer Name','u.last_name','us.name as Supplier Name',
                         's.name as Service','u.mobile_number as customer_number','us.mobile_number as supplier_number')
                         ->get();

                    return DataTables::of($selected)->addColumn('action', function($selected) {
                        return '<a class="ajaxify btn btn-info btn-sm btnEdit" href="' . url(self::baseURL()) . '/order_detail'.'/'. $selected->id . '">Details</a>
                        <a class="ajaxify btn btn-secondary btn-sm btnEdit" href="' . url(self::baseURL()) .'/forward_order'.'/'. $selected->id .'/'. $selected->user_supplier_id .'">Forward</a>
                        <a class="ajaxify btn btn-primary btn-sm btnEdit" href="' . url(self::baseURL()) . '/orders'.'/' . $selected->id . '/edit">Edit</a>';
                        // <a class="ajaxify btn btn-danger btn-sm btnEdit" href="' . url(self::baseURL()) . '/'. $selected->id . '">Delete</a>';
                    })
                    ->rawColumns(['action'])->make(true);
                }
            return view('admin.order.view', ['title' => 'Admin User', 'fields' => ['order_id','Customer Name','customer_number','Supplier Name','supplier_number','Service','service_quantity','request_status','created_at'],'service_data'=>$service_data]);
        }

        public function orderDetail($id) {
                if (!Auth::guard('admin')->check()) {
                    return redirect()->route('admin.login');
                    throw UnauthorizedException::notLoggedIn();
                }
                else{
                    if (request()->ajax()) {
                        $selected =  DB::table('orders as o')
                         ->Join('users as u','u.id','=','o.user_customer_id')
                         ->Join('users as us','us.id','=','o.user_supplier_id')
                         ->Join('services as s','s.id','=','o.service_id')
                         //  ->select('ti.*','ci.country_name','ri.role')
                         ->select('o.*','u.name','u.last_name','us.name as supplier_name','s.name as service')
                         ->where('o.id','=',$id)
                         ->get();

                    return DataTables::of($selected)
                                    ->addColumn('action', function($selected) {
                                        return '';
                                    })
                                    ->rawColumns(['action'])->make(true);
                                }
                    return view('admin.order.orderDetail', ['title' => 'Admin User', 'fields' => ['rating']]);
               }
        }

        public function forward_view($id,$supplier_id) {

            $selected =  DB::table('users as u')
                        ->select("u.*")
                        ->where('user_type','supplier')
                        ->where('id','!=',$supplier_id)
                        ->get();

            return view($this->baseVIEW .'.'.'forward_view', ['title' => 'Forward Order to Other Supplier', 'data'=>$selected,'listUrl' => url(self::baseURL()).'/orders','order_id'=>$id]);
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            // $data = Order::find($id);

            $data =  DB::table('orders as o')
            ->leftJoin('services as s','s.id','=','o.service_id')
            ->where('o.id','=',$id)
            ->select('o.*','s.name')
            ->get();


            return view($this->baseVIEW . '.form', ['title' => 'Edit Order Details', 'action' => url(self::baseURL()) . '/orders' .'/' . $id, 'actionMessage' => 'Order Edited Successfully', 'data' => $data, 'listUrl' => url(self::baseURL_forward()), 'inputTypeTexts' => self::rules()]);
        }

  /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Configuration  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id) {
        try {
            $model = Order::find($id);
            $model->service_quantity    = $request->service_quantity;
            $model->service_id          = $request->service_id;
            $model->request_status      = $request->request_status;
            $model->save();
            return response()->json(array("success" => true));
        } catch (\Exception $ex) {
            dd($ex->getMessage());
        }
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Configuration  $role
     * @return \Illuminate\Http\Response
     */
    public function update_order_supplier(Request $request,$id) {
            try {
                $model = Order::find($id);
                $model->user_supplier_id = $request->user_supplier_id;
                $model->save();
                return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service $role
     * @return \Illuminate\Http\Response
     */
    public function multipledestroy_services(Request $request) {
        try {
            foreach ($request->checked as $id):
                Service::destroy($id);
            endforeach;
            $data = 'Success';
        } catch (Exception $ex) {
            $data = 'something went wrong';
        }
    }

//===================================FILTER SUPPLIER==================================//

    public function filterSupplier(Request $request)
    {   
       $gov_id          =  $request->governorate_id;
       $city_id         =  $request->city_id;
       $service_status  =  $request->service_status;
       $service         =  $request->service_id;


       $selected = DB::table('users as u')
                    ->leftJoin('user_suppliers as us','u.id','=','us.user_id')
                    ->where('u.user_type','supplier')
                    ->where('us.governorate_id','=',$gov_id)
                    ->where('us.city_id','=',$city_id)
                    ->where('u.state','=',$service_status)
                    ->where('us.role_supplier_id','like', '%"'. $service .'"%')
                    ->select('u.*')
                    ->get();

        if($gov_id == "" || $gov_id ==null){
            $selected = DB::table('users as u')
                    ->leftJoin('user_suppliers as us','u.id','=','us.user_id')
                    ->where('u.user_type','supplier')
                    ->where('us.city_id','=',$city_id)
                    ->where('u.state','=',$service_status)
                    ->where('us.role_supplier_id','like', '%"'. $service .'"%')
                    ->select('u.*')
                    ->get();
        }
        if($city_id == "" || $city_id ==null)
        { $selected = DB::table('users as u')
                    ->leftJoin('user_suppliers as us','u.id','=','us.user_id')
                    ->where('u.user_type','supplier')
                    ->where('us.governorate_id','=',$gov_id)
                    ->where('u.state','=',$service_status)
                    ->where('us.role_supplier_id','like', '%"'. $service .'"%')
                    ->select('u.*')
                    ->get();
        }
        if($service_status == "" || $service_status ==null)
        {$selected = DB::table('users as u')
                    ->leftJoin('user_suppliers as us','u.id','=','us.user_id')
                    ->where('u.user_type','supplier')
                    ->where('us.governorate_id','=',$gov_id)
                    ->where('us.city_id','=',$city_id)
                    ->where('us.role_supplier_id','like', '%"'. $service .'"%')
                    ->select('u.*')
                    ->get();
        }
        if($service == "" || $service==null)
        { $selected = DB::table('users as u')
                    ->leftJoin('user_suppliers as us','u.id','=','us.user_id')
                    ->where('u.user_type','supplier')
                    ->where('us.governorate_id','=',$gov_id)
                    ->where('us.city_id','=',$city_id)
                    ->where('u.state','=',$service_status)
                    ->select('u.*')
                    ->get();
        }
        if($gov_id=="" && $service=="" ){
            $selected = DB::table('users as u')
                    ->leftJoin('user_suppliers as us','u.id','=','us.user_id')
                    ->where('u.user_type','supplier')
                    ->where('u.state','=',$service_status)
                    ->where('us.city_id','=',$city_id)
                    ->select('u.*')
                    ->get();
        }
        if($gov_id=="" && $city_id=="" ){
            $selected = DB::table('users as u')
                    ->leftJoin('user_suppliers as us','u.id','=','us.user_id')
                    ->where('u.user_type','supplier')
                    ->where('u.state','=',$service_status)
                    ->where('us.role_supplier_id','like', '%"'. $service .'"%')
                    ->select('u.*')
                    ->get();
        }
        if($gov_id=="" && $service_status=="" ){
            $selected = DB::table('users as u')
                    ->leftJoin('user_suppliers as us','u.id','=','us.user_id')
                    ->where('u.user_type','supplier')
                    ->where('us.role_supplier_id','like', '%"'. $service .'"%')
                     ->where('us.city_id','=',$city_id)
                    ->select('u.*')
                    ->get();
        }
        if($city_id=="" && $service_status=="" ){
            $selected = DB::table('users as u')
                    ->leftJoin('user_suppliers as us','u.id','=','us.user_id')
                    ->where('u.user_type','supplier')
                    ->where('us.role_supplier_id','like', '%"'. $service .'"%')
                     ->where('us.governorate_id','=',$gov_id)
                    ->select('u.*')
                    ->get();
        }
        if($city_id=="" && $service=="" ){
            $selected = DB::table('users as u')
                    ->leftJoin('user_suppliers as us','u.id','=','us.user_id')
                    ->where('u.user_type','supplier')
                    ->where('u.state','=',$service_status)
                     ->where('us.governorate_id','=',$gov_id)
                    ->select('u.*')
                    ->get();
        }
        if($service_status=="" && $service=="" ){
            $selected = DB::table('users as u')
                    ->leftJoin('user_suppliers as us','u.id','=','us.user_id')
                    ->where('u.user_type','supplier')
                    ->where('us.city_id','=',$city_id)
                     ->where('us.governorate_id','=',$gov_id)
                    ->select('u.*')
                    ->get();
        }
        if($gov_id=="" && $city_id=="" && $service_status=="")
        {
            $selected = DB::table('users as u')
            ->leftJoin('user_suppliers as us','u.id','=','us.user_id')
            ->where('u.user_type','supplier')
            ->where('us.role_supplier_id','like', '%"'. $service .'"%')
            ->select('u.*')
            ->get();
        }
        if($gov_id=="" && $city_id=="" && $service=="")
        {
            $selected = DB::table('users as u')
            ->leftJoin('user_suppliers as us','u.id','=','us.user_id')
            ->where('u.user_type','supplier')
            ->where('u.state','=',"$service_status")
            ->select('u.*')
            ->get();
        }
        if($gov_id=="" && $service_status=="" && $service=="")
        {
            $selected = DB::table('users as u')
            ->leftJoin('user_suppliers as us','u.id','=','us.user_id')
            ->where('u.user_type','supplier')
            ->where('us.city_id','=',$city_id)
            ->select('u.*')
            ->get();
        }
        if($city_id=="" && $service_status=="" && $service=="")
        {
            $selected = DB::table('users as u')
            ->leftJoin('user_suppliers as us','u.id','=','us.user_id')
            ->where('u.user_type','supplier')
            ->where('us.governorate_id','=',$gov_id)
            ->select('u.*')
            ->get();
        }

              return DataTables::of($selected)
                     ->addColumn('action', function($selected) {
                 return '<a class="ajaxify btn btn-success btn-sm btnEdit" href="' . url(self::baseURL()) . '/' .'suppliers/' . $selected->id . '">Manage Profile</a>&nbsp;<a class="ajaxify btn btn-info btn-sm btnEdit" href="' . url(self::baseURL()) . '/' .'suppliers/' . $selected->id . '/edit">Edit</a>
                <button type="submit" class="btn btn-warning btn-sm btnDelete" data-remove="' . url(self::baseURL()) . '/'.'suppliers/' . $selected->id . '">Delete</button>
                     <a class="ajaxify btn btn-primary btn-sm notify" href = "' . url(config('app.admin_prefix') . '/notification/supplier/' . $selected->id) . '">Notify</a>
                  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 <input class="delete-suppliers ml-5" type="checkbox" id="' . $selected->id . '" name="chk">';
                     })
                     ->addColumn('status', function($selected) {
              $checkbox = ($selected->state == '1') ? 'checked' : '';
                 return '<input class="change-state" id="change-state-' . $selected->id . '" data-id="' . $selected->id . '" type="checkbox" ' . $checkbox . ' data-toggle="toggle">';
                     })
                     ->addColumn('checkbox', function ($selected) {
                 return '<input class="set-favsupplier" type="checkbox" id="' . $selected->id . '" name="chk">';
                     })
                     ->addColumn('notify_checkbox', function ($selected) {
                        return '<input class="send_notification" type="checkbox" id="' . $selected->id . '"  name="chk_notify">';
                    })
                     ->rawColumns(['action', 'status', 'status','notify_checkbox','checkbox'])->make(true);

        return view($this->baseVIEW_supplier . '.index', ['title' => 'Supplier', 'createUrl' => url(self::baseURL().'/'.'suppliers/' . '/create'), 'fields' => ['name', 'mobile_number', 'user_type','status'],'location_data'=>$location_data,'services'=>$services]);

    }


//===================================FILTER SUPPLIER==================================//

public function filterCustomer(Request $request)
{

   $customer_type   =  $request->customer_type;

   $selected = DB::table('users as u')
                ->where('u.user_type','customer')
                ->where('u.customer_type',$customer_type)
                ->select('u.*')
                ->get();


        return DataTables::of($selected)
                ->addColumn('action', function($selected) {
                 return '<a class="ajaxify btn btn-info btn-sm btnEdit" href="' . url(self::baseURL()) .'/customers'. '/' . $selected->id . '/edit">Edit</a>
                         <button type="submit" class="btn btn-warning btn-sm btnDelete" data-remove="' . url(self::baseURL()) .'/customers'. '/' . $selected->id . '">Delete</button>
                         <a class="ajaxify btn btn-info btn-sm notify" href="' . url(config('app.admin_prefix') .'/notification/customer/' . $selected->id) . '">Notify</a>
                         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                         <input class="delete-customers ml-5" type="checkbox" id="' . $selected->id . '" name="chk">';
                     })
                     ->addColumn('status', function($selected) {
                        $checkbox = ($selected->state == '1') ? 'checked' : '';
                        return '<input class="change-state" id="change-state-' . $selected->id . '" data-id="' . $selected->id . '" type="checkbox" ' . $checkbox . ' data-toggle="toggle">';
                    })
                    ->addColumn('notify_checkbox', function ($selected) {
                        return '<input class="send_notification_customer" type="checkbox" id="' . $selected->id . '"  name="chk_notify">';
                    })
                    ->rawColumns(['action', 'status','notify_checkbox'])->make(true);

                return view($this->baseVIEW_customer . '.index', ['title' => 'Customer', 'createUrl' => url(self::baseURL() .'/customers' . '/create'), 'fields' => ['name', 'mobile_number', 'status'],'location_data'=>$location_data]);

}


//======================================FILTER ORDER =============================================//

    public function filterOrder(Request $request)
    {

       $service_id      =  $request->service_type  ;
       $start_date      =  date("$request->start_date");
       $end_date        =  date("$request->end_date");
       $request_status  =  $request->request_status;

       $selected = DB::table('orders as o')
        ->leftJoin('users as u','u.id','=','o.user_customer_id')
        ->leftJoin('users as us','us.id','=','o.user_supplier_id')
        ->leftJoin('services as s','s.id','=','o.service_id')
        ->where('o.service_id','=',$service_id)
        ->whereBetween('o.created_at', [$start_date, $end_date])
        ->where('o.request_status',$request_status)
        ->select('o.*','u.name as Customer Name','u.last_name','us.name as Supplier Name',
        's.name as Service','u.mobile_number as customer_number','us.mobile_number as supplier_number')
        ->get();



        if($service_id=="" || $service_id == null){
            $selected = DB::table('orders as o')
                ->leftJoin('users as u','u.id','=','o.user_customer_id')
                ->leftJoin('users as us','us.id','=','o.user_supplier_id')
                ->leftJoin('services as s','s.id','=','o.service_id')
                ->whereBetween('o.created_at', [$start_date, $end_date])
                ->where('o.request_status',$request_status)
                ->select('o.*','u.name as Customer Name','u.last_name','us.name as Supplier Name',
                's.name as Service','u.mobile_number as customer_number','us.mobile_number as supplier_number')
                ->get();
       }

       if($request_status=="" || $request_status==null){
        $selected = DB::table('orders as o')
                ->leftJoin('users as u','u.id','=','o.user_customer_id')
                ->leftJoin('users as us','us.id','=','o.user_supplier_id')
                ->leftJoin('services as s','s.id','=','o.service_id')
                ->where('o.service_id','=',$service_id)
                ->whereBetween('o.created_at', [$start_date, $end_date])
                ->select('o.*','u.name as Customer Name','u.last_name','us.name as Supplier Name',
                's.name as Service','u.mobile_number as customer_number','us.mobile_number as supplier_number')
                ->get();
       }

        if($start_date=="" || $end_date=="" ){
            $selected = DB::table('orders as o')
                ->leftJoin('users as u','u.id','=','o.user_customer_id')
                ->leftJoin('users as us','us.id','=','o.user_supplier_id')
                ->leftJoin('services as s','s.id','=','o.service_id')
                ->where('o.request_status',$request_status)
                ->where('o.service_id','=',$service_id)
                ->select('o.*','u.name as Customer Name','u.last_name','us.name as Supplier Name',
                's.name as Service','u.mobile_number as customer_number','us.mobile_number as supplier_number')
                ->get();
       }

       if($request_status=="" &&  $start_date==""  ){
            $selected = DB::table('orders as o')
                ->leftJoin('users as u','u.id','=','o.user_customer_id')
                ->leftJoin('users as us','us.id','=','o.user_supplier_id')
                ->leftJoin('services as s','s.id','=','o.service_id')
                ->where('o.service_id','=',$service_id)
                ->select('o.*','u.name as Customer Name','u.last_name','us.name as Supplier Name',
                's.name as Service','u.mobile_number as customer_number','us.mobile_number as supplier_number')
                ->get();
       }

       if($service_id=="" && $start_date=="" ){
            $selected = DB::table('orders as o')
            ->leftJoin('users as u','u.id','=','o.user_customer_id')
            ->leftJoin('users as us','us.id','=','o.user_supplier_id')
            ->leftJoin('services as s','s.id','=','o.service_id')
            ->where('o.request_status',$request_status)
            ->select('o.*','u.name as Customer Name','u.last_name','us.name as Supplier Name',
            's.name as Service','u.mobile_number as customer_number','us.mobile_number as supplier_number')
            ->get();
       }

       if($request_status=="" && $service_id=="" )
       {
        $selected = DB::table('orders as o')
            ->leftJoin('users as u','u.id','=','o.user_customer_id')
            ->leftJoin('users as us','us.id','=','o.user_supplier_id')
            ->leftJoin('services as s','s.id','=','o.service_id')
            ->whereBetween('o.created_at', [$start_date, $end_date])
            ->select('o.*','u.name as Customer Name','u.last_name','us.name as Supplier Name',
            's.name as Service','u.mobile_number as customer_number','us.mobile_number as supplier_number')
            ->get();
       }



        return DataTables::of($selected)->addColumn('action', function($selected) {
                        return '<a class="ajaxify btn btn-info btn-sm btnEdit" href="' . url(self::baseURL()) . '/order_detail'.'/'. $selected->id . '">Details</a>
                        <a class="ajaxify btn btn-secondary btn-sm btnEdit" href="' . url(self::baseURL()) .'/forward_order'.'/'. $selected->id . '">Forward</a>
                        <a class="ajaxify btn btn-primary btn-sm btnEdit" href="' . url(self::baseURL()) . '/orders'.'/' . $selected->id . '/edit">Edit</a>';
                        // <a class="ajaxify btn btn-danger btn-sm btnEdit" href="' . url(self::baseURL()) . '/'. $selected->id . '">Delete</a>';
                    })
                    ->rawColumns(['action'])->make(true);

        return view('admin.order.view', ['title' => 'Admin User', 'fields' => ['order_id','Customer Name','customer_number','Supplier Name','supplier_number','Service','service_quantity','request_status','created_at'],'service_data'=>$service_data]);

            }


//================================================GAS APP FAVOURITE SUPPLIER======================================//
public function setGasAppFav_Supplier(Request $request)
{
    $data= User::where('user_type','supplier')->get();
    foreach($data as $d){
        $d->gasApp_fav = 'no';
        $d->save();
    }

        try {
            foreach ($request->checked as $id):
               $data = User::find($id);
               $data->gasApp_fav = 'yes';
               $data->save();
            endforeach;
            $data = 'Success';
        } catch (Exception $ex) {
            $data = 'something went wrong';
        }
        return response()->json($data);
}



}
