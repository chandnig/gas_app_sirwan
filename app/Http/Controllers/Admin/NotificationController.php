<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\User;
use Auth;

class NotificationController extends \App\Http\Controllers\AdminController {

    public static function rules() {
        return ['notification_type'=>'required|in:Text,Question','title' => 'required', 'body' => 'required'];
    }

    protected static function baseURL() {
        return config('app.admin_prefix') . '/notification';
    }

    protected $baseVIEW = 'admin.notification';

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function notify(Request $request, $type, $target) {
        $validator = Validator::make(Input::all(), self::rules());
        if ($validator->fails()) {
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            try {
                switch ($type):
                    case'customer':

                        //  $user = User::find($target)->first();
                        // //  \App\Http\Controllers\API\ApiController::pushNotification(['title' => $request->title, 'body' => $request->body], $user->device_token);
                        // $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
                        // $token='diWhHpEdy1k:APA91bHfaE_zy4FUJ_GGDmO3XuJNz5qshyMeyjbIvvdLKI-DkR5rzhS00k9Hwc49yKzJLUraUPbu9-H-XOv8hbT-q-omtzXa8-uAv8Ewej52zO1gH0maKoGP4FLCu9FwVlLSpwBDC_3T';


                        // $notification = [
                        //     'body' => 'this is test',
                        //     'sound' => true,
                        // ];

                        // $extraNotificationData = ["message" => $notification,"moredata" =>'dd'];

                        // $fcmNotification = [
                        //     //'registration_ids' => $tokenList, //multple token array
                        //     'to'        => $token, //single token
                        //     'notification' => $notification,
                        //     'data' => $extraNotificationData
                        // ];

                        // $headers = [
                        //     'Authorization: key=AIzaSyD3ZOcKuwAUyJOcc8r_aV8OlSUxhs7MHOI',
                        //     'Content-Type: application/json'
                        // ];


                        // $ch = curl_init();
                        // curl_setopt($ch, CURLOPT_URL,$fcmUrl);
                        // curl_setopt($ch, CURLOPT_POST, true);
                        // curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        // curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
                        // $result = curl_exec($ch);
                        // curl_close($ch);
                        // dd($result);
                        // break;
                    case'supplier':


                        break;
                    case'role':

                        // $user = User::select('*')->role($target)->get();
                        // \App\Http\Controllers\API\ApiController::pushNotification(['title' => $request->title, 'body' => $request->body], $user->pluck('device_token')->toArray());
                        break;
                    case'governorate':
                        // $userAddress = \App\Models\UserAddress::where('governorate_id', $target)->get();
                        // $user = User::whereIn('id',$userAddress->pluck('user_id'))->get();
                        // \App\Http\Controllers\API\ApiController::pushNotification(['title' => $request->title, 'body' => $request->body], $user->pluck('device_token')->toArray());
                        break;
                    case'location':
                        break;
                endswitch;
//                dd($request->all());
                return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }

        /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function sendNotification(Request $request)
    {
        $fcmUrl = 'https://fcm.googleapis.com/fcm/send';
        $token='diWhHpEdy1k:APA91bHfaE_zy4FUJ_GGDmO3XuJNz5qshyMeyjbIvvdLKI-DkR5rzhS00k9Hwc49yKzJLUraUPbu9-H-XOv8hbT-q-omtzXa8-uAv8Ewej52zO1gH0maKoGP4FLCu9FwVlLSpwBDC_3T';


        $notification = [
            'body' => 'this is test',
            'sound' => true,
        ];

        $extraNotificationData = ["message" => $notification,"moredata" =>'dd'];

        $fcmNotification = [
            //'registration_ids' => $tokenList, //multple token array
            'to'        => $token, //single token
            'notification' => $notification,
            'data' => $extraNotificationData
        ];

        $headers = [
            'Authorization: key=AIzaSyD3ZOcKuwAUyJOcc8r_aV8OlSUxhs7MHOI',
            'Content-Type: application/json'
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
        dd($result);
    }




}
