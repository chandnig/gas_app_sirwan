<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\Models\HowToModel;


class HowToController extends Controller
{
    protected static function rules() {
        return ['howto_customer_en' => 'required','howto_customer_ar'=>'','howto_customer_kr'=>''];
    }
    protected static function ad_homecustomer_rules() {
        return ['ads_customer_en' => 'required','ads_customer_ar'=>'','ads_customer_kr'=>''];
    }
    protected static function ad_businesscustomer_rules() {
        return ['ads_business_customer_en' => 'required','ads_business_customer_ar'=>'','ads_business_customer_kr'=>''];
    }
    protected static function ad_supplier_rules() {
        return ['ads_supplier_en' => 'required','ads_supplier_ar'=>'','ads_supplier_kr'=>''];
    }
    protected static function rules_supplier() {
        return ['howto_supplier_en' => 'required','howto_supplier_ar'=>'',
       'howto_supplier_kr'=>''];
    }


    protected static function baseURL() {
        return config('app.admin_prefix') . '/how_to';
    }

    protected static function baseURL_supplier() {
        return config('app.admin_prefix') . '/howto_supplier';
    }
    protected static function baseURL_ads() {
        return config('app.admin_prefix') . '/ads';
    }
    protected static function baseURL_business_ads() {
        return config('app.admin_prefix') . '/ads_business';
    }
    protected static function baseURL_supplier_ads() {
        return config('app.admin_prefix') . '/ads_supplier';
    }

    protected $baseVIEW = 'admin.how_to';
    protected $baseVIEW_supplier = 'admin.how_to_supplier';
     protected $baseVIEW_ads = 'admin.ads';

    private $__id = '1';

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HowToModel $role
     * @return \Illuminate\Http\Response
     */
    public function index() {

        $id = $this->__id;
        $data = HowToModel::find($id);
        return view($this->baseVIEW . '.form', ['title' => ' How To','type' => 'Customer','CUSTOMER',  'action' => url(self::baseURL()) . '/', 'actionMessage' => 'Updated Successfully', 'data' => $data, 'listUrl' => url(self::baseURL()), 'inputTypeTexts' => self::rules()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Configuration  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {

        $Validator = Validator::make(Input::all(), self::rules());
        if ($Validator->fails()) {
            return response()->json(array('errors' => $Validator->getMessageBag()->toArray()));
        } else {

            try {
                $model = HowToModel::find($this->__id);
                foreach (array_keys(self::rules()) as $key):
                    $model->$key = $request->$key;
                endforeach;
                 //dd($request->all());
                $model->save();
                return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }

      /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\HowToModel $role
    * @return \Illuminate\Http\Response
    */
    public function supplier_view() {

        $id = $this->__id;
        $data = HowToModel::find($id);
        return view($this->baseVIEW_supplier . '.form', ['title' => 'How To','type' => 'Supplier',  'action' => url(self::baseURL_supplier()) . '/', 'actionMessage' => 'Updated Successfully', 'data' => $data, 'listUrl' => url(self::baseURL_supplier()), 'inputTypeTexts' => self::rules_supplier()]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Configuration  $role
     * @return \Illuminate\Http\Response
     */
    public function update_supplier(Request $request) {

        $Validator = Validator::make(Input::all(), self::rules_supplier());
        if ($Validator->fails()) {
            return response()->json(array('errors' => $Validator->getMessageBag()->toArray()));
        } else {

            try {
                $model = HowToModel::find($this->__id);
                foreach (array_keys(self::rules_supplier()) as $key):
                    $model->$key = $request->$key;
                endforeach;
                 //dd($request->all());
                $model->save();
                return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }




//========================ADS FOR HOME CUSTOMER HERE================================//
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HowToModel $role
     * @return \Illuminate\Http\Response
     */
    public function ads_homecustomer(Request $request) {

        $id = $this->__id;
        $data = HowToModel::find($id);

        return view($this->baseVIEW_ads . '.home_customer', ['title' => 'Home Customer Ads','type' => ' Home Customer','CUSTOMER',  'action' => url(self::baseURL_ads()) . '/', 'actionMessage' => 'Updated Successfully', 'data' => $data, 'listUrl' => url(self::baseURL_ads()), 'inputTypeTexts' => self::ad_homecustomer_rules()]);
    }


            /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Configuration  $role
     * @return \Illuminate\Http\Response
     */
    public function update_homecustomer_ads(Request $request) {

        $Validator = Validator::make(Input::all(), self::ad_homecustomer_rules());
        if ($Validator->fails()) {
            return response()->json(array('errors' => $Validator->getMessageBag()->toArray()));
        } else {

            try {
                $model = HowToModel::find($this->__id);
                foreach (array_keys(self::ad_homecustomer_rules()) as $key):
                    $model->$key = $request->$key;
                endforeach;
                $model->save();
                return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }

    }

//========================ADS FOR BUSINES CUSTOMER HERE================================//
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HowToModel $role
     * @return \Illuminate\Http\Response
     */
    public function ads_businesscustomer(Request $request) {

        $id = $this->__id;
        $data = HowToModel::find($id);

        return view($this->baseVIEW_ads . '.business_customer', ['title' => 'Business Customer Ads','type' => 'Business Customer',  'action' => url(self::baseURL_business_ads()) . '/', 'actionMessage' => 'Updated Successfully', 'data' => $data, 'listUrl' => url(self::baseURL_business_ads()), 'inputTypeTexts' => self::ad_businesscustomer_rules()]);
    }


            /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Configuration  $role
     * @return \Illuminate\Http\Response
     */
    public function update_businesscustomer_ads(Request $request) {


         $Validator = Validator::make(Input::all(), self::ad_businesscustomer_rules());
         if ($Validator->fails()) {
             return response()->json(array('errors' => $Validator->getMessageBag()->toArray()));
         } else{

             try {
                 $model = HowToModel::find($this->__id);
                 foreach (array_keys(self::ad_businesscustomer_rules()) as $key):
                     $model->$key = $request->$key;
                 endforeach;
                 $model->save();
                 return response()->json(array("success" => true));
             } catch (\Exception $ex) {
                 dd($ex->getMessage());
             }
         }

    }



//========================ADS FOR SUPPLIER HERE================================//
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HowToModel $role
     * @return \Illuminate\Http\Response
     */
    public function ads_supplier(Request $request) {

        $id = $this->__id;
        $data = HowToModel::find($id);

        return view($this->baseVIEW_ads . '.supplier', ['title' => 'Supplier Ads','type' => 'Supplier',  'action' => url(self::baseURL_supplier_ads()) . '/', 'actionMessage' => 'Updated Successfully', 'data' => $data, 'listUrl' => url(self::baseURL_supplier_ads()), 'inputTypeTexts' => self::ad_supplier_rules()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Configuration  $role
     * @return \Illuminate\Http\Response
     */
    public function update_supplier_ads(Request $request) {

         $Validator = Validator::make(Input::all(), self::ad_supplier_rules());
         if ($Validator->fails()) {
             return response()->json(array('errors' => $Validator->getMessageBag()->toArray()));
         } else{

             try {
                 $model = HowToModel::find($this->__id);
                 foreach (array_keys(self::ad_supplier_rules()) as $key):
                     $model->$key = $request->$key;
                 endforeach;
                 $model->save();
                 return response()->json(array("success" => true));
             } catch (\Exception $ex) {
                 dd($ex->getMessage());
             }
         }

    }

}
