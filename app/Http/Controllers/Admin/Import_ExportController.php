<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use App\Models\MeasurementUnit;
use App\Models\Location;
use App\Models\Order;
use App\User;
use Validator;
use Excel;
use DB;


class Import_ExportController extends Controller
{
//=======================DOWNLOAD ORDER LIST IN EXCEL=======================================//
public function downloadExcel_orders($type)
{
    $data = DB::table('orders as o')
            ->leftJoin('users as u','u.id','=','o.user_customer_id')
            ->leftJoin('users as us','us.id','=','o.user_supplier_id')
            ->leftJoin('services as s','s.id','=','o.service_id')
            ->select('u.name as customer_first_name','u.last_name as customer_last_name','u.mobile_number as customer_mobile','us.mobile_number as supplier_mobile','us.name as supplier_name','s.name as service',
                    'o.service_quantity','o.service_price','o.request_status',
                    'o.rating','o.created_at','o.updated_at','o.deleted_at')
            ->get()->toArray();

        if(count($data) >0)
        {
            $data= json_decode( json_encode($data), true);
            if($data)
            {
                return Excel::create('order_lists', function($excel) use ($data) {
                    $excel->sheet('mySheet', function($sheet) use ($data)
                    {
                        $sheet->fromArray($data);
                    });
                    ob_end_clean();
                })->download($type);
            }
        }
        else{
         return back()->with('error','nothing to export');
        }
}

//=======================DOWNLOAD CUSTOMER's LIST IN EXCEL=======================================//
public function downloadExcel_customer($type)
{
    $data = DB::table('users')
    ->where('user_type','customer')
    ->select('name','last_name','mobile_number','email','user_language','created_at','updated_at','deleted_at')
    ->get()->toArray();

        if(count($data) >0)
        {
            $data= json_decode( json_encode($data), true);
            if($data)
            {
                return Excel::create('customer_list', function($excel) use ($data) {
                    $excel->sheet('mySheet', function($sheet) use ($data)
                    {
                        $sheet->fromArray($data);
                    });
                    ob_end_clean();  
                })->download($type);
            }
        }
        else{
         return back()->with('error','nothing to export');
        }
}

//=======================DOWNLOAD SUPPLIER's LIST IN EXCEL=======================================//
public function downloadExcel_suppier($type)
{
    	$data = DB::table('users as u')
    ->leftJoin('user_suppliers as us', 'us.user_id', '=', 'u.id')
    ->select('u.name','u.name_kr','u.name_ar','u.mobile_number',
            'us.vehicle_number','us.license_number')
    ->get()->toArray();
    if(count($data) >0)
    {
        return Excel::create('supplier_list', function($excel) use ($data) {
            $excel->sheet('mySheet', function($sheet) use ($data)
            {
                $sheet->cell('A1', function($cell) {$cell->setValue('Suppliers Full Name En');   });
                $sheet->cell('B1', function($cell) {$cell->setValue('Suppliers Full Name Kr');   });
                $sheet->cell('C1', function($cell) {$cell->setValue('Suppliers Full Name Ar');   });
                $sheet->cell('D1', function($cell) {$cell->setValue('Mobile Number');   });
                $sheet->cell('E1', function($cell) {$cell->setValue('Vehicle Number');   });
                $sheet->cell('F1', function($cell) {$cell->setValue('License Number');   });
                if (!empty($data)) {
                    foreach ($data as $key => $value) {
                        $i= $key+2;
                        $sheet->cell('A'.$i, $value->name); 
                        $sheet->cell('B'.$i, $value->name_kr); 
                        $sheet->cell('C'.$i, $value->name_ar); 
                        $sheet->cell('D'.$i, $value->mobile_number); 
                        $sheet->cell('E'.$i, $value->vehicle_number); 
                        $sheet->cell('F'.$i, $value->license_number); 
                    }
                    ob_end_clean();  
                }
            });
        })->download($type);
    }  
    else{
        return back()->with('error','nothing to export');
    }  
}

public function downloadExcel_suppier1($type)
{
    $data = DB::table('users as u')
    ->leftJoin('user_suppliers as us', 'us.user_id', '=', 'u.id')
    // ->leftJoin('location as lg', 'us.governorate_id', '=', 'lg.id')
    // ->leftJoin('user_suppliers as us', 'us.user_id', '=', 'u.id')
    ->select('u.name','u.last_name','u.name_ar','u.name_kr','u.mobile_number','u.email','u.user_language',
            'us.vehicle_number','us.license_number','u.created_at','u.updated_at','u.deleted_at')
    ->get()->toArray();

        if(count($data) >0)
        {
            $data= json_decode( json_encode($data), true);
            if($data)
            {
                return Excel::create('supplier_list', function($excel) use ($data) {
                    $excel->sheet('mySheet', function($sheet) use ($data)
                    {
                        $sheet->fromArray($data);
                    });
                    ob_end_clean();  

                })->download($type);
            }
        }
        else{
         return back()->with('error','nothing to export');
        }
}
//=======================DOWNLOAD SERVICE LIST IN EXCEL=======================================//
public function downloadExcel_service($type)
{
    $data = DB::table('services as s')
    ->select('s.name','s.name_kr','s.name_ar','s.parent_id','s.created_at','s.updated_at','s.deleted_at')
    ->get()->toArray();

        if(count($data) >0)
        {
            $data= json_decode( json_encode($data), true);
            if($data)
            {
                return Excel::create('service_list', function($excel) use ($data) {
                    $excel->sheet('mySheet', function($sheet) use ($data)
                    {
                        $sheet->fromArray($data);
                    });
                    ob_end_clean();
                })->download($type);
            }
        }
        else{
         return back()->with('error','nothing to export');
        }
}

//=======================DOWNLOAD MEASUREMENT  LIST IN EXCEL=======================================//
public function downloadExcel_units($type)
{
    $data = DB::table('measurement_units')
    ->select('*')
    ->get()->toArray();

        if(count($data) >0)
        {
            $data= json_decode( json_encode($data), true);
            if($data)
            {
                return Excel::create('measurement_units_list', function($excel) use ($data) {
                    $excel->sheet('mySheet', function($sheet) use ($data)
                    {
                        $sheet->fromArray($data);
                    });
                    ob_end_clean();
                })->download($type);
            }
        }
        else{
         return back()->with('error','nothing to export');
        }
}
//=======================DOWNLOAD GOVERNORATE  LIST IN EXCEL=======================================//
public function downloadExcel_governorate($type)
{
    $data = DB::table('locations')
    ->where('parent_id','=',0)
    ->where('type','governorate')
    ->where('deleted_at',NULL)
    ->select('name','name_kr','name_ar','code','helpline_number')
    ->get()->toArray();

        if(count($data) >0)
        {
            $data= json_decode( json_encode($data), true);
            if($data)
            {
                return Excel::create('governorate', function($excel) use ($data) {
                    $excel->sheet('mySheet', function($sheet) use ($data)
                    {
                        $sheet->fromArray($data);
                    });
                    ob_end_clean();
                })->download($type);
            }
        }
        else{
         return back()->with('error','nothing to export');
        }
}
//=======================DOWNLOAD CITY  LIST IN EXCEL=======================================//
public function downloadExcel_city($type)
{
    $data = DB::table('locations')
    ->where('type','city')
    ->select('*')
    ->get()->toArray();

        if(count($data) >0)
        {
            $data= json_decode( json_encode($data), true);
            if($data)
            {
                return Excel::create('city_list', function($excel) use ($data) {
                    $excel->sheet('mySheet', function($sheet) use ($data)
                    {
                        $sheet->fromArray($data);
                    });
                    ob_end_clean();
                })->download($type);
            }
        }
        else{
         return back()->with('error','nothing to export');
        }
}

//=======================DOWNLOAD SECTION  LIST IN EXCEL=======================================//
public function downloadExcel_section($type)
{
    $data = DB::table('locations')
    ->where('type','section')
    ->select('*')
    ->get()->toArray();

        if(count($data) >0)
        {
            $data= json_decode( json_encode($data), true);
            if($data)
            {
                return Excel::create('section_list', function($excel) use ($data) {
                    $excel->sheet('mySheet', function($sheet) use ($data)
                    {
                        $sheet->fromArray($data);
                    });
                    ob_end_clean();
                })->download($type);
            }
        }
        else{
         return back()->with('error','nothing to export');
        }
}


// public function importFileIntoDBGovernorate(Request $request) {
//     if ($request->hasFile('sample_file')) {
//         $path = $request->file('sample_file')->getRealPath();
//         $data = \Excel::load($path)->get();
//         if ($data->count()) {
//             $goverArr = $data->toArray();
//             foreach( $goverArr as $key_index_1 => $value_index_1){
//                 if($key_index_1 == "1"){
//                     $userArray = $value_index_1;
//                     for($w = 0; $w <= count($userArray); $w++ ){
//                         $arr[] = ['parent_id'=>0,'type' =>'governorate', 'name' => $userArray[$w]['name'],
//                         'name_ar'=>$userArray[$w]['name_ar'],'name_kr'=>$userArray[$w]['name_kr'],
//                     'code'=>$userArray[$w]['code'],'helpline_number'=>$userArray[$w]['helpline_number']];
//                 }
//                 }
//             }
//             if(!empty($arr)){
//                 Location::insert($arr);
//             }
//         }
//     }
//     //} catch (\Exception $ex) {
//     return \redirect()->back();
// }


public function supplierimportFileIntoDB(Request $request) {

    if ($request->hasFile('sample_file')) {
        $path = $request->file('sample_file')->getRealPath();
        $data = \Excel::load($path)->get();
        if ($data->count()) {
            $goverArr = $data->toArray();

            foreach( $goverArr as $key_index_1 => $value_index_1){

                if($key_index_1 == "1"){
                    $userArray = $value_index_1;
                    for($w = 0; $w < count($userArray); $w++ ){
                        if($userArray[$w]['parent_id'] == "city center")
                        {
                            $parent_id=2;
                        }
                        elseif(($userArray[$w]['parent_id'] == "new city kr")){
                            $parent_id=10;

                        }
                        $arr[] = ['parent_id'=>$parent_id,'type' =>'section', 'name' => $userArray[$w]['name'],
                        'name_ar'=>$userArray[$w]['name_ar'],'name_kr'=>$userArray[$w]['name_kr'],
                    'code'=>$userArray[$w]['code']];
                    }
                }
            }

            if(!empty($arr)){
                Location::insert($arr);
            }
        }
    }
    //} catch (\Exception $ex) {
    return \redirect()->back();

//     try {
//         if ($request->hasFile('sample_file')) {
//             $path = $request->file('sample_file')->getRealPath();
//             $data = \Excel::load($path)->get();
//             if ($data->count()) {
//             //  dd($data);
// //                    dd($data->toArray());
//                 $i = 0;
//                 foreach ($data->toArray() as $key => $value) {

//                     $i++;
//                     try {
//                         $model = new User();
//                         foreach (array_keys(UserSupplierController::rules()) as $index):
//                             if ($index == 'role_supplier')
//                                 continue;
//                             if ($index == 'password')
//                                 $model->$index = Hash::make($value[$index]);
//                             else
//                                 $model->$index = $value[$index];
//                         endforeach;
// //                            dd(explode(',', $value['role_supplier']));
//                        $model->assignRole(explode(',', $value['role_supplier']));
// //                            dd($model);
//                         $model->save();
//                     } catch (\Exception $ex) {
//                         continue;
//                     }
//                 }
//                 return \redirect()->back()->with(["message" => $i . ' supplier added']);
//             }
//         }
//     } catch (\Exception $ex) {
//         return \redirect()->back()->withErrors(['error' => 'Something Went Wrong!']);
//     }
//     return \redirect()->back()->withErrors(['error' => 'File is required!']);
}

}
