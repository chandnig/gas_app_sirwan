<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\Models\Configuration;
use Auth;

class ConfigurationController extends \App\Http\Controllers\AdminController {

    protected static function rules() {
        return ['tnc' => 'required', 'tnc_kr' => '', 'tnc_ar' => ''];
    }

    protected static function baseURL() {
        return config('app.admin_prefix') . '/terms&condition';
    }

    protected $baseVIEW = 'admin.configuration';
    private $__id = '1';

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Configuration  $role
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $id = $this->__id;
        $data = Configuration::find($id);
        return view($this->baseVIEW . '.form', ['title' => 'Terms, Ads, How To', 'action' => url(self::baseURL()) . '/', 'actionMessage' => 'Configuration Updated Successfully', 'data' => $data, 'listUrl' => url(self::baseURL()), 'inputTypeTexts' => self::rules()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Configuration  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request) {

        $Validator = Validator::make(Input::all(), self::rules());
        if ($Validator->fails()) {
            return response()->json(array('errors' => $Validator->getMessageBag()->toArray()));
        } else {

            try {
                $model = Configuration::find($this->__id);
                foreach (array_keys(self::rules()) as $key):
                    $model->$key = $request->$key;
                endforeach;
                 //dd($request->all());
                $model->save();
                return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }

}
