<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\Models\Location;
use App\Models\Service;
use App\Models\GasAppFee;

class GasAppFeeController extends Controller
{

    protected static function baseURL() {
        return config('app.admin_prefix') . '/service_fees';
    }

    protected $baseVIEW = 'admin.gasApp_fees';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $service = [];
        $govId = 0;
        if($request->id)
        {
            $govId = $request->id;
            $serviceData = Service::where('state',"1")->get();
            //dd($serviceData);
            $serviceArray = array();
            foreach($serviceData as $key => $service)
            {
                if(strpos($service['location'],$govId)) {
                    $serviceId =  $service['id'];
                    // echo $serviceId."<br>";
                    $gasApp = GasAppFee::where('governorate_id','=',$govId)->where('service_id','=',$serviceId)->first();
                    $serviceArray[$key]['id'] = $serviceId;
                    $serviceArray[$key]['govId'] = $govId;
                    $serviceArray[$key]['name'] = $service['name'];
                    $serviceArray[$key]['name_kr'] = $service['name_kr'];
                    $serviceArray[$key]['name_ar'] = $service['name_ar'];
                    $serviceArray[$key]['service_price'] = $gasApp['service_price'];
                    $serviceArray[$key]['service_price_many'] = $gasApp['service_price_many'];
                    $serviceArray[$key]['service_quantity'] = $gasApp['service_quantity'];
                }
            }
            //dd($serviceArray);
            $service = $serviceArray;
        }
        // $service = Service::all();
        $location_data = Location::where('type','governorate')->get();
        return view($this->baseVIEW . '.index', ['title' => 'Gas App  Service Fees','action' => url(self::baseURL()) . '/', 'actionMessage' => 'Updated Successfully', 'listUrl' => url(self::baseURL()),'location_data'=>$location_data,'service'=>$service,'govId'=>$govId]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $governorate_id = $request->governorate_id;
        $new_data = $request->dataArray;

        GasAppFee::where('governorate_id','=',$governorate_id)->delete();

        $fees_data= GasAppFee::insert($new_data);
        if($fees_data)
        {
            $data="Success";
        }
        else{
            $data="Failure";
        }
        return response()->json($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return $id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getGasApp_fees(Request $request)
    {
        $governorate_id = $request->governorate_id;

        $location_data = Location::where('type','governorate')->get();
        $service = Service::all();
        $data = GasAppFee::where('governorate_id','=',$governorate_id)->get();

        return view($this->baseVIEW . '.search_view', ['title' => 'Gas App  Service Fees','action' => url(self::baseURL()) . '/', 'actionMessage' => 'Updated Successfully', 'listUrl' => url(self::baseURL()),'location_data'=>$location_data,'service'=>$service,'data'=>$data]);

    }
}
