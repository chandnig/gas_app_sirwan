<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\User;
use App\Models\UserSupplierServiceRate;
use Auth;

class SupplierProductController extends Controller
{
    public static function rules() {
        return ['' => 'required|max:100', 'email' => '', 'mobile_number' => 'required|unique:users,mobile_number', 'password' => '', 'driver_vehicle_number' => 'required|unique:user_drivers,vehicle_number', 'driver_license_number' => 'required|unique:user_drivers,license_number'];
    }

    protected static function baseURL() {
        return config('app.admin_prefix') . '/drivers';
    }

    protected $baseVIEW = 'admin.driver';


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $url = explode('/', url()->previous());
        // $supplier = end($url);


        // $validator = Validator::make($request->all(), [
        //     'supplier_id' => 'required',
        // ]);


        // if ($validator->fails()) {
        //     return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        // } else {
        //     try {
        //         $model = new UserSupplier();
        //         $model->governorate_id = $request->governate_id;
        //         $model->city_id = $request->city_id;
        //         $model->section_ids = $request->section_id;
        //         $model->user_id = $request->user_id;
        //         $model->vehicle_number = $request->vehicle_number;
        //         $model->license_number = $request->license_number;
        //         $model->save();
        //         return response()->json(array("success" => true));
        //     } catch (\Exception $ex) {
        //         dd($ex->getMessage());
        //     }
        // }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $url = explode('/', url()->previous());
        $supplier = end($url);


        $validator = Validator::make($request->all(), [
            'supplier_id' => 'required',
        ]);


        if ($validator->fails()) {
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            try {
                $model = new UserSupplierServiceRate();
                $model->supplier_id = $request->supplier_id;
                $model->rate_cards = $request->rate_cards;
                $model->save();
                return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          try {
                $model =  UserSupplierServiceRate::where('supplier_id','=',$id)->first();
                $model->rate_cards = $request->rate_cards;
                $model->save();
                return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
