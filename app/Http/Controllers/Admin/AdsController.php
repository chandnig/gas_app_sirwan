<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\Models\HowToModel;


class AdsController extends \App\Http\Controllers\AdminController
{
    protected static function ad_homecustomer_rules() {
        return ['ads_customer_en' => 'required','ads_customer_ar'=>'','ads_customer_kr'=>''];
    }
    protected static function ad_businesscustomer_rules() {
        return ['ads_business_customer_en' => 'required','ads_business_customer_ar'=>'','ads_business_customer_kr'=>''];
    }
    protected static function ad_supplier_rules() {
        return ['ads_supplier_en' => 'required','ads_supplier_ar'=>'','ads_supplier_kr'=>''];
    }

    protected static function baseURL_ads() {
        return config('app.admin_prefix') . '/ads';
    }
    protected static function baseURL_business_ads() {
        return config('app.admin_prefix') . '/ads_business';
    }
    protected static function baseURL_supplier_ads() {
        return config('app.admin_prefix') . '/ads_supplier';
    }

    protected $baseVIEW_ads = 'admin.ads';

    private $__id = '1';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = $this->__id;
        $data = HowToModel::find($id);

        return view($this->baseVIEW_ads . '.home_customer', ['title' => 'Home Customer Ads','type' => ' Home Customer','CUSTOMER',  'action' => url(self::baseURL_ads()) . '/', 'actionMessage' => 'Updated Successfully', 'data' => $data, 'listUrl' => url(self::baseURL_ads()), 'inputTypeTexts' => self::ad_homecustomer_rules()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Validator = Validator::make(Input::all(), self::ad_homecustomer_rules());
        if ($Validator->fails()) {
            return response()->json(array('errors' => $Validator->getMessageBag()->toArray()));
        } else {

            try {
                $model = HowToModel::find($this->__id);
                foreach (array_keys(self::ad_homecustomer_rules()) as $key):
                    $model->$key = $request->$key;
                endforeach;
                $model->save();
                return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }




//========================ADS FOR BUSINES CUSTOMER HERE================================//
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HowToModel $role
     * @return \Illuminate\Http\Response
     */
    public function ads_businesscustomer(Request $request) {

        $id = $this->__id;
        $data = HowToModel::find($id);

        return view($this->baseVIEW_ads . '.business_customer', ['title' => 'Business Customer Ads','type' => 'Business Customer',  'action' => url(self::baseURL_business_ads()) . '/', 'actionMessage' => 'Updated Successfully', 'data' => $data, 'listUrl' => url(self::baseURL_business_ads()), 'inputTypeTexts' => self::ad_businesscustomer_rules()]);
    }


            /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Configuration  $role
     * @return \Illuminate\Http\Response
     */
    public function update_businesscustomer_ads(Request $request) {


         $Validator = Validator::make(Input::all(), self::ad_businesscustomer_rules());
         if ($Validator->fails()) {
             return response()->json(array('errors' => $Validator->getMessageBag()->toArray()));
         } else{

             try {
                 $model = HowToModel::find($this->__id);
                 foreach (array_keys(self::ad_businesscustomer_rules()) as $key):
                     $model->$key = $request->$key;
                 endforeach;
                 $model->save();
                 return response()->json(array("success" => true));
             } catch (\Exception $ex) {
                 dd($ex->getMessage());
             }
         }

    }



//========================ADS FOR SUPPLIER HERE================================//
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HowToModel $role
     * @return \Illuminate\Http\Response
     */
    public function ads_supplier(Request $request) {

        $id = $this->__id;
        $data = HowToModel::find($id);

        return view($this->baseVIEW_ads . '.supplier', ['title' => 'Supplier Ads','type' => 'Supplier',  'action' => url(self::baseURL_supplier_ads()) . '/', 'actionMessage' => 'Updated Successfully', 'data' => $data, 'listUrl' => url(self::baseURL_supplier_ads()), 'inputTypeTexts' => self::ad_supplier_rules()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Configuration  $role
     * @return \Illuminate\Http\Response
     */
    public function update_supplier_ads(Request $request) {

         $Validator = Validator::make(Input::all(), self::ad_supplier_rules());
         if ($Validator->fails()) {
             return response()->json(array('errors' => $Validator->getMessageBag()->toArray()));
         } else{

             try {
                 $model = HowToModel::find($this->__id);
                 foreach (array_keys(self::ad_supplier_rules()) as $key):
                     $model->$key = $request->$key;
                 endforeach;
                 $model->save();
                 return response()->json(array("success" => true));
             } catch (\Exception $ex) {
                 dd($ex->getMessage());
             }
         }

    }
}
