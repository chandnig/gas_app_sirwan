<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\User;
use App\Models\UserSupplier;
use Auth;

class SupplierLocationController extends Controller
{


    public static function rules() {
        return ['governorate_id'=>'required','city_id'=>'','section_id'=>'','vehicle_number'=>'','license_number'=>''];
    }

    protected static function baseURL() {
        return config('app.admin_prefix') . '/supplier_location';
    }

    protected $baseVIEW = 'admin.supplier_location';

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $url = explode('/', url()->previous());
        $supplier = end($url);

        $supplier_data = UserSupplier::where('user_id',$supplier)->get();

        return view($this->baseVIEW . '.form', ['title' => 'Add Suppliers Information', 'action' => url(self::baseURL()), 'actionMessage' => 'Data Added Successfully', 'listUrl' => url(self::baseURL()),
        'inputTypeTexts' => self::rules(),'supplier_data'=>$supplier_data,'supplier'=>$supplier]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request) {

        $rules = self::rules();
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            try {

                $model = new UserSupplier();
                $model->governorate_id = $request->governorate_id;
                $model->city_id = $request->city_id;
                $model->section_ids = serialize($request->section_id);
                $model->user_id = $request->user_id;
                $model->vehicle_number = $request->vehicle_number;
                $model->license_number = $request->license_number;
                $model->save();
                return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }

    // /**
    //  * Display the specified resource.
    //  *
    //  * @param  \App\User  $model
    //  * @return \Illuminate\Http\Response
    //  */
    // public function show($id, Request $request) {
    //     $data = User::where('id', $id)->first();
    //     return view($this->baseVIEW . '.view', ['title' => 'Supplier', 'actionMessage' => 'Supplier List', 'data' => $data, 'listUrl' => url(self::baseURL()), 'editUrl' => url(self::baseURL()) . '/' . $id . '/edit', 'driver_fields' => ['name', 'mobile_number', 'status']]);
    // }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $model
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $data = UserSupplier::where('user_id',$id)->first();
        return view($this->baseVIEW . '.form', ['title' => 'Edit Supplier', 'action' => url(self::baseURL()) . '/' . $id, 'actionMessage' => 'Supplier Updated Successfully', 'data' => $data, 'listUrl' => url(self::baseURL()), 'inputTypeTexts' => self::rules()]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $model
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {


        $rules = self::rules();
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            try {
                $model = UserSupplier::where('user_id','=',$id)->first();
                $model->governorate_id = $request->governorate_id;
                $model->city_id = $request->city_id;
                $model->section_ids = json_encode($request->section_id);
                $model->vehicle_number = $request->vehicle_number;
                $model->license_number = $request->license_number;
//                dd($request->role_supplier
                $model->save();
                return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $model
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        if (User::destroy($id)) {
            $data = 'Success';
        } else {
            $data = 'Failed';
        }
        return response()->json($data);
    }


       /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_location() {
        $url = explode('/', url()->previous());
        $supplier = end($url);
        return view($this->baseVIEW . '.form', ['title' => 'Create Driver', 'action' => url(self::baseURL()), 'actionMessage' => 'Driver Created Successfully', 'listUrl' => url(self::baseURL()), 'inputTypeTexts' => self::rules(), 'supplier' => $supplier]);
    }




//==============================DELETE VEHICLE===========================================//
    public function deleteVehicle(Request $request,$id)
    {
        $data = UserSupplier::where('user_id','=',$id)->first();
        $data->vehicle_number = "";
        $data->vehicle_image = "";
        $data->save();
        return response()->json($data);

    }
//==============================DELETE VEHICLE===========================================//
    public function deleteLicense(Request $request,$id)
    {
        $data = UserSupplier::where('user_id','=',$id)->first();
        $data->license_number = "";
        $data->license_image = "";
        $data->save();


        if ( $data->save()) {
            $data = 'Success';
        } else {
            $data = 'Failed';
        }
        return response()->json($data);

    }
}
