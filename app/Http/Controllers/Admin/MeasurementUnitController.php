<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;
use Validator;
use App\Models\MeasurementUnit;
use App\User;
use Auth;

class MeasurementUnitController extends Controller
{

    protected static function rules() {
        return ['measurement_unit'=>'required','measurement_unit_kr'=>'required','measurement_unit_ar'=>'required'];
    }

    protected static function baseURL() {
        return config('app.admin_prefix') .'/measurements';
    }

    protected $baseVIEW = 'admin.measurement_unit';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            $selected = MeasurementUnit::select('*');
            return DataTables::of($selected)->editColumn('created_at', function ($data) {
                                return $data->created_at->toDayDateTimeString();
                            })
                            ->editColumn('updated_at', function ($data) {
                                return $data->updated_at->toDayDateTimeString();
                            })
                            ->addColumn('action', function($selected) {
                                return '<a class="ajaxify btn btn-info btn-sm btnEdit" href="' . url(self::baseURL()) . '/' . $selected->id . '/edit">Edit</a>
                                        <button type="submit" class="btn btn-warning btn-sm btnDelete" data-remove="' . url(self::baseURL()) . '/' . $selected->id . '">Delete</button>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input class="delete-units ml-5" type="checkbox" id="' . $selected->id . '" name="chk">';
                        })

                            ->rawColumns(['action', 'status'])->make(true);
        }
        return view($this->baseVIEW . '.index', ['title' => 'Measurement Units', 'createUrl' => url(self::baseURL() . '/create'), 'fields' => ['measurement_unit','measurement_unit_ar','measurement_unit_kr']]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $url = explode('/', url()->previous());
        $supplier = end($url);

        return view($this->baseVIEW . '.form', ['title' => 'Add Unit', 'action' => url(self::baseURL()), 'actionMessage' => 'New Unit Added Successfully', 'listUrl' => url(self::baseURL()), 'inputTypeTexts' => self::rules()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = self::rules();
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            try {
                $model = new MeasurementUnit;
                $model->measurement_unit = $request->measurement_unit;
                $model->measurement_unit_ar = $request->measurement_unit_ar;
                $model->measurement_unit_kr = $request->measurement_unit_kr;
                $model->row_status = 'active';
                $model->save();
                return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = MeasurementUnit::find($id);
        return view($this->baseVIEW . '.form', ['title' => 'Edit Measurement Unit', 'action' => url(self::baseURL()) . '/' . $id, 'actionMessage' => 'Measurement Unit Updated Successfully', 'data' => $data, 'listUrl' => url(self::baseURL()), 'inputTypeTexts' => self::rules()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = self::rules();
        $validator = Validator::make(Input::all(), $rules);
        if ($validator->fails()) {
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        } else {
            try {
                $model = MeasurementUnit::find($id);
                $model->measurement_unit = $request->measurement_unit;
                $model->measurement_unit_ar = $request->measurement_unit_ar;
                $model->measurement_unit_kr = $request->measurement_unit_kr;
                $model->save();
                return response()->json(array("success" => true));
            } catch (\Exception $ex) {
                dd($ex->getMessage());
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (MeasurementUnit::destroy($id)) {
            $data = 'Success';
        } else {
            $data = 'Failed';
        }
        return response()->json($data);
    }


    //=========================DELETE MULTIPLE MEASUREMENT UNITS ==========================================

    public function multipledestroy_units(Request $request) {
        try {
            foreach ($request->checked as $id):
                MeasurementUnit::destroy($id);
            endforeach;
            $data = 'Success';
        } catch (Exception $ex) {
            $data = 'something went wrong';
        }
        return response()->json($data);
    }
    public function multipledestroy_customers(Request $request) {
        try {
            foreach ($request->checked as $id):
                User::destroy($id);
            endforeach;
            $data = 'Success';
        } catch (Exception $ex) {
            $data = 'something went wrong';
        }
        return response()->json($data);

    }
}
