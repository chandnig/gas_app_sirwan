<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;
use App\User;
use App\Models\Order;

class GasApplication {

    /**
     * Send notification to users using FCM 
     * @param type $title
     * @param type $message
     * @param type $user
     * @param type $type
     */
    public static function sendOrderNotification($userType, $type, Order $order) {
        $key = "user_" . $userType . "_id";
        $notification = new \App\Models\GasNotification();
        $notification->title = trans('notification/' . $userType . '/order.title_' . $type);
        $notification->message = trans('notification/' . $userType . '/order.message_' . $type);
        $notification->user_id = $order->$key;
        $notification->user_type = $userType;
        $notification->notification_type = $type;
        $notification->save();
        $user = User::where('id', $order->$key)->first();
//        dd($notification->toArray());
        \App\Http\Controllers\API\ApiController::pushNotification(['title' => $notification->title, 'body' => $notification->message], $user->device_token,$notification->toArray());
        return true;
    }

    /**
     * Send notification to users using FCM 
     * @param type $title
     * @param type $message
     * @param type $user
     * @param type $type
     */
    public static function sendNotification($title, $message, $user, $type) {
        $notification = new \App\Models\GasNotification();
        $notification->title = $title;
        $notification->message = $message;
        $notification->user_type = $user;
        $notification->notification_type = $type;
        $notification->save();
        return true;
    }

    /**
     * default Parameters
     * @param Class $table Object of class name Blueprint
     * @param Bool $userParent Set <b>TRUE</b> to set users id as a foreign key
     * @author Gaurav Sethi <gaurav@netscapelabs.com>
     * @return default Parameters
     */
    public static function getUserRoles($roleID = null, $type = 'key') {
        $roles = config('GasApplication.user_roles');
        if ($type == 'key'):
            return ($roleID != null) ? $roles[$roleID] : $roles;
        else:
            $rolesM = [];
            foreach ($roles as $key => $role):
                if ((strpos($role, $roleID) !== false)):
                    $rolesM[$key] = $role;
                endif;
            endforeach;
            return $rolesM;
        endif;
    }

}
